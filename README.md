## flxcard

### What is provided by the package

The package provides a low level C++ API for the control of the FELIX PCIe card.
Part of the package are a number of tools, based on the API calls, for resetting, initializing and
monitoring FELIX cards and devices.
The API also forms the basis for all other developed FELIX software such as the _elinkconfig_
configuration tool, the _ftools_ toolset and the core FELIX data-acquisition software.

### Links to documentation

Documentation for the API can be found here:

https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/doxygen/master/flxcard/flxcard.pdf

A separate document about building and using this package, including examples, is found here:

https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/docs/flxcard_package.pdf

The API and tools in this package rely on the _flx_ and _cmem\_rcc_ drivers being installed.
More information about the drivers and their installation can be found in the FELIX user manual:

https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/felix-user-manual/versions/Latest/5_software_installation.html#_5_2_software_installation_instructions

also also:
https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/docs/felix_driver.pdf
and
https://edms.cern.ch/ui/file/336290/3/cmem_rcc.pdf.

See also the more general TDAQ support software doc:
https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/docs/FLX_support_SW.pdf

### Author information

The S/W in this package was originally developed by Markus Joos (CERN)
with many inputs from the FELIX firmware developers,
and is currently maintained by Henk Boterenbrood (Nikhef).

The following tools in this package are _not_ (yet) currently actively maintained:
- flx-dma-test
- flx-dump-blocks
- flx-irq-test
- flx-spi
- flx-throughput
