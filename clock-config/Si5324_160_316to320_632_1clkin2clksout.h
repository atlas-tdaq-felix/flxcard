// *** NB! This is generated code:
// *** awk -v ARRAYNAME=Si5324Regs_320 -f si5324-regs.awk Si5324_160_316to320_632_1clkin2clksout.txt >[this-file]
#ifndef Si5324_160_316to320_632_1clkin2clksout_H
#define Si5324_160_316to320_632_1clkin2clksout_H
// HEADER
//  Date: Wednesday, February 2, 2022 4:41 PM
//  File Version: 3
//  Software Name: Precision Clock EVB Software
//  Software Version: 5.1
//  Software Date: July 23, 2014
//  Part number: Si5324
// END_HEADER
// PROFILE
//  Name: Si5324
// INPUT
//  Name: CKIN
//  Channel: 1
//  Frequency (MHz): 160.316000
//  N3: 81
//  Maximum (MHz): 162.000000
//  Minimum (MHz): 151.562500
// END_INPUT
// PLL
//  Name: PLL
//  Frequency (MHz): 5130.112000
//  f3 (MHz): 1.979210
//  N1_HS: 4
//  N2_HS: 9
//  N2_LS: 288
//  Phase Offset Resolution (ns): 0.77971
//  BWSEL_REG Option: Frequency (Hz)
//  10:    9
//   9:   17
//   8:   34
//   7:   69
//   6:  138
//   5:  281
//   4:  580
// END_PLL
// OUTPUT
//  Name: CKOUT
//  Channel: 1
//  Frequency (MHz): 320.632000
//  NC1_LS: 4
//  CKOUT1 to CKIN1 Ratio: 2 / 1
//  Maximum (MHz): 324.000000
//  Minimum (MHz): 303.125000
// END_OUTPUT
// OUTPUT
//  Name: CKOUT
//  Channel: 2
//  Frequency (MHz): 320.632000
//  NC_LS: 4
//  CKOUT2 to CKOUT1 Ratio: 1 / 1
//  Maximum (MHz): 324.000000
//  Minimum (MHz): 303.125000
// END_OUTPUT
// CONTROL_FIELD
//  Register-based Controls
//         FREE_RUN_EN: 0x0
//     CKOUT_ALWAYS_ON: 0x0
//          BYPASS_REG: 0x0
//           CK_PRIOR2: 0x1
//           CK_PRIOR1: 0x0
//           CKSEL_REG: 0x0
//               DHOLD: 0x0
//             SQ_ICAL: 0x1
//           BWSEL_REG: 0xA
//         AUTOSEL_REG: 0x0
//            HIST_DEL: 0x12
//               ICMOS: 0x3
//               SLEEP: 0x0
//          SFOUT2_REG: 0x7
//          SFOUT1_REG: 0x7
//           FOSREFSEL: 0x1
//              HLOG_2: 0x0
//              HLOG_1: 0x0
//            HIST_AVG: 0x18
//           DSBL2_REG: 0x0
//           DSBL1_REG: 0x0
//              PD_CK2: 0x1
//              PD_CK1: 0x0
//          FLAT_VALID: 0x1
//              FOS_EN: 0x0
//             FOS_THR: 0x0
//             VALTIME: 0x1
//               LOCKT: 0x1
//         CK2_BAD_PIN: 0x1
//         CK1_BAD_PIN: 0x1
//             LOL_PIN: 0x1
//             INT_PIN: 0x0
//          INCDEC_PIN: 0x1
//        CK1_ACTV_PIN: 0x1
//           CKSEL_PIN: 0x1
//         CK_ACTV_POL: 0x1
//          CK_BAD_POL: 0x1
//             LOL_POL: 0x1
//             INT_POL: 0x1
//            LOS2_MSK: 0x1
//            LOS1_MSK: 0x1
//            LOSX_MSK: 0x1
//            FOS2_MSK: 0x1
//            FOS1_MSK: 0x1
//             LOL_MSK: 0x1
//               N1_HS: 0x0
//              NC1_LS: 0x3
//              NC2_LS: 0x3
//               N2_LS: 0x11F
//               N2_HS: 0x5
//                 N31: 0x50
//                 N32: 0x50
//          CLKIN2RATE: 0x3
//          CLKIN1RATE: 0x3
//            FASTLOCK: 0x1
//             LOS1_EN: 0x3
//             LOS2_EN: 0x3
//             FOS1_EN: 0x1
//             FOS2_EN: 0x1
//    INDEPENDENTSKEW1: 0x0
//    INDEPENDENTSKEW2: 0x0
// END_CONTROL_FIELD
// REGISTER_MAP

clockchip_reg_t Si5324Regs_320[] = {
 {   0, 0x14 },
 {   1, 0xE4 },
 {   2, 0xA2 },
 {   3, 0x15 },
 {   4, 0x12 },
 {   5, 0xED },
 {   6, 0x3F },
 {   7, 0x29 },
 {   8, 0x00 },
 {   9, 0xC0 },
 {  10, 0x00 },
 {  11, 0x42 },
 {  19, 0x09 },
 {  20, 0x3E },
 {  21, 0xFF },
 {  22, 0xDF },
 {  23, 0x1F },
 {  24, 0x3F },
 {  25, 0x00 },
 {  31, 0x00 },
 {  32, 0x00 },
 {  33, 0x03 },
 {  34, 0x00 },
 {  35, 0x00 },
 {  36, 0x03 },
 {  40, 0xA0 },
 {  41, 0x01 },
 {  42, 0x1F },
 {  43, 0x00 },
 {  44, 0x00 },
 {  45, 0x50 },
 {  46, 0x00 },
 {  47, 0x00 },
 {  48, 0x50 },
 {  55, 0x1B },
 { 131, 0x1F },
 { 132, 0x02 },
 { 137, 0x01 },
 { 138, 0x0F },
 { 139, 0xFF },
 { 142, 0x00 },
 { 143, 0x00 },
 { 136, 0x40 }
};
// END_REGISTER_MAP
// END_PROFILE
#endif // Si5324_160_316to320_632_1clkin2clksout_H
