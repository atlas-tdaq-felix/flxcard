#! /usr/bin/awk -f
# Convert an Si5324 clock chip register settings text file into a .h file to be included by flx-init.
#
# Copies of settings files are now stored in directory clock-config of the flxcard repository,
# but previously used files can be found in:
# https://gitlab.cern.ch/atlas-tdaq-felix/hardware/-/tree/master/TTCfx/tools
#

BEGIN {
    array_name = ARRAYNAME
    if( array_name == "" ) {
        array_name = "Si5324Regs"
    }

    split( ARGV[ARGC-1], pair, "." )
    filename = pair[1]

    print "// *** NB! This is generated code:"
    printf( "// *** awk -v ARRAYNAME=%s -f si5324-regs.awk %s >[this-file]\n", ARRAYNAME, ARGV[ARGC-1] )
    printf( "#ifndef %s_H\n", filename )
    printf( "#define %s_H\n", filename )

    init = 1;
}

substr( $1, 1, 1 ) == "#" {
    # Remove first char '#'
    comment = substr( $0, 2, length($0)-1 )
    # Remove \r from \n\r ? 
    if( substr( comment, length(comment), 1 ) == "\r" ) {
        comment = substr( comment, 1, length(comment)-1 )
    }
    if( init == 0 ) {
        printf "\n};"
        print ""
        init = -1
    }
    print "// " comment
}

substr( $1, 1, 1 ) != "#" {
    if( init == 1 ) {
        init = 0
        print ""
        printf( "clockchip_reg_t %s[] = {\n", array_name );
    } else {
        printf( ",\n" )
    }
    addr = substr( $2, 1, 2 )
    printf( " { %4s 0x%s }", $1, addr )
}

END {
    printf( "#endif // %s_H\n", filename )
}
