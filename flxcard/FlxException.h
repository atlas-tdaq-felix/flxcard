#ifndef FLXEXCEPTION_H
#define FLXEXCEPTION_H

#include <stdexcept>
#include <sstream>

// Macro
#define THROW_FLX_EXCEPTION(errorCode, message) \
  do { std::ostringstream oss; \
    oss << message; \
    throw FlxException(FlxException::errorCode, oss.str()); \
  } while(0)

// ----------------------------------------------------------------------------

class FlxException : public std::runtime_error 
{
 public:  

  enum
  {
    UNDEF_MIN = 0,
    NOTOPENED = 1,
    MAPERROR,
    UNMAPERROR,
    IOCTL,
    PARAM,
    I2C,
    DMA,
    GBT,
    REG_ACCESS,
    HW,
    LOCK_VIOLATION,
    CONFIG,
    UNDEF_MAX
  };

  FlxException( u_int errorId, std::string errorText )
    : std::runtime_error( errorText ), m_errorId( errorId ) {}

  virtual ~FlxException() throw () {}

  u_int errorId()    const { return m_errorId; }
  u_int getErrorId() const { return errorId(); }

  std::string errorString( u_int errorId = 0 )    const;
  std::string getErrorString( u_int errorId = 0 ) const { return errorString( errorId ); }

 private:
  const u_int m_errorId;
};

// ----------------------------------------------------------------------------
#endif // FLXEXCEPTION_H
