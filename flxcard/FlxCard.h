/******************************************************************* 
 * \mainpage                                                       * 
 *                                                                 * 
 * @author: Markus Joos, CERN                                      *
 *  Maintainer: Henk Boterenbrood, Nikhef                          * 
 *                                                                 *
 * @brief                                                          *
 * This note defines an application program interface (API) for    *
 * the use of the FLX PCIe I/O card in the ATLAS read-out system.  *
 * The intention of the API is to satisfy the needs of simple      *
 * test programs as well as the requirements of the                *
 * FelixApplication.                                               *
 *                                                                 *
 * @copyright CERN, Nikhef                                         *
 ******************************************************************/ 

#ifndef FLXCARD_H
#define FLXCARD_H

#include <cstdint>
#include <vector>
#include <string>
#include <sys/types.h>
#include <utility> // std::pair

#include "cmem_rcc/cmem_rcc.h"
#include "flxcard/flx_common.h"
#include "regmap/regmap.h"
#include "flxcard/LinkConfig.h"

// Constants
#define WAIT_TIME_600                     600
#define WAIT_TIME_200                     200
#define NUM_INTERRUPTS                    8
#define FLX_GBT_FILE_NOT_FOUND            -1
#define FLX_GBT_VERSION_NOT_FOUND         -2
#define FLX_GBT_TMODE_FEC                 0
#define FLX_GBT_ALIGNMENT_CONTINUOUS      0
#define FLX_DMA_WRAPAROUND                1
#define FLX_GBT_ALIGNMENT_ONE             1
#define FLX_GBT_CHANNEL_AUTO              1
#define FLX_GBT_TMODE_WideBus             1

// Firmware types
#define FIRMW_GBT                         0
#define FIRMW_FULL                        1
#define FIRMW_LTDB                        2
#define FIRMW_FEI4                        3
#define FIRMW_PIXEL                       4
#define FIRMW_STRIP                       5
#define FIRMW_FELIG_GBT                   6
#define FIRMW_FELIG                       6 // Backwards compatible
#define FIRMW_FMEMU                       7
#define FIRMW_MROD                        8
#define FIRMW_LPGBT                       9
#define FIRMW_INTERLAKEN                  10
#define FIRMW_FELIG_LPGBT                 11
#define FIRMW_HGTD_LUMI                   12
#define FIRMW_BCM_PRIME                   13
#define FIRMW_FELIG_PIXEL                 14
#define FIRMW_FELIG_STRIP                 15
const std::vector<std::string> FIRMW_STR{ "GBT",   "FULL",  "LTDB",   "FEI4", "PIXEL",
                                          "STRIP", "FELIG", "FM-EMU", "MROD", "LPGBT",
                                          "INTERLAKEN", "FELIG-LPGBT", "HGTD-LUMI",
                                          "BCM-PRIME",  "FELIG-PIXEL", "FELIG-STRIP" };

// I2C definitions
#define I2C_ADDR_SWITCH1_FLX_711          0x70
#define I2C_ADDR_SWITCH1_FLX_710          0x70
#define I2C_ADDR_SWITCH1_FLX_709          0x74
#define I2C_ADDR_SWITCH2_FLX_709          0x75
#define I2C_ADDR_SWITCH1_FLX_128          0x74
#define I2C_ADDR_SWITCH2_FLX_128          0x75
#define I2C_ADDR_SWITCH3_FLX_128          0x76
#define I2C_ADDR_SWITCH1_FLX_182          0x77
#define I2C_ADDR_SWITCH1_FLX_155          0x77
#define I2C_FULL_FLAG                     (1 << 25)
#define I2C_EMPTY_FLAG                    (1 << 8)
#define I2C_DELAY                         1000
#define I2C_SLEEP                         100

// I2C error codes
#define I2C_DEVICE_ERROR_NOT_EXISTING     1
#define I2C_DEVICE_ERROR_INVALID_PORT     2
#define I2C_DEVICE_ERROR_INVALID_ADDRESS  3
#define I2C_DEVICE_ERROR_INVALID_REGISTER 4
#define I2C_DEVICE_ERROR_INVALID_DATA     5

// Interrupts
#define ALL_IRQS                          0xFFFFFFFF

// Monitoring devices
#define FPGA_MONITOR                      0x001
#define POD_MONITOR_ALL                   0x002
#define POWER_MONITOR                     0x004
#define FIREFLY_MONITOR                   0x008
#define POD_MONITOR_LOS                   0x100
#define POD_MONITOR_TEMP_VOLT             0x200
#define POD_MONITOR_POWER                 0x400
#define POD_MONITOR_POWER_RX              0x800
// (Backwards compatability)
#define POD_MONITOR                       POD_MONITOR_ALL

// Resource locking
#define LOCK_NONE                         0
#define LOCK_DMA0                         1
#define LOCK_DMA1                         2
#define LOCK_I2C                          4
#define LOCK_FLASH                        8
#define LOCK_ELINK                        16
#define LOCK_ALL                          0xffffffff
#define LOCK_DMA(n)                       (1<<(16+n))

// Other constants
#define ALL_BITS                          0xFFFFFFFFFFFFFFFF

// Register model
typedef struct
{
  volatile u_long start_address;        /*  low half, bits  63:00 */
  volatile u_long end_address;          /*  low half, bits 127:64 */
  volatile u_long tlp         :11;      /* high half, bits  10:00 */
  volatile u_long fromhost    : 1;      /* high half, bit      11 */
  volatile u_long wrap_around : 1;      /* high half, bit      12 */
  volatile u_long reserved    :51;      /* high half, bits  63:13 */
  volatile u_long sw_pointer;           /* high half, bits 127:64 */
} dma_descriptor_t;

typedef struct
{
  volatile u_long fw_pointer;           /* bits  63:00 */
  volatile u_long descriptor_done : 1;  /* bit      64 */
  volatile u_long even_addr_dma   : 1;  /* bit      65 */
  volatile u_long even_addr_pc    : 1;  /* bit      66 */
} dma_status_t;

typedef struct
{
  volatile u_int  control;              /* bits  63:00 */
  volatile u_int  data;                 /* bits  95:64 */
  volatile u_long address;              /* bits 127:96 */
} int_vec_t;

typedef struct
{
  volatile u_long date_time : 40;       /* bits  39:00 */
  volatile u_long reserved  : 24;       /* bits  63:40 */
  volatile u_long revision  : 16;       /* bits  79:64 */
} board_id_t;

typedef struct
{
  dma_descriptor_t DMA_DESC[8];         /* 0x000 - 0x0ff */
  u_char           unused1[256];        /* 0x100 - 0x1ff */
  dma_status_t     DMA_DESC_STATUS[8];  /* 0x200 - 0x27f */
  u_char           unused2[128];        /* 0x280 - 0x2ff */
  volatile u_int   BAR0_VALUE;          /* 0x300 - 0x303 */
  u_char           unused3[12];         /* 0x304 - 0x30f */
  volatile u_int   BAR1_VALUE;          /* 0x310 - 0x313 */
  u_char           unused4[12];         /* 0x314 - 0x31f */
  volatile u_int   BAR2_VALUE;          /* 0x320 - 0x323 */
  u_char           unused5[220];        /* 0x324 - 0x3ff */
  volatile u_int   DMA_DESC_ENABLE;     /* 0x400 - 0x403 */
  u_char           unused7[28];         /* 0x404 - 0x41f */
  volatile u_int   DMA_RESET;           /* 0x420 - 0x423 */
  u_char           unused8[12];         /* 0x424 - 0x42f */
  volatile u_int   SOFT_RESET;          /* 0x430 - 0x433 */
  u_char           unused9[12];         /* 0x434 - 0x43f */
  volatile u_int   REGISTERS_RESET;     /* 0x440 - 0x443 */
} flxcard_bar0_regs_t;

typedef struct
{
  int_vec_t        INT_VEC[8];          /* 0x000 - 0x07f */
  u_char           unused1[128];        /* 0x080 - 0x0ff */
  volatile u_int   INT_TAB_ENABLE;      /* 0x100 - 0x103 */
} flxcard_bar1_regs_t;

typedef struct
{
  const char *name;
  const char *description;
  u_char      address;
  u_char      i2c_switch_val[3];
} i2c_device_t;

typedef struct
{
  float  temperature;
  float  vccint;
  float  vccaux;
  float  vccbram;
  u_long fanspeed;
  u_long dna;
} fpga_monitoring_data_t;

typedef struct
{
  bool  absent;
  char  name[10];
  int   temp;
  float v33;
  float v25;
  int   los;
  char  vname[16];
  char  voui[3];
  char  vpnum[16];
  char  vrev[2];
  char  vsernum[16];
  char  vdate[8];
  float optical_power[12];
} minipod_parameters_t;

typedef struct
{
  std::string name;
  float VCCINT_current;
  float VCCINT_voltage;
  float MGTAVCC_current;
  float MGTAVCC_voltage;
  float FPGA_internal_diode_temperature;
  float MGTAVTT_current;         // Only FLX-712
  float MGTAVTT_voltage;         // Only FLX-712
  float MGTAVTTC_voltage;        // Only FLX-711
  float MGTVCCAUX_voltage;       // Only FLX-711
  float LTC2991_1_internal_temperature;
  float vcc;
} ltc1_parameters_t;

typedef struct
{
  std::string name;
  float PEX0P9V_current;
  float PEX0P9V_voltage;
  float SYS18_current;
  float SYS18_voltage;
  float SYS12_voltage;           // Only FLX-711
  float SYS25_current;           // Only FLX-712 
  float SYS25_voltage;
  float PEX8732_internal_diode_temperature;
  float LTC2991_2_internal_temperature;
  float vcc;
} ltc2_parameters_t;

typedef struct
{
  std::string name;
  uint32_t t_local_hi;  // In degrees C
  uint32_t t_local_lo;  // In 1/10000ths C
  uint32_t t_remote_hi; // In degrees C
  uint32_t t_remote_lo; // In 1/10000ths C
  uint32_t status;
  uint32_t device_id;
  uint32_t manufacturer_id;
} tmp435_parameters_t;

typedef struct
{
  std::string name;
  uint32_t config_reg;
  uint32_t shunt_volt_reg;
  uint32_t bus_volt_reg;
  uint32_t power_reg;
  uint32_t current_reg;
  uint32_t calib_reg;
  uint32_t manufacturer_id;
  float    r_shunt;
} ina226_parameters_t;

typedef struct
{
  std::string name;
  uint32_t mfr_special_id;
  uint32_t vin;
  uint32_t iin;
  uint32_t pin;
  uint32_t vout[2];
  uint32_t iout[2];
  uint32_t pout[2];
  uint32_t temp_die;
  uint32_t temp_ext[2];
} ltm4700_parameters_t;

typedef struct
{
  std::string name;
  bool     valid;
  uint32_t manufacturer_id;
  uint32_t silicon_rev;
  uint32_t input_cfg[10][8];
  uint32_t fault_stat[6];
  uint32_t adc[12];
} adm1066_parameters_t;

typedef struct
{
  std::string name;
  bool     valid;
  uint32_t ic_device_id;
  uint64_t ic_device_rev;
  uint32_t adc[17];
  uint32_t status_vout[17];
} adm1266_parameters_t;

typedef struct
{
  std::string name;
  bool absent;

  // FireFly TX lower memory map
  uint32_t status;
  uint32_t status_summ;
  uint32_t latched_alarms_tx_los;
  uint32_t latched_alarms_tx_fault;
  uint32_t latched_alarms_temp;
  uint32_t latched_alarms_vcc33;
  uint32_t latched_alarms_cdr_lol;
  int32_t  case_temp;
  uint32_t vcc;
  uint32_t elapsed_optime;
  uint32_t chan_disable;
  uint32_t squelch_disable;
  uint32_t polarity_invert;
  uint64_t input_equalization;
  uint32_t cdr_enable;
  uint32_t mask_tx_los_alarms;
  uint32_t mask_fault_flags;
  uint32_t mask_temp_alarms;
  uint32_t mask_vcc33_alarms;
  uint32_t mask_cdr_lol_alarms;
  uint32_t firmware_version;

  // Upper page 0;
  std::string vendor_part;
  std::string vendor_serial;
} ffly_tx_parameters_t;

typedef struct
{
  std::string name;
  bool absent;

  // FireFly RX lower memory map
  uint32_t status;
  uint32_t status_summ;
  uint32_t latched_alarms_rx_los;
  uint32_t latched_alarms_rx_power;
  uint32_t latched_alarms_temp;
  uint32_t latched_alarms_vcc33;
  uint32_t latched_alarms_cdr_lol;
  int32_t  case_temp;
  uint32_t vcc;
  uint32_t elapsed_optime;
  uint32_t chan_disable;
  uint32_t output_disable;
  uint32_t polarity_invert;
  uint64_t output_swing;
  uint64_t output_preemphasis;
  uint32_t cdr_enable;
  // ...etc...
  uint32_t firmware_version;

  // Upper page 0;
  std::string vendor_part;
  std::string vendor_serial;
} ffly_rx_parameters_t;

typedef struct
{
  std::string name;
  bool absent;

  // FireFly TX/RX lower memory map
  uint32_t status;
  uint32_t latched_alarms_tx_los;
  uint32_t latched_alarms_tx_fault;
  uint32_t latched_alarms_cdr_lol;
  uint32_t latched_alarms_temp;
  uint32_t latched_alarms_vcc;
  uint32_t latched_alarms_rx_power;
  uint32_t elapsed_optime;
  int32_t  case_temp;
  uint32_t vcc_3v3;
  uint32_t vcc_1v8;
  uint32_t rx_optical_power[4];
  uint32_t firmware_revision;
  uint32_t tx_chan_disable;
  uint32_t cdr_enable;
  uint32_t cdr_rate_select;
  uint32_t mask_los_alarms;
  uint32_t mask_tx_fault_flags;
  uint32_t mask_cdr_lol_alarms;
  uint32_t mask_temp_alarms;
  uint32_t mask_vcc_alarms;

  // Upper page 0;
  std::string vendor_part;
  std::string vendor_serial;
} ffly_tr_parameters_t;

typedef struct
{
  fpga_monitoring_data_t fpga;
  minipod_parameters_t   minipod[8];
  ltc1_parameters_t      ltc1;
  ltc2_parameters_t      ltc2;
  ina226_parameters_t    ina226[10+1]; // +1 for FLX-155
  tmp435_parameters_t    tmp435[7+1];  // +1 for FLX-155
  adm1066_parameters_t   adm1066[2];
  adm1266_parameters_t   adm1266;
  ltm4700_parameters_t   ltm4700;
  ffly_tx_parameters_t   ffly_tx[2+2]; // +2 for FLX-155
  ffly_rx_parameters_t   ffly_rx[2+2]; // +2 for FLX-155
  ffly_tr_parameters_t   ffly_tr[1+1]; // +1 for FLX-155
} monitoring_data_t;

typedef struct
{
  u_int n_devices;
  u_int cdmap[MAXCARDS][2];
} device_list_t;

// Macros
#define TOHEX(n) std::hex << "0x" << ((uint64_t)n) << std::dec

class FlxCard
{
public:
    /// The constructor of an FlxCard object only initializes a few class variables. It does not interact with the FELIX H/W.
    FlxCard();
    
    ~FlxCard();

    /**
     * \defgroup DriverInteraction Driver interaction 
     * @{
     * A set of functions that interact with the driver, there are methods to open the API for instance, but also to open an alternative
     * (backdoor) way to reading and writing the registers by memory-mapping a struct directly on the register map (BAR space).
     */
    /**
     *  Opens the \e flx driver and links one Flx PCIe card to the FlxCard object
     *  and has to be called before any other reference to the method is made. 
     *  In case of problems, check (more /proc/flx) if the driver is running and if it can see all cards. 
     *  The method also checks if the major version of the register map of the F/W running on the FLX card 
     *  matches with the version of the “regmap” library. This is to prevent running the API on incompatible FLX cards. 
     * 
     *  Each FLX device has a number of resources that cannot be shared by multiple processes,
     *  such as a DMA controller or the onboard I2C-bus. The purpose of the 
     *  resource locking bits is to allow a process to declare to the driver that it requires exclusive access to such a
     *  resource. If a resource is locked by a process, the driver will refuse other processes that request access to
     *  the same resource. In such a case card_open() throws an exception. The parameter \e lock_mask passes a collection
     *  of bits to the driver. These bits are defined in FlxCard.h. Currently the following resources are defined:  
     *
     *      #define LOCK_DMA0    1 
     *      #define LOCK_DMA1    2 
     *      #define LOCK_I2C     4 
     *      #define LOCK_FLASH   8 
     *      #define LOCK_ELINK   16 
     *      #define LOCK_ALL     0xffffffff 
     *
     *  Note that DMA-related lock bits are currently also located in bits 16 and up:
     *  there is copy of LOCK_DMA0 and LOCK_DMA1 (in bit 16 and 17 resp.)
     *  and space for more DMA lock bits (required for next-generation firmware versions).
     *
     *  Example: To lock access to DMA1 and the FLASH memory of the first FLX device:
     *
     *      card_open( 0, LOCK_DMA1 | LOCK_FLASH )
     *  The lock will be held until the owning process calls card_close(). If a process terminates with an error the driver will
     *  release the resources that were locked by that process. Users can find an overview of what resources are locked by
     *  looking at the content of \e /proc/flx.
     *  Processes that do not request any locks still have full access to the respective resources. That is to say that a
     *  process that does not lock DMA1 still can call the DMA related methods of the API.
     *  The method lock_mask() can be called to figure out what resources are locked. This was done on purpose because
     *  the application programmers know best if their applications (for example for diagnostic purposes) should be able to run even
     *  if a resource is locked. Consequently, the application programmers bear the responsibility for using the resource locking
     *  in a correct way.
     *
     *  @b Errors
     *  In case of an error, an exception is thrown.
     *  @param device_nr The number of the FLX device (0…N-1) that is to be linked to the object
     *  @param lock_mask The resources that are to be locked, defined as a bit mask
     *  @param read_config Read and locally store the (e-)link configuration
     *  @param ignore_version For some tools it may be useful to be able to open the FELIX device despite a mismatch between software and firmware version (in that case results in a warning, but does not throw an exception)
     */
    void card_open( int device_nr, u_int lock_mask,
                    bool read_config = false,
                    bool ignore_version = false );
    
    /**
     * Unlinks a Flx PCIe card. It must be called before closing the application.
     * 
     * @b Errors
     * In case of an error, an exception is thrown.
     */
    void card_close();
    
    /**
     * Returns the number of FLX \e cards that are installed in the host computer,
     * taking into account single-device FLX-709 cards.
     * As it is a static method you do not have to instantiate an FlxCard object to call it.
     * For errors see method number_of_devices().
     */
    static u_int number_of_cards();
    
    /**
     * Returns the number of FLX \e devices (PCIe endpoints) that are installed in the host computer. 
     * As it is a static method you do not have to instantiate an FlxCard object to call it.
     *
     * @b Errors
     * In case of an error, no exception is thrown but “0” is returned.
     * Therefore “zero cards found” can also mean that: 
     * - the flx driver was not running 
     * - the device node `/dev/flx` was missing 
     * - the "GETCARDS" ioctl() of the flx driver did not work
     */
    static u_int number_of_devices();

    /**
     * Returns a structure with information about the cards and devices installed in the PC.
     * As it is a static method you do not have to instantiate an FlxCard object to call it.
     *
     * The parameter `device_list_t.n_devices` gives the total number of devices.
     * It is identical to the value returned by number_of_devices()
     * The value of `device_list_t.cdmap[device][0]` (with device = 0...(`device_list_t.n_devices`-1 )
     * is the type of the card as read from the \e CARD_TYPE register (offset 0xA0 of BAR2)
     * The value of `device_list_t.cdmap[device][1]` (with device = 0...(`device_list_t.n_devices`-1 )
     * is the relative number of the device with respect to the card.
     * That means that for a FLX-709 and FLX-710 it will always be "0" because
     * these are single device cards. For a FLX-711, FLX-712 or FLX-128 it can be "0" or "1".
     *
     * @b Errors 
     * In case of an error, an exception is thrown.
     */
    static device_list_t device_list();

    /**
     * Returns the FELIX device number associated with the first device of the selected card number.
     * if for example the host machine contains two FLX-712 cards then card 0 is accessed through device 0,
     * and card 1 through device 2. If on the other hand there is one FLX-709 and one FLX-712 card in the machine,
     * then it could be either devices 0 and 1 or devices 0 and 2, depending on the order in which
     * the cards are enumerated by the host system.
     * This function is used by tools that act on a card rather than a device (for example,
     * card I2C-bus access is through device 0 of the card only).
     * @param card_number The number of the FLX card (0…N-1) in the host machine
     */
    static int card_to_device_number( int card_number );

    /**
     * Returns information about a device's resources that are currently locked
     * by \e other instances of FlxCard objects,
     * which may reside in either the same process or in different processes.
     * The value returned is a global resource-lock value retrieved from the driver.
     * The individual lock bits are declared in FlxCard.h
     * @param device_nr The number of the FLX device (0…N-1)
     * 
     * Example:
     * @code
     *     FlxCard flxcard; 
     *     u_int lock_bits = flxcard.lock_mask( 0 );
     *     if( lock_mask & LOCK_DMA1 )
     *       cout << "Device 0 DMA1 is locked (by others)"
     * @endcode
     * @b Note
     * This method can be called without first calling open_card().
     * 
     * @b Errors
     * In case of an error, an exception is thrown.
     */
    u_int lock_mask( int device_nr );
    u_int get_lock_mask( int device_nr ) { return lock_mask( device_nr ); } // Backwards-compatibility
    
    /**
     * Returns the PCI base address of the specified PCI register block.
     * @param bar The number of a BAR register block; legal values are 0, 1 or 2
     * @return the PCI base address of the specified PCI register block
     */
    u_long openBackDoor( int bar );
    /**
     * Is an alias for `openBackDoor(0)`.
     */
    u_long bar0Address() { return openBackDoor( 0 ); }
    /**
     * Is an alias for `openBackDoor(1)`.
     */
    u_long bar1Address() { return openBackDoor( 1 ); }
    /**
     * Is an alias for `openBackDoor(2)`.
     */
    u_long bar2Address() { return openBackDoor( 2 ); }
    /** @}*/
    
    /**
     * \defgroup DMA
     * @{
     * A set of functions to setup, start and stop DMA transfers and to interact with the circular buffer PC pointers.
     */
    /**
     * Calls the flx driver in order to determine the maximum number of TLP bytes that the H/W can support. 
     * 
     * @b Errors 
     * In case of an error, an exception is thrown.
     */
    int  dma_max_tlp_bytes() { return m_maxTlpBytes; }
    
    /**
     * First stops the DMA channel identified by dma_id.
     * It does not check if the channel is busy.
     * Then it programs the channel and starts a new DMA from-device-to-host operation. 
     * 
     * Note: The bits 10:0 of the DMA descriptor (bitfield NUM_WORDS in the Wupper documentation)
     * will be set to the maximum TLP size supported by the system.
     * Therefore, the transfer size has to be a multiple of that value. 
     * 
     * @b Errors 
     * In case of an error, an exception is thrown.
     * @param dma_id The DMA channel (descriptor) to be used; valid numbers are 0..7 
     * @param dst The value for the start_address field of the descriptor.
     *            NOTE: You have to provide a physical memory address 
     * @param size The size of the transfer in bytes 
     * @param flags The value for the wrap_around field of the descriptor;
     *              "1" means: enable wrap around, i.e. use continuous DMA
     */
    void dma_to_host( u_int dma_id, u_long dst, size_t size, u_int flags );
    
    /**
     * First stops the DMA channel identified by \e dma_id.
     * It does not check if the channel is busy.
     * Then it programs the channel and starts a new DMA from-host-to-device operation. 
     * 
     * Note: The size of the transfer has to be a multiple of 32 bytes. 
     * 
     * The method internally computes the optimal value for the bits 10:0
     * of the DMA descriptor (bitfield NUM_WORDS in the Wupper documentation).  
     * 
     * The algorithm used is this: 
     *     1. if transfersize % 32 != 0: error("number of bytes transferred must be a multiple of 32") 
     *     2. tlp = get_max_tlp() 
     *     3. if transfersize % tlp == 0: do transfer 
     *     4. else: tlp = tlp >> 1 && goto 3
     * 
     * Note: The algorithm above is currently not used:
     * the transfer unit size is set to the minimum of 32 bytes,
     * to make sure small messages are always sent, in particular when continuous DMA is enabled.
     * The upper 16 bits of the flags parameter can be used to force a transfer size unequal to
     * (so larger than) 32 bytes (but must be a multiple of 32).
     *
     * @b Errors
     * In case of an error, an exception is thrown.
     * 
     * @param dma_id The DMA channel (descriptor) to be used; valid numbers are 0..7 
     * @param src The value for the start_address field of the descriptor.
     *            NOTE: You have to provide a physical memory address 
     * @param size The size of the transfer in bytes 
     * @param flags The value for the wrap_around field of the descriptor. “1” means:
     *              enable wrap around, i.e. use continuous DMA
     */
    void dma_from_host( u_int dma_id, u_long src, size_t size, u_int flags );
    
    /**
     * Returns whether the DMA channel identified by dma_id is enabled or not.
     * @param dma_id The DMA channel (descriptor) to be used; valid numbers are 0..7 
     */
    bool dma_enabled( u_int dma_id );

    /**
     * Is blocking. It returns once the DMA on channel dma_id has ended.
     * The method has an internal (hard wired) time out of 1 second. 
     * 
     * @b Errors 
     * In case the DMA has not ended after 1 second an exception is thrown.
     * @param dma_id The DMA channel (descriptor) to be used; valid numbers are 0..7
     */
    void dma_wait( u_int dma_id );
    
    /**
     * Clears the DMA channel identified by dma_id.
     * It does not check the status of that channel.
     * @param dma_id The DMA channel (descriptor) to be used; valid numbers are 0..7
     */
    void dma_stop( u_int dma_id );
    
    /**
     * Advances the internal read pointer of the DMA channel by the number of bytes given.
     * This is used when operating the DMA engine in "endless DMA" / "circular buffer" mode.
     * This method is to be called by the user after processing data at the head of the 
     * circular buffer to free the buffer for DMA writes or reads of the FLX card again. 
     * @param dma_id The DMA channel (descriptor) to be used; valid numbers are 0..7 
     * @param dst See below. NOTE: You have to provide a physical memory address 
     * @param size If the value of the read_ptr field, after adding bytes is larger than dst + size,
     *             size will be subtracted from read_ptr
     * @param bytes This value will be added to the \e read_ptr field of the DMA descriptor
     */
    void dma_advance_ptr( u_int dma_id, u_long dst, size_t size, size_t bytes );
    
    /**
     * Directly writes the read pointer of a DMA channel.
     * @param dma_id The DMA channel (descriptor) to be used; valid numbers are 0..7 
     * @param dst The read pointer register of the DMA channel will be set to the value in \e dst
     */
    void dma_set_ptr( u_int dma_id, u_long dst );
    
    /**
     * Returns the current value of the SW_POINTER.
     * That is to say the bits 127:64 of the DMA_DESC_[0..7]a as defined in the
     * <a href="https://gitlab.cern.ch/atlas-tdaq-felix/documents/blob/master/Wupper/wupper.pdf">Wupper</a>
     * documentation. 
     * @param dma_id The DMA channel (descriptor) to be used; valid numbers are 0..7
     */
    u_long dma_get_ptr( u_int dma_id );
    /* For backwards compatibility: */
    u_long dma_get_read_ptr( u_int dma_id ) { return dma_get_ptr( dma_id ); }
    
    // Sets the DMA_FIFO_FLUSH register to "1".
    //void dma_fifo_flush();  //MJ: disabled. See FlxCard.cpp
    
    /**
     * Triggers a reset by the DMA_RESET register in BAR0.
     */
    void dma_reset();
    
    /**
     * Reads the status register of a DMA descriptor
     * (i.e. one of the DMA_DESC_STATUS_* registers)
     * and returns the value of the FW_POINTER bit field (bits 63:0).
     * @param dma_id The DMA channel (descriptor) to be queried; valid numbers are 0..7
     */
    u_long dma_get_fw_ptr( u_int dma_id );
    /* For backwards compatibility: */
    u_long dma_get_current_address( u_int dma_id ) { return dma_get_fw_ptr( dma_id ); }
    
    /**
    * Reads the \e even_addr_dma and \e even_pc_dma bits of a DMA channel and compares them. 
    * @param dma_id The DMA channel (descriptor) to be used; valid numbers are 0..7 
    * @return Returns \e true if the bits have the same value and \e false if not.
    */
    bool dma_cmp_even_bits( u_int dma_id );

    /**
     * @}
     */
     
    /**
     * \defgroup I2C
     * @{
     * A set of functions that interact with the onboard I2C bus.
     */
        
    /**
     * These methods read and write 8-bit data words from/to a named I2C device.
     * 
     * @b Errors
     * In case of an error, an exception is thrown.
     * @param *device_str A string of the form “p1:p2:p3:addr”: see table below for details.
     *                    Alternatively the string can be the name of the I2C endpoint device
     *                    You must use the predefined names: find out by running "flx-i2c list"
     * @param reg_addr The address of the register within the device
     * @param data The data that is to be written to the register
     * @param value A pointer to a value that will be filled with the data read from the I2C device
     *
     * Description of the sub-parameters in parameter \e device_str:
     * | Sub-parameter | Description |
     * |---------------|--------------------------------------------------|
     * | p1            | The port number on the primary I2C switch to select; run "flx-i2c list" to find out where the I2C device you want to access, is located in the I2C tree |
     * | p2            | In case of cascaded switches the secondary switch's port number to select |
     * | p3            | In case of cascaded switches this is the third switch's port number |  
     * | addr          | The I2C address of the endpoint device behind the switch(es) |
     *
     * As an alternative to a string of the format defined above, parameter \e device_str
     * can a symbolic name of an I2C endpoint device; get a listing of all available I2C devices
     * on all platforms by running "flx-i2c list all".
     */
    void i2c_read( const char *device_str, u_char reg_addr, u_char *value, int nbytes = 1 );
    
    /**
     * See i2c_read() for details
     */
    void i2c_write( const char *device_str, u_char reg_addr, u_char data );
    void i2c_write( const char *device_str, u_char reg_addr, u_char *data, int nbytes );

    /**
     * These methods read or write one byte of data to/from an I2C address.
     * In case the I2C is stuck the method will abort after 1 second with a time-out error. 
     * Before transferring the byte the methods call private function \e i2c_wait_not_full()
     * in order to make sure that the I2C interface is not busy. 
     * 
     * @b Errors 
     * In case of an error, an exception is thrown.
     * @param dev_addr The address of the I2C device
     * @param byte The byte that is to be written to the I2C device
     */
    void i2c_write_byte( u_char dev_addr, u_char byte );
    
    /**
     * @param dev_addr The address of the I2C device 
     * @param reg_addr The register address inside the I2C device 
     * @param byte The byte that is to be written to the register in the I2C device
     */
    void i2c_write_byte( u_char dev_addr, u_char reg_addr, u_char byte );
    
    /**
     * @param dev_addr The address of the I2C device 
     * @param reg_addr The register address inside the I2C device to read from
     */
    u_char i2c_read_byte( u_char dev_addr, u_char reg_addr );
    
    /**
     * Can be used to flush the I2C read FIFO.
     * Up to 16 bytes are flushed, the exact number returned optionally in 'count'.
     * @b Errors
     * Returns false if after 16 bytes flushed the FIFO is still not empty.
     * @param count Number of bytes flushed
     */
    bool i2c_flush( u_int *count = 0 );

    /**
     * Read or write one or two bytes of data to/from
     * an I2C address plus register address.
     */
    void i2c_write_bytes( uint8_t dev_addr, uint8_t reg_addr,
                          int nbytes, uint8_t *bytes );
    void i2c_read_bytes ( uint8_t dev_addr, uint8_t reg_addr,
                          int nbytes, uint8_t *bytes );

    /**
     * @}
     */
     
     /**
     * \defgroup GBT GBT and transceiver related methods
     * @{
     * A set of functions dedicated to setting up GBT links.
     */
    
    /**
     * Is used to initialize the GBT Wrapper registers, and to perform the TX and RX configuration. 
     * 
     * @b Errors 
     * In case of an error, an exception is thrown.
     * @param alignment Tx time domain crossing mode selection for each channel (GBT TX_TC_METHOD) 
     * @param channel_mode Set GBT mode (normal FEC or Wide-bus mode) for all channels.
     *                     (GBT_DATA_TXFORMAT, GBT_DATA_RXFORMAT) 
     */
    u_int gbt_setup( int alignment, int channel_mode );
    
    /**
     * Is used to load the tx phase values from a file, for subsequent
     * configuration of the registers \e GBT_TX_TC_DLY_VALUE1 to \e GBT_TX_TC_DLY_VALUE4
     * before the GBT initialization. 
     * 
     * @b Errors 
     * In case of an error, an exception is thrown.
     * @param svn_version The current SVN (old) of GIT (new) version
     * @param filename The file to load the tx phase values from
     */
    long int gbt_version_delay( u_long svn_version, char *filename );
    
    /**
     * This function will select the channel through the register RXUSRCLK_FREQ_CHANNEL.
     * Then it will wait for the register RXUSRCLK_FREQ_VALID to go high,
     * indicating that the measurement is done. 
     * The return value will be read from RXUSRCLK_FREQ_VAL
     * and it represents the measured frequency in Hz.  
     * @b Errors 
     * In case of an error, an exception is thrown;
     * if the functionality has not (yet) been implemented in the FPGA firmware, 
     * the function throws a TIMEOUT exception.
     * This also happens when a non-existing channel is selected.
     * @param channel The transceiver channel number on which the recovered receiver clock
     *                (rxusrclk) has to be measured
     * @param value Frequency of the recovered receiver clock of the selected channel in Hz
     */
    u_long rxusrclk_freq( u_int channel );
    
    /**
     * Resets the GTH receivers. It method is meant for FULL mode F/W only. 
     * @param quad The quad to be reset; legal values are 0..5
     */
    void gth_rx_reset( int quad = -1 );

    /**
     * @}
     */

     /**
     * \defgroup Interrupt
     * @{
     * This section describes the interrupt handler (IRQ) related functions.
     */

    /**
     * Enables one interrupt channel or all channels of one FLX card.
     * If called with interrupt set to the number of an interrupt
     * only this channel will be enabled.
     * If called with interrupt set to ALL_IRQS or if the interrupt parameter
     * is omitted, all channels of the given card will be enabled.
     *
     * This method calls a function of the flx device driver.
     * @b Errors
     * In case of an error, an exception is thrown.
     * @param interrupt The number of the interrupt; legal values are 0..7 and ALL_IQRS
     */
    void irq_enable( u_int interrupt = ALL_IRQS );
    
    /**
     * Disables one interrupt channel or all channels of one FLX card.
     * If called with interrupt set to the number of an interrupt
     * only this channel will be disabled.
     * If called with interrupt set to ALL_IRQS or if the interrupt parameter 
     * is omitted, all channels of the given card will be disabled. 
     * 
     * This method calls a function of the flx device driver. 
     * 
     * @b Errors 
     * In case of an error, an exception is thrown.
     * @param interrupt The number of the interrupt; legal values are 0..7 and ALL_IRQS
     */
    void irq_disable( u_int interrupt = ALL_IRQS );
    
    /**
     * Is blocking. It suspends the execution of a user application
     * until the interrupts of the number specified in 
     * interrupt has been received by the flx driver. 
     * 
     * The waiting takes place in the driver. 
     * 
     * @b Errors 
     * In case of an error, an exception is thrown.
     * @param interrupt The number (0..7) of the interrupt that is to be waited for
     */
    void irq_wait( u_int interrupt );
    
    /**
     * Instructs the driver to clear unsolicited interrupts.
     * These are interrupts that may have been received by the driver
     * while no user application was waiting for them.
     * If the driver has received an unsolicited interrupt and irq_wait()
     * is called by an application without clearing the interrupt,
     * the application will continue immediately instead of waiting for an interrupt.  
     * 
     * As this method interferes with the interrupt flags of the device driver
     * it should only be called if no applications are waiting
     * for the interrupts specified in interrupt. 
     * 
     * @b Errors 
     * In case of an error, an exception is thrown.
     * @param interrupt The number (0..7) of the interrupt that is to be cleared or ALL_IQRS
     *                  to clear all interrupts of that given card
     */
    void irq_clear( u_int interrupt = ALL_IRQS );
    
    /**
     * Instructs the driver to cancel wait requests
     * for one particular interrupt or for all interrupts of one FLX card. 
     * It will therefore unblock applications that are waiting for an interrupt.
     * As the function is setting an internal flag of the 
     * driver in order to simulate an interrupt by S/W
     * it should only be called if an application is blocked. 
     * 
     * @b Errors 
     * In case of an error, an exception is thrown.
     * @param interrupt The number (0..7) of the interrupt that is to be cancelled
     *                  or ALL_IQRS to cancel all interrupts of that given card
     */
    void irq_cancel( u_int interrupt = ALL_IRQS );
    
    /**
     * Resets the counters of one or all interrupts of the given FLX card.
     * The counters count the number of times a given interrupt has been signalled.
     * Their values are shown in the file /proc/flx. This method makes a call to the driver. 
     * 
     * @b Errors 
     * In case of an error, an exception is thrown.
     * @param interrupt The number (0..7) of the interrupt that is to be reset
     *                  or ALL_IQRS to reset all interrupts of that given card
     */
    void irq_reset_counters( u_int interrupt = ALL_IRQS );
    
    /**
     * @}
     */
     
     /**
     * \defgroup REG Register and bitfield access
     * @{
     * A set of functions for looking up registers and bitfields by name (string)
     * and to get and set individual bitfields and full 64-bits registers, by name.
     */

    /**
     * Reads the value of a bit field and returns its value as the value of the method.  
     * The method is only for the registers in BAR2.
     * For BAR0 and BAR1 register access use the bar0Address() and bar1Address() functions
     * and map the pointer to the respective structure from FlxCard.h. 
     * 
     * @b Errors 
     * In case of an error, an exception is thrown.
     * @param key A string with the name of the bit field
     * @param show_options If true displays (on standard output) a list of bitfield options
     *                     that match 'key' if a unique match was not found
     */
    u_long cfg_get_option( const char *key, bool show_options = false );
    
    /**
     * Writes a value to a bit field.
     * The method is only for the registers in BAR2. 
     * For BAR0 and BAR1 register access use the bar0Address() and bar1Address() functions
     * and map the pointer to the respective structure from FlxCard.h. 
     * 
     * @b Errors 
     * In case of an error, an exception is thrown.
     * @param *key A string with the name of the register or bit field 
     * @param value The value that is to be written to the register 
     * @param show_options If true displays (on standard output) a list of bitfield options
     *                     that match 'key', if a unique match was not found
     */
    void cfg_set_option( const char *key, u_long value, bool show_options = false );
    
    /**
     * Reads the value of a register or and returns its value as the value of the method.  
     * The method is only for the registers in BAR2.
     * For BAR0 and BAR1 register access use the bar0Address() and bar1Address() functions
     * and map the pointer to the respective structure from FlxCard.h. 
     * 
     * @b Errors 
     * In case of an error, an exception is thrown.
     * @param key A string with the name of the register
     */
    u_long cfg_get_reg( const char *key );

    /**
     * Writes a value to a register.
     * The method is only for the registers in BAR2. 
     * For BAR0 and BAR1 register access use the bar0Address() and bar1Address() functions
     * and map the pointer to the respective structure from FlxCard.h. 
     * 
     * @b Errors 
     * In case of an error, an exception is thrown.
     * @param *key A string with the name of the register or bit field 
     * @param value The value that is to be written to the register 
     */
    void cfg_set_reg( const char *key, u_long value );
    
    /**
     * Triggers a reset by the REGISTERS_RESET register in BAR0.
     * This causes all registers in the BAR2 area to be reset to their power-on default values. 
     * The registers in BAR0 and BAR1 will keep their values.
     */
    void registers_reset();

    /**
     * Returns a pointer to a `regmap_register_t` struct in the array of these structs
     * describing all the FELIX registers. Internally used by cfg_get_reg() and cfg_set_reg().
     * @param name The string containing the register name:
     *             is case-insensitive and '_' characters may be replaced by '-'
     */
    static regmap_register_t *cfg_register( const char *name );
    /**
     * Returns a pointer to a `regmap_bitfield_t` struct in the array of these structs
     * describing all the FELIX register bitfield. Internally used by cfg_get_option() and cfg_set_option().
     * @param name The string containing the bitfield name:
     *             is case-insensitive and '_' characters may be replaced by '-'
     */
    static regmap_bitfield_t *cfg_bitfield( const char *name );
    /**
     * Compiles and returns a string containing a list of register bitfield names
     * that match the given name, meaning that string 'name' is either a substring of the full bitfield name
     * or that the full bitfield name starts with 'name'.
     * Internally used by cfg_get_option() and cfg_set_option().
     * @param name The string with the name substring:
     *             is case-insensitive and '_' characters may be replaced by '-'
     * @param include_all_substr If false only bitfield names that start with the given substring
     *                           are returned, if true names are returned that contain the substring
     *                           anywhere in their name
     */
    static std::string cfg_bitfield_options( const char *name, bool include_all_substr = true );

    /**
     * @}
     */
     
     /**
     * \defgroup Tools
     * @{
     * A set of functions that can mostly be described as methods that interact with the card or device endpoint.
     */
    /**
     * Returns the H/W type of the FLX card, read in card_open().
     * Currently these types are known (and supported):
     * FLX-709, FLX-710, FLX-711, FLX-712, FLX-128, FLX-181, FLX-182, FLX-155
     * @return integer value of the card model, a number like 709, 712, etc.
     */
    u_int card_model() { return m_cardType; }
    u_int card_type() { return card_model(); }

    /**
     * Returns the firmware type of the FLX card, read in card_open().
     */
    u_int firmware_type() { return m_firmwareType; }

    /**
     * Returns the firmware type of the FLX card as a (short) string.
     */
    std::string firmware_type_string()
      { return( m_firmwareType<FIRMW_STR.size() ? FIRMW_STR[m_firmwareType] : std::string("????") ); }

    /**
     * Returns a descriptive name of the firmware type and version running on the FLX card.
     */
    std::string firmware_string() { return m_firmwareString; }

    /**
     * Returns whether the firmware type is an lpGBT-type firmware.
     */
    bool lpgbt_type() { return m_lpgbtType; }

    /**
     * Returns whether the lpGBT-type firmware uses FEC12 (otherwise FEC5)
     */
    bool lpgbt_fec12() { return m_lpgbtFec12; }

    /**
     * Returns whether the lpGBT-type firmware has lpGBT at 5Gbps (default 10Gbps)
     */
    bool lpgbt_5gbps() { return m_lpgbt5Gbps; }

    /**
     * Returns whether the firmware type is a FULLMODE-type firmware.
     */
    bool fullmode_type() { return m_fullmodeType; }

    /**
     * Triggers a reset by the SOFT_RESET register in BAR0.  
     */
    void soft_reset();
    
    /**
     * Triggers a reset by the CRFROMHOST_RESET register in BAR2.
     */
    void cr_fromhost_reset();

     /**
     * @return Number of physical links connected to the PCIe endpoint.
     **/
    u_int number_of_channels() { return m_numberOfChans; }

    /**
     * @return Indices of the e-link number for EC ToHost (receive) and FromHost (send).
     **/
    std::pair<int,int> ec_elink_indices();

    /**
     * @return Indices of the e-link number for IC ToHost (receive) and FromHost (send).
     **/
    std::pair<int,int> ic_elink_indices();

    /**
     * @}
     */
     
    /**
     * \defgroup Monitoring
     * @{
     * A set of functions to support monitoring of the FLX card
     */

    /**
     * Retrieves monitoring data from various devices on the FLX card
     * (FPGA, MiniPODs, power monitoring ICs, temperature ICs, FireFly devices),
     * like status, temperatures, currents, voltages, power readings.
     * The following bits are defined in 'device_mask':
     * - FPGA_MONITOR Retrieve information about the FPGA
     * - POD_MONITOR_ALL  Retrieve various sets of information from the (upto 8) MiniPODs
     * - POD_MONITOR_LOS  Retrieve Loss-of-Signal information from the MiniPODs
     * - POD_MONITOR_TEMP_VOLTS  Retrieve temperature and voltage readings from the MiniPODs
     * - POD_MONITOR_POWER  Retrieve optical link power readings from the MiniPODs
     * - POWER_MONITOR  Retrieve information from the LTC2991 or INA226 power monitoring devices
     * - FIREFLY_MONITOR  Retrieve information from the FireFly devices
     * The method returns a structure of type `monitoring_data_t` which is filled
     * in accordance with the given 'monitor mask' bits (parts of the structure may remain empty).
     * See the data structures defined in FlxCard.h for the organisation of the data and names of the parameters.
     * See application flx-info.cpp for an example of decoding the structure.
     * Note: It is likely that additional parameters and readout options will be added in the future.
     *
     * @b Errors
     * In case of an error, an exception is thrown.
     * @param mon_mask A mask of bits to select the parts to be monitored.
     * @return The method returns a structure of type `monitoring_data_t`.
     */
    monitoring_data_t get_monitoring_data( u_int mon_mask );

    /**
     * @return The temperature of the FPGA (in degrees celcius).
     **/
    float fpga_temperature();

    /**
     * @return The speed of the fan installed on the FPGA (in RPM), -1 returned in case of an invalid reading.
     **/
    int fpga_fanspeed();
    
    /**
     * @return The temperatures of the MiniPODs (in degrees celcius), -1 returned for missing devices.
     *         MiniPOD order: TX1, RX1, TX2, RX2, etc.
     **/
    std::vector<int> minipods_temperature();

    /**
     * @return The optical power readings of the MiniPODs (in microWatts),
     *         0.0 returned for unconnected channels and missing devices.
     *         MiniPOD order: 12 channels TX1, 12 channels RX1, 12 channels TX2, 12 channels RX2, etc.
     **/
    std::vector<float> minipods_optical_power();

    /**
     * Determines if the FireFly identified by a name is accessible via I2C.
     * @return Whether the FireFly device with the given name is accessible,
     *         and if true, device part number and serial number are returned
     *         as strings in vendor_part and vendor_sn respectively.
     **/
    bool fireFlyDetect( const std::string &device_name,
                        std::string &vendor_part,
                        std::string &vendor_sn );

    /**
     * @}
     */
     
    /**
     * \defgroup Configuration
     * @{
     * Functions pertaining to configuration of an FLX device and its (e-)links
     */

    void configure( std::string filename, bool do_links = true, bool do_registers = true );

    /**
     * Reads a FELIX device (e-)link configuration -plus additional register settings,
     * if any- from a .yelc file, and stores it in a member structure.
     */
    void readConfiguration( std::string filename );

    /**
     * Reads a FELIX device (e-)link configuration from this FELIX device and
     * stores it in a member structure.
     * Leaves any stored additional register settings untouched.
     */
    void readConfiguration();

    /**
     * Returns the number of enabled ToHost e-link numbers of this FELIX device,
     * optionally only those assigned to the given DMA index.
     */
    u_int number_of_elinks_tohost( int dma_index = -1 );

    /**
     * Returns the number of enabled FromHost e-link numbers of this FELIX device,
     * including TTC-type e-links.
     */
    u_int number_of_elinks_toflx();
    u_int number_of_elinks_fromhost() { return number_of_elinks_toflx(); }

    /**
     * Returns a list of the enabled ToHost e-link numbers of this FELIX device,
     * optionally only those assigned to the given DMA index.
     */
    std::vector<flxcard::elink_descr_t> elinks_tohost( int dma_index = -1 );

    /**
     * Returns a list of the enabled FromHost e-link numbers of this FELIX device,
     * not including TTC-type e-links.
     */
    std::vector<flxcard::elink_descr_t> elinks_toflx();
    std::vector<flxcard::elink_descr_t> elinks_fromhost() { return elinks_toflx(); }

    /**
     * Returns the number of ToHost DMA descriptors of this FELIX device.
     */
    u_int number_of_dma_tohost() { return m_numberOfDma; }

    /**
     * Returns the link mode, defined in LinkConfig.h
     */
    unsigned int link_mode();

    /**
     * Returns true if the e-link is enabled.
     */
    bool is_elink_enabled( u_int channel, u_int egroup, u_int epath, bool is_to_flx );

    /**
     * Returns the DMA id assigned to the given e-link
     */
    unsigned int tohost_elink_dmaid( uint16_t elinknr );

    /**
     * Returns the DMA id assigned to the given e-link
     */
    unsigned int tohost_elink_dmaid( u_int channel, u_int egroup, u_int epath );

    /**
     * Returns true if the e-link is enabled.
     */
    bool has_tohost_elink_streams( u_int channel, u_int egroup, u_int epath );

    /**
     * @returns the detector id set up in the register map.
     * @details Returns -1 if the VALID bit is not set.
     */
    uint8_t detector_id();

    /**
     * @returns the connector id set up in the register map.
     * @details Returns the connector number from 0 to 3 if the register is not set
     * @param channel: the channel number to resolve the connector ID in case of
     * an FLX-155 card with more than 24 links.
     */
    uint16_t connector_id( uint16_t channel );

    /**
     * @}
     */
     
private:
    static int  m_cardsOpen;
    int         m_fd;
    int         m_deviceNumber;
    int         m_maxTlpBytes;
    u_int       m_cardType;
    u_int       m_firmwareType;
    bool        m_configRead;
    bool        m_lpgbtType;
    bool        m_lpgbtFec12;
    bool        m_lpgbt5Gbps;
    bool        m_fullmodeType;
    u_int       m_numberOfChans;
    u_int       m_numberOfDma;
    u_int       m_fromHostDataFormat;
    std::string m_firmwareString;
    u_int       m_myLocks;
    u_int       m_myLockTag;  //MJ: do we still need the lock tag??
    u_long      m_bar0Base;
    u_long      m_bar1Base;
    u_long      m_bar2Base;

    // Space to store a device's e-link configuration, for all links + Emulator 'link'
    flxcard::LinkConfig m_linkConfig[flxcard::FLX_LINKS + 1];

    // Space to store additional register settings
    std::vector<flxcard::regsetting_t> m_regSettings;

public:
    /** @} */
    /** \defgroup MemberVariables Public member variables 
     * @{  
     * Public member variables of class FlxCard.
     */
    volatile flxcard_bar0_regs_t *m_bar0;
    volatile flxcard_bar1_regs_t *m_bar1;
    volatile flxcard_bar2_regs_t *m_bar2;

    // Pointer to a list of this FLX device's I2C devices
    i2c_device_t *m_i2cDevices;

    /**
     * @}
     */

private:    
    u_long map_memory_bar( u_long pci_addr, size_t size );
    void   unmap_memory_bar( u_long vaddr, size_t size );

    void   i2c_wait_not_full( );
    void   i2c_wait_not_empty( );
    int    i2c_parse_address_string( const char *str,
                                     u_char *port1, u_char *port2, u_char *port3,
                                     u_char *dev_addr );
    void   i2c_set_switches( u_char switch1_val, u_char switch2_val, u_char switch3_val );

    void   gbt_tx_configuration( int channel_tx_mode, int alignment );
    int    gbt_rx_configuration( int channel_rx_mode );
    int    gbt_software_alignment( int number_channels );
    bool   gbt_channel_alignment( u_int channel );
    void   gbt_topbot_oddeven_set( u_int channel, u_int topbot, u_int oddeven );
    bool   gbt_topbot_alignment( u_int channel, u_int topbot,
                                 u_long *phase_found, u_int *oddeven );
    u_long gbt_shift_phase( u_int channel );

    int    check_digic_value2( const char *str, u_long *version, u_long *delay );

    std::string lockbits2string( u_int locks );

    void configureLinks( flxcard::LinkConfig *link_config, int *dma_index_invalid_count );
};

#endif // FLXCARD_H
