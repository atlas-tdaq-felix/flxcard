#ifndef LINKCONFIG_H
#define LINKCONFIG_H

#include <cstdint>
#include <utility>
#include <string>

namespace flxcard {

// Constants for FELIX/FLX-cards
const int FLX_LINKS              = 24;
const int FLX_TOHOST_GROUPS      = (7+1); // One 'mini group' for EC/IC/AUX/TTC2H
const int FLX_FROMHOST_GROUPS    = (5+1); // One 'mini group' for EC/IC/AUX
const int TOHOST_MINI_GROUP      = FLX_TOHOST_GROUPS-1;   // ToHost 'mini' group index
const int FROMHOST_MINI_GROUP    = FLX_FROMHOST_GROUPS-1; // FromHost 'mini' group index
const int FLX_MAXCHUNK_UNIT      = 512;

// Legacy RM4 enable bits values, for non-egroup e-links
// (in TO/FROMHOST_MINI_GROUP, except FULL_ENABLED_BIT, which is in egroup 0)
const uint32_t EC_ENABLED_BIT    = 14;
const uint32_t IC_ENABLED_BIT    = 13;
const uint32_t AUX_ENABLED_BIT   = 12;
const uint32_t TTC2H_ENABLED_BIT = 10;
const uint32_t FULL_ENABLED_BIT  = 16;
const uint32_t EC_ENABLED        = (1<<EC_ENABLED_BIT);    // 0x4000
const uint32_t IC_ENABLED        = (1<<IC_ENABLED_BIT);    // 0x2000
const uint32_t AUX_ENABLED       = (1<<AUX_ENABLED_BIT);   // 0x1000
const uint32_t TTC2H_ENABLED     = (1<<TTC2H_ENABLED_BIT); // 0x0400
const uint32_t FULL_ENABLED      = (1<<FULL_ENABLED_BIT);  // 0x10000

enum : unsigned int {
  LINKMODE_GBT = 0,
  LINKMODE_GBTWIDE,
  LINKMODE_FULL,
  LINKMODE_LPGBT10_F5,
  LINKMODE_LPGBT5_F5,
  LINKMODE_LPGBT10_F12,
  LINKMODE_LPGBT5_F12,
  LINKMODE_INVALID
};

// For additional register settings optionally included in configuration files
typedef struct register_setting {
  std::string name;
  uint64_t value;
} regsetting_t;

//typedef std::pair<unsigned int, unsigned int> n_egroups_epaths_pair_t;
using n_egroups_epaths_pair_t = std::pair<unsigned int, unsigned int>;

// Copied from felix-star: elink_type_t, encoding_t
enum elink_type_t : uint32_t {
    NONE_ELINK_TYPE,
    DAQ,
    TTC,
    DCS,
    IC
};
enum encoding_t : uint32_t {
    ENC_DIRECT,
    ENC_8B10B,
    ENC_HDLC,
    ENC_TTC,
    ENC_8B10B_STRIPS,
    ITK_PIXEL,
    ENDEAVOUR,
    ENC_INTERLAKEN
};

typedef struct elink_descr {
  uint32_t     id;
  //elink_type_t type;
  //encoding_t   encoding;
  uint32_t     type;
  uint32_t     encoding;
  int          dma_index;
  bool         has_streams;
} elink_descr_t;

// Structure/class containing a single (lp)GBT-link's configuration for FELIX
class LinkConfig
{
 private:
  // Link type: GBT, GBT-wide, FULL or lpGBT
  uint32_t _linkMode;

  // Clock: TTC or local
  bool     _ttcClock;

  // FromHost link mode: GBT or LTI (applies to FULL mode only)
  bool     _ltiTtc;

  // ToHost:
  // E-link configuration bitmask, 7+1 e-groups per link
  uint32_t _enablesToHost[FLX_TOHOST_GROUPS];
  // E-link group width (actually a width code: 0=2bits, 1=4, 2=8, 3=16, 4=32)
  // 7+1 (to Host) e-groups per link
  uint32_t _widthToHost[FLX_TOHOST_GROUPS];
  // E-link group mode words, 7+1 (to Host) e-groups per link
  uint64_t _modesToHost[FLX_TOHOST_GROUPS];
  // DMA index for ToHost E-links (1 nibble per E-path, 7+1 groups * 8)
  uint64_t _dmaIndices[FLX_TOHOST_GROUPS];
  // Stream-ID indicator bits (1 per ToHost E-path, 7 groups * 8 bits)
  uint64_t _streamIdBits[FLX_TOHOST_GROUPS-1];
  // E-link number offsets for EC, IC and AUX
  int _ecToHostIndex, _icToHostIndex, _auxToHostIndex;
  // Maximum ToHost chunk length word (4x3 bits per GBT)
  uint32_t _maxChunkWord;

  // FromHost:
  // E-link enable configuration bitmask, 5+1 e-groups per link
  uint32_t _enablesFromHost[FLX_FROMHOST_GROUPS];
  // E-link group width (actually a width code: 0=2bits, 1=4, 2=8, 3=16, 4=32)
  // 5+1 (from Host) e-groups per link
  uint32_t _widthFromHost[FLX_FROMHOST_GROUPS];
  // E-link group mode words, 5+1 (from Host) e-groups per link
  uint64_t _modesFromHost[FLX_FROMHOST_GROUPS];
  // E-link group TTC option value, 5 (from Host) e-groups per link
  uint32_t _ttcOptionFromHost[FLX_FROMHOST_GROUPS-1];
  // E-link number offsets for EC, IC and AUX
  int _ecFromHostIndex, _icFromHostIndex, _auxFromHostIndex;

  // --------------------------------------------------------------------------

 public:
  LinkConfig()
  {
    _linkMode = LINKMODE_GBT;

    _ttcClock = false;

    _ltiTtc = false;

    // Default 2-bit elinks, '8b10b' mode, standard-mode GBT
    // In case of full-mode: link enabled (bit 16 set in 'enables' word)
    int egroup;
    for( egroup=0; egroup<FLX_TOHOST_GROUPS-1; ++egroup )
      {
        _enablesToHost[egroup] = 0x00;
        _widthToHost[egroup]   = 0; // 2-bit
        _modesToHost[egroup]   = 0x11111111; // All '8b10b'
        _dmaIndices[egroup]    = 0;
      }

    // If FULL mode then link is enabled
    //_enablesToHost[0] |= FULL_ENABLED;

    // EC+IC+AUX+TTCtoHost group
    //_enablesToHost[TOHOST_MINI_GROUP] = 0x4400; // EC, TTCtoHost
    //_enablesToHost[TOHOST_MINI_GROUP] = 0x6400; // EC, IC, TTCtoHost
    //_enablesToHost[TOHOST_MINI_GROUP] = 0x7400; // EC, IC, AUX, TTCtoHost
    //_enablesToHost[TOHOST_MINI_GROUP] = EC_ENABLED; // EC, not TTCtoHost
    _enablesToHost[TOHOST_MINI_GROUP] = 0; // None enabled
    _widthToHost[TOHOST_MINI_GROUP]   = 0; // 2-bit
    _modesToHost[TOHOST_MINI_GROUP]   = ((uint64_t)2 << (EC_ENABLED_BIT*4));  // EC 'HDLC'
    _modesToHost[TOHOST_MINI_GROUP]  |= ((uint64_t)2 << (IC_ENABLED_BIT*4));  // IC 'HDLC'
    _modesToHost[TOHOST_MINI_GROUP]  |= ((uint64_t)2 << (AUX_ENABLED_BIT*4)); // AUX 'HDLC'
    _dmaIndices[TOHOST_MINI_GROUP]    = 0;

    for( egroup=0; egroup<FLX_FROMHOST_GROUPS-1; ++egroup )
      {
        _enablesFromHost[egroup]   = 0x00;
        _widthFromHost[egroup]     = 0; // 2-bit
        _modesFromHost[egroup]     = 0x11111111; // All '8b10b'
        _ttcOptionFromHost[egroup] = 0;
      }
    // EC+IC+AUX group
    //_enablesFromHost[FROMHOST_MINI_GROUP] = EC_ENABLED; // EC
    _enablesFromHost[FROMHOST_MINI_GROUP] = 0; // None enabled
    _widthFromHost[FROMHOST_MINI_GROUP]   = 0; // 2-bit
    _modesFromHost[FROMHOST_MINI_GROUP]   = ((uint64_t)2 << (EC_ENABLED_BIT*4));  // EC 'HDLC'
    _modesFromHost[FROMHOST_MINI_GROUP]  |= ((uint64_t)2 << (IC_ENABLED_BIT*4));  // IC 'HDLC'
    _modesFromHost[FROMHOST_MINI_GROUP]  |= ((uint64_t)2 << (AUX_ENABLED_BIT*4)); // AUX 'HDLC'

    //maxChunkWord = 0x249; Or rather (512 bytes max chunksize):
    //_maxChunkWord = (1<<(0*3)) | (1<<(1*3)) | (1<<(2*3)) | (1<<(3*3));

    // Max chunk size set to all 0 (no maximum)
    _maxChunkWord = (0<<(0*3)) | (0<<(1*3)) | (0<<(2*3)) | (0<<(3*3));

    for( egroup=0; egroup<FLX_TOHOST_GROUPS-1; ++egroup )
      _streamIdBits[egroup] = 0;

    // Values corresponding to LINKMODE_GBT:
    _ecToHostIndex    = 40; // 0x28
    _ecFromHostIndex  = 40;
    _icToHostIndex    = 41; // 0x29
    _icFromHostIndex  = 41;
    _auxToHostIndex   = 42; // 0x2A
    _auxFromHostIndex = 42;
  }

  ~LinkConfig() {}

  // --------------------------------------------------------------------------

  static n_egroups_epaths_pair_t numberOfEgroupsEpathsToHost( uint32_t linkMode )
  {
    unsigned int egroups, epaths;
    if( linkMode >= LINKMODE_INVALID )
      {
        egroups = 0;
        epaths = 0;
      } 
    else if ( linkMode != LINKMODE_FULL )
      {
        // Groups: GBT 5, GBT-wide 7, lpGBT-FEC5 7, lpGBT-FEC12 6
        if( linkMode == LINKMODE_LPGBT10_F5 ||
            linkMode == LINKMODE_LPGBT5_F5 )
          {
            egroups = 7;
            epaths = 4;
          }
      else if( linkMode == LINKMODE_LPGBT10_F12 ||
               linkMode == LINKMODE_LPGBT5_F12 )
        {
          egroups = 6;
          epaths = 4;
        }
      else if( linkMode == LINKMODE_GBTWIDE )
        {
          egroups = 7;
          epaths = 8;
        }
      else
        {
          egroups = 5;
          epaths = 8;
        }
      }
    else
      {
        // LINKMODE_FULL
        egroups = 1;
        epaths = 1;
      }
    return std::make_pair( egroups, epaths );
  }

  // --------------------------------------------------------------------------

  static n_egroups_epaths_pair_t numberOfEgroupsEpathsFromHost( uint32_t linkMode )
  {
    unsigned int egroups, epaths;
    if( linkMode == LINKMODE_FULL || linkMode >= LINKMODE_INVALID )
      {
        egroups = 0;
        epaths = 0;
      } 
    else
      {
        // Groups: GBT 5, GBT-wide 5, lpGBT-FEC5 4, lpGBT-FEC12 4
        if( linkMode == LINKMODE_LPGBT10_F5 ||
            linkMode == LINKMODE_LPGBT5_F5 )
          {
            egroups = 4;
            epaths = 5; // STRIP firmware has 5
          }  
        else if( linkMode == LINKMODE_LPGBT10_F12 ||
                 linkMode == LINKMODE_LPGBT5_F12 )
          {
            egroups = 4;
            epaths = 4;
          }
        else if( linkMode == LINKMODE_GBTWIDE )
          {
            egroups = 5;
            epaths = 8;
          }
        else
          {
            egroups = 5;
            epaths = 8;
          }
      }
    return std::make_pair( egroups, epaths );
  }

  // --------------------------------------------------------------------------

  int numberOfElinksToHost()
  {
    // Return the enabled ToHost (E-)link count,
    // not including EC, IC, AUX, TTCtoHost, and not dependent on DMA index
    if( _linkMode >= LINKMODE_INVALID )
      return 0;

    int cnt = 0;
    if( _linkMode != LINKMODE_FULL )
      {
        n_egroups_epaths_pair_t groups_paths = numberOfEgroupsEpathsToHost( _linkMode );
        uint32_t grp_cnt = groups_paths.first;
        uint32_t path_cnt = groups_paths.second;

        for( uint32_t grp=0; grp<grp_cnt; ++grp )
          for( uint32_t path=0; path<path_cnt; ++path )
            if( _enablesToHost[grp] & (1<<path) )
              ++cnt;
      }
    else
      {
        // FULLMODE link
        if( _enablesToHost[0] & FULL_ENABLED )
          cnt = 1;
      }
    return cnt;
  }

  int numberOfElinksToHost( int dma_index )
  {
    // Return the enabled ToHost (E-)link count, assigned to the given DMA index
    std::vector<flxcard::elink_descr_t> elinks =
      this->elinksEnabledToHost( 0, dma_index );
    return (int) elinks.size();
  }

  // --------------------------------------------------------------------------

  int numberOfElinksFromHost()
  {
    // Return the number of enabled FromHost E-links, not including EC, IC, AUX
    n_egroups_epaths_pair_t groups_paths = numberOfEgroupsEpathsFromHost( _linkMode );
    uint32_t grp_cnt = groups_paths.first;
    uint32_t path_cnt = groups_paths.second;

    int cnt = 0;
    for( uint32_t i=0; i<grp_cnt; ++i )
      for( uint32_t j=0; j<path_cnt; ++j )
        if( _enablesFromHost[i] & (1<<j) )
          ++cnt;
    return cnt;
  }

  // --------------------------------------------------------------------------

  int numberOfEcIcToHost()
  {
    // Return the number of enabled ToHost EC/IC/AUX/TTCtoHost E-links
    if( _linkMode >= LINKMODE_INVALID )
      return 0;

    int cnt = 0;
    if( _linkMode != LINKMODE_FULL )
      {
        // EC + IC + AUX + TTCtoHost (skip duplicates in bits 0-7)
        for( uint32_t j=8; j<16; ++j )
          if( _enablesToHost[TOHOST_MINI_GROUP] & (1<<j) )
            ++cnt;
      }
    else
      {
        // TTCtoHost only
        if( _enablesToHost[TOHOST_MINI_GROUP] & TTC2H_ENABLED )
          ++cnt;
      }
    return cnt;
  }

  // --------------------------------------------------------------------------

  int numberOfEcIcFromHost()
  {
    // Return the number of enabled FromHost EC/IC/AUX E-links
    int cnt = 0;
    // EC + IC + AUX (skip duplicates in bits 0-7)
    for( uint32_t j=8; j<16; ++j )
      if( _enablesFromHost[FROMHOST_MINI_GROUP] & (1<<j) )
        ++cnt;
    return cnt;
  }

  // --------------------------------------------------------------------------

  uint32_t enablesToHost( uint32_t egroup )
  {
    if( egroup >= FLX_TOHOST_GROUPS )
      return 0;
    return _enablesToHost[egroup];
  }

  void setEnablesToHost( uint32_t egroup, uint32_t enables )
  {
    if( egroup >= FLX_TOHOST_GROUPS )
      return;
    _enablesToHost[egroup] = enables;
  }

  // --------------------------------------------------------------------------

  uint32_t enablesFromHost( uint32_t egroup )
  {
    if( egroup >= FLX_FROMHOST_GROUPS )
      return 0;
    return _enablesFromHost[egroup];
  }

  void setEnablesFromHost( uint32_t egroup, uint32_t enables )
  {
    if( egroup >= FLX_FROMHOST_GROUPS )
      return;
    _enablesFromHost[egroup] = enables;
  }

  // --------------------------------------------------------------------------

  uint32_t widthToHost( uint32_t egroup )
  {
    if( egroup >= FLX_TOHOST_GROUPS )
      return 0;
    return _widthToHost[egroup];
  }

  void setWidthToHost( uint32_t egroup, uint32_t width )
  {
    if( egroup >= FLX_TOHOST_GROUPS )
      return;
    _widthToHost[egroup] = width;
  }

  // --------------------------------------------------------------------------

  uint32_t widthFromHost( uint32_t egroup )
  {
    if( egroup >= FLX_FROMHOST_GROUPS )
      return 0;
    return _widthFromHost[egroup];
  }

  void setWidthFromHost( uint32_t egroup, uint32_t width )
  {
    if( egroup >= FLX_FROMHOST_GROUPS )
      return;
    _widthFromHost[egroup] = width;
  }

  // --------------------------------------------------------------------------

  uint64_t modesToHost( uint32_t egroup )
  {
    if( egroup >= FLX_TOHOST_GROUPS )
      return 0;
    return _modesToHost[egroup];
  }

  void setModesToHost( uint32_t egroup, uint64_t modes )
  {
    if( egroup >= FLX_TOHOST_GROUPS )
      return;
    _modesToHost[egroup] = modes;
  }

  int mode( int egroup, int epath, bool to_host = true )
  {
    // Returns the currently set mode for the given E-group/E-path
    uint64_t modes;
    if( to_host )
      {
        if( egroup >= FLX_TOHOST_GROUPS )
          return 0;
        modes = _modesToHost[egroup];
      }
    else
      {
        if( egroup >= FLX_FROMHOST_GROUPS )
          return 0;
        modes = _modesFromHost[egroup];
      }
    return( (modes >> (epath*4)) & 0xF );
  }

  // --------------------------------------------------------------------------

  uint64_t modesFromHost( uint32_t egroup )
  {
    if( egroup >= FLX_FROMHOST_GROUPS )
      return 0;
    return _modesFromHost[egroup];
  }

  void setModesFromHost( uint32_t egroup, uint64_t modes )
  {
    if( egroup >= FLX_FROMHOST_GROUPS )
      return;
    _modesFromHost[egroup] = modes;
  }

  // --------------------------------------------------------------------------

  uint32_t ttcOptionFromHost( uint32_t egroup )
  {
    if( egroup >= FLX_FROMHOST_GROUPS-1 )
      return 0;
    return _ttcOptionFromHost[egroup];
  }

  void setTtcOptionFromHost( uint32_t egroup, uint32_t option )
  {
    if( egroup >= FLX_FROMHOST_GROUPS-1 )
      return;
    _ttcOptionFromHost[egroup] = option;
  }

  // --------------------------------------------------------------------------

  uint32_t dmaIndices( uint32_t egroup )
  {
    if( egroup >= FLX_TOHOST_GROUPS )
      return 0;
    return _dmaIndices[egroup];
  }

  void setDmaIndices( uint32_t egroup, uint32_t indices )
  {
    if( egroup >= FLX_TOHOST_GROUPS )
      return;
    _dmaIndices[egroup] = indices;
  }

  int dmaIndex( int egroup, int epath )
  {
    // Returns the currently set DMA index for the given E-group/E-path
    uint64_t indices;
    if( egroup >= FLX_TOHOST_GROUPS )
      return 0;
    indices = _dmaIndices[egroup];
    return( (indices >> (epath*4)) & 0xF );
  }

  // --------------------------------------------------------------------------

  uint32_t maxChunkWord()
  {
    return _maxChunkWord;
  }

  void setMaxChunkWord( uint32_t chunkword )
  {
    _maxChunkWord = chunkword;
  }

  void setMaxChunkSize( int index, int size )
  {
    _maxChunkWord &= ~(7 << (index*3));
    _maxChunkWord |= (size/FLX_MAXCHUNK_UNIT) << (index*3);
  }

  // --------------------------------------------------------------------------

  uint32_t linkMode()
  {
    return _linkMode;
  }

  void setLinkMode( uint32_t mode )
  {
    _linkMode = mode;

    // Adjust other settings dependent on the mode
    if( mode == LINKMODE_LPGBT10_F5 ||
        mode == LINKMODE_LPGBT10_F12 )
      {
        for( int i=0; i<FLX_TOHOST_GROUPS-1; ++i )
          if( _widthToHost[i] < 2 ) // Invalid width, so set to:
            _widthToHost[i] = 2;    // lpGBT-10 8-bit

        for( int i=0; i<FLX_TOHOST_GROUPS-1; ++i )
          _enablesToHost[i] &= ~0xF0; // Only 4 paths per ToHost group

        for( int i=0; i<FLX_FROMHOST_GROUPS-1; ++i )
          //_enablesFromHost[i] &= ~0xF0; // Only 4 paths per FromHost group
          _enablesFromHost[i] &= ~0xE0;   // STRIP firmware has 5...
      }
    else if( mode == LINKMODE_LPGBT5_F5 ||
             mode == LINKMODE_LPGBT5_F12 )
      {
        for( int i=0; i<FLX_TOHOST_GROUPS-1; ++i )
          if( _widthToHost[i] < 1 ) // Invalid width, so set to:
            _widthToHost[i] = 1;    // lpGBT-5 4-bit

        for( int i=0; i<FLX_TOHOST_GROUPS-1; ++i )
          _enablesToHost[i] &= ~0xF0; // Only 4 paths per ToHost group

        for( int i=0; i<FLX_FROMHOST_GROUPS-1; ++i )
          _enablesFromHost[i] &= ~0xF0; // Only 4 paths per FromHost group
      }
  }

  // --------------------------------------------------------------------------

  bool ltiTtc()
  {
    return _ltiTtc;
  }

  void setLtiTtc( bool b )
  {
    _ltiTtc = b;
  }

  // --------------------------------------------------------------------------

  bool ttcClock()
  {
    return _ttcClock;
  }

  void setTtcClock( bool b )
  {
    _ttcClock = b;
  }

  // --------------------------------------------------------------------------

  uint32_t streamIdBits( uint32_t egroup )
  {
    if( egroup >= FLX_TOHOST_GROUPS-1 )
      return 0;
    return _streamIdBits[egroup];
  }

  void setStreamIdBits( uint32_t egroup, uint32_t bits )
  {
    if( egroup >= FLX_TOHOST_GROUPS-1 )
      return;
    _streamIdBits[egroup] = bits;
  }

  bool hasStreamId( int egroup, int epath )
  {
    if( egroup >= FLX_TOHOST_GROUPS-1 )
      return false;
    return( (_streamIdBits[egroup] & (1<<epath)) != 0 );
  }

  // --------------------------------------------------------------------------

  int ecToHostIndex()  { return _ecToHostIndex; }
  int icToHostIndex()  { return _icToHostIndex; }
  int auxToHostIndex() { return _auxToHostIndex; }

  int ecFromHostIndex()  { return _ecFromHostIndex; }
  int icFromHostIndex()  { return _icFromHostIndex; }
  int auxFromHostIndex() { return _auxFromHostIndex; }

  void setEcToHostIndex( int i )  { _ecToHostIndex = i; }
  void setIcToHostIndex( int i )  { _icToHostIndex = i; }
  void setAuxToHostIndex( int i ) { _auxToHostIndex = i; }

  void setEcFromHostIndex( int i )  { _ecFromHostIndex = i; }
  void setIcFromHostIndex( int i )  { _icFromHostIndex = i; }
  void setAuxFromHostIndex( int i ) { _auxFromHostIndex = i; }

  // --------------------------------------------------------------------------

  bool isEnabled( int egroup, int epath, int *nbits, bool to_host = true )
  {
    // Determine whether a certain E-link number (defined by its 'egroup' and
    // 'path') in this LinkConfig is enabled, and if so,
    // how many bits wide it is (returned in 'nbits')

    // Take into account special egroup/epath numbers for EC,IC etc. e-links
    // in combination with from/to direction (which is asymetrical)
    // (this is a bit messy, blame it on legacy...)
    if( egroup == TOHOST_MINI_GROUP )
      {
        if( epath == 7 ) // EC
          epath = EC_ENABLED_BIT;
        else if( epath == 6 ) // IC
          epath = IC_ENABLED_BIT;
        else if( epath == 5 ) // SCA-AUX
          epath = AUX_ENABLED_BIT;
        else if( epath == 3 ) // TTCtoHost
          epath = TTC2H_ENABLED_BIT;
      }
    else if( to_host && _linkMode == LINKMODE_FULL &&
             egroup == 0 && epath == 0 )
      {
        epath = FULL_ENABLED_BIT;
      }

    // In FromHost direction the special egroup is a different one...
    if( !to_host && egroup == TOHOST_MINI_GROUP )
      egroup = FROMHOST_MINI_GROUP;

    if( nbits )
      {
        if( to_host )
          {
            if( _linkMode == LINKMODE_FULL )
              *nbits = 0;
            else
              *nbits = LinkConfig::widthCodeToBits( _widthToHost[egroup] );
          }
        else
          {
            *nbits = LinkConfig::widthCodeToBits( _widthFromHost[egroup] );
          }
      }

    uint32_t enable_bits;
    if( to_host )
      {
        if( egroup >= FLX_TOHOST_GROUPS )
          return false;
        enable_bits = _enablesToHost[egroup];
      }
    else
      {
        if( egroup >= FLX_FROMHOST_GROUPS )
          return false;
        enable_bits = _enablesFromHost[egroup];
      }

    bool enabled = ((enable_bits & (1 << epath)) != 0);
    return enabled;
  }

  // --------------------------------------------------------------------------

  std::vector<elink_descr_t> elinksEnabledToHost( uint32_t chan, int dma_index = -1 )
  {
    // By default includes the e-links for all available DMAs
    std::vector<elink_descr_t> elinks;
    elink_descr_t elnk;
    uint32_t chan_offset = (chan << 6);

    // TTC2Host e-link first (not to end up somewhere in the middle)
    uint32_t enable_bits = _enablesToHost[TOHOST_MINI_GROUP];
    if( (enable_bits & TTC2H_ENABLED) != 0 )
      {
        elnk.dma_index = dmaIndex( TOHOST_MINI_GROUP, 0 );
        // Want e-links assigned to a certain DMA index?
        if( dma_index == -1 ||
            (dma_index >= 0 && elnk.dma_index == dma_index) )
          {
            elnk.id          = 0x600;
            elnk.encoding    = mode( TOHOST_MINI_GROUP, TTC2H_ENABLED_BIT );
            elnk.type        = TTC;
            elnk.has_streams = false;
            elinks.push_back( elnk );
          }
      }

    // 'Regular' e-links
    if( _linkMode != LINKMODE_FULL )
      {
        n_egroups_epaths_pair_t groups_paths = numberOfEgroupsEpathsToHost( _linkMode );
        uint32_t grp_cnt = groups_paths.first;
        uint32_t path_cnt = groups_paths.second;

        for( uint32_t grp=0; grp<grp_cnt; ++grp )
          for( uint32_t path=0; path<path_cnt; ++path )
            if( _enablesToHost[grp] & (1<<path) )
              {
                elnk.dma_index = this->dmaIndex( grp, path );
                // Want e-links assigned to a certain DMA index?
                if( dma_index == -1 ||
                    (dma_index >= 0 && elnk.dma_index == dma_index) )
                  {
                    elnk.id       = chan_offset + grp*path_cnt + path;
                    elnk.encoding = mode( grp, path );
                    if( elnk.encoding == ENC_HDLC )
                      elnk.type = DCS;
                    else
                      elnk.type = DAQ;
                    elnk.has_streams = this->hasStreamId( grp, path );
                    elinks.push_back( elnk );
                  }
              }
      }
    else
      {
        // FULLMODE link
        if( _enablesToHost[0] & FULL_ENABLED )
          {
            elnk.dma_index = dmaIndex( 0, 0 );
            // Want links assigned to a certain DMA index?
            if( dma_index == -1 ||
                (dma_index >= 0 && elnk.dma_index == dma_index) )
              {
                elnk.id          = chan_offset + 0;
                elnk.encoding    = ENC_8B10B;
                elnk.type        = DAQ;
                elnk.has_streams = false;
                elinks.push_back( elnk );
              }
          }
      }

    // IC/EC/AUX e-links
    enable_bits = _enablesToHost[TOHOST_MINI_GROUP];
    if( _linkMode != LINKMODE_FULL )
      {
        if( (enable_bits & EC_ENABLED) != 0 )
          {
            // EC DMA index setting, stored as path 7
            elnk.dma_index = dmaIndex( TOHOST_MINI_GROUP, 7 );
            // Want e-links assigned to a certain DMA index?
            if( dma_index == -1 ||
                (dma_index >= 0 && elnk.dma_index == dma_index) )
              {
                elnk.id          = chan_offset + _ecToHostIndex;
                elnk.encoding    = mode( TOHOST_MINI_GROUP, EC_ENABLED_BIT );
                elnk.type        = DCS;
                elnk.has_streams = false;
                elinks.push_back( elnk );
              }
          }
        if( (enable_bits & IC_ENABLED) != 0 )
          {
            // IC DMA index setting, stored as path 6
            elnk.dma_index = dmaIndex( TOHOST_MINI_GROUP, 6 );
            // Want e-links assigned to a certain DMA index?
            if( dma_index == -1 ||
                (dma_index >= 0 && elnk.dma_index == dma_index) )
              {
                elnk.id          = chan_offset + _icToHostIndex;
                elnk.encoding    = mode( TOHOST_MINI_GROUP, IC_ENABLED_BIT );
                elnk.type        = IC;
                elnk.has_streams = false;
                elinks.push_back( elnk );
              }
          }
        if( (enable_bits & AUX_ENABLED) != 0 )
          {
            // AUX DMA index setting, stored as path 5
            elnk.dma_index = dmaIndex( TOHOST_MINI_GROUP, 5 );
            // Want e-links assigned to a certain DMA index?
            if( dma_index == -1 ||
                (dma_index >= 0 && elnk.dma_index == dma_index) )
              {
                elnk.id          = chan_offset + _auxToHostIndex;
                elnk.encoding    = mode( TOHOST_MINI_GROUP, AUX_ENABLED_BIT );
                elnk.type        = DCS;
                elnk.has_streams = false;
                elinks.push_back( elnk );
              }
          }
      }

    return elinks;
  }

  // --------------------------------------------------------------------------

  std::vector<elink_descr_t> elinksEnabledFromHost( uint32_t chan,
                                                    bool exclude_ttc = true )
  {
    // By default does not include TTC-type e-links
    std::vector<elink_descr_t> elinks;
    elink_descr_t elnk;
    uint32_t chan_offset = (chan << 6);

    n_egroups_epaths_pair_t groups_paths = numberOfEgroupsEpathsFromHost( _linkMode );
    uint32_t grp_cnt = groups_paths.first;
    uint32_t path_cnt = groups_paths.second;

    for( uint32_t grp=0; grp<grp_cnt; ++grp )
      for( uint32_t path=0; path<path_cnt; ++path )
        if( _enablesFromHost[grp] & (1<<path) )
          {
            elnk.id       = chan_offset + grp*path_cnt + path;
            elnk.encoding = mode( grp, path, false );
            if( elnk.encoding == ENC_HDLC )
              elnk.type = DCS;
            else if( elnk.encoding == ENC_TTC )
              elnk.type = TTC;
            else
              elnk.type = DAQ;

            if( !(elnk.type == TTC && exclude_ttc) )
              elinks.push_back( elnk );
          }

    // IC/EC/AUX/TTC2Host e-links
    uint32_t enable_bits = _enablesFromHost[FROMHOST_MINI_GROUP];
    if( (enable_bits & EC_ENABLED) != 0 )
      {
        elnk.id       = chan_offset + _ecFromHostIndex;
        elnk.encoding = mode( FROMHOST_MINI_GROUP, EC_ENABLED_BIT, false );
        elnk.type     = DCS;
        elinks.push_back( elnk );
      }
    if( (enable_bits & IC_ENABLED) != 0 )
      {
        elnk.id       = chan_offset + _icFromHostIndex;
        elnk.encoding = mode( FROMHOST_MINI_GROUP, IC_ENABLED_BIT, false );
        elnk.type     = IC;
        elinks.push_back( elnk );
      }
    if( (enable_bits & AUX_ENABLED) != 0 )
      {
        elnk.id       = chan_offset + _auxFromHostIndex;
        elnk.encoding = mode( FROMHOST_MINI_GROUP, AUX_ENABLED_BIT, false );
        elnk.type     = DCS;
        elinks.push_back( elnk );
      }

    return elinks;
  }

  // --------------------------------------------------------------------------

  static uint64_t epathBitsWord( uint32_t enables, uint32_t width )
  {
    // Compile a 64-bit word in which each byte indicates the number
    // of bits per E-path/FIFO, in order of (up to 8) E-paths,
    // zero meaning E-path/FIFO is not in use
    // (utility function used e.g. for emulator data generation)
    uint64_t nbits_word = 0;
    for( int i=0; i<8; ++i )
      if( enables & (1<<i) )
        nbits_word |= ((uint64_t) width) << (i*8);
    return nbits_word;
  }

  // --------------------------------------------------------------------------

  static int widthCodeToBits( uint32_t width_code )
  {
    if( width_code == 0 )      return 2;
    else if( width_code == 1 ) return 4;
    else if( width_code == 2 ) return 8;
    else if( width_code == 3 ) return 16;
    else if( width_code == 4 ) return 32;
    return 0;
  }

  // --------------------------------------------------------------------------

  static uint16_t elinkNumber( uint32_t channel, uint32_t egroup,
                               uint32_t epath, bool lpgbt_type )
  {
    int egroup_shift = (lpgbt_type) ? 2 : 3;
    return (channel << 6) + (egroup << egroup_shift) + epath;
  }

  static uint16_t channelNumber( uint32_t elink )
  {
    return (elink >> 6);
  }

  static uint16_t egroupNumber( uint32_t elink, bool lpgbt_type )
  {
    int egroup_shift = (lpgbt_type) ? 2 : 3;
    return (elink & 0x3f) >> egroup_shift;
  }

  static uint16_t epathNumber( uint32_t elink, bool lpgbt_type )
  {
    int egroup_shift = (lpgbt_type) ? 2 : 3;
    return egroup_shift == 2 ? elink & 0x03 : elink & 0x07;
  }

  // --------------------------------------------------------------------------
};
} // namespace flxcard
#endif // LINKCONFIG_H
