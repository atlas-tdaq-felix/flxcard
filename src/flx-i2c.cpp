/*******************************************************************/
/*                                                                 */
/* This is the C++ source code of the flx-i2c application          */
/*                                                                 */
/* Author: Markus Joos, CERN                                       */
/*                                                                 */
/* From Nov 2020: maintenance by H.Boterenbrood, Nikhef            */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>
#include <iostream>

#if DEBUG_LEVEL > 0
#include "DFDebug/DFDebug.h"
#endif
#include "flxcard/FlxCard.h"
#include "flxcard/FlxException.h"
#include "felixtag.h"

// Globals
FlxCard flxCard;

enum i2c_command
  {
    I2C_UNKNOWN,
    I2C_READ_BYTE,
    I2C_WRITE_BYTE,
    I2C_READ_WORD,
    I2C_WRITE_WORD,
    I2C_LIST
  };

// Externals (see FlxCard.cpp)
extern i2c_device_t i2c_devices_FLX_709[];
extern i2c_device_t i2c_devices_FLX_710[];
extern i2c_device_t i2c_devices_FLX_711[];
extern i2c_device_t i2c_devices_FLX_128[];
extern i2c_device_t i2c_devices_FLX_182[];
extern i2c_device_t i2c_devices_FLX_155[];

static void flx_i2c_list(u_long card_type);
static void list_all_devices();
static void display_help();

// ----------------------------------------------------------------------------

int main( int argc, char **argv )
{
  int  opt;
  int  card_number;
  int  device_number  = 0;
  bool locks_override = false;
  bool flush          = false;

  if( argc < 2 )
    {
      display_help();
      return 1;
    }

#if DEBUG_LEVEL > 0
  while( (opt = getopt(argc, argv, "c:D:EfhV")) != -1 )
#else
  while( (opt = getopt(argc, argv, "c:EfhV")) != -1 )
#endif // DEBUG_LEVEL
    {
      switch( opt )
        {
        case 'c':
          card_number = atoi( optarg );
          try {
            // Map card number to a device number
            device_number = FlxCard::card_to_device_number( card_number );
          }
          catch(FlxException &ex) {
            std::cout << "### Exception thrown: " << ex.what() << std::endl;
            return 1;
          }
          if( device_number < 0 )
            {
              printf( "### -c: Card number %d not in range [0,%u]\n",
                      card_number, FlxCard::number_of_cards()-1 );
              return 1;
            }
          break;

#if DEBUG_LEVEL > 0
        case 'D':
          {
            int debuglevel = atoi(optarg);
            DF::GlobalDebugSettings::setup(debuglevel, DFDB_FELIXCARD);
          }
          break;
#endif // DEBUG_LEVEL

        case 'E':
          locks_override = true;
          break;

        case 'f':
          flush = true;
          break;

        case 'h':
          display_help();
          return 0;

        case 'V':
          printf( "Version %s\n", FELIX_TAG );
          return 0;

        default:
          printf( "Usage: flx-i2c COMMAND [OPTIONS]\n"
                  "Try flx-i2c -h for more information.\n" );
          return 1;
        }
    }

  int command = I2C_UNKNOWN;
  if( optind < argc )
    {
      const char *cmd = argv[optind];
      if( strcmp(cmd, "read") == 0 || strcmp(cmd, "r") == 0 )
        command = I2C_READ_BYTE;
      else if( strcmp(cmd, "write") == 0 || strcmp(cmd, "w") == 0 )
        command = I2C_WRITE_BYTE;
      else if( strcmp(cmd, "Read") == 0 || strcmp(cmd, "R") == 0 )
        command = I2C_READ_WORD;
      else if( strcmp(cmd, "Write") == 0 || strcmp(cmd, "W") == 0 )
        command = I2C_WRITE_WORD;
      else if( strcasecmp(cmd, "list") == 0 || strcasecmp(cmd, "l") == 0 )
        command = I2C_LIST;
      else
        {
          printf( "### Unrecognized command '%s'\n" , cmd );
          printf( "Usage: flx-i2c COMMAND [OPTIONS]\n"
                  "Try flx-i2c -h for more information.\n" );
          return 1;
        }
      ++optind;
    }
  else if( !flush )
    {
      printf( "### No command given\n" );
      printf( "Usage: flx-i2c COMMAND [OPTIONS]\n"
              "Try flx-i2c -h for more information.\n" );
      return 1;
    }

  try {
    uint32_t locks = ((locks_override || command == I2C_LIST) ? LOCK_NONE : LOCK_I2C);
    bool ignore_version = (command == I2C_LIST ? true : false);

    // In case of lock issues, retry opening the device every second
    // until successful, for a configurable number of times
    int try_open_max = 5, try_open_count = 0;
    while( true )
      {
        try {
          flxCard.card_open( device_number, locks, false, ignore_version );
        }
        catch( FlxException &ex ) {
          if( ex.errorId() != FlxException::LOCK_VIOLATION ||
              try_open_count >= try_open_max )
            // Not a lock issue or persistent lock issue: rethrow exception
            throw ex;
          ++try_open_count;
          printf( "Waiting for locks, retrying... (%d)\n", try_open_count );
          sleep( 1 );
          continue; // Retry card_open()
        }
        break; // Card succesfully opened
      }
  }
  catch( FlxException &ex ) {
    std::cout << "### ERROR open: " << ex.what() << std:: endl;
    return 1;
  }

  u_long card_type = flxCard.card_type();
  if( card_type != 712 &&
      card_type != 711 &&
      card_type != 710 &&
      card_type != 709 &&
      card_type != 128 &&
      card_type != 182 &&
      card_type != 155 )
    {
      printf( "### Unknown card type: %ld\n", card_type );
      return 1;
    }

  try {
    // Flush I2C read FIFO before operation?
    if( flush )
      {
        printf( "Flushed " );
        u_int count;
        bool flushed = flxCard.i2c_flush( &count );
        printf( "%u I2C bytes", count );
        if( !flushed )
          printf( " (still not EMPTY!)" );
        printf( "\n" );
      }

    switch( command )
      {
      case I2C_READ_BYTE:
      case I2C_READ_WORD:
        if( argc == optind + 2 )
          {
            u_char reg_addr;
            u_char byte[2];
            int    nbytes;
            u_int  val;

            nbytes = (command == I2C_READ_BYTE ? 1 : 2);

            sscanf(argv[optind+1], "%x", &val);
            reg_addr = val;

            printf("Reading register 0x%02x of device %s\n",
                   reg_addr, argv[optind]);
            flxCard.i2c_read( argv[optind], reg_addr, byte, nbytes );

            if( command == I2C_READ_BYTE )
              val = (u_int) byte[0];
            else
              // 2-byte word: little-endian
              val = (u_int) byte[0] | (((u_int) byte[1]) << 8);
              // 2-byte word: big-endian
              //val = (u_int) byte[1] | (((u_int) byte[0]) << 8);

            printf("Register %s = 0x", argv[optind+1]);
            if( command == I2C_READ_BYTE )
              printf("%02X\n", val);
            else
              printf("%04X\n", val);
          }
        else
          {
            printf( "### Number of 'read' parameters incorrect\n" );
            return 1;
          }
        break;

      case I2C_WRITE_BYTE:
      case I2C_WRITE_WORD:
        if( argc == optind + 3 )
          {
            u_char reg_addr, reg_data[2];
            int    nbytes;
            u_int  val;

            nbytes = (command == I2C_WRITE_BYTE ? 1 : 2);

            sscanf(argv[optind+1], "%x", &val);
            reg_addr = (u_char) val;

            sscanf(argv[optind+2], "%x", &val);
            reg_data[0] = (u_char) (val & 0xFF);
            reg_data[1] = (u_char) ((val>>8) & 0xFF);

            printf("Writing 0x" );
            if( command == I2C_WRITE_BYTE )
              printf("%02X", val);
            else
              printf("%04X", val);
            printf(" to register 0x%02x of device %s\n", reg_addr, argv[optind]);

            flxCard.i2c_write( argv[optind], reg_addr, reg_data, nbytes );
          }
        else
          {
            printf( "### Number of 'write' parameters incorrect\n" );
            return 1;
          }
        break;

      case I2C_LIST:
        if( argc == optind )
          {
            flx_i2c_list( card_type );
          }
        else
          {
            if( argc == optind + 1 )
              {
                if( (strcasecmp(argv[optind], "all")) == 0 )
                  list_all_devices();
                else
                  printf( "### 'List' command incorrect\n" );
              }
            else
              {
                printf( "### Number of 'list' parameters incorrect\n" );
              }
          }
        break;

      default:
        if( !flush )
          printf( "Usage: flx-i2c COMMAND [OPTIONS]\n"
                  "Try flx-i2c -h for more information.\n" );
        break;
      }
  }
  catch( FlxException &ex ) {
    std::cout << "### ERROR: " << ex.what() << std:: endl;
    return 1;
  }

  try {
    flxCard.card_close();
  }
  catch( FlxException &ex ) {
    std::cout << "### ERROR close: " << ex.what() << std:: endl;
    return 1;
  }

  return 0;
}

// ----------------------------------------------------------------------------

static void flx_i2c_list(u_long card_type)
{
  i2c_device_t *devices;

  if(card_type == 712)   //711 and 712 are identical wrt I2C
    {
      devices = i2c_devices_FLX_711;
      printf("Card type: FLX-712\n");
      printf("Switch I2C address: 0x%x\n", I2C_ADDR_SWITCH1_FLX_711);
    }
  else if(card_type == 711)
    {
      devices = i2c_devices_FLX_711;
      printf("Card type: FLX-711\n");
      printf("Switch I2C address: 0x%x\n", I2C_ADDR_SWITCH1_FLX_711);
    }
  else if(card_type == 710)
    {
      devices = i2c_devices_FLX_710;
      printf("Card type: FLX-710\n");
      printf("Switch I2C address: 0x%x\n", I2C_ADDR_SWITCH1_FLX_710);
    }
  else if(card_type == 709)
    {
      devices = i2c_devices_FLX_709;
      printf("Card type: FLX-709\n");
      printf("Primary switch I2C address: 0x%x\n", I2C_ADDR_SWITCH1_FLX_709);
      printf("Secondary (cascaded) switch I2C address: 0x%x\n", I2C_ADDR_SWITCH2_FLX_709);
    }
  else if(card_type == 128)
    {
      devices = i2c_devices_FLX_128;
      printf("Card type: FLX-128\n");
      printf("Primary switch I2C address: 0x%x\n", I2C_ADDR_SWITCH1_FLX_128);
      printf("2nd switch I2C address: 0x%x\n", I2C_ADDR_SWITCH2_FLX_128);
      printf("3rd switch I2C address: 0x%x\n", I2C_ADDR_SWITCH3_FLX_128);
    }
  else if(card_type == 182)
    {
      devices = i2c_devices_FLX_182;
      printf("Card type: FLX-182\n");
      printf("Switch I2C address: 0x%x\n", I2C_ADDR_SWITCH1_FLX_182);
    }
  else if(card_type == 155)
    {
      devices = i2c_devices_FLX_155;
      printf("Card type: FLX-155\n");
      printf("Switch I2C address: 0x%x\n", I2C_ADDR_SWITCH1_FLX_155);
    }
  else
    {
      printf("### Card type unknown\n");
      exit(-1);
    }

  printf("=> List of I2C devices:\n");
  printf("Device              Type                   Switch port(s)    Address\n");
  printf("======================================================================\n");

  for(; devices->name != NULL; devices++)
    {
      int switch_val, portnr1, portnr2, portnr3;

      switch_val = devices->i2c_switch_val[0];
      for( portnr1=0; portnr1<8; ++portnr1 )
        if( (switch_val & (1<<portnr1)) != 0 )
          break;

      switch_val = devices->i2c_switch_val[1];
      for( portnr2=0; portnr2<8; ++portnr2 )
        if( (switch_val & (1<<portnr2)) != 0 )
          break;

      switch_val = devices->i2c_switch_val[2];
      for( portnr3=0; portnr3<8; ++portnr3 )
        if( (switch_val & (1<<portnr3)) != 0 )
          break;

      printf( "%-20s%-25s", devices->name, devices->description );

      if( portnr1 != 8 )
        printf( "%d", portnr1 );
      else
        printf( "-" );

      if( portnr2 != 8 )
        printf( ":%d", portnr2 );
      else
        printf( ":-" );

      if( portnr3 != 8 )
        printf( ":%-8d", portnr1 );
      else
        printf( ":-       " );

      printf( "     0x%x\n", devices->address );
    }
  printf("\n");
}

// ----------------------------------------------------------------------------

static void list_all_devices()
{
  flx_i2c_list(712);
  flx_i2c_list(711);
  flx_i2c_list(710);
  flx_i2c_list(709);
  flx_i2c_list(128);
  flx_i2c_list(182);
  flx_i2c_list(155);
}

// ----------------------------------------------------------------------------

static void display_help()
{
  printf("Usage: flx-i2c [OPTIONS] COMMAND\n");
  printf("Read or write data (1 or 2 bytes) from/to an I2C device on a FELIX card,\n");
  printf("with 'device' selected by name or I2C address.\n");
  printf("Options:\n");
  printf("  -c NUMBER          Use card indicated by NUMBER, default: 0\n");
#if DEBUG_LEVEL > 0
  printf("  -D level           Configure debug output at API level\n");
  printf("                     0=disabled, 5, 10, 20 progressively more verbose output, default: 0.\n");
#endif // DEBUG_LEVEL
  printf("  -E                 Execute the command even if resources are locked\n");
  printf("  -f                 Flush I2C read FIFO, before read or write\n");
  printf("  -h                 Display help\n");
  printf("  -V                 Display the version number\n");
  printf("Commands:\n");
  printf("  l[ist]  [all]                                       List available devices from the card used\n");
  printf("                                                      or all (by adding 'all') card models.\n");
  printf("  r[ead]  DEVICE_NAME                REG_ADDR         Read a byte from a known I2C device.\n");
  printf("                                                      (see: flx-i2c list)\n");
  printf("  r[ead]  PORT1:[PORT2:]DEV_ADDRESS  REG_ADDR         Read a byte from a known or unknown I2C\n");
  printf("                                                      device providing port and address.\n");
  printf("  w[rite] DEVICE_NAME                REG_ADDR  DATA   Write a byte to a known I2C device.\n");
  printf("                                                      (see: flx-i2c list)\n");
  printf("  w[rite] PORT1:[PORT2:]DEV_ADDRESS  REG_ADDR  DATA   Write a byte to a known or unknown I2C\n");
  printf("                                                      device providing port and address.\n");
  printf(" Use capital 'R' or 'W' (instead of 'r' and 'w') to read or write a 2-byte register.\n");
}

// ----------------------------------------------------------------------------
