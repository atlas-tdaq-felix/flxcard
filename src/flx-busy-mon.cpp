#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <thread>
#include <csignal>
#include <mutex>
using namespace std;

#include "arg.h"
#include "flxcard/FlxCard.h"
#include "flxcard/FlxException.h"
#include "felixtag.h"

void display_help();

// Globals
FlxCard  flx;
int      busy_intrpt = 6;
bool     running;
uint64_t intrpt_count = 0;
uint64_t intrpt_count_nochange = 0;
uint64_t busy_on_total = 0; // In ns
uint64_t busy_on, busy_off; // In ns
bool     busy_is_on;
bool     monitor_latched_busy = false;
int      busy_exit_percentage = 0;
mutex    mtx; // Mutex to control access to the global variables

// ----------------------------------------------------------------------------

void busy_status_display()
{
  uint64_t b_start = busy_on_total;
  uint64_t b_end;
  uint64_t b_on;
  bool     b_is_on;
  double   percentage;
  int      secs = 0;

  // Initialize t_start asap
  struct timespec ts;
  uint64_t t_start, t_end;
  clock_gettime( CLOCK_REALTIME, &ts );
  t_start = ts.tv_sec*1000000000LL + ts.tv_nsec;

  // For monitoring latched BUSYs
  std::ostringstream oss;
  bool latched = false;
  flxcard_bar2_regs_t *bar2 = (flxcard_bar2_regs_t *) flx.bar2Address();
  uint64_t chans = bar2->NUM_OF_CHANNELS;
  uint64_t endpoints = bar2->NUMBER_OF_PCIE_ENDPOINTS;
  if( chans > 24 ) chans = 24;

  cout << fixed << setprecision(3);
  cout << " T  #Interrupts   BUSY%/sec     Total BUSY time" << endl;
  cout << "--- ------------- ------------- ----------------" << endl;

  while( running )
    {
      sleep( 1 ); // Sleep 1 second
      clock_gettime( CLOCK_REALTIME, &ts );
      if( !running ) break;

      // Take snapshot of main (interrupt) thread status
      mtx.lock();
      b_end   = busy_on_total;
      b_is_on = busy_is_on;
      b_on    = busy_on;
      mtx.unlock();

      // Monitor latched BUSYs ?
      if( monitor_latched_busy )
        {
          bool dma_latched, fifo_latched;
          oss.clear(); oss.str( "" ); // Reset ostringstream
          oss << " (Latched:";
          latched = false;

          dma_latched = bar2->DMA_BUSY_STATUS.TOHOST_BUSY_LATCHED;
          if( dma_latched )
            {
              // Clear
              uint64_t c = bar2->DMA_BUSY_STATUS.ENABLE;
              bar2->DMA_BUSY_STATUS.ENABLE = c;
              oss << " DMA";
            }

          fifo_latched =
            bar2->BUSY_MAIN_OUTPUT_FIFO_STATUS.HIGH_THRESH_CROSSED_LATCHED;
          if( fifo_latched )
            {
              // Clear
              uint64_t c =
                bar2->BUSY_MAIN_OUTPUT_FIFO_STATUS.HIGH_THRESH_CROSSED_LATCHED;
              bar2->BUSY_MAIN_OUTPUT_FIFO_STATUS.HIGH_THRESH_CROSSED_LATCHED = c;
              oss << " FIFO";
            }

          latched = dma_latched || fifo_latched;

          // Displays for which links one or more E-link latched BUSYs are present
          uint64_t elink_latched;
          int devnr, devlnk;
          bool first = true;
          for( uint64_t lnk=0; lnk<chans*endpoints; ++lnk )
            {
              elink_latched = bar2->TTC_BUSY_ACCEPTED_G[lnk].TTC_BUSY_ACCEPTED;
              if( elink_latched != 0 )
                {
                  devnr = lnk/chans;
                  devlnk = lnk - chans*(lnk/chans);
                  if( first )
                    {
                      oss << " Elnk";
                      first = false;
                    }
                  oss << " " << devnr << "-" << devlnk;
                  latched = true;
                }
            }
          oss << ")";
          if( latched )
            bar2->TTC_BUSY_CLEAR = 1;
        }

      t_end = ts.tv_sec*1000000000LL + ts.tv_nsec;
      if( b_is_on )
        // Busy is active, so its duration is not included yet in busy_on_total!
        b_end += t_end - b_on;

      // Busy percentage per second, corrected for this loop's 'second' duration
      percentage = (((b_end - b_start)/10000000.0) /
                    ((t_end-t_start)/1000000000.0));

      ++secs;
      cout << setw(3) << secs << ": cnt=" << setw(4) << intrpt_count;
      if( intrpt_count_nochange > 0 )
        cout << " (" << intrpt_count_nochange << ")";
      else
        cout << "    ";
      uint64_t s = b_end/1000000000LL; // Seconds
      cout << " BUSY " << setw(7) << percentage << "%"
           << " (Total " << s << "."
           << (b_end - s*1000000000LL)/1000000L << "s "   // Milliseconds
           << (b_end - s*1000000000LL)%1000000L << "ns) "; // Nanoseconds
      t_start = t_end;
      b_start = b_end;

      if( monitor_latched_busy && latched )
        cout << oss.str();

      cout << endl;

      if( busy_exit_percentage >= 1 &&
          (int) percentage >= busy_exit_percentage )
        {
          if( (int) percentage > busy_exit_percentage )
            cout << "-> Exceeded";
          else
            cout << "-> Reached";
          cout << " BUSY percentage " << busy_exit_percentage
               << "%..." << endl;
          running = false;
          // Cancel main thread's irq_wait()
          flx.irq_cancel( busy_intrpt );
        }
    }
}

// ----------------------------------------------------------------------------

void sigint_handler( int signal )
{
  // Ctrl-C handler
  running = false;
  // NB: no need to call irq_cancel! Driver cancels irq_wait() on a signal
  // (it even causes a 'false' interrupt for the next process/user,
  //  unless an additional call to irq_clear() is added)
  //flx.irq_cancel( busy_intrpt );
  //flx.irq_clear( busy_intrpt );
}

// ----------------------------------------------------------------------------

int main( int argc, char **argv )
{
  int device_number = 0;

  int opt;
  while( (opt = getopt(argc, argv, "B:d:hi:LV")) != -1 )
    {
      switch( opt )
        {
        case 'B':
          if( sscanf( optarg, "%d", &busy_exit_percentage ) != 1 )
            arg_error( 'B' );
          if( busy_exit_percentage < 1 || busy_exit_percentage > 100 )
            arg_range( 'B', 1, 100 );
          break;

        case 'd':
          if( sscanf( optarg, "%d", &device_number ) != 1 )
            arg_error( 'd' );
          break;

        case 'h':
          display_help();
          return 0;

        case 'i':
          if( sscanf( optarg, "%d", &busy_intrpt ) != 1 )
            arg_error( 'i' );
          if( busy_intrpt < 0 || busy_intrpt > 7 )
            arg_range( 'i', 0, 7 );
          break;

        case 'V':
          printf( "Version %s\n", FELIX_TAG );
          return 0;

        case 'L':
          monitor_latched_busy = true;
          break;

        default:
          fprintf( stderr, "Usage: flx-busy-mon [OPTIONS]\n"
                   "Try flx-busy-mon -h for more information.\n" );
          return 1;
        }
    }

  // Install Ctrl-C handler
  std::signal( SIGINT, sigint_handler );

  try {
    flx.card_open( device_number, LOCK_NONE, false, true );
    printf( "Firmware: %s\n", flx.firmware_string().c_str() );
    flxcard_bar2_regs_t *bar2 = (flxcard_bar2_regs_t *) flx.bar2Address();

    printf( "Monitoring BUSY interrupt...\n" );
    running = true;

    // Start the BUSY status display thread
    std::thread *t = new std::thread( busy_status_display );

    struct timespec ts;
    clock_gettime( CLOCK_REALTIME, &ts );
    if( bar2->TTC_DEC_CTRL.BUSY_OUTPUT_STATUS )
      {
        busy_on = ts.tv_sec*1000000000LL + ts.tv_nsec;
        busy_off = busy_on;
        busy_is_on = true;
      }
    else
      {
        busy_off = ts.tv_sec*1000000000LL + ts.tv_nsec;
        busy_on = busy_off;
        busy_is_on = false;
      }

    flx.irq_enable( busy_intrpt );
    // The BUSY monitoring loop
    while( running )
      {
        flx.irq_wait( busy_intrpt );
        if( !running ) break; // Interrupted
        clock_gettime( CLOCK_REALTIME, &ts );

        mtx.lock();
        if( bar2->TTC_DEC_CTRL.BUSY_OUTPUT_STATUS )
          {
            // BUSY changed to active
            if( !busy_is_on )
              {
                busy_is_on = true;
                busy_on = ts.tv_sec*1000000000LL + ts.tv_nsec;
              }
            else
              {
                // No BUSY transition (missed one or more?)
                ++intrpt_count_nochange;
              }
          }
        else
          {
            // BUSY changed to inactive
            if( busy_is_on )
              {
                busy_is_on = false;
                busy_off = ts.tv_sec*1000000000LL + ts.tv_nsec;
                busy_on_total += (busy_off - busy_on);
              }
            else
              {
                // No BUSY transition (missed one or more?)
                ++intrpt_count_nochange;
              }
          }
        mtx.unlock();

        // Increment interrupt counter
        ++intrpt_count;
      }
    flx.irq_disable( busy_intrpt );

    t->join();
    delete t;

    flx.card_close();
  }
  catch( FlxException &ex ) {
    std::cout << "### ERROR: " << ex.what() << std::endl;
    return 1;
  }

  cout << " Exiting..." << endl;
  return 0;
}

// ----------------------------------------------------------------------------

void display_help()
{
  printf( "Usage: flx-busy-mon [OPTIONS]\n" );
  printf( "Displays percentage of BUSY (per second), total BUSY time,\n" );
  printf( "as well as number of BUSY interrupts (i.e. changes of the BUSY signal) detected\n"
          "(in brackets the number of interrupts not matching a detected"
          " BUSY change, if any)\n"
          "in a single line of output, once per second\n\n" );
  printf( "Options:\n" );
  printf( "  -d NUMBER      Use device indicated by NUMBER. Default: 0.\n" );
  printf( "  -h             Display help.\n" );
  printf( "  -V             Display the version number.\n" );
  printf( "  -i NUMBER      Interrupt number [0..7] of BUSY signal "
          "(default: 6).\n" );
  printf( "  -L             Monitor and report latched BUSY sources (and clear when set).\n" );
  printf( "  -B <perc>      Exit when BUSY percentage reaches or exceeds <perc> percent.\n" );
}

// ----------------------------------------------------------------------------
