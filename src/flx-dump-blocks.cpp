/*******************************************************************/
/*                                                                 */
/* This is the C++ source code of the flx-dump-blocks application  */
/*                                                                 */
/* Author: Markus Joos, CERN                                       */
/*                                                                 */
/**C 2015 Ecosoft - Made from at least 80% recycled source code*****/


#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <cassert>
#include <unistd.h>
#include <stdlib.h>
#include <iostream>

#if DEBUG_LEVEL > 0
#include "DFDebug/DFDebug.h"
#endif // DEBUG_LEVEL
#include "cmem_rcc/cmem_rcc.h"
#include "flxcard/FlxCard.h"
#include "flxcard/FlxException.h"
#include "felixtag.h"

#define DMA_ID              0
#define BUFSIZE             1024
#define APPLICATION_NAME    "flx-dump-blocks"

#ifndef BF_GBT_EMU_ENA_TOHOST
#define BF_GBT_EMU_ENA_TOHOST BF_FE_EMU_ENA_EMU_TOHOST
#endif
#ifndef BF_GBT_EMU_ENA_TOFRONTEND
#define BF_GBT_EMU_ENA_TOFRONTEND BF_FE_EMU_ENA_EMU_TOFRONTEND
#endif


//Globals
FlxCard flxCard;


void display_help()
{
  printf("Usage: %s [OPTIONS]\n", APPLICATION_NAME);
  printf("Reads raw data from a FLX and writes them into a file.\n\n");
  printf("Options:\n");
  printf("  -d NUMBER      Use card indicated by NUMBER. Default: 0.\n");
#if DEBUG_LEVEL > 0
  printf("  -D level       Configure debug output at API level. 0=disabled, 5, 10, 20 progressively more verbose output. Default: 0.\n");
#endif // DEBUG_LEVEL
  printf("  -n NUMBER      Dump NUMBER blocks. Default: 100.\n");
  printf("  -f FILENAME    Dump blocks into the given file. Default: 'out.blocks'.\n");
  printf("  -F             Enable To-Frontend GBT emulator\n");
  printf("  -E             Execute the command even if resources are locked\n");
  printf("  -h             Display help.\n");
  printf("  -V             Display the version number\n");
}


/*****************************/
int main(int argc, char **argv)
/*****************************/
{
  int iknowwhatido = 0, loop, ret, bsize, handle, opt, device_number = 0, emu_to_frontend = 0;
  u_int nblocks = 100;
  u_long vaddr, paddr, opt_emu_ena_to_host, opt_emu_ena_to_frontend;
  const char *filename = "out.blocks";

#if DEBUG_LEVEL > 0
  while((opt = getopt(argc, argv, "hd:f:n:D:FVE")) != -1)
#else
  while((opt = getopt(argc, argv, "hd:f:n:FVE")) != -1)
#endif // DEBUG_LEVEL
  {
    switch (opt)
    {
      case 'd':
	device_number = atoi(optarg);
	break;

      case 'f':
	filename = optarg;
	break;

#if DEBUG_LEVEL > 0
      case 'D':
          {
            int debuglevel = atoi(optarg);
            DF::GlobalDebugSettings::setup(debuglevel, DFDB_FELIXCARD);
          }
	break;
#endif // DEBUG_LEVEL

      case 'n':
	nblocks = atoi(optarg);
	break;

      case 'h':
	display_help();
	exit(0);

      case 'F':
        emu_to_frontend = 1;
        break;

      case 'V':
        printf("This is version %s of %s\n", FELIX_TAG, APPLICATION_NAME);
	exit(0);

      case 'E':
	iknowwhatido = 1;
	break;

      default:
	fprintf(stderr, "Usage: %s [OPTIONS]\nTry %s -h for more information.\n", APPLICATION_NAME, APPLICATION_NAME);
	exit(-1);
    }
  }

  try
  {
    if (iknowwhatido)
      flxCard.card_open(device_number, LOCK_NONE);
    else
      flxCard.card_open(device_number, LOCK_DMA0); //keep LOCK_DMA0 in sync with DMA_ID

    // save current state
    opt_emu_ena_to_host     = flxCard.cfg_get_option(BF_GBT_EMU_ENA_TOHOST);
    opt_emu_ena_to_frontend = flxCard.cfg_get_option(BF_GBT_EMU_ENA_TOFRONTEND);

    flxCard.cfg_set_option(BF_GBT_EMU_ENA_TOHOST, 1);

    if(emu_to_frontend)
    {
      flxCard.cfg_set_option(BF_GBT_EMU_ENA_TOFRONTEND, 1);
    }

    for(loop = 0; loop < 8; loop++)
      flxCard.dma_wait(loop);

    //flxCard.dma_reset();
    //flxCard.soft_reset();
    //flxCard.dma_fifo_flush();  MJ: Method disabled (requsted by Frans)

    ret = CMEM_Open();
    bsize = BUFSIZE * nblocks;
    if (!ret)
      ret = CMEM_SegmentAllocate(bsize, (char *)"FlxThroughput", &handle);

    if (!ret)
      ret = CMEM_SegmentPhysicalAddress(handle, &paddr);

    if (!ret)
      ret = CMEM_SegmentVirtualAddress(handle, &vaddr);

    if (ret)
    {
      rcc_error_print(stdout, ret);
      exit(-1);
    }

    FILE *f = fopen(filename, "w");
    if(!f)
    {
      printf("Could not open file for writing\n");
      exit(-1);
    }

    flxCard.dma_to_host(DMA_ID, paddr, BUFSIZE * nblocks, 0);
    flxCard.dma_wait(DMA_ID);

    if(nblocks != fwrite((void*)(vaddr), 1024, nblocks, f))
    {
      fprintf(stderr, APPLICATION_NAME": I/O Error\n");
      exit(-1);
    }

    fclose(f);

    // reset to initial state
    flxCard.cfg_set_option(BF_GBT_EMU_ENA_TOHOST, opt_emu_ena_to_host);
    flxCard.cfg_set_option(BF_GBT_EMU_ENA_TOFRONTEND, opt_emu_ena_to_frontend);

    ret = CMEM_SegmentFree(handle);
    if (!ret)
      ret = CMEM_Close();
    if (ret)
      rcc_error_print(stdout, ret);

    flxCard.card_close();
  }
  catch(FlxException &ex)
  {
    std::cout << "ERROR. Exception thrown: " << ex.what() << std:: endl;
    exit(-1);
  }
  exit(0);
}
