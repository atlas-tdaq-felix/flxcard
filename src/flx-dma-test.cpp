/*******************************************************************/
/*                                                                 */
/* This is the C++ source code of the flx-dma-test application     */
/*                                                                 */
/* Author: Markus Joos, CERN                                       */
/*                                                                 */
/* From Nov 2020: maintenance by H.Boterenbrood, Nikhef            */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <cstring>
#include <iostream>

#include "cmem_rcc/cmem_rcc.h"
#if DEBUG_LEVEL > 0
#include "DFDebug/DFDebug.h"
#endif // DEBUG_LEVEL
#include "flxcard/FlxCard.h"
#include "flxcard/FlxException.h"
#include "felixtag.h"

#define DMASIZE 1024
#define BUFSIZE (16*DMASIZE)
#define DMA_ID  0

#ifndef BF_GBT_EMU_ENA_TOHOST
#define BF_GBT_EMU_ENA_TOHOST BF_FE_EMU_ENA_EMU_TOHOST
#endif
#ifndef BF_GBT_EMU_ENA_TOFRONTEND
#define BF_GBT_EMU_ENA_TOFRONTEND BF_FE_EMU_ENA_EMU_TOFRONTEND
#endif

// Function prototypes
void display_help( );
void dump_buffer ( u_long virt_addr, u_long size );

// ----------------------------------------------------------------------------

int main( int argc, char **argv )
{
  int opt;
  int device_number = 0;
  int repeat = 10;
#if DEBUG_LEVEL > 0
  while( (opt = getopt(argc, argv, "d:D:hr:V")) != -1 )
#else
  while( (opt = getopt(argc, argv, "d:hr:V")) != -1 )
#endif // DEBUG_LEVEL
    {
      switch (opt)
        {
        case 'd':
          device_number = atoi( optarg );
          break;

#if DEBUG_LEVEL > 0
        case 'D':
          {
            int debuglevel = atoi(optarg);
            DF::GlobalDebugSettings::setup(debuglevel, DFDB_FELIXCARD);
          }
          break;
#endif // DEBUG_LEVEL

        case 'h':
          display_help();
          return 0;

        case 'r':
          repeat = atoi( optarg );
          if( repeat < 0 )
            {
              printf( "### -r: out-of-range\n" );
              return 1;
            }
          break;

        case 'V':
          printf( "Version %s\n", FELIX_TAG );
          return 0;

        default:
          printf( "Usage: flx-dma-stat [OPTIONS]\n"
                  "Try flx-info -h for more information.\n" );
          return 1;
        }
    }

  FlxCard flx;
  int     ret;
  int     handle;
  u_long  vaddr;
  u_long  paddr;
  //u_long  opt_emu_ena_to_host;
  //u_long  opt_emu_ena_to_frontend;
  u_long  opt_tofrontend_fanout_sel;
  u_long  opt_tohost_fanout_sel;
  try {
    flx.card_open( device_number, LOCK_NONE );

    // Save current state
    //opt_emu_ena_to_host = flx.cfg_get_option( BF_GBT_EMU_ENA_TOHOST );
    //opt_emu_ena_to_frontend = flx.cfg_get_option( BF_GBT_EMU_ENA_TOFRONTEND );
    opt_tofrontend_fanout_sel =
      flx.cfg_get_option( BF_GBT_TOFRONTEND_FANOUT_SEL );
    opt_tohost_fanout_sel =
      flx.cfg_get_option( BF_GBT_TOHOST_FANOUT_SEL );

    // Just in case
    for( int i=0; i<8; ++i )
      flx.dma_stop( i );

    //flx.dma_reset();
    flx.soft_reset();
    //flx.dma_fifo_flush(); MJ: Method disabled (requested by Frans)

    flx.cfg_set_option( BF_GBT_EMU_ENA_TOFRONTEND, 0 );
    flx.cfg_set_option( BF_GBT_EMU_ENA_TOHOST, 0 ); // Disable internal emu
    flx.cfg_set_option( BF_GBT_TOFRONTEND_FANOUT_SEL, 0x000000 );
    flx.cfg_set_option( BF_GBT_TOHOST_FANOUT_SEL, 0xFFFFFF );

    u_long board_id = flx.cfg_get_option(REG_GIT_TAG);
    char git_tag[8];
    for( int i=0; i<8; ++i )
      git_tag[i] = (board_id >> (8 * i)) & 0xff;
    printf("Board ID (GIT): %s\n", git_tag);

    ret = CMEM_Open();
    if( ret == 0 )
      ret = CMEM_SegmentAllocate( BUFSIZE, (char *)"flx-dma-test", &handle );
    if( ret == 0 )
      ret = CMEM_SegmentPhysicalAddress( handle, &paddr );
    if( ret == 0 )
      ret = CMEM_SegmentVirtualAddress( handle, &vaddr );
    if( ret != 0 )
      {
        rcc_error_print( stdout, ret );
        return -1;
      }

    printf( "Allocated Memory Segment, size=0x%X (%u)\n", BUFSIZE, BUFSIZE );
    printf( "  Phys. Addr: 0x%016lx\n", paddr );
    printf( "  Virt. Addr: 0x%016lx\n", vaddr );

    printf( "\nBuffer before to-host DMA:\n" );
    memset( (void *)vaddr, 0xFF, BUFSIZE );
    dump_buffer( vaddr, DMASIZE );

    // Single-shot DMA
    flx.dma_to_host( DMA_ID, paddr, DMASIZE, 0 );
    flx.cfg_set_option( BF_GBT_EMU_ENA_TOHOST, 1 ); // Enable internal emu
    flx.dma_wait( DMA_ID );

    u_long baraddr0 = flx.bar0Address();
    flxcard_bar0_regs_t *bar0 = (flxcard_bar0_regs_t *) baraddr0;

    printf("Start Ptr:   0x%016lx\n", bar0->DMA_DESC[0].start_address);
    printf("End Ptr:     0x%016lx\n", bar0->DMA_DESC[0].end_address);
    printf("Enable:      0x%0x\n", bar0->DMA_DESC_ENABLE);
    printf("Softw Ptr:   0x%016lx\n", bar0->DMA_DESC[0].sw_pointer);
    printf("Firmw Ptr:   0x%016lx\n", bar0->DMA_DESC_STATUS[0].fw_pointer);
    printf("Descriptor done: 0x%x\n",
           (uint32_t)bar0->DMA_DESC_STATUS[0].descriptor_done);
    printf("Even Addr DMA  : 0x%x\n",
           (uint32_t)bar0->DMA_DESC_STATUS[0].even_addr_dma);
    printf("Even Addr PC   : 0x%x\n",
           (uint32_t)bar0->DMA_DESC_STATUS[0].even_addr_pc);

    printf("\nBuffer after to-host DMA:\n");
    dump_buffer( vaddr, DMASIZE );

    for( int i=0; i<repeat; ++i )
      {
        printf("\n--------------------\nDMA %d:\n", i);

        // ###The following does not work for *single-shot* DMA...
        //flx.dma_advance_ptr( DMA_ID, paddr, BUFSIZE, 256 );
        //flx.dma_wait( DMA_ID );

        flx.dma_to_host( DMA_ID, paddr+(i*DMASIZE)%BUFSIZE, DMASIZE, 0 );
        flx.dma_wait( DMA_ID );

        printf("Softw Ptr:   0x%016lx\n", bar0->DMA_DESC[0].sw_pointer);
        printf("Firmw Ptr:   0x%016lx\n", bar0->DMA_DESC_STATUS[0].fw_pointer);
        printf("Descriptor done DMA0: 0x%x\n", (uint32_t)bar0->DMA_DESC_STATUS[0].descriptor_done);
        printf("Even Addr. DMA  DMA0: 0x%x\n", (uint32_t)bar0->DMA_DESC_STATUS[0].even_addr_dma);
        printf("Even Addr. PC   DMA0: 0x%x\n", (uint32_t)bar0->DMA_DESC_STATUS[0].even_addr_pc);
        dump_buffer( vaddr + (i*DMASIZE)%BUFSIZE, DMASIZE );
        usleep( 250000 );
      }

    // Reset to initial state
    //flx.cfg_set_option( BF_GBT_EMU_ENA_TOHOST, opt_emu_ena_to_host );
    //flx.cfg_set_option( BF_GBT_EMU_ENA_TOFRONTEND, opt_emu_ena_to_frontend );
    flx.cfg_set_option( BF_GBT_EMU_ENA_TOHOST, 0 ); // Disable internal emu
    flx.cfg_set_option( BF_GBT_TOFRONTEND_FANOUT_SEL, opt_tofrontend_fanout_sel );
    flx.cfg_set_option( BF_GBT_TOHOST_FANOUT_SEL, opt_tohost_fanout_sel );

    ret = CMEM_SegmentFree( handle );
    if( ret == 0 )
      ret = CMEM_Close();
    if( ret != 0 )
      rcc_error_print( stdout, ret );

    flx.card_close();

    printf( "\n=> Done (%d DMAs)\n", repeat );
  }
  catch( FlxException &ex ) {
    std::cout << "### ERROR: " << ex.what() << std:: endl;
    return -1;
  }

  return 0;
}

// ----------------------------------------------------------------------------

void display_help()
{
  printf( "Usage: flx-dma-test [OPTIONS]\n" );
  printf( " Initiates DMA transfers (1K blocks) and displays\n" );
  printf( " the DMA memory contents on the screen in 0.5s intervals.\n\n" );
  printf( "Options:\n" );
  printf( "  -d <number>  Use card indicated by <number>. Default: 0.\n" );
#if DEBUG_LEVEL > 0
  printf( "  -D <level>   Configure debug output at API level:\n" );
  printf( "               0=disabled, 5, 10, 20 progressively more verbose. Default: 0.\n" );
#endif // DEBUG_LEVEL
  printf( "  -h           Display help.\n" );
  printf( "  -V           Display the version number\n" );
  printf( "  -r <blocks>  Number of DMA transfers to execute (default: 10).\n" );
}

// ----------------------------------------------------------------------------

void dump_buffer( u_long virt_addr, u_long size )
{
  u_char *buf = (u_char *) virt_addr;
  for( u_long i=0; i<size; ++i, ++buf )
    {
      if( (i & 0x1F) == 0 )
        printf( "\n%4lu: ", i );
      printf( "%02x ", *buf );
    }
  printf("\n");
}

// ----------------------------------------------------------------------------
