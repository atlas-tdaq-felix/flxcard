/*******************************************************************/
/*                                                                 */
/* This is the C++ source code of the flx-throughput application   */
/*                                                                 */
/* Author: Markus Joos, CERN                                       */
/*                                                                 */
/**C 2017 Ecosoft - Made from at least 80% recycled source code*****/


#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <time.h>
#include <signal.h>
#include <iostream>

#if DEBUG_LEVEL > 0
#include "DFDebug/DFDebug.h"
#endif // DEBUG_LEVEL
#include "cmem_rcc/cmem_rcc.h"
#include "rcc_error/rcc_error.h"
#include "flxcard/FlxCard.h"
#include "flxcard/FlxException.h"
#include "felixtag.h"

#define DMA_ID              0
#define BLOCKSIZE           1024
#define APPLICATION_NAME    "flx-throughput"

#ifndef BF_GBT_EMU_ENA_TOHOST
#define BF_GBT_EMU_ENA_TOHOST BF_FE_EMU_ENA_EMU_TOHOST
#endif
#ifndef BF_GBT_EMU_ENA_TOFRONTEND
#define BF_GBT_EMU_ENA_TOFRONTEND BF_FE_EMU_ENA_EMU_TOFRONTEND
#endif


//Globals
FlxCard flxCard;
int cont;


//Prototypes
void SigQuitHandler(int /*signum*/);


/*********************************/
void SigQuitHandler(int /*signum*/)
/*********************************/
{
  cont = 0;
}


/*****************/
void display_help()
/*****************/
{
  printf("Usage: %s [OPTIONS]\n", APPLICATION_NAME);
  printf("Reads raw data from a FLX and writes them into a file.\n\n");
  printf("Options:\n");
  printf("  -d NUMBER      Use card indicated by NUMBER. Default: 0.\n");
  printf("  -b NUM         Use a buffer of size NUM blocks. Default: 100 blocks of 1 KB each.\n");
  printf("  -o NUM         Terminate the program after NUM DMAs. Default: (0), run forever\n");
  printf("  -w             Use circular buffer wraparound mechanism.\n");
  printf("  -E             Execute the command even if resources are locked\n");
#if DEBUG_LEVEL > 0
  printf("  -D level       Configure debug output at API level. 0=disabled, 5, 10, 20 progressively more verbose output. Default: 0.\n");
#endif // DEBUG_LEVEL
  printf("  -h             Display help.\n");
  printf("  -V             Display the version number\n");
}


/**********/
double now()
/**********/
{
  struct timespec tp;

  clock_gettime(CLOCK_MONOTONIC, &tp);
  return tp.tv_sec + 1e-9 * tp.tv_nsec;
}


/*****************************/
int main(int argc, char **argv)
/*****************************/
{
  int iknowwhatido = 0, handle, ret, loop, device_number = 0, nblocks = 100, wraparound = 0, opt, maxops = 0;
  double timedelta = 2, t0, t1;
  u_long bsize, paddr, blocks_read = 0, opt_emu_ena_to_host, opt_emu_ena_to_frontend;
  int sa_stat;
  static struct sigaction sa;

  sigemptyset(&sa.sa_mask);
  sa.sa_flags = 0;
  sa.sa_handler = SigQuitHandler;
  sa_stat = sigaction(SIGINT, &sa, NULL);
  if (sa_stat < 0)
  {
    printf("Cannot install signal handler (error = %d)\n", sa_stat);
    exit(0);
  }

#if DEBUG_LEVEL > 0
  while((opt = getopt(argc, argv, "hd:b:wD:Vo:E")) != -1)
#else
  while((opt = getopt(argc, argv, "hd:b:wVo:E")) != -1)
#endif // DEBUG_LEVEL
  {
    switch (opt)
    {
      case 'd':
        device_number = atoi(optarg);
        break;

      case 'b':
        nblocks = atoi(optarg);
        break;

      case 'o':
        maxops = atoi(optarg);
        break;

#if DEBUG_LEVEL > 0
      case 'D':
          {
            int debuglevel = atoi(optarg);
            DF::GlobalDebugSettings::setup(debuglevel, DFDB_FELIXCARD);
          }
        break;
#endif // DEBUG_LEVEL

      case 'w':
        wraparound = 1;
        break;

      case 'h':
        display_help();
        exit(0);

      case 'V':
        printf("This is version %s of %s\n", FELIX_TAG, APPLICATION_NAME);
        exit(0);

      case 'E':
        iknowwhatido = 1;
        break;

      default:
        fprintf(stderr, "Usage: %s [OPTIONS]\nTry %s -h for more information.\n", APPLICATION_NAME, APPLICATION_NAME);
        exit(-1);
    }
  }

  try
  {
    if (iknowwhatido)
      flxCard.card_open(device_number, LOCK_NONE);
    else
      flxCard.card_open(device_number, LOCK_DMA0); //Note: if DMA_ID is changed the lock bit has to be adapted

    flxCard.cfg_set_option(BF_GBT_EMU_ENA_TOFRONTEND, 0);
    flxCard.cfg_set_option(BF_GBT_EMU_ENA_TOHOST, 0);

    for(loop = 0; loop < 8; loop++)
      flxCard.dma_stop(loop);

    //flxCard.dma_reset();
    //flxCard.soft_reset();
    //flxCard.dma_fifo_flush();  MJ: Method disabled (requsted by Frans)

    // save current state
    opt_emu_ena_to_host     = flxCard.cfg_get_option(BF_GBT_EMU_ENA_TOHOST);
    opt_emu_ena_to_frontend = flxCard.cfg_get_option(BF_GBT_EMU_ENA_TOFRONTEND);

    flxCard.cfg_set_option(BF_GBT_EMU_ENA_TOFRONTEND, 0);
    flxCard.cfg_set_option(BF_GBT_EMU_ENA_TOHOST, 1);

    ret = CMEM_Open();
    bsize = BLOCKSIZE * nblocks;
    if (!ret)
      ret = CMEM_GFPBPASegmentAllocate(bsize, (char *)"FlxThroughput", &handle);

    if (!ret)
      ret = CMEM_SegmentPhysicalAddress(handle, &paddr);

    if (ret)
    {
      rcc_error_print(stdout, ret);
      exit(-1);
    }

    t0 = now();
    t1 = now();
    cont = 1;

    if(wraparound)
    {
      printf("Terminate the program with ctrl-c\n");
      flxCard.irq_clear(1); // A function to init/reset an interrupt (not available yet)
      flxCard.irq_enable(1); // ToHost wrap-around interrupt
      flxCard.dma_to_host(DMA_ID, paddr, bsize, FLX_DMA_WRAPAROUND);
    }

    while(cont)
    {
      if(wraparound)
      {
        flxCard.irq_wait(1); // Wait for ToHost wrap-around
        flxCard.dma_advance_ptr(DMA_ID, paddr, bsize, bsize);
        blocks_read += nblocks;
      }
      else
      {
        flxCard.dma_to_host(DMA_ID, paddr, bsize, 0);
        flxCard.dma_wait(DMA_ID);
        blocks_read += nblocks;
      }

      t1 = now();
      if(t1 - t0 > timedelta)
      {
        printf("Blocks read:  %lu\n", blocks_read);
        printf("Blocks rate:  %.3f blocks/s\n", blocks_read / (t1 - t0));

        double dmaperformance =  (double)blocks_read * (double)BLOCKSIZE / ((t1 - t0) * 1024. * 1024. * 1024.);
        printf("DMA Read:     %.3f GiB/s\n", dmaperformance);

        printf("\n");
        blocks_read = 0;
        t0 = t1;
      }

      if(maxops)
      {
        maxops--;
         if(maxops == 0)
           cont = 0;
      }
    }

    printf("Loop terminated. Cleaning up....\n");
    printf("Blocks read:  %lu\n", blocks_read);
    printf("Blocks rate:  %.3f blocks/s\n", blocks_read / (t1 - t0));
    double dmaperformance =  (double)blocks_read * (double)BLOCKSIZE / ((t1 - t0) * 1024. * 1024. * 1024.);
    printf("DMA Read:     %.3f GiB/s\n", dmaperformance);




    flxCard.irq_disable(1);                                                 // ToHost wrap-around interrupt
    flxCard.cfg_set_option(BF_GBT_EMU_ENA_TOHOST, opt_emu_ena_to_host);       // reset to initial state
    flxCard.cfg_set_option(BF_GBT_EMU_ENA_TOFRONTEND, opt_emu_ena_to_frontend);   // reset to initial state

    ret = CMEM_SegmentFree(handle);
    if (!ret)
      ret = CMEM_Close();
    if (ret)
      rcc_error_print(stdout, ret);

    flxCard.card_close();
  }
  catch(FlxException &ex)
  {
    std::cout << "ERROR. Exception thrown: " << ex.what() << std:: endl;
    exit(-1);
  }

  exit(0);
}
