/*******************************************************************/
/*                                                                 */
/* This is the C++ source code of the flx-info application         */
/*                                                                 */
/* Author: Markus Joos, CERN                                       */
/* (based on code from a.rodriguez@cern.ch                         */
/*                                                                 */
/* From Nov 2020: maintenance by H.Boterenbrood, Nikhef            */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <iostream>

#if DEBUG_LEVEL > 0
#include "DFDebug/DFDebug.h"
#endif // DEBUG_LEVEL
#include "flxcard/FlxCard.h"
#include "flxcard/FlxException.h"
#include "felixtag.h"

enum info_command
  {
   INFO_UNKNOWN,
   INFO_LINK,
   INFO_ELINK,
   INFO_LINK_RESET,
   INFO_ADN2814,
   INFO_LMK03200,
   INFO_CXP,
   INFO_DDR3,
   INFO_EGROUP,
   INFO_SFP,
   INFO_SI5324,
   INFO_SI5345,
   INFO_ICS8N4Q,
   INFO_FPGA,
   INFO_POWER,
   INFO_POD,
   INFO_FREQ,
   INFO_TTC,
   INFO_FIREFLY,
   INFO_LSPCI,
   INFO_ALL,
   INFO_COUNT,
   INFO_CARDS
  };

// ----------------------------------------------------------------------------

// Function prototypes
void display_help();
void display_FW_date();
void display_FW_version();
void display_channels_alignment( bool clear = false,
                                 bool display_fec_incr = false );
void display_channels_alignment_mrod();
void display_channels_autoreset( bool clear = false );
void display_elinks_alignment( bool clear = false );
void display_adn2814();
void display_cxp();
void display_lmk03200();
void display_si5324();
void display_ics();
void display_si5345();
void display_ttc( bool clear = false );
void display_lti( bool clear = false );
void display_ddr3();
void display_sfp();
void display_egroups( int argc, char **argv, int arg_index );
void display_egroup( int channel, int endpoint = 0 );
void display_egroup_raw( int channel, int endpoint = 0 );
void display_fpga();
void display_ltc2991();
void display_ina226_tmp435( bool display_adm1x66 );
void display_pod( int request );
void display_freq();
void display_firefly();

// Globals
FlxCard flxCard;
u_long  card_type     = 0;
int     device_number = 0;
bool    verbose       = false;
flxcard_bar2_regs_t *bar2 = 0;
 
// ----------------------------------------------------------------------------

int main(int argc, char **argv)
{
  int  opt;
  int  card_number;
  int  arg_index     = 0;
  bool common_info   = false;
  bool link_info     = false;
  bool linkreset_info= false;
  bool elink_info    = false;
  bool adn_info      = false;
  bool cxp_info      = false;
  bool si5324_info   = false;
  bool lmk03200_info = false;
  bool si5345_info   = false;
  bool ics_info      = false;
  bool ddr3_info     = false;
  bool egroup_info   = false;
  bool sfp_info      = false;
  bool fpga_info     = false;
  bool power_info    = false;
  bool pod_info      = false;
  bool freq_info     = false;
  bool ttc_info      = false;
  bool firefly_info  = false;
  bool lspci_info    = false;
  uint32_t locks     = LOCK_NONE;
  int  try_open_max  = 5;
  bool clear         = false;
  bool fec_incr      = false;

#if DEBUG_LEVEL > 0
  while((opt = getopt(argc, argv, "hvc:D:s:Vz")) != -1)
#else
  while((opt = getopt(argc, argv, "hvc:Fs:Vz")) != -1)
#endif // DEBUG_LEVEL
    {
      switch( opt )
        {
        case 'c':
          card_number = atoi( optarg );
          try {
            // Map card number to a device number
            device_number = FlxCard::card_to_device_number( card_number );
          }
          catch( FlxException &ex ) {
            std::cout << "### ERROR: " << ex.what() << std::endl;
            return 1;
          }
          if( device_number < 0 )
            {
              printf( "### -c: Card number %d not in range [0,%u]\n",
                      card_number, FlxCard::number_of_cards()-1 );
              return 1;
            }
          arg_index = arg_index + 2;
          break;

        case 'z':
          clear = true;
          break;

        case 'F':
          fec_incr = true;
          break;

        case 'v':
          verbose = true;
          ++arg_index;
          break;

#if DEBUG_LEVEL > 0
        case 'D':
          {
            int debuglevel = atoi(optarg);
            DF::GlobalDebugSettings::setup(debuglevel, DFDB_FELIXCARD);
          }
          break;
#endif // DEBUG_LEVEL

        case 'h':
          display_help();
          return 0;

        case 'V':
          printf( "Version %s\n", FELIX_TAG );
          return 0;

        case 's':
          // Number of seconds to try to open the card (i.e. once per second)
          try_open_max = atoi(optarg);
          break;

        default:
          fprintf( stderr, "Usage: flx-info COMMAND [OPTIONS]\n"
                   "Try flx-info -h for more information.\n" );
          return 1;
        }
    }

  int command = INFO_UNKNOWN;
  int pod_request = POD_MONITOR_ALL;
  bool display_adm1x66 = false;
  if( optind != argc )
    {
      if( strcasecmp(argv[optind], "LINK") == 0 ||
          strcasecmp(argv[optind], "GBT")  == 0 )          command = INFO_LINK;
      else if( strcasecmp(argv[optind], "LINKRESET")== 0 ) command = INFO_LINK_RESET;
      else if( strcasecmp(argv[optind], "ELINK")    == 0 ) command = INFO_ELINK;
      else if( strcasecmp(argv[optind], "ADN2814")  == 0 ) command = INFO_ADN2814;
      else if( strcasecmp(argv[optind], "CXP")      == 0 ) command = INFO_CXP;
      else if( strcasecmp(argv[optind], "DDR3")     == 0 ) command = INFO_DDR3;
      else if( strcasecmp(argv[optind], "EGROUP")   == 0 ) command = INFO_EGROUP;
      else if( strcasecmp(argv[optind], "SFP")      == 0 ) command = INFO_SFP;
      else if( strcasecmp(argv[optind], "SI5324")   == 0 ) command = INFO_SI5324;
      else if( strcasecmp(argv[optind], "SI5345")   == 0 ) command = INFO_SI5345;
      else if( strcasecmp(argv[optind], "ICS8N4Q")  == 0 ) command = INFO_ICS8N4Q;
      else if( strcasecmp(argv[optind], "LMK03200") == 0 ) command = INFO_LMK03200;
      else if( strcasecmp(argv[optind], "ALL")      == 0 ) command = INFO_ALL;

      else if( strcasecmp(argv[optind], "LTC")      == 0 ) command = INFO_POWER; // Backwards-compatible
      else if( strcasecmp(argv[optind], "POWER")    == 0 ) command = INFO_POWER;
      else if( strcasecmp(argv[optind], "POWERSEQ") == 0 ) command = INFO_POWER;
      else if( strncasecmp(argv[optind],"POD", 3)   == 0 ) command = INFO_POD;
      else if( strcasecmp(argv[optind], "FREQ")     == 0 ) command = INFO_FREQ;
      else if( strcasecmp(argv[optind], "TTC")      == 0 ) command = INFO_TTC;
      else if( strcasecmp(argv[optind], "LTI")      == 0 ) command = INFO_TTC;
      else if( strcasecmp(argv[optind], "FPGA")     == 0 ) command = INFO_FPGA;
      else if( strcasecmp(argv[optind], "FFLY")     == 0 ) command = INFO_FIREFLY;
      else if( strcasecmp(argv[optind], "FIREFLY")  == 0 ) command = INFO_FIREFLY;
      else if( strcasecmp(argv[optind], "LSPCI")    == 0 ) command = INFO_LSPCI;
      else if( strcasecmp(argv[optind], "COUNT")    == 0 ) command = INFO_COUNT;
      else if( strcasecmp(argv[optind], "CARDS")    == 0 ) command = INFO_CARDS;

      if( command == INFO_POD )
        {
          if( strcasecmp(argv[optind], "POD") == 0 )
            pod_request = POD_MONITOR_ALL;
          else if( strcasecmp(argv[optind], "PODTEMP") == 0 )
            pod_request = POD_MONITOR_TEMP_VOLT;
          else if( strcasecmp(argv[optind], "PODVCC") == 0 )
            pod_request = POD_MONITOR_TEMP_VOLT;
          else if( strcasecmp(argv[optind], "PODPOWER") == 0 )
            pod_request = POD_MONITOR_POWER;
          else if( strcasecmp(argv[optind], "PODPOWERRX") == 0 )
            pod_request = POD_MONITOR_POWER_RX;
          else
            command = INFO_UNKNOWN;
        }

      if( command == INFO_POWER )
        display_adm1x66 = (strcasecmp(argv[optind], "POWERSEQ") == 0 );

      if( command == INFO_UNKNOWN )
        {
          fprintf( stderr, "Unrecognized command '%s'\n", argv[1] );
          fprintf( stderr, "Usage: flx-info COMMAND [OPTIONS]\n"
                   "Try flx-info -h for more information.\n" );
          return 1;
        }
    }

  switch(command)
    {
    case INFO_COUNT:
      // Just display the number of FELIX devices and return
      printf( "%d\n", FlxCard::number_of_devices() );
      return 0;

    case INFO_CARDS:
      // Just display the number of FELIX *cards* and return
      printf( "%d\n", FlxCard::number_of_cards() );
      return 0;

    case INFO_LINK:
      link_info = true;
      break;

    case INFO_LINK_RESET:
      linkreset_info = true;
      break;

    case INFO_ELINK:
      elink_info = true;
      break;

    case INFO_ADN2814:
      adn_info = true;
      break;

    case INFO_CXP:
      cxp_info = true;
      break;

    case INFO_DDR3:
      ddr3_info = true;
      break;

    case INFO_EGROUP:
      egroup_info = true;
      break;

    case INFO_SFP:
      sfp_info = true;
      break;

    case INFO_SI5324:
      si5324_info = true;
      break;

    case INFO_LMK03200:
      lmk03200_info = true;
      break;

    case INFO_SI5345:
      si5345_info = true;
      break;

    case INFO_ICS8N4Q:
      ics_info = true;
      break;

    case INFO_FPGA:
      fpga_info = true;
      break;

    case INFO_POWER:
      power_info = true;
      break;

    case INFO_POD:
      pod_info = true;
      break;

    case INFO_FREQ:
      freq_info = true;
      break;

    case INFO_TTC:
      ttc_info = true;
      break;

    case INFO_FIREFLY:
      firefly_info = true;
      break;

    case INFO_LSPCI:
      lspci_info = true;
      break;

    case INFO_ALL:
      fpga_info     = true;
      power_info    = true;
      pod_info      = true;
      freq_info     = true;
      link_info     = true;
      linkreset_info= true;
      elink_info    = true;
      common_info   = true;
      adn_info      = true;
      cxp_info      = true;
      ddr3_info     = true;
      sfp_info      = true;
      si5324_info   = true;
      si5345_info   = true;
      lmk03200_info = true;
      egroup_info   = true;
      ttc_info      = true;
      firefly_info  = true;
      lspci_info    = false;
      break;

    default:
      common_info = true;
      adn_info    = true;
    }

  // Set resource locks as necessary
  if( power_info || pod_info || adn_info || cxp_info || ddr3_info ||
      sfp_info || si5324_info || si5345_info || firefly_info )
    locks |= LOCK_I2C;

  // ### Modified (23 Aug 2022): do not write a flash-related register
  // to read jumper setting (if that would be necessary; see below)
  //if( common_info )
  //  locks |= LOCK_FLASH; // To read firmware selection jumpers

  try {
    // In case of required locks unavailable, retry a configurable number of seconds
    int try_open_count = 0;
    while( true )
      {
        try {
          flxCard.card_open( device_number, locks, false, true );
        }
        catch( FlxException &ex ) {
          if( ex.errorId() != FlxException::LOCK_VIOLATION ||
              try_open_count >= try_open_max )
            // Not a lock issue or persistent lock issue: rethrow exception
            throw ex;
          ++try_open_count;
          printf( "Waiting for locks, retrying... (%d)\n", try_open_count );
          sleep( 1 );
          continue; // Retry card_open()
        }
        break; // Card succesfully opened
      }

    card_type = flxCard.card_type();
    bar2      = (flxcard_bar2_regs_t *) flxCard.bar2Address();

    // Card and firmware types will be reported as part of 'common_info'
    if( !common_info && !lspci_info )
      {
        printf( "Card type : " );
        if( card_type == 712 || card_type == 711 || card_type == 710 ||
            card_type == 709 || card_type == 128 ||
            card_type == 182 || card_type == 155 )
          {
            printf( "FLX-%lu\n", card_type );
          }
        else
          {
            printf( "<unknown> (WARNING!)\n" );
            //printf( "Exiting...\n" );
            //return 1;
          }
        printf( "Firmw type: %s\n", flxCard.firmware_type_string().c_str() );
      }
  }
  catch( FlxException &ex ) {
    std::cout << "### ERROR open: " << ex.what() << std::endl;
    return 1;
  }

  try {
    if( common_info )
      {
        u_long val;

        printf("\nGeneral information\n");
        printf("------------------------\n");

        printf("Card type             : ");
        if( card_type == 712 || card_type == 711 || card_type == 710 ||
            card_type == 709 || card_type == 128 ||
            card_type == 182 || card_type == 155 )
          printf( "FLX-%lu\n", card_type );
        else
          printf( "<unknown>\n");

        printf( "Firmware type         : %s", flxCard.firmware_type_string().c_str() );
#if REGMAP_VERSION >= 0x500
        if( flxCard.fullmode_type() && bar2->FULLMODE_HALFRATE )
          printf( " (4.8Gbs)" );
#endif // REGMAP_VERSION
        printf( "\n" );

        // Register map version stored as hex: 0x0300 -> 3.0
        u_long reg_map_version = bar2->REG_MAP_VERSION;
        u_long major = (reg_map_version & 0xFF00) >> 8;
        u_long minor = (reg_map_version & 0x00FF) >> 0;
        printf("Register map version  : %ld.%ld\n", major, minor);

        display_FW_date();
        display_FW_version();

        // ### Don't touch flash register
        //if( flxCard.m_bar2->CONFIG_FLASH_WR.PAR_CTRL == 1 )
        //  flxCard.m_bar2->CONFIG_FLASH_WR.PAR_CTRL = 0;
        // Show jumper setting, provided it can be read out
        printf("F/W partition jumpers : ");
        if( bar2->CONFIG_FLASH_WR.PAR_CTRL == 0 )
          {
            val = bar2->CONFIG_FLASH_RD.PAR_RD;
            printf("%ld\n", 3 - val); // 2-bit inverted
          }
        else
          {
            printf("---\n");
          }

        u_long channels    = bar2->NUM_OF_CHANNELS;
        u_long endpoints   = bar2->NUMBER_OF_PCIE_ENDPOINTS;
        u_long descriptors = bar2->GENERIC_CONSTANTS.DESCRIPTORS;
        u_long interrupts  = bar2->GENERIC_CONSTANTS.INTERRUPTS;

        printf("Number of channels    : %lu\n", channels);
        printf("Number of endpoints   : %lu\n", endpoints);
        printf("Number of descriptors : %lu", descriptors);
#if REGMAP_VERSION >= 0x500
        u_long tohost_descr   = bar2->GENERIC_CONSTANTS.TOHOST_DESCRIPTORS;
        u_long fromhost_descr = bar2->GENERIC_CONSTANTS.FROMHOST_DESCRIPTORS;
        if( tohost_descr > 0 || fromhost_descr > 0 )
          printf("  (ToHost %lu, FromHost %lu)", tohost_descr, fromhost_descr);
        printf("\n");
        u_long trickle_descr = bar2->GENERIC_CONSTANTS.TRICKLE_DESCRIPTORS;
        u_long trickle_index = bar2->GENERIC_CONSTANTS.TRICKLE_DESCRIPTOR_INDEX;
        if( trickle_descr > 0 )
          printf("Number of trickles    : %lu  (first=#%lu)\n",
                 trickle_descr, trickle_index);
#else
        printf("\n");
#endif // REGMAP_VERSION
        printf("Number of interrupts  : %lu\n", interrupts);

        val = bar2->MMCM_MAIN.MAIN_INPUT;
        switch(val)
          {
          case 0:
            printf("MAIN clk src selectable: ");
            val = bar2->MMCM_MAIN.LCLK_SEL;
            printf("%s\n", val?"LOCAL":"TTC");
            break;
          case 1:
            printf("MAIN clock source     : TTC fixed\n");
            break;
          case 2:
            printf("MAIN clock source     : LCLK fixed\n");
            break;
          default:
            printf("MAIN clock source     : <invalid>\n");
            break;
          }

        val = bar2->MMCM_MAIN.PLL_LOCK;
        if( val & 1 )
          printf("Internal PLL Lock     : YES\n");
        else
          printf("Internal PLL Lock     : NO\n");

        val = flxCard.m_bar2->GENERATE_GBT;
        printf("GBT Wrapper generated : %s\n", val?"YES":"NO");

        if( verbose )
          {
            val = bar2->FPGA_DNA;
            printf("FPGA DNA              : 0x%016lx\n", val);
          }

        if( verbose )
          {
            printf("\nCentral Router settings\n");
            printf("------------------------\n");

            val = bar2->INCLUDE_EGROUPS[0].INCLUDE_EGROUP.FROMHOST_02;
            printf("FROMHOST_EPROC2       : %s\n", val?"YES":"NO");

            val = bar2->INCLUDE_EGROUPS[0].INCLUDE_EGROUP.FROMHOST_04;
            printf("FROMHOST_EPROC4       : %s\n", val?"YES":"NO");

            val = bar2->INCLUDE_EGROUPS[0].INCLUDE_EGROUP.FROMHOST_08;
            printf("FROMHOST_EPROC8       : %s\n", val?"YES":"NO");

            val = bar2->INCLUDE_EGROUPS[0].INCLUDE_EGROUP.FROMHOST_HDLC;
            printf("FROMHOST_HDLC         : %s\n", val?"YES":"NO");

            val = bar2->INCLUDE_EGROUPS[0].INCLUDE_EGROUP.TOHOST_02;
            printf("TOHOST_EPROC2         : %s\n", val?"YES":"NO");

            val = bar2->INCLUDE_EGROUPS[0].INCLUDE_EGROUP.TOHOST_04;
            printf("TOHOST_EPROC4         : %s\n", val?"YES":"NO");

            val = bar2->INCLUDE_EGROUPS[0].INCLUDE_EGROUP.TOHOST_08;
            printf("TOHOST_EPROC8         : %s\n", val?"YES":"NO");

            val = bar2->INCLUDE_EGROUPS[0].INCLUDE_EGROUP.TOHOST_16;
            printf("TOHOST_EPROC16        : %s\n", val?"YES":"NO");

            val = bar2->INCLUDE_EGROUPS[0].INCLUDE_EGROUP.TOHOST_HDLC;
            printf("TOHOST_HDLC           : %s\n", val?"YES":"NO");

            val = bar2->CR_GENERICS.XOFF_INCLUDED;
            printf("XOFF_INCLUDED         : %s\n", val?"YES":"NO");

            val = bar2->CR_GENERICS.DIRECT_MODE_INCLUDED;
            printf("DIRECT_MODE_INCLUDED  : %s\n", val?"YES":"NO");

            val = bar2->CR_GENERICS.FROM_HOST_INCLUDED;
            printf("FROM_HOST_INCLUDED    : %s\n", val?"YES":"NO");

            val = bar2->WIDE_MODE;
            printf("Wide mode             : %s\n", val?"YES":"NO");

#if REGMAP_VERSION < 0x500
            val = bar2->TTC_EMU_CONST_GENERATE_TTC_EMU;
#else
            val = bar2->GENERATE_TTC_EMU;
#endif // REGMAP_VERSION
            printf("TTC Emulator          : %s\n", (val & 0x2)?"YES":"NO");
          }
      }

    if( fpga_info )
      display_fpga();

    if( adn_info )
      display_adn2814();

    if( power_info )
      {
        // Power status and temperatures
        if( card_type == 182 || card_type == 155 )
          display_ina226_tmp435( display_adm1x66 );
        else
          display_ltc2991();
      }

    if( pod_info )
      display_pod( pod_request );

    if( freq_info )
      display_freq();

    if( ttc_info )
      {
        if( card_type == 182 || card_type == 155 )
          display_lti( clear );
        else
          display_ttc( clear );
      }

    if( si5324_info )
      display_si5324();

    if( lmk03200_info )
      display_lmk03200();

    if( si5345_info )
      display_si5345();

    if( ics_info )
      display_ics();

    if( cxp_info )
      display_cxp();

    if( ddr3_info )
      display_ddr3();

    if( egroup_info )
      display_egroups( argc, argv, arg_index );

    if( elink_info )
      display_elinks_alignment( clear );

    if( link_info )
      display_channels_alignment( clear, fec_incr );

    if( linkreset_info )
      display_channels_autoreset( clear );

    if( sfp_info )
      display_sfp();
  }
  catch( FlxException &ex ) {
    std::cout << "### ERROR: " << ex.what() << std::endl;
    return 1;
  }

  if( firefly_info )
    display_firefly();

  if( lspci_info )
    {
      printf("Output of \"lspci | grep -e Xil -e CERN\"\n");
      printf("----------------------------------------\n");
      system("/sbin/lspci | grep -e Xil -e CERN");
    }

  try {
    flxCard.card_close();
  }
  catch( FlxException &ex ) {
    std::cout << "### ERROR close: " << ex.what() << std::endl;
    return 1;
  }

  return 0;
}

// ----------------------------------------------------------------------------

void display_help()
{
  printf("Usage: flx-info [OPTIONS] [COMMAND]\n");
  printf("Displays information about an FLX card.\n\n");
  printf("Options:\n");
  printf("  -c <number>         Use card indicated by <number> (default 0).\n");
#if DEBUG_LEVEL > 0
  printf("  -D <level>          Set API debug output level.\n");
  printf("                      0=disabled, 5, 10, 20 progressively more verbose output. Default:0.\n");
#endif // DEBUG_LEVEL
  printf("  -s <secs>           If necessary wait up to <secs> seconds for free (I2C) lock,\n");
  printf("                      accessing the card for actions involving I2C ops (default: 5 secs).\n");
  printf("  -h                  Display help.\n");
  printf("  -v                  Verbose mode.\n");
  printf("  -V                  Display the version number.\n");
  printf("  -z                  Clear the TTC/LTI counters (commands TTC or LTI),\n");
  printf("                      or clear latched signals and error counters\n");
  printf("                      (commands LINK, ELINK or LINKRESET).\n" );
  printf("  -F                  Show FEC error counter increments during one second,\n");
  printf("                      in addition, if any errors (with command LINK).\n");
  printf("Commands: (case-insensitive)\n");
  printf("  COUNT               Display the number of FELIX devices installed in the host.\n");
  printf("  CARDS               Display the number of FELIX cards installed in the host.\n");
  printf("  FPGA                Display the status of the FPGA.\n");
  printf("  POWER               Display the status of the power monitoring devices\n");
  printf("                      (LTC2991 or IN226/TMP435).\n");
  printf("  POWERSEQ            As 'POWER', and in addition some FLX-182/155 LTM4700 and ADM1x66 info.\n");
  printf("  POD                 Display the overall status of the MiniPODs.\n");
  printf("  PODTEMP or PODVCC   Display temperature and voltage (VCC) readings of the MiniPODs.\n");
  printf("  PODPOWER            Display optical power readings of the MiniPODs.\n");
  printf("  PODPOWERRX          Display optical power readings of the RX MiniPODs only.\n");
  printf("  TTC or LTI          Display info from TTC or LTI related registers.\n");
  printf("  FREQ                Display the RXUSRCLK frequency.\n");
  printf("  LINK (or GBT)       Display the channel alignment status\n");
  printf("                      (plus PLL LOL and FEC error counters; see option -F).\n");
  printf("  ELINK               Display the E-link alignment status\n");
  printf("                      (plus 'PATH' error counters).\n");
  printf("  LINKRESET           Display the channel auto-reset counters.\n");
  printf("  ADN2814             Display the ADN2814 register 0x4.\n");
  printf("  CXP                 Display the temperature and voltage from CXP1 and CXP2.\n");
  printf("  SFP                 Display info from the Small Form Factor Pluggable transceivers.\n");
  printf("  DDR3                Display the values from DDR3 RAM memory.\n");
  printf("  SI5324              Display the SI5324 status (on FLX-709 only).\n");
  printf("  SI5345              Display the SI5345 status.\n");
  printf("  LMK03200            Display the LMK03200 status.\n");
  printf("  ICS8N4Q             Display the ICS8N4Q status (on FLX-710 only).\n");
  printf("  FFLY or FIREFLY     Display status of the FireFly modules (FLX-182/155 only).\n");
  printf("  EGROUP [<ch>] [RAW] Display the values from EGROUP registers:\n");
  printf("                      if no channel number <ch> is specified, display all available,\n");
  printf("                      using hexadecimal notation if RAW is specified.\n");
  printf("  LSPCI               Output of 'lspci | grep -e Xil -e CERN'.\n");
  printf("  ALL                 Display all.\n");
}

// ----------------------------------------------------------------------------

void display_FW_date()
{
  u_long date = bar2->BOARD_ID_TIMESTAMP;
  u_int version_year   = (date >> 32) & 0xFF;
  u_int version_month  = (date >> 24) & 0xFF;
  u_int version_day    = (date >> 16) & 0xFF;
  u_int version_hour   = (date >>  8) & 0xFF;
  u_int version_minute = (date >>  0) & 0xFF;

  printf("Firmware datetime     : 20%02x/%02x/%02x %02x:%02x\n",
         version_year, version_month, version_day,
         version_hour, version_minute);
}

// ----------------------------------------------------------------------------

void display_FW_version()
{
  // GIT revision number
  u_long value = bar2->GIT_TAG;

  u_long index;
  char git_tag[8+1];
  for( index=0; index<8; ++index )
    git_tag[index] = (value >> (8 * index)) & 0xff;
  git_tag[8] = '\0';

  printf("GIT tag               : %s\n", git_tag);

  value = bar2->GIT_COMMIT_NUMBER;
  printf("GIT commit number     : %lu\n", value);
  value = bar2->GIT_HASH;
  printf("GIT hash              : 0x%08lx\n", value);
}

// ----------------------------------------------------------------------------

void display_channels_alignment( bool clear, bool display_fec_incr )
{
#if REGMAP_VERSION < 0x500
  ulong fw = flxCard.firmware_type();
  if( fw == FIRMW_MROD )
    {
      display_channels_alignment_mrod();
      return;
    }
#endif // REGMAP_VERSION

  u_long aligned          = bar2->GBT_ALIGNMENT_DONE;
  u_long error_flags      = bar2->GBT_ERROR;
  u_long number_channels  = bar2->NUM_OF_CHANNELS;
  u_long number_endpoints = bar2->NUMBER_OF_PCIE_ENDPOINTS;

  printf("\nLink alignment status\n");
  printf("------------------------\n");

  if( verbose )
    {
      printf("Number of channels: %lu\n", number_channels);
      printf("Content of REG_GBT_ALIGNMENT_DONE: 0x%lx\n", aligned);
    }

  int group = number_channels;

  if( number_endpoints > 0 )
    number_channels *= number_endpoints;

  // Split list of channels into groups per card (for 712/711)
  for( u_long i=0; i<number_channels/group; ++i )
    {
      u_long index;
      u_long min = i * group;
      u_long max = min + group;
      if( max > number_channels )
        max = number_channels;

      if( i > 0 ) printf( "\n" );
      printf("Channel |");
      for( index=min; index<max; ++index )
        printf(" %2lu  ", index);

      printf("\n        ");
      for( index=min; index<max; ++index )
        printf("-----");

      printf("\nAligned |");
      for( index=min; index<max; ++index )
        {
          if( (aligned & (1ULL << index)) != 0 )
            {
              if( (error_flags & (1ULL << index)) == 0 )
                printf(" YES ");
              else
                printf(" Err ");
            }
          else
            {
              printf(" NO  ");
            }
        }
      printf( "\n" );
    }

  // Various latched stuff (impacting link alignment),
  // shown only in case of an occurrence, then cleared
  bool nl = true;
  uint64_t lost = bar2->GBT_ALIGNMENT_LOST.ALIGNMENT_LOST;
  if( lost != 0 )
    {
      if( nl ) printf( "\n" );
      nl = false;
      printf( "Link alignment lost (latched): 0x%012lX", lost );
      if( clear )
        {
          bar2->GBT_ALIGNMENT_LOST.ALIGNMENT_LOST = 1; // Clear
          printf( " (cleared)" );
        }
      printf( "\n" );
    }
  uint64_t qpll_lol = bar2->GBT_PLL_LOL_LATCHED.QPLL_LOL_LATCHED;
  uint64_t cpll_lol = bar2->GBT_PLL_LOL_LATCHED.CPLL_LOL_LATCHED;
  if( qpll_lol || cpll_lol )
    {
      if( nl ) printf( "\n" );
      nl = false;
      printf( "PLL LOL (latched): QPLL=0x%03lX CPLL=0x%012lX", qpll_lol, cpll_lol );
      if( clear )
        {
          bar2->GBT_PLL_LOL_LATCHED.QPLL_LOL_LATCHED = 1; // Clear
          // Work-around for typo in register map, i.e. GBT_PLL_LOL_LATCHED not a 'TRIGGER' register
          // (19 July 2023, to be removed at some point)
          *(((uint64_t *) &bar2->GBT_PLL_LOL_LATCHED) + 8) = 0x1;
          *(((uint64_t *) &bar2->GBT_PLL_LOL_LATCHED) + 8) = 0x0;
          printf( " (cleared)" );
        }
      printf( "\n" );
    }
  uint64_t si5345_lol = bar2->HK_CTRL_FMC.SI5345_LOL_LATCHED;
  if( si5345_lol )
    {
      if( nl ) printf( "\n" );
      nl = false;
      printf( "SI5345 LOL (latched): %lu", si5345_lol );
      if( clear )
        {
          bar2->HK_CTRL_FMC.SI5345_LOL_LATCHED = 1; // Clear
          printf( " (cleared)" );
        }
      printf( "\n" );
    }
  uint64_t mmcm_lol = bar2->MMCM_MAIN.LOL_LATCHED;
  if( mmcm_lol )
    {
      if( nl ) printf( "\n" );
      nl = false;
      printf( "MMCM LOL (latched): %lu", mmcm_lol );
      if( clear )
        {
          bar2->MMCM_MAIN.LOL_LATCHED = 1; // Clear
          printf( " (cleared)" );
        }
      printf( "\n" );
    }

  if( number_channels > flxcard::FLX_LINKS )
    number_channels = flxcard::FLX_LINKS;

  // FEC counters
  bool has_fec = false;
  uint64_t fec[2][flxcard::FLX_LINKS];
  for( uint64_t chan=0; chan<number_channels; ++chan )
    {
      fec[0][chan] = bar2->GT_FEC_ERR_CNT_GEN[chan].GT_FEC_ERR_CNT;
      if( fec[0][chan] != 0 )
        has_fec = true;
    }
  if( has_fec && display_fec_incr )
    {
      // Wait for a second and reread the counters,
      // to be able to show the counters' increase per second
      usleep( 1000000 );
      for( uint64_t chan=0; chan<number_channels; ++chan )
        fec[1][chan] = bar2->GT_FEC_ERR_CNT_GEN[chan].GT_FEC_ERR_CNT;
    }
  bool first = true;
  for( uint64_t chan=0; chan<number_channels; ++chan )
    if( fec[0][chan] != 0 )
      {
        if( first )
          {
            printf( "\nFEC error counters (!= 0):\n" );
            printf( "--------------------------\n" );
            if( display_fec_incr )
              printf( "Link  ErrCount  Last-second\n" );
            else
              printf( "Link  ErrCount\n" );
            first = false;
          }
        printf( " %2lu   %8lu", chan, fec[0][chan] );
        if( display_fec_incr )
          printf( "   %7lu", fec[1][chan] - fec[0][chan] );
        printf( "\n" );
      }
}

// ----------------------------------------------------------------------------

void display_channels_alignment_mrod()
{
#if REGMAP_VERSION < 0x500
  printf("\nLink alignment status\n");
  printf("------------------------\n");
  printf("(FLX-MROD firmware):\n");

  if( verbose )
    {
      printf("Number of channels: %lu\n", bar2->NUM_OF_CHANNELS);
    }

  printf( "Per channel from left to right: RXRECDATA, RXRECIDLES and RXALIGNBSY register bit\n" );
  printf( "('*'/'#' = 1, '-' = 0)\n" );

  u_long alignbsy;
  u_long recidles;
  u_long recdata;

  // Assume there are 2 endpoints and 2x24 channels in total, by default
  for( int endpoint=0; endpoint<2; ++endpoint )
    {
      if( endpoint == 0 )
        {
          alignbsy = bar2->MROD_EP0_RXALIGNBSY;
          recidles = bar2->MROD_EP0_RXRECIDLES;
          recdata  = bar2->MROD_EP0_RXRECDATA;
        }
      else
        {
          alignbsy = bar2->MROD_EP1_RXALIGNBSY;
          recidles = bar2->MROD_EP1_RXRECIDLES;
          recdata  = bar2->MROD_EP1_RXRECDATA;
        }
      printf( "Endpoint %d (EP%d):\n", endpoint, endpoint );
      u_long mask = 1;
      // Split output in 2 rows of 12 channels per endpoint
      for( int j=0; j<2; ++j )
        {
          printf( "Chan |" );
          for( int i=0; i<12; ++i )
            printf( " %3d |", i+j*12 );
          printf( "\n" );
          printf( "     |" );
          for( int i=0; i<12; ++i, mask<<=1 )
            {
              printf( " " );
              if( recdata  & mask ) printf( "#" ); else printf( "-" );
              if( recidles & mask ) printf( "*" ); else printf( "-" );
              if( alignbsy & mask ) printf( "*" ); else printf( "-" );
              printf( " |" );
            }
          printf( "\n\n" );
        }
    }
#endif // REGMAP_VERSION
}

// ----------------------------------------------------------------------------

void display_channels_autoreset( bool clear )
{
  u_long number_channels = bar2->NUM_OF_CHANNELS;
  u_long number_endpoints = bar2->NUMBER_OF_PCIE_ENDPOINTS;
  if( number_endpoints > 0 )
    number_channels *= number_endpoints;

#if REGMAP_VERSION >= 0x500
  if( number_channels > flxcard::FLX_LINKS )
    number_channels = flxcard::FLX_LINKS;
#endif // REGMAP_VERSION

  // GT-AUTO-RX-RESET counters
  bool first = true;
  for( uint64_t chan=0; chan<number_channels; ++chan )
    if( bar2->GT_AUTO_RX_RESET_CNT_GEN[chan].GT_AUTO_RX_RESET_CNT.VALUE != 0 )
      {
        uint64_t *val = (uint64_t *)
          &bar2->GT_AUTO_RX_RESET_CNT_GEN[chan].GT_AUTO_RX_RESET_CNT;
        if( first )
          {
            printf( "\nAUTO-RX-RESET count\n" );
            printf( "-------------------------\n" );
            printf( "Link AutoRxReset\n" );
            first = false;
          }
        printf( " %2lu   %6lu\n", chan, *val );
        if( clear )
          *val = 0;
      }
  if( !first )
    {
      if( clear )
        printf( "(Cleared)\n" );
    }
  else
    {
      printf( "<all AUTO-RX-RESET counters 0>\n" );
    }
}

// ----------------------------------------------------------------------------

void display_elinks_alignment( bool clear )
{
  printf("\nE-link alignment status\n");
  printf("------------------------\n");

  uint64_t chans  = bar2->NUM_OF_CHANNELS;
  uint64_t endpts = bar2->NUMBER_OF_PCIE_ENDPOINTS;

  FlxCard flx;
  volatile flxcard_bar2_regs_t *bar2;
#if REGMAP_VERSION >= 0x500
  // Alignment status
  for( uint64_t ep=0; ep<endpts; ++ep )
    {
      // For each endpoint open the corresponding FELIX device
      try {
        flx.card_open( device_number + ep, LOCK_NONE, false, true );
        bar2 = flx.m_bar2;
        if( ep == 0 )
          {
            uint64_t nstreams = bar2->AXI_STREAMS_TOHOST.NUMBER_OF_STREAMS;
            printf( "Streams-per-link: %lu (ToHost)\n", nstreams );
          }
      }
      catch( FlxException &ex ) {
        std::cout << "### ERROR: " << ex.what() << std::endl;
        continue;
      }

      printf( "Device %lu ('*'=aligned, '-'=not aligned)\n", device_number+ep );
      printf( "Elink0        8       16       24       32       40       48       56\n" );

      // From regmap-struct.h, addresses 0x2180-0x22FF (384 bytes):
      // flxcard_decoding_link_status_arr_t DECODING_LINK_STATUS_ARR[24];
      uint64_t decoding_link_aligned;
      for( uint64_t i=0; i<chans; ++i )
        {
          printf( "%3ld:", i );
          decoding_link_aligned =
            bar2->DECODING_LINK_STATUS_ARR[i].DECODING_LINK_ALIGNED;
          uint64_t mask = 0x1ULL;
          for( int lnk=0; lnk<58; ++lnk, mask<<=1 )
            {
              if( (lnk & 0x7) == 0 )
                printf( " " );
              if( decoding_link_aligned & mask )
                printf( "*" );
              else
                printf( "-" );
            }
          printf( "\n" );
        }

      if( ep != 0 )
        flx.card_close();
    }
#endif // REGMAP_VERSION

  // Re-alignment status
  bool all_clear = true;
  for( uint64_t ep=0; ep<endpts; ++ep )
    {
      // For each endpoint open the corresponding FELIX device
      try {
        flx.card_open( device_number + ep, LOCK_NONE, false, true );
        bar2 = flx.m_bar2;
      }
      catch( FlxException &ex ) {
        std::cout << "### ERROR: " << ex.what() << std::endl;
        continue;
      }

      bool first = true;
      uint64_t elink_realignment_status;
      uint64_t elink_realignment_count;
      for( uint64_t i=0; i<chans; ++i )
        {
          elink_realignment_status =
              bar2->ELINK_REALIGNMENT_STATUS_GEN[i].ELINK_REALIGNMENT_STATUS;
          elink_realignment_count =
              bar2->ELINK_REALIGNMENT_COUNT_GEN[i].ELINK_REALIGNMENT_COUNT;
          if( elink_realignment_status != 0 ||
              elink_realignment_count != 0 )
            {
              if( first )
                {
                  printf( "\n=> Realignment:\n" );
                  printf( "Device %lu ('*'=realigned, '-'=not realigned)\n", device_number+ep );
                  printf( "Elink0        8       16       24       32       40       48       56   Count\n" );
                  first = false;
                  all_clear = false;
                }
              printf( "%3ld:", i );
              uint64_t mask = 0x1ULL;
              for( int lnk=0; lnk<58; ++lnk, mask<<=1 )
                {
                  if( (lnk & 0x7) == 0 )
                    printf( " " );
                  if( elink_realignment_status & mask )
                    printf( "*" );
                  else
                    printf( "-" );
                }
              printf( "   %lu", elink_realignment_count );
              printf( "\n" );
            }
        }
      if( !first && clear )
        {
          bool enabled = bar2->ELINK_REALIGNMENT.ENABLE;
          bar2->ELINK_REALIGNMENT.ENABLE = enabled;
          printf( "(Cleared)\n" );
        }

      if( ep != 0 )
        flx.card_close();
    }
  if( all_clear )
    printf( "(Realignment status all clear)\n" );

#if REGMAP_VERSION >= 0x500
  // 'Path' errors
  all_clear = true;
  for( uint64_t ep=0; ep<endpts; ++ep )
    {
      // For each endpoint open the corresponding FELIX device
      try {
        flx.card_open( device_number + ep, LOCK_NONE, false, true );
        bar2 = flx.m_bar2;
      }
      catch( FlxException &ex ) {
        std::cout << "### ERROR: " << ex.what() << std::endl;
        continue;
      }

      bool first = true;
      for( uint64_t i=0; i<chans; ++i )
        {
          for( uint64_t grp=0; grp<7; ++grp )
            {
              bar2->PATH_ERRORS[i].LINK_ERRORS.EGROUP_SELECT = grp;
              uint64_t count = bar2->PATH_ERRORS[i].LINK_ERRORS.COUNT;
              if( count != 0 )
                {
                  if( first )
                    {
                      printf( "\n=> Path errors Device %lu:\n", device_number + ep );
                      first = false;
                      all_clear = false;
                    }
                  printf( "Link %2lu Egroup %lu: %lu\n", i, grp, count );
              }
            }
          if( !first && clear )
            {
              bar2->PATH_ERRORS[i].LINK_ERRORS.CLEAR_COUNTERS = 1;
              bar2->PATH_ERRORS[i].LINK_ERRORS.CLEAR_COUNTERS = 0;
            }
        }
      if( !first && clear )
        printf( "(Cleared)\n" );

      if( ep != 0 )
        flx.card_close();
    }
  if( all_clear )
    printf( "(Path error status all clear)\n" );
#endif // REGMAP_VERSION
}

// ----------------------------------------------------------------------------

void display_adn2814()
{
  if( card_type == 128 || card_type == 182 || card_type == 155 )
    return; // Skip silently

  char device[] = "ADN2814";
  u_char misc_value  = 0;
  u_char ctrla_value = 0;
  u_char ctrlb_value = 0;
  try {
    flxCard.i2c_read(device, 4, &misc_value);
    flxCard.i2c_read(device, 8, &ctrla_value);
    flxCard.i2c_read(device, 9, &ctrlb_value);
  }
  catch( FlxException &ex ) {
    std::cout << "### ERROR: " << ex.what() << std::endl;
    exit(-1);
  }

  printf("\nTTC (ADN2814) status\n");
  printf("------------------------\n");

  if( verbose )
    {
      printf("Misc value 0x%x\n", misc_value);
      printf("CTRLA value 0x%x\n", ctrla_value);
      printf("CTRLB value 0x%x\n", ctrlb_value);
    }

  bool d3 = ((misc_value & (1<<3)) != 0); // LOL status
  bool d5 = ((misc_value & (1<<5)) != 0); // LOS status

  if( d5 )
    {
      printf("Bit D5=1 (LOS): No light. Check fiber connection to FLX card\n");
      printf("(ADN2814 MISC register: 0x%02x)\n", misc_value);
    }
  else
    {
      if( d3 )
        printf("Bit D5=0 (LOS) and D3=1 (LOL): "
               "There is light, but FLX card may have an internal problem\n");
      else
        printf("TTC optical connection is up and working\n");
    }
}

// ----------------------------------------------------------------------------

void display_cxp()
{
  const char *device[4] = { "CXP1_TX", "CXP1_RX", "CXP2_TX", "CXP2_RX" };

  printf("\nCXP status\n");
  printf("------------------------\n");
  if( card_type != 710 )
    {
      printf("Skipping CXP: not on this card type\n");
      return;
    }

  int devnr;
  for( devnr=0; devnr<4; ++devnr )
    {
      const char *devname = device[devnr];
      try {
        u_char aux_value;
        flxCard.i2c_read(devname, 22, &aux_value);
        char valueMSB = (char) aux_value;

        u_char valueLSB = 0;
        flxCard.i2c_read(devname, 23, &valueLSB);
        float temperature = ((float) valueLSB) / 256.0;
        temperature = temperature + valueMSB;
        printf("\n%s 1st temperature monitor: %.2f C\n", devname, temperature);

        u_char voltageMSB = 0, voltageLSB = 0;
        flxCard.i2c_read(devname, 26, &voltageMSB);
        flxCard.i2c_read(devname, 27, &voltageLSB);

        u_int um_voltage = (voltageMSB << 8) + voltageLSB;
        float voltage = (float) um_voltage / 10000;
        printf("%s voltage monitor 3.3 V: %.3f V\n", devname, voltage);

        printf("%s LOS channels status:\n", devname);
        u_char LOS_status;
        flxCard.i2c_read(devname, 0x08, &LOS_status);

        printf("                C0  C1  C2  C3  C4  C5  C6  C7  C8  C9  C10 C11 \n");
        printf("              --------------------------------------------------\n");
        printf("Signal status |");

        int index;
        for( index=0; index<8; ++index )
          {
            if( (LOS_status & (1 << index)) != 0 )
              printf(" -- ");
            else
              printf(" OK ");
          }

        flxCard.i2c_read(devname, 0x07, &LOS_status);
        for( index=0; index<4; ++index )
          {
            if( (LOS_status & (1 << index)) != 0 )
              printf(" -- ");
            else
              printf(" OK ");
          }
      }
      catch( FlxException &ex ) {
        std::cout << "### ERROR: " << ex.what() << std::endl;
        exit(-1);
      }
      printf("|\n");
      printf("              --------------------------------------------------\n");
    }
}

// ----------------------------------------------------------------------------

void display_lmk03200()
{
  printf("\nLMK03200 status\n");
  printf("------------------------\n");

  u_long value = bar2->LMK_LOCKED;
  printf("Bitfield BF_LMK_LOCKED holds value %lu\n", value);
}

// ----------------------------------------------------------------------------

void display_si5324()
{
  printf("\nSI5324 status\n");
  printf("------------------------\n");
  if( card_type != 709 )
    {
      printf("Skipping SI5324: not on this card type\n");
      return;
    }

  try {
    u_char regval = 0;
    flxCard.i2c_read("REC_CLOCK", 0x81, &regval);
    printf("Decoding register 129 (raw data = 0x%02x)\n", regval);
    printf("LOS2_INT: %s\n", (regval & 0x4)?"Error: CLK2 LOS":"CLK2 OK");

    flxCard.i2c_read("REC_CLOCK", 0x82, &regval);
    printf("Decoding register 130 (raw data = 0x%02x)\n", regval);
    printf("LOL_INT:  %s\n", (regval & 0x1)?"PLL unlocked":"PLL locked");
  }
  catch( FlxException &ex ) {
    std::cout << "### ERROR: " << ex.what() << std::endl;
    exit(-1);
  }
}

// ----------------------------------------------------------------------------

void display_ics()
{
  printf("\nICS8N4Q status\n");
  printf("------------------------\n");
  if( card_type != 710)
    {
      printf("Skipping ICS: not on this card type\n");
      return;
    }

  try {
    u_char regval = 0;
    u_int index;
    for (index = 0; index < 24; index++)
      {
        flxCard.i2c_read("CLOCK_RAM", index, &regval);
        printf("Raw data of register %d = 0x%02x\n", index, regval);
      }
  }
  catch( FlxException &ex ) {
    std::cout << "### ERROR: " << ex.what() << std::endl;
    exit(-1);
  }
}

// ----------------------------------------------------------------------------

bool si5345_detected( const char *name )
{
  u_char current_page;
  u_char reg_index, reg_val;

  // Set the register page to 0
  current_page = 0;
  reg_index = 0x01;
  flxCard.i2c_write( name, reg_index, current_page );

  // Check whether the Si5345 chip is present and accessible
  // by reading the device part number from a register
  reg_index = 0x02;
  flxCard.i2c_read( name, reg_index, &reg_val );
  if( reg_val != 0x45 )
    {
      //tprintf( "### %s part nr: read 0x%02X, expected 0x45; "
      //         "device not present/accessible?\n", name, reg_val );
      return false;
    }
  return true;
}

// ----------------------------------------------------------------------------

void display_si5345()
{
  printf("\nSI5345 status\n");
  printf("------------------------\n");

  if( card_type == 709 || card_type == 711 || card_type == 712 )
    {
      try {
        u_char regval = 0;
        flxCard.i2c_read( "SI5345", 0xE, &regval );
        printf( "Loss of lock (LOL): %s\n",
                (regval & 0x2)?"Yes":"No" );

        flxCard.i2c_read( "SI5345", 0xD, &regval );
        printf( "Loss of signal (LOS) input 1: %s\n",
                (regval & 0x1)?"Yes":"No" );

        if( verbose )
          {
            printf( "Loss of signal (LOS) input 2: %s\n",
                    (regval & 0x2)?"Yes":"No" );
            printf( "Loss of signal (LOS) input 3: %s\n",
                    (regval & 0x4)?"Yes":"No" );
            printf( "Loss of signal (LOS) input 4: %s\n",
                    (regval & 0x8)?"Yes":"No" );
          }
      }
      catch( FlxException &ex ) {
        std::cout << "### ERROR: " << ex.what() << std::endl;
        exit(-1);
      }
    }
  else if( card_type == 182 )
    {
      try {
        // Detect the SI5345A: early prototype FLX-182s have
        // a different I2C address for the SI5345-A device
        const char *NAMES_A[] = { "SI5345A", "SI5345A_BETA" };
        const char *name = NAMES_A[0];
        bool detected = si5345_detected( name );
        if( !detected )
          {
            name = NAMES_A[1];
            detected = si5345_detected( name );
            if( !detected )
              printf( "### No SI5345 detected (tried 2 different I2C addresses)\n" );
          }

        if( detected )
          {
            u_char regval = 0;
            flxCard.i2c_read( name, 0xE, &regval );
            printf( "%s: loss of lock (LOL): %s\n",
                    name, (regval & 0x2)?"Yes":"No" );

            flxCard.i2c_read( name, 0xD, &regval );
            printf( "%s: loss of signal (LOS) input 1: %s\n",
                    name, (regval & 0x1)?"Yes":"No" );

            if( verbose )
              {
                printf( "%s: loss of signal (LOS) input 2: %s\n",
                        name, (regval & 0x2)?"Yes":"No" );
                printf( "%s: loss of signal (LOS) input 3: %s\n",
                        name, (regval & 0x4)?"Yes":"No" );
                printf( "%s: loss of signal (LOS) input 4: %s\n",
                        name, (regval & 0x8)?"Yes":"No" );
              }
          }
      }
      catch( FlxException &ex ) {
        std::cout << "### ERROR: " << ex.what() << std::endl;
        exit(-1);
      }
    }
  else if( card_type == 155 )
    {
      try {
        // Detect the SI5345A
        const char *name = "SI5345A";
        bool detected = si5345_detected( name );
        if( !detected )
          printf( "### SI5345A not detected\n" );

        if( detected )
          {
            u_char regval = 0;
            flxCard.i2c_read( name, 0xE, &regval );
            printf( "%s: loss of lock (LOL): %s\n",
                    name, (regval & 0x2)?"Yes":"No" );

            flxCard.i2c_read( name, 0xD, &regval );
            printf( "%s: loss of signal (LOS) input 1: %s\n",
                    name, (regval & 0x1)?"Yes":"No" );

            if( verbose )
              {
                printf( "%s: loss of signal (LOS) input 2: %s\n",
                        name, (regval & 0x2)?"Yes":"No" );
                printf( "%s: loss of signal (LOS) input 3: %s\n",
                        name, (regval & 0x4)?"Yes":"No" );
                printf( "%s: loss of signal (LOS) input 4: %s\n",
                        name, (regval & 0x8)?"Yes":"No" );
              }
          }
      }
      catch( FlxException &ex ) {
        std::cout << "### ERROR: " << ex.what() << std::endl;
        exit(-1);
      }
    }
  else
    {
      printf( "Skipping SI5345: not on this card type\n" );
      return;
    }
}

// ----------------------------------------------------------------------------

void display_ttc( bool clear )
{
  if( clear )
    {
      // Clear counters
      bar2->TTC_ECR_MONITOR.VALUE = 0;
      bar2->TTC_TTYPE_MONITOR.VALUE = 0;
      bar2->TTC_BCR_PERIODICITY_MONITOR.VALUE = 0;
      printf("-> TTC counters cleared\n");
    }
  //else
    {
      printf("\nTTC monitor status\n");
      printf("------------------------\n");

      u_long value = bar2->TTC_L1ID_MONITOR;
      printf("TTC L1ID / XL1ID  = 0x%08lx\n", value);

      value = bar2->TTC_ECR_MONITOR.VALUE;
      printf("ECR counter       = %lu\n", value);

      value = bar2->TTC_TTYPE_MONITOR.VALUE;
      printf("TType counter     = %lu\n", value);

      value = bar2->TTC_BCR_PERIODICITY_MONITOR.VALUE;
      printf("BCR!=3584 counter = %lu\n", value);

      printf( "CDRLOCK-monitor   : ADN_LOL=%u (latched=%u), ADN_LOS=%u (latched=%u)\n"
              "                    CDR_LOCKED=%u (CDR_LOST latched=%u)\n",
              bar2->TTC_CDRLOCK_MONITOR.ADN_LOL,
              bar2->TTC_CDRLOCK_MONITOR.ADN_LOL_LATCHED,
              bar2->TTC_CDRLOCK_MONITOR.ADN_LOS,
              bar2->TTC_CDRLOCK_MONITOR.ADN_LOS_LATCHED,
              bar2->TTC_CDRLOCK_MONITOR.CDRLOCKED,
              bar2->TTC_CDRLOCK_MONITOR.CDRLOCK_LOST );
      bar2->TTC_CDRLOCK_MONITOR.CDRLOCK_LOST = 1; // Clear latched signals
    }
}

// ----------------------------------------------------------------------------

void display_lti( bool clear )
{
#if REGMAP_VERSION >= 0x500
  if( clear )
    {
      // Clear counters
      bar2->LTITTC_SL0ID_MONITOR.VALUE    = 0;
      bar2->LTITTC_SORB_MONITOR.VALUE     = 0;
      bar2->LTITTC_GRST_MONITOR.VALUE     = 0;
      bar2->LTITTC_SYNC_MONITOR.VALUE     = 0;
      bar2->LTITTC_TTYPE_MONITOR.VALUE    = 0;
      bar2->LTITTC_L0ID_ERR_MONITOR.VALUE = 0;
      bar2->LTITTC_BCR_ERR_MONITOR.VALUE = 0;
      bar2->LTITTC_CRC_ERR_MONITOR.VALUE = 0;
      printf("-> LTI counters cleared\n");
    }
  //else
    {
      printf("\nLTI status\n");
      printf("------------------------\n");

      printf( "FM LTI links     = 0x%06lX (per link: 1=9.6Gbs LTI)\n",
              bar2->LINK_FULLMODE_LTI );
      printf( "ALIGNMENT_DONE   = %lu\n",   bar2->LTITTC_ALIGNMENT_DONE );
      printf( "CPLL_FBCLK_LOST  = %lu\n",   bar2->LTITTC_CPLL_FBCLK_LOST );
      printf( "QPLL_LOCK        = %u\n",    bar2->LTITTC_PLL_LOCK.QPLL_LOCK );
      printf( "CPLL_LOCK        = %u\n",    bar2->LTITTC_PLL_LOCK.CPLL_LOCK );
      printf( "RXCDR_LOCK       = %lu\n",   bar2->LTITTC_RXCDR_LOCK );
      printf( "RXRESET_DONE     = %lu\n",   bar2->LTITTC_RXCDR_LOCK );
      printf( "RX_BYTEISALIGNED = %lu\n",   bar2->LTITTC_RX_BYTEISALIGNED );
      printf( "RX_DISP_ERROR    = 0x%lX\n", bar2->LTITTC_RX_DISP_ERROR );
      printf( "RX_NOTINTABLE    = 0x%lX\n", bar2->LTITTC_RX_NOTINTABLE );

      printf( "-> Counters:\n" );
      printf( "SL0ID            = %u\n", bar2->LTITTC_SL0ID_MONITOR.VALUE );
      printf( "SORB             = %u\n", bar2->LTITTC_SORB_MONITOR.VALUE );
      printf( "GRST             = %u\n", bar2->LTITTC_GRST_MONITOR.VALUE );
      printf( "SYNC             = %u\n", bar2->LTITTC_SYNC_MONITOR.VALUE );
      printf( "TTYPE            = %u\n", bar2->LTITTC_TTYPE_MONITOR.VALUE );
      printf( "L0ID_ERR         = %u\n", bar2->LTITTC_L0ID_ERR_MONITOR.VALUE );
      printf( "BCR_ERR          = %u\n", bar2->LTITTC_BCR_ERR_MONITOR.VALUE );
      printf( "CRC_ERR          = %u\n", bar2->LTITTC_CRC_ERR_MONITOR.VALUE );
    }
#endif // REGMAP_VERSION
}

// ----------------------------------------------------------------------------

void display_ddr3()
{
  u_char spd_revision = 0;
  u_char dram_type = 0;
  u_char module_type = 0;
  u_char density = 0;
  u_char ba_bits = 0;
  u_char mod_org = 0;
  u_char n_ranks = 0;
  u_char dev_width = 0;
  u_char mod_width = 0;
  u_char pb_width = 0;
  u_char mtb_dividend = 0;
  u_char mtb_divisor = 0;
  u_char tckmin = 0;
  u_char taamin = 0;
  u_char twrmin = 0;
  u_char trcdmin = 0;
  u_char trrdmin = 0;
  u_char trpmin = 0;
  u_char trasrcupper = 0;
  u_char trasmin_aux = 0;
  u_char trcmin_aux = 0;
  u_char trfcmin_aux1 = 0;
  u_char trfcmin_aux2 = 0;
  u_char twtrmin = 0;
  u_char trtpmin = 0;
  u_char tfawmin_aux1 = 0;
  u_char tfawmin_aux2 = 0;
  u_char tropts = 0;
  u_char cassupport_aux1 = 0;
  u_char cassupport_aux2 = 0;

  printf("\nDDR3 status\n");
  printf("------------------------\n");
  if( !(card_type == 709 || card_type == 710) )
    {
      printf("Skipping DDR3: not on this card type\n");
      return;
    }

  try {
    const char *device[2];
    device[0] = "DDR3-1";
    device[1] = "DDR3-2";

    const char *devname;
    for( int devnr=0; devnr<2; ++devnr )
      {
        devname = device[devnr];

        u_char part_number[40] = "\0";
        for( int index=128; index<145; ++index )
          flxCard.i2c_read(devname, index, &part_number[index - 128]);

        printf("Device %s:\n", devname);
        printf("----------------------------------------------\n");
        printf("Part number      : %s\n", part_number);

        flxCard.i2c_read(devname, 1, &spd_revision);
        printf("SPD Revision     : %d.%d\n", (spd_revision >> 4), (spd_revision & 0x0f));

        flxCard.i2c_read(devname, 2, &dram_type);
        printf("DRAM Device Type : 0x%x\n", dram_type);

        flxCard.i2c_read(devname, 3, &module_type);
        printf("Module Type      : 0x%x\n", module_type);

        flxCard.i2c_read(devname, 4, &density);
        ba_bits = (density & 0x70);
        ba_bits = (ba_bits >> 4);
        ba_bits = ba_bits + 3;
        u_int sd_cap  = 256 * (1 << (density & 0xf));

        printf("Bank Address     : %d bit\n", (int)ba_bits);
        printf("SDRAM Capacity   : %d Mbit\n", (int)sd_cap);

        flxCard.i2c_read(devname, 7, &mod_org);
        n_ranks = ((mod_org >> 3) & 0x7) + 1;
        dev_width = 4 * (1 << (mod_org & 0x07));
        printf("Number of Ranks  : %d \n", (int)n_ranks);
        printf("Device Width     : %d bit\n", (int)dev_width);

        flxCard.i2c_read(devname, 8, &mod_width);
        pb_width = (mod_width & 0x7);
        pb_width = (1 << pb_width);
        pb_width = pb_width * 8;
        printf("Bus Width        : %d bit\n", (int)pb_width);

        u_int total_cap = sd_cap / 8 * pb_width / dev_width * n_ranks;
        printf("Total Capacity   : %d MB\n", total_cap);

        flxCard.i2c_read(devname, 10, &mtb_dividend);
        flxCard.i2c_read(devname, 11, &mtb_divisor);
        float timebase = (float)mtb_dividend / (float)mtb_divisor;
        printf("Medium Timebase  : %.2f ns\n", timebase);

        flxCard.i2c_read(devname, 12, &tckmin);
        printf("tCKmin           : %.2f ns\n", tckmin * timebase);

        flxCard.i2c_read(devname, 16, &taamin);
        printf("tAAmin           : %.2f ns\n", taamin * timebase);

        flxCard.i2c_read(devname, 17, &twrmin);
        printf("tWRmin           : %.2f ns\n", twrmin * timebase);

        flxCard.i2c_read(devname, 18, &trcdmin);
        printf("tRCDmin          : %.2f ns\n", trcdmin * timebase);

        flxCard.i2c_read(devname, 19, &trrdmin);
        printf("tRRDmin          : %.2f ns\n", trrdmin * timebase);

        flxCard.i2c_read(devname, 20, &trpmin);
        printf("tRPmin           : %.2f ns\n", trpmin * timebase);

        flxCard.i2c_read(devname, 21, &trasrcupper);
        flxCard.i2c_read(devname, 22, &trasmin_aux);
        u_short trasmin = ((trasrcupper & 0x0f) << 8) |trasmin_aux;
        printf("tRASmin          : %.2f ns\n", trasmin * timebase);

        flxCard.i2c_read(devname, 23, &trcmin_aux);
        u_short trcmin = ((trasrcupper & 0xf0) << 4) | trcmin_aux;
        printf("tRCmin           : %.2f ns\n", trcmin * timebase);

        flxCard.i2c_read(devname, 25, &trfcmin_aux1);
        flxCard.i2c_read(devname, 24, &trfcmin_aux2);
        u_short trfcmin = (trfcmin_aux1 << 8) | trfcmin_aux2;
        printf("tRFCmin          : %.2f ns\n", trfcmin * timebase);

        flxCard.i2c_read(devname, 26, &twtrmin);
        printf("tWTRmin          : %.2f ns\n", twtrmin * timebase);

        flxCard.i2c_read(devname, 27, &trtpmin);
        printf("tRTPmin          : %.2f ns\n", trtpmin * timebase);

        flxCard.i2c_read(devname, 28, &tfawmin_aux1);
        flxCard.i2c_read(devname, 29, &tfawmin_aux2);
        u_short tfawmin = ((tfawmin_aux1 << 8) & 0x0f) | tfawmin_aux2;
        printf("tFAWmin          : %.2f ns\n", tfawmin * timebase);

        flxCard.i2c_read(devname, 32, &tropts);
        printf("Thermal Sensor   : %d\n", (tropts >> 7) & 1);

        flxCard.i2c_read(devname, 15, &cassupport_aux1);
        flxCard.i2c_read(devname, 14, &cassupport_aux2);
        u_short cassupport = (cassupport_aux1 << 8) | cassupport_aux2;
        printf("CAS Latencies    : ");
        for( int index=0; index<14; ++index )
          {
            if( (cassupport & (1<<index)) != 0 )
              printf("%d  ", (index + 4));
          }
        printf("\n");
      }
  }
  catch( FlxException &ex ) {
    std::cout << "### ERROR: " << ex.what() << std::endl;
    exit(-1);
  }
}

// ----------------------------------------------------------------------------

void display_sfp()
{
  const char *device[8];
  device[2] = "SFP1-1";     //FLX-311
  device[1] = "SFP1-2";
  device[0] = "SFP2-1";     //FLX-311
  device[3] = "SFP2-2";
  device[4] = "SFP3-1";
  device[5] = "SFP3-2";
  device[6] = "SFP4-1";
  device[7] = "SFP4-2";;

  u_char loss_os[4] = { 0, 0, 0, 0 };

  printf("\nSFP status:\n");
  printf("------------------------\n");
  if( card_type != 709 )
    {
      printf("Skipping SFP: not on this card type\n");
      return;
    }

  try {
    const char *devname;
    for( int devnr=0; devnr<8; ++devnr )
      {
        devname = device[devnr];
        if( (devnr & 1) == 0 )
          {
            u_char valueMSB = 0;
            flxCard.i2c_read(devname, 96, &valueMSB);

            u_char part_number[40] = "\0";
            for( int index=20; index<26; ++index )
              flxCard.i2c_read(devname, index, &part_number[index - 20]);

            for( int index=40; index<51; ++index )
              flxCard.i2c_read(devname, index, &part_number[index - 40 + 6]);

            if( verbose )
              {
                printf("Device %s\n", devname);
                printf("----------------------------------------------\n");
                printf("Part number               : %s\n", part_number);
              }
          }
        else
          {
            // Enhanced memory
            u_char valueMSB = 0, valueLSB = 0;
            flxCard.i2c_read(devname, 96, &valueMSB);
            flxCard.i2c_read(devname, 97, &valueLSB);

            u_short u_temperature;
            u_temperature = valueMSB;
            u_temperature = (u_temperature << 8);
            u_temperature = u_temperature + valueLSB;
            float temperature = ((float) u_temperature)/256.0;
            if( verbose )
              printf("%s temperature monitor: %.2f C\n", devname, temperature);

            u_char voltageMSB = 0, voltageLSB = 0;
            flxCard.i2c_read(devname, 98, &voltageMSB);
            flxCard.i2c_read(devname, 99, &voltageLSB);

            u_int um_voltage = voltageMSB;
            um_voltage = (um_voltage << 8) + voltageLSB;
            float voltage = (float)um_voltage / 10000;
            if( verbose )
              printf("%s voltage monitor    : %.3f V\n", devname, voltage);

            u_char aux_value;
            flxCard.i2c_read(devname, 110, &aux_value);

            u_char tx_fault = ((aux_value & (1 << 2)) >> 2);
            u_char rx_lost  = ((aux_value & (1 << 1)) >> 1);
            //data_ready = (aux_value & 1);

            if( verbose )
              printf("Transmission fault state  : %u", tx_fault);

            if( verbose )
              {
                if(tx_fault == 1)
                  printf("  Fault on data transmission\n");
                else
                  printf("  Transmitting data\n");
              }

            loss_os[devnr/2] = rx_lost;
            if( verbose )
              printf("Loss of Signal state      : %u", rx_lost);

            if( verbose )
              {
                if(rx_lost == 1)
                  printf("  Signal not received or under threshold\n");
                else
                  printf("  Receiving signal\n");

                printf("Data ready                : %u", tx_fault);

                if(tx_fault == 1)
                  printf("  Transceiver not ready\n");
                else
                  printf("  Transceiver ready\n");
                printf("\n");
              }
          }
      }

    // FLX-311
    u_int sfp_sequence[4] = { 1, 0, 2, 3 };
    printf("Channel     |  0    1    2    3\n");
    printf("            --------------------\n");
    printf("Link status |");
    for( int lnk=0; lnk<4; ++lnk )
      {
        if( loss_os[sfp_sequence[lnk]] == 1 )
          printf(" --  ");
        else
          printf(" OK  ");
      }
    printf("\n\n");
  }
  catch( FlxException &ex ) {
    std::cout << "### ERROR: " << ex.what() << std::endl;
    exit(-1);
  }
}

// ----------------------------------------------------------------------------

void display_egroups(int argc, char **argv, int arg_index)
{
  printf("\nE-groups status\n");
  printf("------------------------\n");

  try {
    u_long endpoints = bar2->NUMBER_OF_PCIE_ENDPOINTS;
    int n_channel = 0;
    u_long value = 0;

    if(argc == (4 + arg_index))
      {
        if(strcasecmp(argv[3 + arg_index], "RAW") == 0)
          {
            n_channel = atoi(argv[2 + arg_index]);
            if((n_channel < 24) && (n_channel >= 0))
              for( u_long ep=0; ep<endpoints; ++ep )
                display_egroup_raw( n_channel, ep );
            else
              fprintf(stderr, "flx-info: error: Invalid number of channel\n");
          }
        else
          fprintf(stderr, "flx-info: error: Invalid argument\n");
      }
    else
      {
        if(argc == (3 + arg_index))
          {
            if(strcasecmp(argv[2 + arg_index], "RAW") == 0)
              {
                value = bar2->NUM_OF_CHANNELS;
                printf("Displaying %lu channels\n", value);
                for( u_long ep=0; ep<endpoints; ++ep )
                  for( u_long chan=0; chan<value; ++chan )
                    display_egroup_raw( chan, ep );
              }
            else
              {
                n_channel = atoi(argv[2 + arg_index]);
                if((n_channel < 24) && (n_channel >= 0))
                  for( u_long ep=0; ep<endpoints; ++ep )
                    display_egroup( n_channel, ep );
                else
                  fprintf(stderr, "flx-info: error: Invalid number of channel\n");
              }
          }
        else
          {
            if(argc == (2 + arg_index))
              {
                value = bar2->NUM_OF_CHANNELS;
                for( u_long ep=0; ep<endpoints; ++ep )
                  for( u_long chan=0; chan<value; ++chan )
                    display_egroup( chan, ep );
              }
            else
              fprintf(stderr, "flx-info: error: Invalid number of arguments\n");
          }
      }
  }
  catch( FlxException &ex ) {
    std::cout << "### ERROR: " << ex.what() << std::endl;
    exit(-1);
  }
}

// ----------------------------------------------------------------------------

void display_egroup( int channel, int endpoint )
{
  FlxCard *pflx = &flxCard;
  flxcard_bar2_regs_t *bar2 = (flxcard_bar2_regs_t *) pflx->bar2Address();
  u_long fw = bar2->FIRMWARE_MODE;
  if( verbose )
    printf( "Firmware mode: %s\n", flxCard.firmware_type_string().c_str() );

  if( endpoint != 0 )
    {
      try {
        pflx = new FlxCard;
        pflx->card_open( device_number + endpoint, LOCK_NONE, false, true );
        bar2 = (flxcard_bar2_regs_t *) pflx->bar2Address();
      }
      catch( FlxException &ex ) {
        std::cout << "### ERROR: " << ex.what() << std::endl;
        return;
      }
    }

  char regname[64];

  if( pflx->fullmode_type() )
    {
      if( fw == FIRMW_FULL )
        {
          printf("Endpoint %d CHANNEL %d, FULLMODE(%lu) firmware: ",
                 endpoint, channel, fw);
#if REGMAP_VERSION < 0x500
          if( (bar2->CR_FM_PATH_ENA & (1ULL << channel)) != 0 )
#else
            if( bar2->DECODING_EGROUP_CTRL_GEN[channel].DECODING_EGROUP[0].EGROUP.EPATH_ENA == 1 )
#endif // REGMAP_VERSION
              printf( "enabled\n" );
            else
              printf( "disabled\n" );
        }
      else if( fw == FIRMW_FMEMU )
        {
          printf("Endpoint %d CHANNEL %d, FMEMU(%lu) firmware: <to be done>\n",
                 endpoint, channel, fw);
        }
      else if( fw == FIRMW_MROD )
        {
          printf("Endpoint %d CHANNEL %d, FLX-MROD(%lu) firmware: ",
                 endpoint, channel, fw);
          u_long csm_enable;
          if( endpoint == 0 )
            csm_enable = bar2->MROD_EP0_CSMENABLE;
          else
            csm_enable = bar2->MROD_EP1_CSMENABLE;
          if( (csm_enable & (1ULL << channel)) != 0 )
              printf( "enabled\n" );
            else
              printf( "disabled\n" );
        }
      else
        {
          printf("Endpoint %d CHANNEL %d, %s firmware: ----\n",
                 endpoint, channel, pflx->firmware_type_string().c_str() );
        }

      if( endpoint != 0 )
        {
          pflx->card_close();
          delete pflx;
        }
      return;
    }

  printf( "--> ENDPOINT %d:\n", endpoint );

  printf("--> CHANNEL %d <--------------------------------------------------------------------------------\n", channel);
#if REGMAP_VERSION < 0x500
  printf("               MAX CHUNK                           WIDTH+(ENCODING) per PATH                   \n");
  printf("          IN2  IN4  IN8  IN16      0       1       2       3       4       5       6       7   \n");
  printf("-----------------------------------------------------------------------------------------------\n");

  // ToHost
  for( int egroup=0; egroup<7; ++egroup )
    {
      sprintf(regname, "CR_TOHOST_GBT%02d_EGROUP%d_CTRL", channel, egroup);
      u_long value = pflx->cfg_get_reg( regname );
      if( verbose )
        printf("Register %s: 0x%016lX\n", regname, value);

      u_int chunk_length  = (value & 0x7FF80000000) >> 31;
      u_int encoding      = (value & 0x7FFF8000) >> 15;
      u_int eproc_enabled = (value & 0x7FFF);
      if( verbose )
        printf("chunk_length = 0x%08x, encoding = 0x%08x, eproc_enabled = 0x%08x\n",
               chunk_length, encoding, eproc_enabled);

      printf("TH_E_%d | ", egroup);

      int bitmask = 0x7;
      for(int cnt = 0; cnt < 4; ++cnt, bitmask<<=3)
        {
          u_int chunk_length_aux = (chunk_length & bitmask) >> (cnt * 3);
          printf("%4d ", chunk_length_aux * 512);
        }

      printf(" |");

      bitmask = 0x3;
      for(int epath = 0; epath < 8; ++epath, bitmask<<=2)
        {
          u_int elink_width = 0;
          switch(epath)
            {
            case 0:
              if(eproc_enabled & (1 << 3))       elink_width = 4;
              else if(eproc_enabled & (1 << 7))  elink_width = 2;
              break;

            case 1:
              if(eproc_enabled & (1 << 1))       elink_width = 8;
              else if(eproc_enabled & (1 << 8))  elink_width = 2;
              break;

            case 2:
              if(eproc_enabled & (1 << 4))       elink_width = 4;
              else if(eproc_enabled & (1 << 9))  elink_width = 2;
              break;

            case 3:
              if(eproc_enabled & (1 << 0))       elink_width = 16;
              else if(eproc_enabled & (1 << 10)) elink_width = 2;
              break;

            case 4:
              if(eproc_enabled & (1 << 5))       elink_width = 4;
              else if(eproc_enabled & (1 << 11)) elink_width = 2;
              break;

            case 5:
              if(eproc_enabled & (1 << 2))       elink_width = 8;
              else if(eproc_enabled & (1 << 12)) elink_width = 2;
              break;

            case 6:
              if(eproc_enabled & (1 << 6))       elink_width = 4;
              else if(eproc_enabled & (1 << 13)) elink_width = 2;
              break;

            case 7:
              if(eproc_enabled & (1 << 14))      elink_width = 2;
              break;

            default:
              break;
            }

          if(elink_width != 0)
            {
              printf("  %2d", elink_width);
              u_int c_encoding = (encoding & bitmask) >> (epath * 2);
              if(c_encoding == 0)       printf("(D) ");
              else if(c_encoding == 1)  printf("(8) ");
              else if(c_encoding == 2)  printf("(H) ");
              else                      printf("(?) ");
            }
          else
            printf("   ---  ");
        }
      printf("\n");
    }

  printf("-----------------------------------------------------------------------------------------------\n");

  // FromHost
  for( int egroup=0; egroup<5; ++egroup )
    {
      sprintf(regname, "CR_FROMHOST_GBT%02d_EGROUP%d_CTRL", channel, egroup);
      u_long value = pflx->cfg_get_reg( regname );
      if( verbose )
        printf("Register %s: 0x%016lX\n", regname, value);

      u_int encoding      = (value & 0x7FFFFFFF8000) >> 15;
      u_int eproc_enabled = (value & 0x7FFF);
      if( verbose )
        printf("encoding = 0x%08x, eproc_enabled = 0x%08x\n", encoding, eproc_enabled);

      printf("FH_E_%d                        |", egroup);

      int bitmask = 0xF;
      for(int epath = 0; epath < 8; ++epath, bitmask<<=4)
        {
          u_int elink_width = 0;
          switch(epath)
            {
            case 0:
              if(eproc_enabled & (1 << 3))       elink_width = 4;
              else if(eproc_enabled & (1 << 7))  elink_width = 2;
              break;
            case 1:
              if(eproc_enabled & (1 << 1))       elink_width = 8;
              else if(eproc_enabled & (1 << 8))  elink_width = 2;
              break;
            case 2:
              if(eproc_enabled & (1 << 4))       elink_width = 4;
              else if(eproc_enabled & (1 << 9))  elink_width = 2;
              break;
            case 3:
              if(eproc_enabled & (1 << 0))       elink_width = 16;
              else if(eproc_enabled & (1 << 10)) elink_width = 2;
              break;
            case 4:
              if(eproc_enabled & (1 << 5))       elink_width = 4;
              else if(eproc_enabled & (1 << 11)) elink_width = 2;
              break;
            case 5:
              if(eproc_enabled & (1 << 2))       elink_width = 8;
              else if(eproc_enabled & (1 << 12)) elink_width = 2;
              break;
            case 6:
              if(eproc_enabled & (1 << 6))       elink_width = 4;
              else if(eproc_enabled & (1 << 13)) elink_width = 2;
              break;
            case 7:
              if(eproc_enabled & (1 << 14))      elink_width = 2;
              break;
            default:
              break;
            }

          if(elink_width != 0)
            {
              printf("  %2d", elink_width);

              u_int c_encoding = (encoding & bitmask) >> (epath * 4);
              if(c_encoding == 0)         printf("(D) ");
              else if(c_encoding == 1)    printf("(8) ");
              else if(c_encoding == 2)    printf("(H) ");
              else if(c_encoding == 3)
                {
                  if(elink_width == 2)      printf("(T0)");
                  else if(elink_width == 4) printf("(T1)");
                  else if(elink_width == 8) printf("(T3)");
                  else                      printf("(?) ");
                }
              else if(c_encoding == 4)
                {
                  if(elink_width == 4)      printf("(T2)");
                  else if(elink_width == 8) printf("(T4)");
                  else                      printf("(?) ");
                }
              else
                printf("(?) ");
            }
          else
            printf("   ---  ");
        }
      printf("\n");
    }
  printf("\n");

#else

  // ToHost
  for( int egroup=0; egroup<7; ++egroup )
    {
      //bar2->DECODING_EGROUP_CTRL_GEN[channel].DECODING_EGROUP[egroup].EGROUP.xxx
      //volatile u_long EPATH_ENA                :  8;  /* bits   7: 0 */
      //volatile u_long EPATH_WIDTH              :  3;  /* bits  10: 8 */
      //volatile u_long PATH_ENCODING            : 32;  /* bits  42:11 */
      //volatile u_long REVERSE_ELINKS           :  8;  /* bits  50:43 */
      //volatile u_long EPATH_ALMOST_FULL        :  8;  /* bits  58:51 */

      sprintf(regname, "DECODING_LINK%02d_EGROUP%d_CTRL", channel, egroup);
      u_long value = pflx->cfg_get_reg( regname );
      //if( verbose )
      printf("Register %s: 0x%016lX\n", regname, value);
      // ### TBD: Register contents subdivided
    }

  printf("-----------------------------------------------------------------------------------------------\n");

  // FromHost
  for( int egroup=0; egroup<5; ++egroup )
    {
      //bar2->ENCODING_EGROUP_CTRL_GEN[channel].ENCODING_EGROUP[egroup].EGROUP.xxx
      //volatile u_long EPATH_ENA                :  8;  /* bits   7: 0 */
      //volatile u_long PATH_ENCODING            : 32;  /* bits  39: 8 */
      //volatile u_long EPATH_WIDTH              :  3;  /* bits  42:40 */
      //volatile u_long REVERSE_ELINKS           :  8;  /* bits  50:43 */
      //volatile u_long EPATH_ALMOST_FULL        :  8;  /* bits  58:51 */
      //volatile u_long TTC_OPTION               :  4;  /* bits  62:59 */

      sprintf(regname, "ENCODING_LINK%02d_EGROUP%d_CTRL", channel, egroup);
      u_long value = pflx->cfg_get_reg( regname );
      //if( verbose )
      printf("Register %s: 0x%016lX\n", regname, value);
      // ### TBD: Register contents subdivided
    }
  printf("\n");

#endif // REGMAP_VERSION

  if( endpoint != 0 )
    {
      pflx->card_close();
      delete pflx;
    }
}

// ----------------------------------------------------------------------------

void display_egroup_raw( int channel, int endpoint )
{
  FlxCard *pflx = &flxCard;
  if( endpoint != 0 )
    {
      try {
        pflx = new FlxCard;
        pflx->card_open( device_number + endpoint, LOCK_NONE, false, true );
      }
      catch( FlxException &ex ) {
        std::cout << "### ERROR: " << ex.what() << std::endl;
        return;
      }
    }

  char regname[64];
  printf( "--> ENDPOINT %d:\n", endpoint );
  printf("--> CHANNEL %d <--------------------------------------------------------------------------\n", channel);

#if REGMAP_VERSION < 0x500
  printf("      MAX CHUNK                             ENCODING per PATH                             \n");
  printf("     LENGTH WORD    0       1       2       3       4       5       6       7      ENABLES\n");
  printf("------------------------------------------------------------------------------------------\n");

  // ToHost
  for( int egroup=0; egroup<7; ++egroup )
    {
      sprintf(regname, "CR_TOHOST_GBT%02d_EGROUP%d_CTRL", channel, egroup);
      u_long value = pflx->cfg_get_reg( regname );
      if( verbose )
        printf("Register %s: 0x%016lX\n", regname, value);

      u_int chunk_length = ((value & 0x7FF80000000) >> 31);
      u_int encoding = ((value & 0x7FFF8000) >> 15);
      u_int eprocs = (value & 0x00007FFF);

      printf("TH_E_%d | 0x%03x | ", egroup, chunk_length);
      int bitmask = 0x3;
      for(int epath = 0; epath < 8; ++epath, bitmask<<=2)
        {
          u_int c_encoding = ((encoding & bitmask) >> (epath * 2));
          printf("  0x%02x  ", c_encoding);
        }
      printf("| 0x%04x\n", eprocs);
    }

  printf("------------------------------------------------------------------------------------------\n");

  // FromHost
  for( int egroup=0; egroup<5; ++egroup )
    {
      sprintf(regname, "CR_FROMHOST_GBT%02d_EGROUP%d_CTRL", channel, egroup);
      u_long value = pflx->cfg_get_reg( regname );
      if( verbose )
        printf("Register %s: 0x%016lX\n", regname, value);

      u_int encoding = ((value & 0x7FFFFFFF8000) >> 15);
      u_int eprocs = (value & 0x00007FFF);

      printf("FH_E_%d         | ", egroup);
      int bitmask = 0xF;
      for(int epath = 0; epath < 8 ; ++epath, bitmask<<=4)
        {
          u_int c_encoding = ((encoding & bitmask) >> (epath * 4));
          printf("  0x%02x  ", c_encoding);
        }
      printf("| 0x%04x\n", eprocs);
    }
  printf( "\n" );

#else

  // ToHost
  for( int egroup=0; egroup<7; ++egroup )
    {
      sprintf(regname, "DECODING_LINK%02d_EGROUP%d_CTRL", channel, egroup);
      u_long value = pflx->cfg_get_reg( regname );
      //if( verbose )
      printf("Register %s: 0x%016lX\n", regname, value);
      // ### TBD: Register contents to be split out
    }

  printf("-----------------------------------------------------------------------------------------------\n");

  // FromHost
  for( int egroup=0; egroup<5; ++egroup )
    {
      sprintf(regname, "ENCODING_LINK%02d_EGROUP%d_CTRL", channel, egroup);
      u_long value = pflx->cfg_get_reg( regname );
      //if( verbose )
      printf("Register %s: 0x%016lX\n", regname, value);
      // ### TBD: Register contents to be split out
    }
  printf("\n");

#endif // REGMAP_VERSION

  if( endpoint != 0 )
    {
      pflx->card_close();
      delete pflx;
    }
}

// ----------------------------------------------------------------------------

void display_fpga()
{
  printf("\nFPGA status\n");
  printf("------------------------\n");

  if( card_type == 182 || card_type == 155 )
    {
      printf( "FPGA info not available on FLX-182/155 (directly)\n" );
      return;
    }

  monitoring_data_t mondata = flxCard.get_monitoring_data(FPGA_MONITOR);

  printf( "Temperature                :  %f C\n", mondata.fpga.temperature );
  printf( "Internal voltage  [VccInt] :  %f V\n", mondata.fpga.vccint );
  printf( "Auxiliary voltage [VccAux] :  %f V\n", mondata.fpga.vccaux );
  printf( "BRAM voltage      [VccBRAM]:  %f V\n", mondata.fpga.vccbram );
  printf( "DNA                        :  0x%016lx \n", mondata.fpga.dna );
  printf( "Fan speed                  :  %lu RPM\n", mondata.fpga.fanspeed );
}

// ----------------------------------------------------------------------------

void display_ltc2991()
{
  printf("\nPower status\n");
  printf("------------------------\n");

  if( card_type != 712 && card_type != 711)
    {
      printf("LTC2991 info only available on FLX-711 and FLX-712\n");
      return;
    }

  monitoring_data_t mondata = flxCard.get_monitoring_data(POWER_MONITOR);

  printf("%s:\n", mondata.ltc1.name.c_str());

  printf("|Parameter                          | Value     | Unit|\n");
  printf("|================================== | ========= | ====|\n");
  printf("|VCCINT current                     | %9.5f |    A|\n", mondata.ltc1.VCCINT_current);
  printf("|VCCINT voltage                     | %9.5f |    V|\n", mondata.ltc1.VCCINT_voltage);
  printf("|MGTAVCC current                    | %9.5f |    A|\n", mondata.ltc1.MGTAVCC_current);
  printf("|MGTAVCC voltage                    | %9.5f |    V|\n", mondata.ltc1.MGTAVCC_voltage);
  printf("|FPGA internal diode temperature    | %9.5f |    C|\n", mondata.ltc1.FPGA_internal_diode_temperature);

  if(card_type == 712)
    {
      printf("|MGTAVTT current                    | %9.5f |    A|\n", mondata.ltc1.MGTAVTT_current);
      printf("|MGTAVTT voltage                    | %9.5f |    V|\n", mondata.ltc1.MGTAVTT_voltage);
    }
  else
    {
      printf("|MGTAVTTC voltage                   | %9.5f |    V|\n", mondata.ltc1.MGTAVTTC_voltage);
      printf("|MGTVCCAUX voltage                  | %9.5f |    V|\n", mondata.ltc1.MGTVCCAUX_voltage);
    }
  printf("|LTC2991_1 internal temperature     | %9.5f |    C|\n", mondata.ltc1.LTC2991_1_internal_temperature);
  printf("|VCC                                | %9.5f |    V|\n", mondata.ltc1.vcc);

  printf("\n%s:\n", mondata.ltc2.name.c_str());
  printf("|Parameter                          | Value     | Unit|\n");
  printf("|================================== | ========= | ====|\n");
  printf("|PEX0P9V current                    | %9.5f |    A|\n", mondata.ltc2.PEX0P9V_current);
  printf("|PEX0P9V voltage                    | %9.5f |    V|\n", mondata.ltc2.PEX0P9V_voltage);
  printf("|SYS18 current                      | %9.5f |    A|\n", mondata.ltc2.SYS18_current);
  printf("|SYS18 voltage                      | %9.5f |    V|\n", mondata.ltc2.SYS18_voltage);

  if(card_type == 712)
    printf("|SYS25 current                      | %9.5f |    A|\n", mondata.ltc2.SYS25_current);
  else
    printf("|SYS12 voltage                      | %9.5f |    V|\n", mondata.ltc2.SYS12_voltage);

  printf("|SYS25 voltage                      | %9.5f |    V|\n", mondata.ltc2.SYS25_voltage);
  printf("|PEX8732 internal diode temperature | %9.5f |    C|\n", mondata.ltc2.PEX8732_internal_diode_temperature);
  printf("|LTC2991_2 internal temperature     | %9.5f |    C|\n", mondata.ltc2.LTC2991_2_internal_temperature);
  printf("|VCC                                | %9.5f |    V|\n", mondata.ltc2.vcc);
}

// ----------------------------------------------------------------------------
#include <cmath>

double linear11ToDouble( uint32_t val )
{
  // Convert Linear11 (Linear-5s-11s) coded value to double
  int32_t mantissa = (val & 0x7800) >> 11;
  if( val & 0x8000 )
    mantissa |= 0xFFFFFFF0; // Extend sign to 32-bit
  int32_t base = val & 0x03FF;
  if( val & 0x400 )
    base |= 0xFFFFFC00; // Extend sign to 32-bit
  return (double) base * std::pow( (double) 2.0, (double) mantissa );
}

// ----------------------------------------------------------------------------

void display_ina226_tmp435( bool display_adm1x66 )
{
  printf("\nPower status\n");
  printf("--------------\n");

  if( !(card_type == 182 || card_type == 155) )
    {
      printf( "INA226/TMP435/ADM1x66 info only available on FLX-182/155\n" );
      return;
    }

  int ina_cnt = 0, tmp_cnt = 0, adm_cnt = 0;
  if( card_type == 182 )
    {
      ina_cnt = 10;
      tmp_cnt = 7;
      adm_cnt = 2;
    }
  else if( card_type == 155 )
    {
      ina_cnt = 11;
      tmp_cnt = 8;
      adm_cnt = 1;
    }

  //printf("                Config  Vshunt[uV]   Vbus[V]    I[A]    Power[W]   Calib  Shunt[mOhm]\n");
  //printf("                ------  ----------  --------  --------  --------  ------  -----------\n");
  printf("                Vbus[V]    I[A]    Power[W]   Calib  Shunt[mOhm]\n");
  printf("                --------  --------  --------  ------  -----------\n");
  monitoring_data_t mondata = flxCard.get_monitoring_data(POWER_MONITOR);
  ina226_parameters_t *ina = &mondata.ina226[0];
  uint32_t power_total = 0;
  for( int i=0; i<ina_cnt; ++i, ++ina )
    {
      if( ina->manufacturer_id == 0x5449 )
        {
          // Vshunt register in units of 1.25uV
          float f = (float) ina->shunt_volt_reg * 1.25;
          //uint32_t vshunt = (uint32_t) (f + 0.5);

          // Vbus register in units of 1.25mV
          f = (float) ina->bus_volt_reg * 1.25;
          uint32_t vbus = (uint32_t) (f + 0.5);

          uint32_t current = ina->current_reg;

          // Power is 25*Current_LSB, so assuming an LSB of 1mA for all devices,
          // the power in mW is the power register value times 25
          uint32_t power = ina->power_reg * 25;
          if( i > 0 ) // Skip the 12V main power source
            power_total += power;

          //printf( "%-13s:  0x%04X    %5u      %2u.%03u    %2u.%03u    %2u.%03u    %5u     %5.2f\n",
          printf( "%-13s:  %2u.%03u    %2u.%03u    %2u.%03u    %5u     %5.2f\n",
                  ina->name.c_str(), //ina->config_reg, vshunt,
                  vbus/1000, vbus-(vbus/1000)*1000,
                  current/1000, current-(current/1000)*1000,
                  power/1000, power-(power/1000)*1000,
                  ina->calib_reg, ina->r_shunt );
        }
      else
        {
          printf( "%-13s:    <inaccessible?>\n", ina->name.c_str() );
        }
    }
  printf( "\nPower combined (minus 12V): %u.%03u W\n",
          power_total/1000, power_total-(power_total/1000)*1000 );

  printf("\nTemperatures:  T-local  T-remote  Status\n");
  printf("-------------  -------  --------  ------\n");
  tmp435_parameters_t *tmp = &mondata.tmp435[0];
  for( int i=0; i<tmp_cnt; ++i, ++tmp )
    {
      printf( "%-12s:  %2u.%04u   %2u.%04u   0x%02X",
              tmp->name.c_str(), tmp->t_local_hi, tmp->t_local_lo,
              tmp->t_remote_hi,  tmp->t_remote_lo, tmp->status );
      if( tmp->status != 0 )
        {
          uint32_t s = tmp->status;
          printf( " (" );
          if( s & 0x80 ) printf( "BUSY" );
          if( s & 0x40 ) printf( " LHIGH" );
          if( s & 0x20 ) printf( " LLOW" );
          if( s & 0x10 ) printf( " RHIGH" );
          if( s & 0x08 ) printf( " RLOW" );
          if( s & 0x04 ) printf( " OPEN" );
          if( s & 0x02 ) printf( " RTHRM" );
          if( s & 0x01 ) printf( " LTHRM" );
          printf( ")" );
        }
      printf( "\n" );
    }

  if( !display_adm1x66 )
    return;

  // ### UNDER DEVELOPMENT
  printf("\nLTM4700 device\n");
  printf("---------------\n");
  ltm4700_parameters_t *ltm = &mondata.ltm4700;
  printf( "ID=0x%4X, ", ltm->mfr_special_id );
  printf( "Tdie=%5.2f C\n", linear11ToDouble(ltm->temp_die) );
  printf( "Vin=%5.3f V, Iin=%5.3f A, Pin=%5.3f W\n",
          linear11ToDouble(ltm->vin), linear11ToDouble(ltm->iin),
          linear11ToDouble(ltm->pin) );
  printf( "Chan 0: " );
  printf( "Vout=%5.3f V, Iout=%5.3f A, Pout=%5.3f W, Temp=%5.2f C\n",
          (double) ltm->vout[0] * std::pow(2.0,-12.0), // Linear16 format
          linear11ToDouble(ltm->iout[0]), linear11ToDouble(ltm->pout[0]),
          linear11ToDouble(ltm->temp_ext[0]) );
  printf( "Chan 1: " );
  printf( "Vout=%5.3f V, Iout=%5.3f A, Pout=%5.3f W, Temp=%5.2f C\n",
          (double) ltm->vout[1] * std::pow(2.0,-12.0), // Linear16 format
          linear11ToDouble(ltm->iout[1]), linear11ToDouble(ltm->pout[1]),
          linear11ToDouble(ltm->temp_ext[1]) );
  /*
  // Raw values
  printf( "Vin=%4X Iin=%4X Pin=%4X\n", ltm->vin, ltm->iin, ltm->pin );
  printf( "Tdie= %4X\n", ltm->temp_die );
  printf( "Chan 0: Vout=%4X Iout=%4X Pout=%4X Text0=%4X\n",
          ltm->vout[0], ltm->iout[0], ltm->pout[0], ltm->temp_ext[0] );
  printf( "Chan 1: Vout=%4X Iout=%4X Pout=%4X Text1=%4X\n",
          ltm->vout[1], ltm->iout[1], ltm->pout[1], ltm->temp_ext[1] );
  */

  // ### UNDER DEVELOPMENT
  if( mondata.adm1066[0].valid )
    {
      printf("\nADM1066 device(s)\n");
      printf("------------------\n");
      const char *ADC_INPUT[] = { "VP1", "VP2", "VP3", "VP4", "VH",
                                  "VX1", "VX2", "VX3", "VX4", "VX5",
                                  "AUX1", "AUX2" };
      const char *FAULT_SEL[]   = { "OV   ", "UV/OV", "UV   ", "Off  " };
      const float ADC_FULL_RAW  = 65535.0;
      // Values taken from gitlab atlas-tdaq-felix/felix-versal-tools/flx-181-webapp
      const float ADC_FULL_VP[] = { 4.363*2.048, 2.181*2.048, 2.048, 2.048 };
      const float ADC_FULL_VH[] = { 4.363*2.048, 10.472*2.048 };
      const float ADC_FULL_VX   = 1.000*2.048;
      const float ADC_FULL_AUX  = 4.363*2.048;

      adm1066_parameters_t *adm = &mondata.adm1066[0];
      for( int i=0; i<adm_cnt; ++i, ++adm )
        {
          if( !adm->valid ) continue;
          printf( "-> %-12s (ManufactId=0x%02X SiliconRev=0x%02X)\n",
                  adm->name.c_str(), adm->manufacturer_id, adm->silicon_rev );
          printf( "Fault/Status:" );
          for( int j=0; j<6; ++j )
            printf( " %02X", adm->fault_stat[j] );
          printf( "\n" );
          for( int j=0; j<8+4; ++j )
            {
              // ADC raw value
              printf( "%-4s: %5u = ", ADC_INPUT[j], adm->adc[j] );

              // ADC converted value
              if( strstr( ADC_INPUT[j], "VP" ) != 0 )
                printf( " %5.2fV",
                        ((float) adm->adc[j] / ADC_FULL_RAW) *
                        ADC_FULL_VP[adm->input_cfg[j][5] & 3] );
              else if( strstr( ADC_INPUT[j], "VX" ) != 0 )
                printf( " %5.2fV",
                        ((float) adm->adc[j] / ADC_FULL_RAW) * ADC_FULL_VX );
              else if( strstr( ADC_INPUT[j], "VH" ) != 0 )
                printf( " %5.2fV", ((float) adm->adc[j] / ADC_FULL_RAW) *
                        ADC_FULL_VH[adm->input_cfg[j][5] & 1] );
              else if( strstr( ADC_INPUT[j], "AUX" ) != 0 )
                printf( " %5.2fV",
                        ((float) adm->adc[j] / ADC_FULL_RAW) * ADC_FULL_AUX );

              // Display some ADC settings
              if( strstr( ADC_INPUT[j], "AUX" ) == 0 )
                {
                  if( strstr( ADC_INPUT[j], "VP" ) != 0 ||
                      strstr( ADC_INPUT[j], "VH" ) != 0 )
                    printf( "  Range=%u", adm->input_cfg[j][5] & 3 );
                  else
                    printf( "         " );

                  printf( "  FaultSel=%s", FAULT_SEL[adm->input_cfg[j][4] & 3] );
                }
              printf( "\n" );
            }
        }
    }
  else if( mondata.adm1266.valid )
    {
      printf("\nADM1266 device\n");
      printf("---------------\n");
      const char *ADC_INPUT[] = { "VH1", "VH2",  "VH3",  "VH4",
                                  "VP1", "VP2",  "VP3",  "VP4",
                                  "VP5", "VP6",  "VP7",  "VP8",
                                  "VP9", "VP10", "VP11", "VP12", "VP13" };
      adm1266_parameters_t *adm = &mondata.adm1266;
      printf( "%-12s (DeviceId=0x%08X DeviceRev=0x%016lX)\n",
              adm->name.c_str(), adm->ic_device_id, adm->ic_device_rev );
    }
}

// ----------------------------------------------------------------------------

void display_pod( int request )
{
  u_int podnum, maxpod;
  minipod_parameters_t *mp, *mp2;

  printf("\nMiniPODs status\n");
  printf("------------------------\n");

  if( card_type != 712 && card_type != 711 )
    {
      printf( "MiniPODs only present on FLX-711 and FLX-712\n" );
      return;
    }

  monitoring_data_t mondata, mondata2;
  // Includes the latched LOS values:
  mondata = flxCard.get_monitoring_data( request );

  maxpod = 8;

  if( request == POD_MONITOR_ALL || request == POD_MONITOR_TEMP_VOLT )
    {
      printf("               |");
      mp = mondata.minipod;
      for(podnum = 0; podnum < maxpod; ++podnum, ++mp)
        printf(" %s |", mp->name);

      printf("\n               |");
      for(podnum = 0; podnum < maxpod; ++podnum)
        printf("========|");
      printf("\n");

      printf("Temperature [C]|");
      mp = mondata.minipod;
      for(podnum = 0; podnum < maxpod; ++podnum, ++mp)
        if( !mp->absent )
          printf("%7d |", mp->temp);
        else
          printf("    ---- |");

      printf("\n3.3 VCC     [V]|");
      mp = mondata.minipod;
      for(podnum = 0; podnum < maxpod; ++podnum, ++mp)
        if( !mp->absent )
          printf("%7.2f |", mp->v33);
        else
          printf("    ---- |");

      printf("\n2.5 VCC     [V]|");
      mp = mondata.minipod;
      for(podnum = 0; podnum < maxpod; ++podnum, ++mp)
        if( !mp->absent )
          printf("%7.2f |", mp->v25);
        else
          printf("    ---- |");
      printf( "\n" );

      if( request == POD_MONITOR_TEMP_VOLT )
        return;
    }

  if( request == POD_MONITOR_ALL ||
      request == POD_MONITOR_POWER || request == POD_MONITOR_POWER_RX )
    {
      if( request == POD_MONITOR_POWER_RX )
        printf("\nOptical power (Receive=RX) of channel in uW:\n");
      else
        printf("\nOptical power (Receive=RX or Transmit=TX) of channel in uW:\n");
      printf("        |");
      for( int i=0; i<12; ++i )
        printf( "    %2d   |", i );
      printf("\n");
      printf("        |");
      for( int i=0; i<12; ++i )
        printf( "=========|");
      printf("\n");
      mp = mondata.minipod;
      for(podnum = 0; podnum < maxpod; ++podnum, ++mp)
        if( request != POD_MONITOR_POWER_RX || podnum < maxpod/2 ) // First are RX
          {
            if( !mp->absent )
              {
                printf(" %s | ", mp->name);
                for( int i=0; i<12; ++i )
                  printf("%7.2f | ", mp->optical_power[i]);
                printf("\n");
              }
            else
              {
                printf(" %s | <not detected>\n", mp->name);
              }
          }

      if( request == POD_MONITOR_POWER || request == POD_MONITOR_POWER_RX )
        return;
    }

  // Read again to get the current LOS values (but leave out some other stuff)
  mondata2 = flxCard.get_monitoring_data( POD_MONITOR_LOS );

  printf("\nHow to the read the table below:\n");
  printf("* = FLX link endpoint OK     (no LOS)\n");
  printf("- = FLX link endpoint not OK (LOS)\n");
  printf("First letter:  Current channel status\n");
  printf("Second letter: Latched channel status\n");
  printf("Example: *(-) means channel had lost the signal in the past but the signal is present now.\n");

  printf("\nLatched / current link status of channel:\n");
  printf("        |");
  for( int i=0; i<12; ++i )
    printf("  %2d  |", i );
  printf("\n");
  printf("        |");
  for( int i=0; i<12; ++i )
    printf( "======|");
  printf("\n");
  mp  = mondata.minipod;
  mp2 = mondata2.minipod;
  for(podnum = 0; podnum < maxpod; ++podnum, ++mp, ++mp2)
    if( !mp->absent )
      {
        printf(" %s | ", mp->name);
        for( int bitmask=0x1; bitmask<=0x800; bitmask<<=1 )
          printf("%s(%s) | ", (mp2->los & bitmask)?"-":"*", (mp->los & bitmask)?"-":"*");
        printf("\n");
      }
    else
      {
        printf(" %s | <not detected>\n", mp->name);
      }

  // MiniPOD device info (name, type, serial number, etc)
  printf("\n                |");
  for(podnum = 0; podnum < maxpod; ++podnum)
    printf(" %14s |", mondata.minipod[podnum].name);

  printf("\n                |");
  for(podnum = 0; podnum < maxpod; ++podnum)
    printf("================|");
  printf("\n");

  printf("Name            |");
  mp = mondata.minipod;
  for(podnum = 0; podnum < maxpod; ++podnum, ++mp)
    if( !mp->absent )
      printf(" %s|", mp->vname);
    else
      printf(" --             |");

  printf("\nOUI             | ");
  mp = mondata.minipod;
  for(podnum = 0; podnum < maxpod; ++podnum, ++mp)
    if( !mp->absent )
      {
        for(int cnum = 0; cnum < 3; cnum++)
          printf("0x%02x ", mp->voui[cnum]);
        printf("| ");
      }
    else
      {
        printf("--             | ");
      }

  printf("\nPart number     |");
  mp = mondata.minipod;
  for(podnum = 0; podnum < maxpod; ++podnum, ++mp)
    if( !mp->absent )
      printf(" %14s|", mp->vpnum);
    else
      printf(" --             |");

  printf("\nRevision number | ");
  mp = mondata.minipod;
  for(podnum = 0; podnum < maxpod; ++podnum, ++mp)
    if( !mp->absent )
      {
        for(int cnum = 0; cnum < 2; cnum++)
          printf("0x%02X ", mp->vrev[cnum]);
        printf("     | ");
      }
    else
      {
        printf("--             | ");
      }

  printf("\nSerial number   |");
  mp = mondata.minipod;
  for(podnum = 0; podnum < maxpod; ++podnum, ++mp)
    if( !mp->absent )
      printf(" %14s|", mp->vsernum);
    else
      printf(" --             |");

  printf("\nDate code       |");
  mp = mondata.minipod;
  for(podnum = 0; podnum < maxpod; ++podnum, ++mp)
    if( !mp->absent )
      printf(" %7s        |", mp->vdate);
    else
      printf(" --             |");

  printf("\n");
}

// ----------------------------------------------------------------------------

void display_freq()
{
  printf("\nFrequencies status\n");
  printf("------------------------\n");

  if( card_type != 709 &&
      card_type != 712 &&
      card_type != 711 &&
      card_type != 128 &&
      card_type != 182 &&
      card_type != 155 )
    {
      printf("Frequency info available only for FLX-709, FLX-711, FLX-712, FLX-128, FLX-182 or FLX-155\n");
      return;
    }

  int ep = bar2->NUMBER_OF_PCIE_ENDPOINTS;

  if( ep < 1 )
    {
      printf("### ERROR: using old F/W without support for REG_NUMBER_OF_PCIE_ENDPOINTS\n");
      return;
    }

  int ch = flxCard.number_of_channels() * ep;

  printf( "RXUSRCLK frequency [MHz]:\n" );
  u_long freq = 0;
  for( int i=0; i<ch; ++i )
    {
      try {
        freq = flxCard.rxusrclk_freq(i);
      }
      catch( FlxException &ex ) {
        std::cout << "### ERROR: the FLX card F/W does not support this option " << std:: endl;
        break;
      }

      printf("Chan %2i: %3.6lF\n", i, ((double)freq) / 1000000.0);
    }
}

// ----------------------------------------------------------------------------

void display_firefly()
{
  printf("\nFireFly status\n");
  printf("----------------\n");
  if( !(card_type == 182 || card_type == 155) )
    {
      printf( "Skipping FireFly: not on this card type\n" );
      return;
    }

  monitoring_data_t mondata;
  try {
    mondata = flxCard.get_monitoring_data( FIREFLY_MONITOR );
  }
  catch(FlxException &ex) {
    std::cout << "### ERROR: " << ex.what() << std::endl;
    exit(-1);
  }

  int ff_tx_cnt = 0;
  int ff_rx_cnt = 0;
  int ff_tr_cnt = 0;
  if( card_type == 182 )
    {
      ff_tx_cnt = 2;
      ff_rx_cnt = 2;
      ff_tr_cnt = 1;
    }
  else if( card_type == 155 )
    {
      ff_tx_cnt = 4;
      ff_rx_cnt = 4;
      ff_tr_cnt = 2;
    }

  // FireFly TX (FYI: \u00B1 adds the 'plusminus' character symbol)
  ffly_tx_parameters_t *ffly_tx = &mondata.ffly_tx[0];
  for( int i=0; i<ff_tx_cnt; ++i, ++ffly_tx )
    {
      printf( "%-16s: PartNr=%s, SerialNr=%s\n",
              ffly_tx->name.c_str(),
              ffly_tx->vendor_part.c_str(), ffly_tx->vendor_serial.c_str() );
      printf( "                  T=%d C [\u00B13], Vcc=%u mV [\u00B13%%], OpTime=%u h, Firmw=0x%06X\n",
              ffly_tx->case_temp, ffly_tx->vcc/10, ffly_tx->elapsed_optime*2,
              ffly_tx->firmware_version );

      printf( "                  Ready=%s, CDR-enable=0x%04X, StatSumm=0x%02X",
              ((ffly_tx->status & 1) ? "NO" : "YES"), ffly_tx->cdr_enable, ffly_tx->status_summ );
      if( (ffly_tx->status_summ & 0xA3) != 0 )
        {
          printf( " (" );
          if( ffly_tx->status_summ & 0x80 ) printf( "LOS" );
          if( ffly_tx->status_summ & 0x20 ) printf( " Fault" );
          if( ffly_tx->status_summ & 0x02 ) printf( " T/V-alarm" );
          if( ffly_tx->status_summ & 0x01 ) printf( " Init" );
          printf( ")" );
        }
      printf( "\n" );

      printf( "                  *Alarms*: TxLOS=0x%03X TxFault=0x%03X CDR-LOL=0x%03X ",
              ffly_tx->latched_alarms_tx_los,
              ffly_tx->latched_alarms_tx_fault,
              ffly_tx->latched_alarms_cdr_lol );
      printf( "\n" );
      printf( "                            Temp=" );
      if( ffly_tx->latched_alarms_temp == 0 ) printf( "<none>" );
      if( ffly_tx->latched_alarms_temp & 0x80 ) printf( "Hi" );
      if( ffly_tx->latched_alarms_temp & 0x40 ) printf( "Lo" );
      printf( " Vcc=" );
      if( ffly_tx->latched_alarms_vcc33 == 0 ) printf( "<none>" );
      if( ffly_tx->latched_alarms_vcc33 & 0x80 ) printf( "Hi" );
      if( ffly_tx->latched_alarms_vcc33 & 0x40 ) printf( "Lo" );
      printf( "\n" );

      printf( "\n" );
    }

  // FireFly RX
  ffly_rx_parameters_t *ffly_rx = &mondata.ffly_rx[0];
  for( int i=0; i<ff_rx_cnt; ++i, ++ffly_rx )
    {
      printf( "%-16s: PartNr=%s, SerialNr=%s\n",
              ffly_rx->name.c_str(),
              ffly_rx->vendor_part.c_str(), ffly_rx->vendor_serial.c_str() );
      printf( "                  T=%d C [\u00B13], Vcc=%u mV [\u00B13%%], OpTime=%u h, Firmw=0x%06X\n",
              ffly_rx->case_temp, ffly_rx->vcc/10, ffly_rx->elapsed_optime*2,
              ffly_rx->firmware_version );

      printf( "                  Ready=%s, CDR-enable=0x%04X, StatSumm=0x%02X",
              ((ffly_rx->status & 1) ? "NO" : "YES"), ffly_rx->cdr_enable, ffly_rx->status_summ );
      if( (ffly_rx->status_summ & 0x67) != 0 )
        {
          printf( " (" );
          if( ffly_rx->status_summ & 0x40 ) printf( "LOS" );
          if( ffly_rx->status_summ & 0x20 ) printf( " Fault" );
          if( ffly_rx->status_summ & 0x04 ) printf( " LowPower" );
          if( ffly_rx->status_summ & 0x02 ) printf( " T/V-alarm" );
          if( ffly_rx->status_summ & 0x01 ) printf( " Init" );
          printf( ")" );
        }
      printf( "\n" );

      printf( "                  *Alarms*: RxLOS=0x%03X RxPower=0x%03X CDR-LOL=0x%03X",
              ffly_rx->latched_alarms_rx_los,
              ffly_rx->latched_alarms_rx_power,
              ffly_rx->latched_alarms_cdr_lol );
      printf( "\n" );
      printf( "                            Temp=" );
      if( ffly_rx->latched_alarms_temp == 0 ) printf( "<none>" );
      if( ffly_rx->latched_alarms_temp & 0x80 ) printf( "Hi" );
      if( ffly_rx->latched_alarms_temp & 0x40 ) printf( "Lo" );
      printf( " Vcc=" );
      if( ffly_rx->latched_alarms_vcc33 == 0 ) printf( "<none>" );
      if( ffly_rx->latched_alarms_vcc33 & 0x80 ) printf( "Hi" );
      if( ffly_rx->latched_alarms_vcc33 & 0x40 ) printf( "Lo" );
      printf( "\n" );

      printf( "\n" );
    }

  // FireFly TX/RX
  ffly_tr_parameters_t *ffly_tr = &mondata.ffly_tr[0];
  for( int i=0; i<ff_tr_cnt; ++i, ++ffly_tr )
    {
      printf( "%-16s: PartNr=%s, SerialNr=%s\n",
              ffly_tr->name.c_str(),
              ffly_tr->vendor_part.c_str(), ffly_tr->vendor_serial.c_str() );
      printf( "                  T=%d C [\u00B15], OpTime=%d h, Firmw=0x%06X\n",
              ffly_tr->case_temp, ffly_tr->elapsed_optime*2, ffly_tr->firmware_revision );
      printf( "                  Ready=%s, CDR-enable=0x%02X\n",
              ((ffly_tr->status & 1) ? "NO" : "YES"), ffly_tr->cdr_enable );
      printf( "                  Vcc-3V3: %u mV [\u00B1100mV]\n", ffly_tr->vcc_3v3/10 );
      printf( "                  Vcc-1V8: %u mV [\u00B1100mV]\n", ffly_tr->vcc_1v8/10 );
      printf( "                  RX-power [uW]: %4u %4u %4u %4u\n",
              ffly_tr->rx_optical_power[0]/10, ffly_tr->rx_optical_power[1]/10,
              ffly_tr->rx_optical_power[2]/10, ffly_tr->rx_optical_power[3]/10 );

      printf( "                  *Alarms*: TxLOS=0x%X RxLOS=0x%X TxFault=0x%X RxPower=0x%04X CDR-LOL=0x%02X",
              (ffly_tr->latched_alarms_tx_los >> 4) & 0xF,
              ffly_tr->latched_alarms_tx_los & 0xF,
              ffly_tr->latched_alarms_tx_fault,
              ffly_tr->latched_alarms_rx_power,
              ffly_tr->latched_alarms_cdr_lol );
      printf( "\n" );
      printf( "                            Temp=" );
      if( (ffly_tr->latched_alarms_temp & 0xFE) == 0 ) printf( "<none>" );
      if( ffly_tr->latched_alarms_temp & 0x80 ) printf( "Hi" );
      if( ffly_tr->latched_alarms_temp & 0x40 ) printf( "Lo" );
      if( ffly_tr->latched_alarms_temp & 0x20 ) printf( "WarnHi" );
      if( ffly_tr->latched_alarms_temp & 0x10 ) printf( "WarnLo" );
      if( ffly_tr->latched_alarms_temp & 0x01 ) printf( " <Init>" );
      printf( " Vcc=" );
      if( ffly_tr->latched_alarms_vcc == 0 ) printf( "<none>" );
      if( ffly_tr->latched_alarms_vcc & 0x80 ) printf( "Hi33" );
      if( ffly_tr->latched_alarms_vcc & 0x40 ) printf( "Lo33" );
      if( ffly_tr->latched_alarms_vcc & 0x20 ) printf( "HiWarn33" );
      if( ffly_tr->latched_alarms_vcc & 0x10 ) printf( "LoWarn33" );
      if( ffly_tr->latched_alarms_vcc & 0x08 ) printf( "Hi18" );
      if( ffly_tr->latched_alarms_vcc & 0x04 ) printf( "Lo18" );
      if( ffly_tr->latched_alarms_vcc & 0x02 ) printf( "HiWarn18" );
      if( ffly_tr->latched_alarms_vcc & 0x01 ) printf( "LoWarn18" );
      printf( "\n" );

      printf( "\n" );
    }
}

// ----------------------------------------------------------------------------
