#include <unistd.h>
#include <iostream>
using namespace std;

#include "arg.h"
#include "flxcard/FlxCard.h"
#include "flxcard/FlxException.h"
#include "felixtag.h"

void usage();

// ----------------------------------------------------------------------------

int main( int argc, char **argv )
{
  int device_number = 0;
  int card_number   = 0;
  int chan_index    = -1;
  int rx_index      = -1;
  int tx_index      = -1;
  int disable       = -1;
  string operation;

  int opt;
  while( (opt = getopt(argc, argv, "c:hi:R:T:V")) != -1 )
    {
      switch( opt )
        {
        case 'c':
          card_number = atoi( optarg );
          try {
            // Map card number to the corresponding device number
            device_number = FlxCard::card_to_device_number( card_number );
          }
          catch( FlxException &ex ) {
            std::cout << " ### ERROR: " << ex.what() << std:: endl;
            return 1;
          }
          if( device_number < 0 )
            {
              cout << "### -c: Card number " << card_number
                   << " not in range [0," << FlxCard::number_of_cards()-1
                   << "]" << endl;
              return 1;
            }
          break;

        case 'h':
          usage();
          return 0;

        case 'i':
          if( sscanf( optarg, "%d", &chan_index ) != 1 )
            arg_error( 'i' );
          if( chan_index < 0 || chan_index > 11 )
            arg_range( 'i', 0, 11 );
          break;

        case 'R':
          if( sscanf( optarg, "%d", &rx_index ) != 1 )
            arg_error( 'R' );
          if( rx_index < 1 || rx_index > 4 )
            arg_range( 'R', 1, 4 );
          break;

        case 'T':
          if( sscanf( optarg, "%d", &tx_index ) != 1 )
            arg_error( 'T' );
          if( tx_index < 1 || tx_index > 4 )
            arg_range( 'T', 1, 4 );
          break;

        case 'V':
          cout << "Version " << FELIX_TAG << endl;
          return 0;

        default:
          cout << "Usage: flx-pod [OPTIONS] [CMD]" << endl
               << "Try flx-pod -h for more information." << endl;
          return 1;
        }
    }

  // String defining the operation to perform?
  if( optind < argc )
    {
      operation = string( argv[optind] );
      if( operation == string("dis") || operation == string("disa") ||
          operation == string("disable") )
        disable = 1;
      else if( operation == string("ena") || operation == string("enable") )
        disable = 0;
      else
        {
          cout << "### Unknown qualifier (use: disable|dis(a)|enable|ena): "
               << operation << endl;
          usage();
          return 1;
        }

      ++optind;
      if( optind < argc )
        {
          cout << "### Warning: unexpected argument \""
               << argv[optind] << "\" (ignored)" << endl;
        }
    }

  extern int MINIPOD_CNT;
  extern const char *POD_NAMES[];
  try {
    FlxCard flx;
    flx.card_open( device_number, LOCK_I2C, true );
    printf( "Firmware: %s\n", flx.firmware_string().c_str() );
    uint32_t card_type = flx.card_type();
    if( !(card_type == 712 || card_type == 711) )
      {
        cout << "### flx-pod for FLX-712/711 cards only" << endl;
        return 1;
      }

    int pod_index = -1;
    if( disable != -1 )
      {
        cout << endl;
        if( disable == 1 )
          cout << "=> Disabling ";
        else
          cout << "=> Enabling ";

        if( rx_index == -1 && tx_index == -1 )
          {
            if( chan_index == -1 )
              cout << "*all* channels";
            else
              cout << "channel " << chan_index;
            cout << " on all RX/TX pods";
          }
        else if( rx_index != -1 )
          {
            if( chan_index == -1 )
              cout << "*all* channels";
            else
              cout << "channel " << chan_index;
            cout << " on RX pod #" << rx_index;
            pod_index = rx_index*2 - 1;
          }
        else if( tx_index != -1 )
          {
            if( chan_index == -1 )
              cout << "*all* channels";
            else
              cout << "channel " << chan_index;
            cout << " on TX pod #" << tx_index;
            pod_index = (tx_index - 1)*2;
          }
        cout << "..." << endl << endl;
      }

    cout << "MiniPOD Channels enabled(+)/disabled(-):" << endl << "         "
         << "  0 " << "  1 " << "  2 " << "  3 "
         << "  4 " << "  5 " << "  6 " << "  7 "
         << "  8 " << "  9 " << " 10 " << " 11 " << endl;
    cout << "         ------------------------------------------------"
         << endl;

    uint8_t reg_addr = 152; // Register with first char of 'vname'
    uint8_t reg_val;
    bool pod_present;
    const char *pod_name;
    for( int index=0; index<MINIPOD_CNT; ++index )
      {
        // Try reading a register containing an ASCII character value
        pod_name = POD_NAMES[index];
        flx.i2c_read( pod_name, reg_addr, &reg_val );
        pod_present = (reg_val >= 0x20 && reg_val <= 0x7E); // Printable char?
        if( !pod_present )
          {
            // Try one more time
            flx.i2c_read( pod_name, reg_addr, &reg_val );
            pod_present = (reg_val >= 0x20 && reg_val <= 0x7E);
          }
        cout << " " << pod_name << ": " << (pod_present ? "" : "---");

        if( pod_present )
          {
            uint32_t chan_disabled = 0;
            flx.i2c_read( pod_name, 93, &reg_val );
            chan_disabled |= ((uint32_t) reg_val & 0xFF);
            flx.i2c_read( pod_name, 92, &reg_val );
            chan_disabled |= (((uint32_t) reg_val & 0xFF) << 8);

            if( disable != -1 )
              {
                if( rx_index == -1 && tx_index == -1 )
                  {
                    if( disable == 1 )
                      {
                        if( chan_index == -1 )
                          chan_disabled = 0xFFF;
                        else
                          chan_disabled |= (1 << chan_index);
                      }
                    else
                      {
                        if( chan_index == -1 )
                          chan_disabled = 0x000;
                        else
                          chan_disabled &= ~(1 << chan_index);
                      }
                    flx.i2c_write( pod_name, 93, (uint8_t) (chan_disabled & 0xFF) );
                    flx.i2c_write( pod_name, 92, (uint8_t) ((chan_disabled>>8) & 0xFF) );
                  }
                else if( (rx_index != -1 || tx_index != -1) && index == pod_index )
                  {
                    if( disable == 1 )
                      {
                        if( chan_index == -1 )
                          chan_disabled = 0xFFF;
                        else
                          chan_disabled |= (1 << chan_index);
                      }
                    else
                      {
                        if( chan_index == -1 )
                          chan_disabled = 0x000;
                        else
                          chan_disabled &= ~(1 << chan_index);
                      }
                    flx.i2c_write( POD_NAMES[pod_index], 93,
                                   (uint8_t) (chan_disabled & 0xFF) );
                    flx.i2c_write( POD_NAMES[pod_index], 92,
                                   (uint8_t) ((chan_disabled>>8) & 0xFF) );
                  }
              }

            for( int chan=0; chan<12; ++chan )
              if( chan_disabled & (1 << chan) )
                cout << "  - ";
              else
                cout << "  + ";
          }
        cout << endl;
      }

    flx.card_close();
  }
  catch( FlxException &ex ) {
    std::cout << "### ERROR: " << ex.what() << std::endl;
    return 1;
  }
}

// ----------------------------------------------------------------------------

void usage()
{
  printf( "Usage: flx-pod [OPTIONS] [COMMAND]\n" );
  printf( "Displays an FLX card's MiniPOD channel enable status, and allows\n" );
  printf( "a user to enable or disable individual RX and/or TX channels.\n" );
  printf( "Options:\n" );
  printf( "  -c NUMBER      Use FLX card indicated by NUMBER. Default: 0.\n" );
  printf( "  -h             Display help.\n" );
  printf( "  -V             Display the version number.\n" );
  printf( "  -i NUMBER      MiniPOD channel number: [0..11] (default: all).\n" );
  printf( "  -R NUMBER      Receive (RX) MiniPOD number : [1..4] (default: all).\n" );
  printf( "  -T NUMBER      Transmit (TX) MiniPOD number: [1..4] (default: all).\n" );
  printf( "Commands:\n" );
  printf( " disable | disa | dis   Disable selected MiniPOD channels\n" ); 
  printf( " enable  | ena          Enable selected MiniPOD channels\n" ); 
}

// ----------------------------------------------------------------------------
