/*******************************************************************/
/*                                                                 */
/* This is the C++ source code of the flx-reset application        */
/*                                                                 */
/* Author: Markus Joos, CERN                                       */
/*                                                                 */
/* From Nov 2020: maintenance by H.Boterenbrood, Nikhef            */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <strings.h>
#include <iostream>

#if DEBUG_LEVEL > 0
#include "DFDebug/DFDebug.h"
#endif // DEBUG_LEVEL
#include "flxcard/FlxCard.h"
#include "flxcard/FlxException.h"
#include "felixtag.h"

enum reset_command
  {
   UNKNOWN,
   DMA_RESET,
   SOFT_RESET,
   REGISTERS_RESET,
   ADN2814_RESET,
   GTH_RESET,
   LINK_RESET,
   FROMHOST_RESET,
   ALL
  };

void display_help();

// Globals
FlxCard flxCard;

// ----------------------------------------------------------------------------

int main(int argc, char **argv)
{
  int  opt;
  int  card_number;
  int  device_number  = 0;
  int  quad           = -1;
  int  link_rx        = -1;
  int  link_tx        = -1;
  bool cflag          = false;
  bool dflag          = false;
  bool locks_override = false;
  int  try_open_max   = 5;

  if( argc < 2 )
    {
      display_help();
      return 1;
    }

#if DEBUG_LEVEL > 0
  while( (opt = getopt(argc, argv, "Ehc:d:D:Vq:r:s:t:")) != -1 )
#else
  while( (opt = getopt(argc, argv, "Ehc:d:Vq:r:s:t:")) != -1 )
#endif // DEBUG_LEVEL
    {
      switch( opt )
        {
        case 'd':
          dflag = true;
          device_number = atoi(optarg);
          break;

        case 'q':
          quad = atoi(optarg);
          if( quad < 0 || quad > 11 )
            {
              printf( "### -q: Quad number out-of-range [0..11]\n" );
              return 1;
            }
          break;

        case 'r':
          link_rx = atoi(optarg);
          if( link_rx < 0 || link_rx > 47 )
            {
              printf( "### -r: link number out-of-range [0..47]\n" );
              return 1;
            }
          break;

        case 't':
          link_tx = atoi(optarg);
          if( link_tx < 0 || link_tx > 47 )
            {
              printf( "### -t: link number out-of-range [0..47]\n" );
              return 1;
            }
          break;

        case 'c':
          cflag = true;
          card_number = atoi( optarg );
          try {
            // Map card number to a device number
            device_number = FlxCard::card_to_device_number( card_number );
          }
          catch( FlxException &ex ) {
            std::cout << "### Exception thrown: " << ex.what() << std::endl;
            return 1;
          }
          if( device_number < 0 )
            {
              printf( "### -c: Card number %d not in range [0,%u]\n",
                      card_number, FlxCard::number_of_cards()-1 );
              return 1;
            }
          break;

#if DEBUG_LEVEL > 0
        case 'D':
          {
            int debuglevel = atoi(optarg);
            DF::GlobalDebugSettings::setup(debuglevel, DFDB_FELIXCARD);
          }
          break;
#endif // DEBUG_LEVEL

        case 'h':
          display_help();
          return 0;

        case 'V':
          printf( "Version %s\n", FELIX_TAG );
          return 0;

        case 'E':
          locks_override = true;
          break;

        case 's':
          // Number of seconds to try to open the card (i.e. once per second)
          try_open_max = atoi(optarg);
          break;

        default:
          fprintf(stderr, "Usage: flx-reset COMMAND [OPTIONS]\n"
                  "Try flx-reset -h for more information.\n");
          return 1;
        }
    }

  if( optind == argc )
    {
      fprintf(stderr, "No command given\n"
              "Usage: flx-reset COMMAND [OPTIONS]\n"
              "Try flx-reset -h for more information.\n");
      return 1;
    }

  int command = UNKNOWN;
  if( strcasecmp(argv[optind], "ALL") == 0 )
    command = ALL;
  else if( strcasecmp(argv[optind], "DMA") == 0 ||
           strcasecmp(argv[optind], "DMA_RESET") == 0 ||
           strcasecmp(argv[optind], "DMA-RESET") == 0 )
    command = DMA_RESET;
  else if( strcasecmp(argv[optind], "SOFT") == 0 ||
           strcasecmp(argv[optind], "SOFT_RESET") == 0 ||
           strcasecmp(argv[optind], "SOFT-RESET") == 0 )
    command = SOFT_RESET;
  else if( strcasecmp(argv[optind], "FH") == 0 ||
           strcasecmp(argv[optind], "FROMHOST") == 0 )
    command = FROMHOST_RESET;
  else if( strcasecmp(argv[optind], "GTH") == 0 )
    command = GTH_RESET;
  else if( strcasecmp(argv[optind], "LINK") == 0 )
    command = LINK_RESET;
  else if( strcasecmp(argv[optind], "REG") == 0 ||
           strcasecmp(argv[optind], "REGISTERS") == 0 ||
           strcasecmp(argv[optind], "REGISTERS_RESET") == 0 ||
           strcasecmp(argv[optind], "REGISTERS-RESET") == 0 )
    command = REGISTERS_RESET;
  else if( strcasecmp(argv[optind], "ADN") == 0 ||
           strcasecmp(argv[optind], "ADN2814") == 0 )
    command = ADN2814_RESET;

  if( command == UNKNOWN )
    {
      fprintf(stderr, "Unrecognized command '%s'\n", argv[optind]);
      fprintf(stderr, "Usage: flx-reset COMMAND [OPTIONS]\n"
              "Try flx-reset -h for more information.\n");
      return 1;
    }

  try {
    if( command == ALL )
      {
        if( dflag )
          {
            printf("ERROR: You used option -d. For reset ALL you must use -c\n");
            return 1;
          }

        uint32_t locks = (locks_override ? LOCK_NONE : LOCK_ALL);

        // In case of lock issues, retry opening the device every second
        // until successful, for a configurable number of times
        int try_open_count = 0;
        while( true )
          {
            try {
              flxCard.card_open( device_number, locks );
            }
            catch( FlxException &ex ) {
              if( ex.errorId() != FlxException::LOCK_VIOLATION ||
                  try_open_count >= try_open_max )
                // Not a lock issue or persistent lock issue: rethrow exception
                throw ex;
              ++try_open_count;
              printf( "Waiting for locks, retrying... (%d)\n", try_open_count );
              sleep( 1 );
              continue; // Retry card_open()
            }
            break; // Card succesfully opened
          }

        // Card resets
        if( !(flxCard.card_type() == 182 ||
              flxCard.card_type() == 155 ||
              flxCard.card_type() == 128) )
          {
            printf("Resetting ADN2814\n");
            flxCard.i2c_write("ADN2814", 9, 0xff); // ADN2814
            flxCard.i2c_write("ADN2814", 9, 0x00); // ADN2814
          }
        printf("GTH quads 0-11 reset\n");
        flxCard.gth_rx_reset(); // GTH

        printf("Links RX reset\n");
        flxCard.m_bar2->GBT_RX_RESET = 0xFFFFFFFFFFFF;
        usleep( 1000 );
        flxCard.m_bar2->GBT_RX_RESET = 0x0;
        usleep( 100000 );

        // Device resets
        int ndevices = flxCard.cfg_get_reg( REG_NUMBER_OF_PCIE_ENDPOINTS );
        flxCard.card_close();
        for( int devnr=device_number; devnr<(device_number + ndevices); ++devnr)
          {
            locks = (locks_override ? LOCK_NONE : LOCK_ALL);
            flxCard.card_open( device_number, locks );

            printf("DMA reset, device %d\n", devnr);
            flxCard.dma_reset();
            printf("Soft reset, device %d\n", devnr);
            flxCard.soft_reset();
            printf("Register reset, device %d\n", devnr);
            flxCard.registers_reset();
            printf("CR FromHost reset, device %d\n", devnr);
            flxCard.cr_fromhost_reset();
            flxCard.card_close();
          }
      }
    else
      {
        uint32_t locks = (locks_override ? LOCK_NONE : LOCK_ALL);

        // In case of lock issues, retry opening the device every second
        // until successful, for a configurable number of times
        int try_open_count = 0;
        while( true )
          {
            try {
              flxCard.card_open( device_number, locks );
            }
            catch( FlxException &ex ) {
              if( ex.errorId() != FlxException::LOCK_VIOLATION ||
                  try_open_count >= try_open_max )
                // Not a lock issue or persistent lock issue: rethrow exception
                throw ex;
              ++try_open_count;
              printf( "Waiting for locks, retrying... (%d)\n", try_open_count );
              sleep( 1 );
              continue; // Retry card_open()
            }
            break; // Card succesfully opened
          }

        if( command == DMA_RESET )
          {
            if( cflag )
              printf("WARNING: You used option -c. For DMA reset use -d\n");
            flxCard.dma_reset();
            printf("DMA reset, device %d\n", device_number);
          }

        if( command == SOFT_RESET )
          {
            if( cflag )
              printf("WARNING: You used option -c. For soft reset reset use -d\n");
            flxCard.soft_reset();
            printf("Soft reset, device %d\n", device_number);
          }

        if( command == REGISTERS_RESET )
          {
            if( cflag )
              printf("WARNING: You used option -c. For register reset use -d\n");
            flxCard.registers_reset();
            printf("Register reset, device %d\n", device_number);
          }

        if( command == FROMHOST_RESET )
          {
            if( cflag )
              printf("WARNING: You used option -c. For register reset use -d\n");
            flxCard.cr_fromhost_reset();
            printf("Central Router FromHost reset, device %d\n", device_number);
          }

        if( command == ADN2814_RESET )
          {
            if( flxCard.card_type() == 182 ||
                flxCard.card_type() == 155 ||
                flxCard.card_type() == 128 )
              {
                printf( "### No ADN2814 on this FLX-card type\n" );
              }
            else
              {
                if( dflag )
                  printf("WARNING: You used option -d. For ADN2814 reset use -c\n");
                flxCard.i2c_write("ADN2814", 9, 0xff);
                flxCard.i2c_write("ADN2814", 9, 0x00);
              }
          }

        if( command == GTH_RESET )
          {
            if( dflag )
              {
                printf("WARNING: You used option -d. For GTH reset use -c\n");
              }
            else
              {
                flxCard.gth_rx_reset(quad);
                printf("GTH quads reset, device %d\n", device_number);
              }
          }

        if( command == LINK_RESET )
          {
            if( dflag )
              {
                printf("WARNING: You used option -d. For LINK reset use -c\n");
              }
            else
              {
                if( link_rx == -1 && link_tx == -1 )
                  {
                    // Reset just RX...
                    printf( "Links (RX) reset, device %d\n",
                            device_number );
                    flxCard.m_bar2->GBT_RX_RESET = 0xFFFFFFFFFFFF;
                    usleep( 1000 );
                    flxCard.m_bar2->GBT_RX_RESET = 0x0;
                    sleep( 1 );
                  }
                if( link_tx != -1 )
                  {
                    printf( "Link %d (TX) reset, device %d\n",
                            link_tx, device_number );
                    flxCard.m_bar2->GBT_TX_RESET = 1LL << link_tx;
                    usleep( 1000 );
                    flxCard.m_bar2->GBT_TX_RESET = 0x0;
                    sleep( 1 );
                  }
                if( link_rx != -1 )
                  {
                    printf( "Link %d (RX) reset, device %d\n",
                            link_rx, device_number );
                    flxCard.m_bar2->GBT_RX_RESET = 1LL << link_rx;
                    usleep( 1000 );
                    flxCard.m_bar2->GBT_RX_RESET = 0x0;
                    sleep( 1 );
                  }
              }
          }

        flxCard.card_close();
      }
  }
  catch( FlxException &ex ) {
    std::cout << "### ERROR: " << ex.what() << std:: endl;
    return 1;
  }

  return 0;
}

// ----------------------------------------------------------------------------

void display_help()
{
  printf("Usage: flx-reset COMMAND [OPTIONS]\n");
  printf("Tool to reset various resources on the card.\n");
  printf("\nCommands:\n");
  printf("  DMA             Resets the DMA part of the Wupper core.\n");
  printf("  REG | REGISTERS Resets the registers to default values.\n");
  printf("  SOFT            Global application soft reset.\n");
  printf("  FH | FROMHOST   Central Router FromHost reset.\n");
  printf("  GTH             Reset links RX (for FULL mode F/W only).\n");
  printf("                  operates on register GBT_SOFT_RX_RESET_RESET_ALL).\n");
  printf("    -q <n>        The individual quad (0..11) to reset (default: all).\n");
  printf("  LINK            Reset a link or links (RX and/or TX; default all RX;\n");
  printf("                  operates on registers GBT_[RX|TX]_RESET).\n");
  printf("    -r <n>        Individual RX link (0..47) to reset.\n");
  printf("    -t <n>        Individual TX link (0..47) to reset.\n");
  printf("  ADC| ADN2814    Reset the ADN2814 (not on FLX-182/155 or FLX-128).\n");
  printf("  ALL             Do everything. (Note: use -c, not -d:\n");
  printf("                  resets the card and the resources of all devices of that card).\n");
  printf("Options:\n");
  printf("  -d <number>     Use device indicated by <number>\n");
  printf("                  (applies to commands DMA/SOFT/REGISTERS; default: 0).\n");
  printf("  -c <number>     Use card indicated by <number> (default: 0).\n");
#if DEBUG_LEVEL > 0
  printf("  -D <level>      Configure debug output at API level.\n");
  printf("                  0=disabled, 5, 10, 20 progressively more verbose output (default: 0).\n");
#endif // DEBUG_LEVEL
  printf("  -E              Execute the command even if resources are locked\n");
  printf("  -s <secs>       If necessary wait up to <secs> seconds for locks to be freed\n");
  printf("                  when attempting to open the card (default: 5 secs)\n");
  printf("  -h              Display help.\n");
  printf("  -V              Display the version number.\n");
  printf("\nNote: \n");
  printf("Use -c <number> with ADN, FH, GTH, LINK and ALL.\n");
  printf("Use -d <number> with commands DMA, SOFT and REGISTERS.\n");
  printf("If neither -c nor -d are given, card or device number 0 is used as appropriate.\n");
}

// ----------------------------------------------------------------------------
