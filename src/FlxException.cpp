#include "flxcard/FlxException.h"

static const std::string FLX_ERR_STR[] =
  {
   "???",
   "NOTOPENED",
   "MAPERROR",
   "UNMAPERROR",
   "IOCTL",
   "PARAM",
   "I2C",
   "DMA",
   "GBT",
   "REG_ACCESS",
   "HW",
   "LOCK_VIOLATION",
   "CONFIG"
  };

// ----------------------------------------------------------------------------

std::string FlxException::errorString( u_int errorId ) const
{
  // ###NB: no need to have parameter 'errorId', we have member 'm_errorId'
  std::ostringstream oss;
  oss << "(Err=";
  if( m_errorId <= UNDEF_MIN || m_errorId >= UNDEF_MAX )
    oss << std::string( "???" );
  else
    oss << FLX_ERR_STR[m_errorId];
  oss << ") " << std::string( what() );
  return oss.str();
}

// ----------------------------------------------------------------------------
