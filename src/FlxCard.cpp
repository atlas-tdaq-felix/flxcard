/*******************************************************************/
/*                                                                 */
/* This is the C++ source code of the FlxCard object               */
/*                                                                 */
/* Author: Markus Joos, CERN                                       */
/*                                                                 */
/* From Nov 2020: maintenance by H.Boterenbrood, Nikhef            */

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <linux/types.h>
#include <iostream>
#include <fstream>
#include <cstdint>

#if DEBUG_LEVEL > 0
//#include "DFDebug/DFDebug.h"
#endif // DEBUG_LEVEL
#include "flxcard/FlxCard.h"
#include "flxcard/FlxException.h"

// Special: FFLASH_STATIC is defined in the static compilation of the fflash tool: no yaml
#ifndef FFLASH_STATIC
// NOTE: Ignore the deprecated-declarations warning of gcc13, see FLX-2310
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include "yaml-cpp/yaml.h"
#pragma GCC diagnostic pop
#endif // FFLASH_STATIC

#if DEBUG_LEVEL > 0
//#define LOG(txt) DEBUG_TEXT(DFDB_FELIXCARD,1,"FelixCard::"<<__func__<<"() "<<txt)
#define LOG(txt) std::cout<<"FlxCard::"<<__func__<<"() "<<txt<< std::endl
#else
#define LOG(txt)
#endif // DEBUG_LEVEL

// Register FROMHOST_DATA_FORMAT (RM5)
enum
  {
   FH_FORMAT_REGMAP4 = 0,
   FH_FORMAT_5BIT_LENGTH,
   FH_FORMAT_HDR32_PACKET32,
   FH_FORMAT_HDR32_PACKET64,
   FH_FORMAT_HDR32_PACKET32_8B,
   FH_FORMAT_HDR32_PACKET64_8B,
   FH_FORMAT_HDR32_PACKET128_8B
  };

int FlxCard::m_cardsOpen = 0;
static int m_timeout = 0;

// ----------------------------------------------------------------------------
// (NB: don't use '-' in member 'name' (first parameter): gets mapped to '_';
//      maybe change to using just '-' and map '_' to '-'? (Henk)

i2c_device_t i2c_devices_FLX_709[] =
  {
    {"USR_CLK",         "SI570",                    0x5D, {  1,0,0} },
    {"ADN2814",         "ADN2814",                  0x40, {  2,0,0} },
    {"SI5345",          "SI5345",                   0x68, {  2,0,0} },
    {"FMC_TEMP_SENSOR", "TC74 (on CRORC TEST FMC)", 0x4A, {  2,0,0} },
    {"ID_EEPROM",       "M24C08-WDW6TP",            0x54, {  8,0,0} },
    {"SFP1_1",          "AFBR-709SMZ (Conven Mem)", 0x50, { 16,1,0} },
    {"SFP1_2",          "AFBR-709SMZ (Enhan Mem)",  0x51, { 16,1,0} },
    {"SFP2_1",          "AFBR-709SMZ (Conven Mem)", 0x50, { 16,2,0} },
    {"SFP2_2",          "AFBR-709SMZ (Enhan Mem)",  0x51, { 16,2,0} },
    {"SFP3_1",          "AFBR-709SMZ (Conven Mem)", 0x50, { 16,4,0} },
    {"SFP3_2",          "AFBR-709SMZ (Enhan Mem)",  0x51, { 16,4,0} },
    {"SFP4_1",          "AFBR-709SMZ (Conven Mem)", 0x50, { 16,8,0} },
    {"SFP4_2",          "AFBR-709SMZ (Enhan Mem)",  0x51, { 16,8,0} },
    {"DDR3_1",          "SRAM-MT8KTF51264HZ",       0x51, { 64,0,0} },
    {"DDR3_2",          "SRAM-MT8KTF51264HZ",       0x52, { 64,0,0} },
    {"SI5324",          "SI5324",                   0x68, {128,0,0} },
    {NULL,              NULL,                       0,    {  0,0,0} }
  };

i2c_device_t i2c_devices_FLX_710[] =
  {
    {"CLOCK_RAM",       "ICS8N4Q001L IDT",          0x6E, {  1,0,0} },
    {"CLOCK_SYS",       "ICS8N4Q001L IDT",          0x6E, {  2,0,0} },
    {"CLOCK_CXP1",      "IDT 8N3Q001",              0x6E, {  4,0,0} },
    {"CLOCK_CXP2",      "IDT 8N3Q001",              0x6E, {  8,0,0} },
    {"ADN2814",         "ADN2814 (on TTCfx FMC)",   0x40, { 16,0,0} },
    {"SI5345",          "SI5345",                   0x68, { 16,0,0} },
    {"FMC_TEMP_SENSOR", "TC74 (on CRORC TEST FMC)", 0x4A, { 16,0,0} },
    {"CXP1_TX",         "AFBR-83PDZ",               0x50, { 32,0,0} },
    {"CXP1_RX",         "AFBR-83PDZ",               0x54, { 32,0,0} },
    {"CXP2_TX",         "AFBR-83PDZ",               0x50, { 64,0,0} },
    {"CXP2_RX",         "AFBR-83PDZ",               0x54, { 64,0,0} },
    {"DDR3_1",          "SRAM-MT16JTF25664HZ",      0x50, {128,0,0} },
    {"DDR3_2",          "SRAM-MT16JTF25664HZ",      0x51, {128,0,0} },
    {NULL,              NULL,                       0,    {  0,0,0} }
  };

i2c_device_t i2c_devices_FLX_711[] =
  {
    {"ADN2814",         "ADN2814",                  0x40, {2,0,0} },
    {"SIS53154",        "SI53154",                  0x6B, {2,0,0} },
    {"LTC2991_1",       "LTC2991",                  0x48, {2,0,0} },
    {"LTC2991_2",       "LTC2991",                  0x49, {2,0,0} },
    {"SI5345",          "SI5345",                   0x68, {4,0,0} },
    {"IO_EXPANDER",     "TCA6408A",                 0x20, {4,0,0} },
    {"1ST TX",          "MiniPOD AFBR-814",         0x2C, {8,0,0} },
    {"2ND TX",          "MiniPOD AFBR-814",         0x2D, {8,0,0} },
    {"3RD TX",          "MiniPOD AFBR-814",         0x2E, {8,0,0} },
    {"4TH TX",          "MiniPOD AFBR-814",         0x2F, {8,0,0} },
    {"1ST RX",          "MiniPOD AFBR-824",         0x30, {8,0,0} },
    {"2ND RX",          "MiniPOD AFBR-824",         0x31, {8,0,0} },
    {"3RD RX",          "MiniPOD AFBR-824",         0x32, {8,0,0} },
    {"4TH RX",          "MiniPOD AFBR-824",         0x33, {8,0,0} },
    {NULL,              NULL,                       0,    {0,0,0} }
  };

int MINIPOD_CNT = 8;
// RXes followed by TXes
const char *POD_NAMES[] = { "1st RX", "2nd RX", "3rd RX", "4th RX",
                            "1st TX", "2nd TX", "3rd TX", "4th TX" };
// Each RX followed by TX, alternating
//const char *POD_NAMES[] = { "1st TX", "1st RX", "2nd TX", "2nd RX",
//                            "3rd TX", "3rd RX", "4th TX", "4th RX" };
const char *LTC_NAMES[] = { "LTC2991-1", "LTC2991-2" };


i2c_device_t i2c_devices_FLX_128[] =
  {
    {"IO_EXPANDER",         "TCA6416A",             0x20, { 0,0,0} },
    {"PMBUS_VCCINT",        "INA226",               0x40, { 0,1,0} },
    {"PMBUS_VCCBRAM",       "INA226",               0x41, { 0,1,0} },
    {"PMBUS_VCCVCC1V8",     "INA226",               0x42, { 0,1,0} },
    {"PMBUS_MGTAVCC",       "INA226",               0x46, { 0,1,0} },
    {"PMBUS_MGTAVTT",       "INA226",               0x47, { 0,1,0} },
    {"PMBUS_MGTAVCC",       "INA226",               0x48, { 0,1,0} },
    {"PMBUS_VCCHBM",        "INA226",               0x4C, { 0,1,0} },
    {"PMBUS_VCCAUX_HBM",    "INA226",               0x4D, { 0,1,0} },
    {"MAIN_PMBUS_SYS1V8",   "ISL91211",             0x60, { 0,4,0} },
    {"MAIN_PMBUS_UTIL_1V35","ISL91302",             0x61, { 0,4,0} },
    {"MAIN_PMBUS_QDR_1V3",  "ISL91302",             0x62, { 0,4,0} },
    {"MAIN_PMBUS_VCC_VADJ", "ISL91302",             0x63, { 0,4,0} },
    {"MAIN_PMBUS_VDDQ",     "ISL91302",             0x64, { 0,4,0} },
    {"MAIN_PMBUS_VCCINT",   "ISL68127",             0x65, { 0,4,0} },
    {"MAIN_PMBUS_VCCHBM",   "ISL68301",             0x68, { 0,4,0} },
    {"MAIN_PMBUS_MGTAVTT",  "ISL68301",             0x69, { 0,4,0} },
    {"MAIN_PMBUS_UTIL_3V3", "ISL68301",             0x6A, { 0,4,0} },
    {"MAIN_PMBUS_UTIL_5V0", "ISL68301",             0x6B, { 0,4,0} },
    {"IIC_EEPROM",          "M24C08",               0x54, { 1,0,0} },
    {"SI5328",              "SI5328",               0x68, { 2,0,0} },
    {"QSFP1_SI570",         "SI570",                0x5D, { 8,0,0} },
    {"QSFP2_SI570",         "SI570",                0x5D, {16,0,0} },
    {"QSFP3_SI570",         "SI570",                0x5D, {32,0,0} },
    {"QSFP4_SI570",         "SI570",                0x5D, {64,0,0} },
    {"QSFP1_I2C",           "QSFP",                 0x50, { 0,0,16} },
    {"QSFP2_I2C",           "QSFP",                 0x50, { 0,0,32} },
    {"QSFP3_I2C",           "QSFP",                 0x50, { 0,0,64} },
    {"QSFP4_I2C",           "QSFP",                 0x50, { 0,0,128} },
    {NULL,                  NULL,                   0,    { 0,0,0} }
  };

i2c_device_t i2c_devices_FLX_182[] =
  {
    {"POWER_SEQ_A_1066",    "ADM1066",              0x34, { 4,0,0} },
    {"POWER_SEQ_B_1066",    "ADM1066",              0x35, { 4,0,0} },
    {"POWER_SEQ_1266",      "ADM1266",              0x40, { 4,0,0} },
    {"POWER_GEN",           "LTM4700",              0x4E, { 1,0,0} },

    {"POWER_12P0V",         "INA226",               0x40, { 1,0,0} },
    {"POWER_VCCINT",        "INA226",               0x41, { 1,0,0} },
    {"POWER_MGTAVCC",       "INA226",               0x42, { 1,0,0} },
    {"POWER_MGTAVTT",       "INA226",               0x43, { 1,0,0} },
    {"POWER_SYS12",         "INA226",               0x44, { 1,0,0} },
    {"POWER_SYS15",         "INA226",               0x45, { 1,0,0} },
    {"POWER_SYS18",         "INA226",               0x46, { 1,0,0} },
    {"POWER_SYS25",         "INA226",               0x47, { 1,0,0} },
    {"POWER_SYS33",         "INA226",               0x48, { 1,0,0} },
    {"POWER_SYS38",         "INA226",               0x49, { 1,0,0} },

    {"TEMP_MGTAVCC",        "TMP435",               0x48, { 2,0,0} },
    {"TEMP_MGTAVTT",        "TMP435",               0x49, { 2,0,0} },
    {"TEMP_SYS12",          "TMP435",               0x4A, { 2,0,0} },
    {"TEMP_SYS15",          "TMP435",               0x4B, { 2,0,0} },
    {"TEMP_SYS18",          "TMP435",               0x4C, { 2,0,0} },
    {"TEMP_SYS33",          "TMP435",               0x4D, { 2,0,0} },
    {"TEMP_LTM4642",        "TMP435",               0x4E, { 2,0,0} },

    {"SI570_200MHZ_SYS",    "SI570",                0x60, { 4,0,0} },
    {"SI570_40MHZ_5345A",   "SI570",                0x60, { 8,0,0} },
    {"SI570_100MHZ_SYS",    "SI570",                0x55, { 8,0,0} },
    {"SI570_40MHZ_5345B",   "SI570",                0x60, { 16,0,0} },
    {"SI570_240MHZ_LTI",    "SI570",                0x60, { 32,0,0} },
    {"SI570_322MHZ_100GBE", "SI570",                0x60, { 64,0,0} },
    {"SI570_30MHZ_VERSAL",  "SI570",                0x5D, { 128,0,0} },
    {"SI5345A",             "SI5345",               0x68, { 128,0,0} },
    {"SI5345B",             "SI5345",               0x69, { 128,0,0} },
    {"SI5345A_BETA",        "SI5345",               0x7C, { 128,0,0} },
    {"SI5345B_BETA",        "SI5345",               0x7D, { 128,0,0} },

    {"IO_EXPANDER_A",       "TCA6424",              0x22, { 128,0,0} },
    {"IO_EXPANDER_B",       "TCA6424",              0x23, { 128,0,0} },

    {"FIREFLY_TX1",         "FIREFLY-TX",           0x50, { 16,0,0} },
    {"FIREFLY_RX1",         "FIREFLY-RX",           0x54, { 16,0,0} },
    {"FIREFLY_TX2",         "FIREFLY-TX",           0x50, { 32,0,0} },
    {"FIREFLY_RX2",         "FIREFLY-RX",           0x54, { 32,0,0} },
    {"FIREFLY_TXRX",        "FIREFLY-TXRX",         0x50, { 64,0,0} },
    {NULL,                  NULL,                   0,    { 0,0,0} }
  };

std::vector<std::string> INA226_NAMES_182 = { "POWER_12P0V",   "POWER_VCCINT",
                                              "POWER_MGTAVCC", "POWER_MGTAVTT",
                                              "POWER_SYS12",   "POWER_SYS15",
                                              "POWER_SYS18",   "POWER_SYS25",
                                              "POWER_SYS33",   "POWER_SYS38" };
float INA226_RSHUNT_182[] = { 1.5, 0.25, 1.0, 1.0, 1.0,
                              1.0, 1.0, 1.0,  1.0, 1.0 };

std::vector<std::string> TMP435_NAMES_182 = { "TEMP_MGTAVCC", "TEMP_MGTAVTT",
                                              "TEMP_SYS12",   "TEMP_SYS15",
                                              "TEMP_SYS18",   "TEMP_SYS33",
                                              "TEMP_LTM4642" };

std::vector<std::string> ADM1066_NAMES_182 = { "POWER_SEQ_A_1066", "POWER_SEQ_B_1066" };
std::vector<std::string> ADM1266_NAMES_182 = { "POWER_SEQ_1266" };

std::vector<std::string> FF_NAMES_TX_182   = { "FIREFLY_TX1", "FIREFLY_TX2" };
std::vector<std::string> FF_NAMES_RX_182   = { "FIREFLY_RX1", "FIREFLY_RX2" };
std::vector<std::string> FF_NAMES_TXRX_182 = { "FIREFLY_TXRX" };
std::vector<std::string> FF_NAMES_182      = { "FIREFLY_TX1", "FIREFLY_RX1",
                                               "FIREFLY_TX2", "FIREFLY_RX2",
                                               "FIREFLY_TXRX" };

i2c_device_t i2c_devices_FLX_155[] =
  {
    {"POWER_SEQ",           "ADM1266",              0x40, { 1,0,0} },
    {"POWER_GEN",           "LTM4700",              0x4E, { 2,0,0} },

    {"POWER_12P0V",         "INA226",               0x40, { 4,0,0} },
    {"POWER_VCCINT",        "INA226",               0x41, { 4,0,0} },
    {"POWER_MGTAVCC",       "INA226",               0x42, { 4,0,0} },
    {"POWER_MGTAVTT",       "INA226",               0x43, { 4,0,0} },
    {"POWER_VCC1V2",        "INA226",               0x44, { 8,0,0} },
    {"POWER_VCC1V5",        "INA226",               0x45, { 8,0,0} },
    {"POWER_VCC1V8",        "INA226",               0x46, { 8,0,0} },
    {"POWER_VCC2V5",        "INA226",               0x47, { 16,0,0} },
    {"POWER_VCC3V3",        "INA226",               0x48, { 16,0,0} },
    {"POWER_VCC3V8",        "INA226",               0x49, { 16,0,0} },
    {"POWER_VCC0V88",       "INA226",               0x4A, { 16,0,0} },

    {"TEMP_MGTAVCC",        "TMP435",               0x48, { 32,0,0} },
    {"TEMP_MGTAVTT",        "TMP435",               0x49, { 32,0,0} },
    {"TEMP_MGTAVTT2",       "TMP435",               0x4A, { 32,0,0} },
    {"TEMP_VCC1V5",         "TMP435",               0x4B, { 32,0,0} },
    {"TEMP_VCC1V8",         "TMP435",               0x4C, { 64,0,0} },
    {"TEMP_VCC3V3",         "TMP435",               0x4D, { 64,0,0} },
    {"TEMP_VCC2V5_3V8",     "TMP435",               0x4E, { 64,0,0} },
    {"TEMP_VCC0V88",        "TMP435",               0x4F, { 64,0,0} },

    {"IO_EXPANDER_A",       "TCA6424",              0x20, { 128,0,0} },
    {"IO_EXPANDER_B",       "TCA6424",              0x21, { 128,0,0} },

    {"FIREFLY1_TX",         "FIREFLY-TX",           0x50, { 4,0,0} },
    {"FIREFLY1_RX",         "FIREFLY-RX",           0x54, { 4,0,0} },
    {"FIREFLY2_TX",         "FIREFLY-TX",           0x50, { 8,0,0} },
    {"FIREFLY2_RX",         "FIREFLY-RX",           0x54, { 8,0,0} },
    {"FIREFLY3_TX",         "FIREFLY-TX",           0x50, { 16,0,0} },
    {"FIREFLY3_RX",         "FIREFLY-RX",           0x54, { 16,0,0} },
    {"FIREFLY4_TX",         "FIREFLY-TX",           0x50, { 32,0,0} },
    {"FIREFLY4_RX",         "FIREFLY-RX",           0x54, { 32,0,0} },
    {"FIREFLY5_TXRX",       "FIREFLY-TXRX",         0x50, { 64,0,0} },
    {"FIREFLY6_TXRX",       "FIREFLY-TXRX",         0x50, { 128,0,0} },

    {"SI570_30M_VERSAL",    "SI570",                0x5D, { 128,0,0} },
    {"SI570_200M_SYS",      "SI570",                0x60, { 128,0,0} },
    {"SI570_156M25_1",      "SI570",                0x60, { 4,0,0} },
    {"SI570_156M25_2",      "SI570",                0x60, { 8,0,0} },
    {"SI570_240M_TTC_REF",  "SI570",                0x60, { 16,0,0} },
    {"SI570_322M_100G_REF", "SI570",                0x60, { 32,0,0} },
    {"SI569_OSC_WR_1",      "SI569",                0x55, { 64,0,0} },
    {"SI569_OSC_WR_2",      "SI569",                0x55, { 128,0,0} },
    {"SI5345A",             "SI5345",               0x68, { 64,0,0} },
    {"SI5345B",             "SI5345",               0x69, { 128,0,0} },

    {"DDR4_MINIDIMM",       "DDR4",                 0x53, { 128,0,0} },
    {"EEPROM",              "M24128",               0x54, { 0,0,0} },

    {NULL,                  NULL,                   0,    { 0,0,0} }
  };

std::vector<std::string> INA226_NAMES_155 = { "POWER_12P0V",   "POWER_VCCINT",
                                              "POWER_MGTAVCC", "POWER_MGTAVTT",
                                              "POWER_VCC1V2",  "POWER_VCC1V5",
                                              "POWER_VCC1V8",  "POWER_VCC2V5",
                                              "POWER_VCC3V3",  "POWER_VCC3V8",
                                              "POWER_VCC0V88" };
float INA226_RSHUNT_155[] = { 1.5, 0.25, 1.0, 1.0, 1.0,
                              1.0, 1.0, 1.0,  1.0, 1.0, 1.0 };

std::vector<std::string> TMP435_NAMES_155 = { "TEMP_MGTAVCC", "TEMP_MGTAVTT",
                                              "TEMP_MGTAVTT2",
                                              "TEMP_VCC1V5"   "TEMP_VCC1V8",
                                              "TEMP_VCC3V3"   "TEMP_VCCV2V5_3V8",
                                              "TEMP_VCC0V88" };

std::vector<std::string> ADM1266_NAMES_155 = { "POWER_SEQ" };

std::vector<std::string> FF_NAMES_TX_155   = { "FIREFLY1_TX",   "FIREFLY2_TX",
                                               "FIREFLY3_TX",   "FIREFLY4_TX" };
std::vector<std::string> FF_NAMES_RX_155   = { "FIREFLY1_RX",   "FIREFLY2_RX",
                                               "FIREFLY3_RX",   "FIREFLY4_RX", };
std::vector<std::string> FF_NAMES_TXRX_155 = { "FIREFLY1_TXRX", "FIREFLY2_TXRX" };
std::vector<std::string> FF_NAMES_155      = { "FIREFLY1_TX",   "FIREFLY1_RX",
                                               "FIREFLY2_TX",   "FIREFLY2_RX",
                                               "FIREFLY3_TX",   "FIREFLY3_RX",
                                               "FIREFLY4_TX",   "FIREFLY4_RX",
                                               "FIREFLY1_TXRX", "FIREFLY2_TXRX" };
// ----------------------------------------------------------------------------

void watchdogtimer_handler( int /*signum*/ )
{
  m_timeout = 1;
  LOG( "Timer expired. m_timeout = " << m_timeout );
}

// ----------------------------------------------------------------------------

FlxCard::FlxCard()
  : m_fd( -1 ),
    m_deviceNumber( -1 ),
    m_maxTlpBytes( 256 ),
    m_cardType( 0 ),
    m_firmwareType( 99 ),
    m_configRead( false ),
    m_lpgbtType( false ),
    m_lpgbtFec12( false ),
    m_lpgbt5Gbps( false ),
    m_fullmodeType( false ),
    m_numberOfChans( 0 ),
    m_numberOfDma( 0 ),
    m_fromHostDataFormat( FH_FORMAT_REGMAP4 ),
    m_myLocks( 0 ),
    m_myLockTag( 0 ),
    m_bar0Base( 0 ),
    m_bar1Base( 0 ),
    m_bar2Base( 0 ),
    m_bar0( 0 ),
    m_bar1( 0 ),
    m_bar2( 0 ),
    m_i2cDevices( 0 )
{
}

// ----------------------------------------------------------------------------

FlxCard::~FlxCard()
{
  if( m_fd != -1 )
    this->card_close();
}

// ----------------------------------------------------------------------------

void FlxCard::card_open( int device_nr, u_int lock_mask,
                         bool read_config, bool ignore_version )
{
  LOG( "called for device " << device_nr
       << " with lock_mask " << TOHEX(lock_mask)
       << " (" << lockbits2string(lock_mask) << ")" );

  // Install a signal handler for the implementation of watchdog timers
  // Note: The signal handler should be installed only once
  if( m_cardsOpen == 0 )
    {
      LOG( "Installing signal handler" );
      struct sigaction sa;

      // Install timer_handler as the signal handler for SIGALRM
      memset( &sa, 0, sizeof(sa) );
      sa.sa_handler = watchdogtimer_handler;
      sigaction( SIGALRM, &sa, NULL );
    }
  else
    {
      LOG( "Signal handler already installed" );
    }

  char nodename[30];
  sprintf( &nodename[0], "/dev/flx%d", device_nr );
  LOG( "nodename = " << nodename );

  m_fd = open( nodename, O_RDWR );

  if( m_fd < 0 )
    {
      if( m_cardsOpen == 0 )
        sigaction(SIGALRM, NULL, NULL); // Uninstall signal handler

      LOG( "Failed to open " << nodename);
      THROW_FLX_EXCEPTION(NOTOPENED, "Failed to open " << nodename);
    }
  ++m_cardsOpen;

  card_params_t card_data;
  card_data.slot      = device_nr;
  card_data.lock_mask = lock_mask;

  int iores = ioctl(m_fd, SETCARD, &card_data);
  LOG( "card_data.lock_error  = " << card_data.lock_error);
  LOG( "sizeof(card_params_t) = " << sizeof(card_params_t));

  if( iores < 0 )
    {
      // Clean up...
      close( m_fd );
      m_fd = -1;
      if( m_cardsOpen == 1 )
        sigaction(SIGALRM, NULL, NULL); // Uninstall signal handler
      --m_cardsOpen;
      // ...and exit
      LOG( "Error from ioctl(SETCARD)" );
      THROW_FLX_EXCEPTION(IOCTL, "Error from ioctl(SETCARD)");
    }
  if( card_data.lock_error )
    {
      // Clean up...
      close( m_fd );
      m_fd = -1;
      if( m_cardsOpen == 1 )
        sigaction(SIGALRM, NULL, NULL); // Uninstall signal handler
      --m_cardsOpen;
      // ...and exit
      LOG( "Error from ioctl(SETCARD); "
            "requested resources locked; card_data.lock_error = "
            << TOHEX(card_data.lock_error) );
      THROW_FLX_EXCEPTION(LOCK_VIOLATION, "Some requested resources locked: "
                          << TOHEX(card_data.lock_error)
                          << " (" << lockbits2string(card_data.lock_error) << ")" );
    }

  m_myLockTag = card_data.lock_tag;
  LOG( "m_myLockTag is " << m_myLockTag );

  m_myLocks = lock_mask;
  m_deviceNumber = device_nr;

  m_bar0Base = map_memory_bar( card_data.baseAddressBAR0, 4096 );
  m_bar1Base = map_memory_bar( card_data.baseAddressBAR1, 4096 );
  m_bar2Base = map_memory_bar( card_data.baseAddressBAR2, 65536 );

  m_bar0 = (flxcard_bar0_regs_t *) m_bar0Base;
  m_bar1 = (flxcard_bar1_regs_t *) m_bar1Base;
  m_bar2 = (flxcard_bar2_regs_t *) m_bar2Base;

  LOG( "Obtaining max. TLP size from driver" );
  int tlp_bits;
  iores = ioctl( m_fd, GET_TLP, &tlp_bits );
  if( iores < 0 )
    {
      // Clean up...
      unmap_memory_bar( m_bar0Base, 4096 );
      unmap_memory_bar( m_bar1Base, 4096 );
      unmap_memory_bar( m_bar2Base, 65536 );
      close( m_fd );
      m_fd = -1;
      if( m_cardsOpen == 1 )
        sigaction(SIGALRM, NULL, NULL); // Uninstall signal handler
      --m_cardsOpen;
      // ...and exit
      LOG( "Error from ioctl(GET_TLP)" );
      THROW_FLX_EXCEPTION(IOCTL, "Error from ioctl(GET_TLP)");
    }

  m_maxTlpBytes = 128 << tlp_bits; // MJ: replace 128 by a constant?

  // Does the RM (register model) of the firmware match the RM of the software?
  u_long regmap_version_fw_major = m_bar2->REG_MAP_VERSION >> 8 & 0xff;
  u_long regmap_version_sw_major = REGMAP_VERSION >> 8 & 0xff;
  if( regmap_version_sw_major != regmap_version_fw_major )
    {
      if( !ignore_version )
        {
          // Clean up...
          unmap_memory_bar( m_bar0Base, 4096 );
          unmap_memory_bar( m_bar1Base, 4096 );
          unmap_memory_bar( m_bar2Base, 65536 );
          close( m_fd );
          m_fd = -1;
          if( m_cardsOpen == 1 )
            sigaction(SIGALRM, NULL, NULL); // Uninstall signal handler
          --m_cardsOpen;
          // ...and exit
          LOG( "FW Regmap = " << regmap_version_fw_major
               << " but SW Regmap = " << regmap_version_sw_major );
          THROW_FLX_EXCEPTION(HW, "Regmap versions of HW (" << regmap_version_fw_major
                              << ") and SW (" << regmap_version_sw_major << ") do not match");
        }
      else
        {
          fprintf( stderr, "### WARNING: Regmap versions of HW (%lu) and SW (%lu) do not match\n",
                   regmap_version_fw_major, regmap_version_sw_major );
          // Continuing anyway, at your own risk...
        }
    }

  // Read out and store some basic info about the firmware
  m_cardType      = m_bar2->CARD_TYPE;
  m_firmwareType  = m_bar2->FIRMWARE_MODE;
  m_numberOfChans = m_bar2->NUM_OF_CHANNELS;
#if REGMAP_VERSION < 0x500
  m_numberOfDma   = m_bar2->GENERIC_CONSTANTS.DESCRIPTORS - 1;
  m_fromHostDataFormat = FH_FORMAT_REGMAP4;
#else
  m_numberOfDma   = m_bar2->GENERIC_CONSTANTS.TOHOST_DESCRIPTORS;
  m_fromHostDataFormat = m_bar2->FROMHOST_DATA_FORMAT;
#endif // REGMAP_VERSION
  // GBT type are: FIRMW_GBT, FIRMW_LTDB, FIRMW_FEI4, FIRMW_FELIG(_GBT)
  m_lpgbtType = (m_firmwareType == FIRMW_LPGBT ||
                 m_firmwareType == FIRMW_PIXEL ||
                 m_firmwareType == FIRMW_STRIP ||
                 m_firmwareType == FIRMW_FELIG_LPGBT ||
                 m_firmwareType == FIRMW_FELIG_PIXEL ||
                 m_firmwareType == FIRMW_HGTD_LUMI   ||
                 m_firmwareType == FIRMW_BCM_PRIME);
  m_fullmodeType = (m_firmwareType == FIRMW_FULL  ||
                    m_firmwareType == FIRMW_FMEMU ||
                    m_firmwareType == FIRMW_MROD  ||
                    m_firmwareType == FIRMW_INTERLAKEN );
#if REGMAP_VERSION >= 0x500
  if( m_lpgbtType )
    {
      // Make it a global (all links) setting
      // eventhough the registers are 48-bit significant
      m_lpgbtFec12 = (m_bar2->LPGBT_FEC != 0);
      m_lpgbt5Gbps = (m_bar2->LPGBT_DATARATE != 0);
    }
#endif // REGMAP_VERSION

  if( m_cardType == 712 || m_cardType == 711 )
    // 711 and 712 have the same I2C devices
    m_i2cDevices = i2c_devices_FLX_711;
  else if( m_cardType == 710 )
    m_i2cDevices = i2c_devices_FLX_710;
  else if( m_cardType == 709 )
    m_i2cDevices = i2c_devices_FLX_709;
  else if( m_cardType == 128 )
    m_i2cDevices = i2c_devices_FLX_128;
  else if( m_cardType == 182 )
    m_i2cDevices = i2c_devices_FLX_182;
  else if( m_cardType == 155 )
    m_i2cDevices = i2c_devices_FLX_155;
  else
    m_i2cDevices = 0;

  // Compile a firmware type/version string
  std::ostringstream oss;
  uint64_t ep  = m_bar2->NUMBER_OF_PCIE_ENDPOINTS;
  uint64_t chn = m_bar2->NUM_OF_CHANNELS;
  uint64_t ts  = m_bar2->BOARD_ID_TIMESTAMP;
  uint64_t git = m_bar2->GIT_TAG;
  uint64_t cmt = m_bar2->GIT_COMMIT_NUMBER;
  oss << "FLX" << m_cardType << "-" << firmware_type_string() << "-"
      << ep << "x" << chn << "CH-"
      << std::hex << (ts >> 16) << "-" << (ts & 0xFFFF);
  oss << "-GIT:";
  char ch;
  for( int i=0; i<8; ++i, git>>=8 )
    {
      ch = (char) (git & 0xFF);
      if( ch == 0 ) break;
      oss << ch;
    }
  oss << "/" << std::dec << cmt;
  m_firmwareString = oss.str();

  if( read_config )
    // Read (and locally store) the device's current (e-)link configuration
    this->readConfiguration();

  LOG( "done with m_cardsOpen = " << m_cardsOpen );
}

// ----------------------------------------------------------------------------

void FlxCard::card_close()
{
  LOG( "called" );

  if( m_fd < 0 )
    {
      LOG( "not open" );
      //THROW_FLX_EXCEPTION(NOTOPENED, "The link to the driver is already closed "
      //                    "(or has never been opened)");
      return;
    }

  // Uninstall the signal handler when we close the last instance
  if( m_cardsOpen == 1 )
    {
      LOG( "Uninstall signal handler" );
      sigaction(SIGALRM, NULL, NULL); // Uninstall signal handler
    }
  --m_cardsOpen;
  LOG( "m_cardsOpen = " << m_cardsOpen );

  LOG( "returning locks for tag = "
         << m_myLockTag << ", device " << m_deviceNumber);

  lock_params_t lockparams;
  lockparams.lock_tag = m_myLockTag;
  lockparams.slot     = m_deviceNumber;
  int iores = ioctl(m_fd, RELEASELOCK, &lockparams);
  if( iores < 0 )
    {
      LOG( "Error from ioctl(RELEASELOCK)" );
      THROW_FLX_EXCEPTION(IOCTL, "Error from ioctl(RELEASELOCK)");
    }

  unmap_memory_bar(m_bar0Base, 4096);
  unmap_memory_bar(m_bar1Base, 4096);
  unmap_memory_bar(m_bar2Base, 65536);
  m_bar0Base = 0;
  m_bar1Base = 0;
  m_bar2Base = 0;
  m_bar0     = 0;
  m_bar1     = 0;
  m_bar2     = 0;

  close( m_fd );
  m_fd = -1;
}

// ----------------------------------------------------------------------------

u_int FlxCard::lock_mask( int device_nr )
{
  LOG( "called" );

  bool close_it = false;
  if( m_fd < 0 )
    {
      LOG( "Open device" );

      char nodename[30];
      sprintf( &nodename[0], "/dev/flx%d", device_nr );
      LOG( "nodename = " << nodename );

      m_fd = open(nodename, O_RDWR);
      if( m_fd < 0 )
        {
          LOG( "Failed to open " << nodename );
          THROW_FLX_EXCEPTION(NOTOPENED, "Failed to open " << nodename);
        }
      card_params_t card_data;
      card_data.slot      = device_nr;
      card_data.lock_mask = 0;

      int iores = ioctl(m_fd, SETCARD, &card_data);
      if( iores < 0 )
        {
          LOG( "Error from ioctl(SETCARD)" );
          THROW_FLX_EXCEPTION(IOCTL, "Error from ioctl(SETCARD)");
        }

      close_it = true;
    }

  // Usage of "val":
  // in call to the driver : device number
  // returned by the driver: resource lock bits of the device
  u_int val = device_nr;
  int iores = ioctl( m_fd, GETLOCK, &val );
  if( iores < 0 )
    {
      LOG( "Error from ioctl(GETLOCK)" );
      THROW_FLX_EXCEPTION(IOCTL, "Error from ioctl(GETLOCK)");
    }

  LOG( "Global locks for device " << device_nr << " = " << TOHEX(val) );
  LOG( "Local locks for device " << m_deviceNumber << " = " << TOHEX(m_myLocks) );

  // If applicable, remove any *locally assigned* lock bits
  u_int locks = val;
  if( device_nr == m_deviceNumber )
    locks = val ^ m_myLocks;

  LOG( "Locks for device " << device_nr << " = " << TOHEX(locks) );

  if( close_it )
    {
      LOG( "Close device" );
      close( m_fd );
      m_fd = -1;
    }

  return locks;
}

// ----------------------------------------------------------------------------

u_int FlxCard::number_of_cards()
{
  try {
    device_list_t devlist = FlxCard::device_list();
    int devicezero_cnt = 0;
    for( u_int index=0; index<devlist.n_devices; ++index )
      {
        if( devlist.cdmap[index][1] == 0 )
          ++devicezero_cnt;
      }
    return devicezero_cnt;
  }
  catch( FlxException &ex ) {
    return 0;
  }
}

// ----------------------------------------------------------------------------

u_int FlxCard::number_of_devices()
{
  LOG( "called" );

  // Note: we always open /dev/flx0 (and not one of /dev/flx[n])
  //       because we want to get information for all cards / devices
  int fd = open( "/dev/flx0", O_RDWR );
  if( fd < 0 )
    {
      LOG( "Failed to open /dev/flx0" );
      return 0;
    }

  u_int cdmap[MAXCARDS][2];
  int iores = ioctl(fd, GETCARDS, &cdmap);
  if( iores < 0 )
    {
      LOG( "Error from ioctl(GETCARDS)" );
      close(fd);
      return 0;
    }

  int deviceCount = 0;
  u_int index;
  for( index=0; index<MAXCARDS; ++index )
    {
      if( cdmap[index][0] != 0 )
        {
          LOG( "cdmap[" << index << "] = "
                << TOHEX(cdmap[index][0]) << "," << cdmap[index][1] );
          ++deviceCount;
        }
    }

  close(fd);

  return deviceCount;
}

// ----------------------------------------------------------------------------

device_list_t FlxCard::device_list()
{
  LOG( "called" );

  // Note: we always open /dev/flx0 (and not one of /dev/flx[n])
  //       because we want to get information for all cards / devices
  int fd = open( "/dev/flx0", O_RDWR );
  if( fd < 0 )
    {
      LOG( "Failed to open /dev/flx0" );
      THROW_FLX_EXCEPTION(NOTOPENED, "Failed to open /dev/flx0" );
    }

  u_int cdmap[MAXCARDS][2];
  int iores = ioctl( fd, GETCARDS, &cdmap );
  if( iores < 0 )
    {
      LOG( "Error from ioctl(GETCARDS)" );
      close(fd);
      THROW_FLX_EXCEPTION(IOCTL, "Error from ioctl(GETCARDS)" );
    }

  device_list_t devlist;
  int deviceCount = 0;
  u_int index;
  for( index=0; index<MAXCARDS; ++index )
    {
      devlist.cdmap[index][0] = cdmap[index][0];
      devlist.cdmap[index][1] = cdmap[index][1];
      if( cdmap[index][0] != 0 )
        {
          ++deviceCount;
          LOG( "cdmap[" << index << "] = "
                << TOHEX(cdmap[index][0]) << "," << cdmap[index][1] );
        }
    }
  devlist.n_devices = deviceCount;

  close( fd );

  return devlist;
}

// ----------------------------------------------------------------------------

int FlxCard::card_to_device_number( int card_number )
{
  // Maps a FELIX card number to a FELIX device number taking into account
  // that a FLX-712 consists of 2 devices and a 709 of 1 device
  // (this information is returned in the 'device list');
  // the card number maps to the first device of a dual-device card;
  // returns -1 if the card number is out-of-range or invalid
  int device_number = -1;
  if( card_number > 0 )
    {
      device_list_t devlist = FlxCard::device_list();
      int devicezero_cnt = 0;
      for( u_int index=0; index<devlist.n_devices; ++index )
        {
          if( devlist.cdmap[index][1] == 0 )
            {
              if( devicezero_cnt == card_number )
                {
                  // Found the first device (0) of the requested card number
                  device_number = index;
                  break;
                }
              ++devicezero_cnt;
            }
        }
    }
  else if( card_number == 0 )
    {
      device_number = 0;
    }
  return device_number;
}

// ----------------------------------------------------------------------------

void FlxCard::dma_stop( u_int dma_id )
{
  LOG( "called" );
  m_bar0->DMA_DESC_ENABLE &= ~(1 << dma_id);
}

// ----------------------------------------------------------------------------

void FlxCard::dma_to_host( u_int dma_id, u_long dst, size_t size, u_int flags )
{
  LOG( "called with dma_id = " << dma_id
         << ", dst = " << TOHEX(dst) << ", size = " << size << ", flags = " << flags );
  dma_stop(dma_id);

  if( dst == 0 || size == 0 )
    {
      LOG( "dst or size is zero" );
      THROW_FLX_EXCEPTION(PARAM, "dma_to_host(): dst or size is zero");
    }

  if( (size % m_maxTlpBytes) != 0 )
    {
      LOG( "size is not a multiple of TLP" );
      THROW_FLX_EXCEPTION(PARAM, "dma_to_host(): size is not a multiple of TLP");
    }

  volatile dma_descriptor_t *pdma = &m_bar0->DMA_DESC[dma_id];
  pdma->start_address = dst;
  pdma->end_address   = dst + size;
  pdma->tlp           = m_maxTlpBytes/4;
  LOG( "m_bar0->DMA_DESC[" << dma_id << "].tlp = " << m_maxTlpBytes/4 );
  pdma->fromhost      = 0;
  pdma->wrap_around   = (flags & FLX_DMA_WRAPAROUND) ? 1 : 0;
  pdma->sw_pointer    = dst;

  if(m_bar0->DMA_DESC_STATUS[dma_id].even_addr_pc == m_bar0->DMA_DESC_STATUS[dma_id].even_addr_dma)
    {
      // Make 'even_addr_pc' unequal to 'even_addr_dma', or a (circular) DMA won't start!?
      pdma->sw_pointer -= 1;
      pdma->sw_pointer += 1;
    }

  m_bar0->DMA_DESC_ENABLE |= 1 << dma_id;
  LOG( "DMA started" );
}

// ----------------------------------------------------------------------------

void FlxCard::dma_from_host( u_int dma_id, u_long src, size_t size, u_int flags )
{
  LOG( "called" );
  dma_stop(dma_id);

  if( src == 0 || size == 0 )
    {
      LOG( "src or size is zero" );
      THROW_FLX_EXCEPTION(PARAM, "dma_from_host(): src or size is zero");
    }

  if( (size % 32) != 0 )
    {
      LOG( "size is not a multiple of 32 bytes" );
      THROW_FLX_EXCEPTION(PARAM, "dma_from_host(): size is not a multiple of 32 bytes");
    }

  /*
  u_int best_tlp = m_maxTlpBytes;
  LOG( "first best_tlp = " << best_tlp );
  while(size % best_tlp)
    {
      best_tlp = best_tlp >> 1;
      LOG( "new best_tlp = " << best_tlp );
    }
  LOG( "size " << size << " best_tlp = " << best_tlp );

  // If the start address is not a multiple of 4096 bytes, we have to make sure
  // that the TLP does not cross a 4k boundary (see FLX-937)
  while( (4096-(src & 4095)) % best_tlp )
    {
      best_tlp = best_tlp >> 1;
      LOG( "new best_tlp = " << best_tlp );
    }
  */

  volatile dma_descriptor_t *pdma = &m_bar0->DMA_DESC[dma_id];
  // To make sure each GBT-SCA command (fits in 32 bytes) is sent without delay
  // must set TLP size to 32 bytes in case of circular DMA (FLX-896);
  // in case of single-shot DMA there is an issue starting a DMA whose start address is not
  // aligned to the TLP size and crosses a 4K memory address boundary (FLX-937)
  // so for the time being FromHost TLP is set to 32 bytes only (Henk B, 16 Apr 2019)
  // User provided FromHost TLP parameter? (overruling the default setting)
  if( flags & 0xFFFF0000 )
    {
      int tlp = (flags & 0xFFFF0000) >> 16;
      if( tlp > m_maxTlpBytes )
        tlp = m_maxTlpBytes;
      pdma->tlp = tlp/4;
    }
  else
    {
      // Default FromHost TLP setting
      //if( (flags & FLX_DMA_WRAPAROUND) != 0 ) {
      if( m_fromHostDataFormat == FH_FORMAT_HDR32_PACKET128_8B )
        pdma->tlp = 128/4;
      else if( m_fromHostDataFormat == FH_FORMAT_HDR32_PACKET64 ||
               m_fromHostDataFormat == FH_FORMAT_HDR32_PACKET64_8B )
        pdma->tlp = 64/4;
      else
        pdma->tlp = 32/4;
      //} else {
      //  pdma->tlp = best_tlp/4; }
    }
  pdma->start_address = src;
  pdma->end_address   = src + size;
  pdma->fromhost      = 1;
  pdma->wrap_around   = (flags & FLX_DMA_WRAPAROUND) ? 1 : 0;
  pdma->sw_pointer    = src;

  m_bar0->DMA_DESC_ENABLE |= 1 << dma_id;
}

// ----------------------------------------------------------------------------

bool FlxCard::dma_enabled( u_int dma_id )
{
  return( (m_bar0->DMA_DESC_ENABLE & (1 << dma_id)) != 0 );
}

// ----------------------------------------------------------------------------

void FlxCard::dma_wait( u_int dma_id )
{
  // Set up watchdog
  m_timeout = 0;
  struct itimerval timer;
  timer.it_value.tv_sec     = 1; // One second
  timer.it_value.tv_usec    = 0;
  timer.it_interval.tv_sec  = 0;
  timer.it_interval.tv_usec = 0; // Only one shot
  setitimer(ITIMER_REAL, &timer, NULL);

  LOG( "called for dma_id = " << dma_id );
  while(m_bar0->DMA_DESC_ENABLE & (1 << dma_id))
    {
      LOG( "m_timeout = " << m_timeout
           << " m_bar0->DMA_DESC_ENABLE = " << m_bar0->DMA_DESC_ENABLE );

      if( m_timeout )
        {
          LOG( "ERROR: Timeout" );
          THROW_FLX_EXCEPTION(DMA, "Timeout wait-for-DMA");
        }
    }
  // Stop watchdog
  timer.it_value.tv_usec = 0;
  timer.it_value.tv_sec = 0;
  setitimer(ITIMER_REAL, &timer, NULL);
  LOG( "Done" );
}

// ----------------------------------------------------------------------------

u_long FlxCard::dma_get_fw_ptr( u_int dma_id )
{
  LOG( "called with dma_id = " << dma_id );
  return(m_bar0->DMA_DESC_STATUS[dma_id].fw_pointer);
}

// ----------------------------------------------------------------------------

bool FlxCard::dma_cmp_even_bits( u_int dma_id )
{
  u_long *ulp, offset, lvalue;

  // NOTE: I am not using the flxcard_bar0_regs_t structure
  //       because the EVEN_PC and EVEN_DMA bits must read with one PCI cycle.
  //       This may not be guaranteed by accessing the fields of the structure.

  LOG( "called with dma_id = " << dma_id );

  // Note: This trick is to get the offset of the first STATUS register;
  //       that way we are safe against changes in the register model
  offset = (u_long)&m_bar0->DMA_DESC_STATUS[0] - (u_long)&m_bar0->DMA_DESC[0];
  LOG( "offset = " << TOHEX(offset) );
  ulp = (u_long *)(m_bar0Base + offset);
  // Note: ulp[0] = FW_POINTER of channel 0, ulp[1] = status bits of channel 0,
  //       ulp[2] = FW_POINTER of channel 1, etc
  //lvalue = ulp[1 + (dma_id * 2)];
  //LOG( "lvalue = " << TOHEX(lvalue) );
  //u_int b1 = (lvalue >> 1) & 0x1;
  //u_int b2 = (lvalue >> 2) & 0x1;
  //LOG( "EVEN_PC = " << b2 << ", EVEN_DMA = " << b1 );
  //return( b1 == b2 );

  // The above in a more compact (and faster?) way
  lvalue = ulp[1 + (dma_id * 2)] & 6;   // Extract the 2 'EVEN' bits
  return( lvalue == 6 || lvalue == 0 ); // Return true if they are equal
}

// ----------------------------------------------------------------------------

void FlxCard::dma_advance_ptr( u_int dma_id, u_long dst, size_t size, size_t bytes )
{
  LOG( "called" );

  u_long tmp_sw_pointer = m_bar0->DMA_DESC[dma_id].sw_pointer;
  tmp_sw_pointer += bytes;
  if( tmp_sw_pointer >= dst + size )
    tmp_sw_pointer -= size;

  m_bar0->DMA_DESC[dma_id].sw_pointer = tmp_sw_pointer;
}

// ----------------------------------------------------------------------------

void FlxCard::dma_set_ptr( u_int dma_id, u_long dst )
{
  LOG( "called" );
  m_bar0->DMA_DESC[dma_id].sw_pointer = dst;
}

// ----------------------------------------------------------------------------

u_long FlxCard::dma_get_ptr( u_int dma_id )
{
  LOG( "called" );
  return m_bar0->DMA_DESC[dma_id].sw_pointer;
}

// ----------------------------------------------------------------------------
// MJ: The method below has been temporarily disabled on the request of Frans
// because the DMA_FIFO_FLUSH register is currently not implemented

//void FlxCard::dma_fifo_flush()
//{
//  LOG( "called" );
//  m_bar0->DMA_FIFO_FLUSH = 1;
//}

// ----------------------------------------------------------------------------

void FlxCard::dma_reset()
{
  LOG( "called" );
  m_bar0->DMA_RESET = 1;
}

// ----------------------------------------------------------------------------

void FlxCard::soft_reset()
{
  LOG( "called" );
  m_bar0->SOFT_RESET = 1;
}

// ----------------------------------------------------------------------------

void FlxCard::registers_reset()
{
  LOG( "called" );
  m_bar0->REGISTERS_RESET = 1;
}

// ----------------------------------------------------------------------------

void FlxCard::cr_fromhost_reset()
{
#if REGMAP_VERSION >= 0x500
  LOG( "called" );
  m_bar2->CRFROMHOST_RESET = 1;
#endif // REGMAP_VERSION
}

// ----------------------------------------------------------------------------

void FlxCard::gth_rx_reset( int quad )
{
  uint64_t reset_mask = 0;

  if( quad == -1 )
    reset_mask = 0xFFF;
  else if( quad > -1 && quad < 12 )
    reset_mask = 1ULL << quad;

  LOG( "called, quad = " << quad << ", reset mask = " << reset_mask );

  m_bar2->GBT_SOFT_RX_RESET.RESET_ALL = reset_mask;
  m_bar2->GBT_SOFT_RX_RESET.RESET_ALL = 0;
  m_bar2->GBT_GENERAL_CTRL = 2;
  sleep(5);
  m_bar2->GBT_GENERAL_CTRL = 0;
}

// ----------------------------------------------------------------------------

void FlxCard::i2c_write( const char *device_str, u_char reg_addr, u_char data )
{
  LOG( "called, device_str=" << device_str << " reg_addr=" << (u_int) reg_addr );

  u_char switch1_val = 0, switch2_val = 0, switch3_val = 0, dev_addr = 0;
  int result = i2c_parse_address_string( device_str,
                                         &switch1_val, &switch2_val, &switch3_val,
                                         &dev_addr );
  if( result == 0 )
    {
      i2c_set_switches( switch1_val, switch2_val, switch3_val );

      // Write 1-byte device register
      i2c_write_byte( dev_addr, reg_addr, data );

      LOG( "dev_addr=" << (u_int) dev_addr
           << " reg_addr=" << (u_int) reg_addr
           << " data="    << (u_int) data << " ("
           << " switch1=" << (u_int) switch1_val
           << " switch2=" << (u_int) switch2_val
           << " switch3=" << (u_int) switch3_val << ")" );
    }
  else
    {
      if( result == I2C_DEVICE_ERROR_INVALID_PORT )
        {
          LOG( "Invalid I2C port" );
          THROW_FLX_EXCEPTION(I2C, "Invalid I2C port");
        }
      if( result == I2C_DEVICE_ERROR_INVALID_ADDRESS )
        {
          LOG( "Invalid I2C address" );
          THROW_FLX_EXCEPTION(I2C, "Invalid I2C address");
        }

      LOG( "I2C device \"" << device_str << "\" unknown" );
      THROW_FLX_EXCEPTION(I2C, "I2C device \"" << device_str << "\" unknown");
    }
}

// ----------------------------------------------------------------------------

void FlxCard::i2c_write( const char *device_str, u_char reg_addr, u_char *data, int nbytes )
{
  LOG( "called, device_str=" << device_str << " reg_addr=" << (u_int) reg_addr
       << "nbytes=" << nbytes );

  u_char switch1_val = 0, switch2_val = 0, switch3_val = 0, dev_addr = 0;
  int result = i2c_parse_address_string( device_str,
                                         &switch1_val, &switch2_val, &switch3_val,
                                         &dev_addr );
  if( result == 0 )
    {
      i2c_set_switches( switch1_val, switch2_val, switch3_val );

      // Write 'nbytes'-byte device register
      i2c_write_bytes( dev_addr, reg_addr, nbytes, data );
    }
  else
    {
      if( result == I2C_DEVICE_ERROR_INVALID_PORT )
        {
          LOG( "Invalid I2C port" );
          THROW_FLX_EXCEPTION(I2C, "Invalid I2C port");
        }
      if( result == I2C_DEVICE_ERROR_INVALID_ADDRESS )
        {
          LOG( "Invalid I2C address" );
          THROW_FLX_EXCEPTION(I2C, "Invalid I2C address");
        }

      LOG( "I2C device \"" << device_str << "\" unknown" );
      THROW_FLX_EXCEPTION(I2C, "I2C device \"" << device_str << "\" unknown");
    }
}

// ----------------------------------------------------------------------------

void FlxCard::i2c_read( const char *device_str, u_char reg_addr, u_char *value, int nbytes )
{
  LOG( "called, device_str=" << device_str << " reg_addr=" << (u_int) reg_addr );

  u_char switch1_val = 0, switch2_val = 0, switch3_val = 0, dev_addr = 0;
  int result = i2c_parse_address_string( device_str,
                                         &switch1_val, &switch2_val, &switch3_val,
                                         &dev_addr );
  if( result == 0 )
    {
      i2c_set_switches( switch1_val, switch2_val, switch3_val );

      // Read 1-byte device register
      //*value = i2c_read_byte( dev_addr, reg_addr );
      // Read 'nbytes' device register
      i2c_read_bytes( dev_addr, reg_addr, nbytes, value );

      LOG( "dev_addr=" << (u_int) dev_addr
           << " reg_addr=" << (u_int) reg_addr
           << " value="   << (u_int) *value << " ("
           << " switch1=" << (u_int) switch1_val
           << " switch2=" << (u_int) switch2_val
           << " switch3=" << (u_int) switch3_val << ")" );
    }
  else
    {
      if( result == I2C_DEVICE_ERROR_INVALID_PORT )
        {
          LOG( "Invalid I2C port" );
          THROW_FLX_EXCEPTION(I2C, "Invalid I2C port");
        }
      if( result == I2C_DEVICE_ERROR_INVALID_ADDRESS )
        {
          LOG( "Invalid I2C address" );
          THROW_FLX_EXCEPTION(I2C, "Invalid I2C address");
        }

      LOG( "I2C device \"" << device_str << "\" unknown" );
      THROW_FLX_EXCEPTION(I2C, "I2C device \"" << device_str << "\" unknown");
    }
}

// ----------------------------------------------------------------------------

void FlxCard::i2c_write_byte( u_char dev_addr, u_char byte )
{
  LOG( "i2c_write_byte(dev_addr, byte) called" );

  i2c_wait_not_full();

  u_long value = 0; // I2C write
  value |= ((u_long) dev_addr << 1);
  value |= ((u_long) byte << 8);
  cfg_set_reg( REG_I2C_WR, value );
  usleep( I2C_DELAY );
}

// ----------------------------------------------------------------------------

void FlxCard::i2c_write_byte( u_char dev_addr, u_char reg_addr, u_char byte )
{
  LOG( "i2c_write_byte(dev_addr, reg_addr, byte) called" );

  i2c_wait_not_full();

  u_long value = 0; // I2C write
  value |= ((u_long) dev_addr << 1);
  value |= ((u_long) reg_addr << 8);
  value |= ((u_long) byte << 16);
  value |= (1 << 24); // 'Write two bytes'
  cfg_set_reg( REG_I2C_WR, value );
  usleep( I2C_DELAY );
}

// ----------------------------------------------------------------------------

void FlxCard::i2c_write_bytes( uint8_t dev_addr, uint8_t reg_addr,
                               int nbytes, uint8_t *bytes )
{
  LOG( "called" );

  i2c_wait_not_full();

  // In FELIX firmware can write 1 or 2 bytes (to the given register address)
  u_long value = 0; // I2C write
  value |= ((u_long) dev_addr << 1);
  value |= ((u_long) reg_addr << 8);
  value |= ((u_long) bytes[0] << 16);
  if( nbytes > 1 )
    {
      value |= ((u_long) bytes[1] << 27);
      value |= (1 << 26); // 'Write three bytes' (incl reg address)
    }
  else
    {
      value |= (1 << 24); // 'Write two bytes' (incl reg address)
    }
  cfg_set_reg( REG_I2C_WR, value );
  usleep( I2C_DELAY );
}

// ----------------------------------------------------------------------------

u_char FlxCard::i2c_read_byte( u_char dev_addr, u_char reg_addr )
{
  LOG( "called, dev_addr=" << (u_int) dev_addr << " reg_addr=" << (u_int) reg_addr );

  i2c_wait_not_full();

  u_long value = 1; // I2C read
  value |= ((u_long) dev_addr << 1);
  value |= ((u_long) reg_addr << 8);
  value |= (1 << 24); // 'Write two bytes' (Means something else in a read op)
  cfg_set_reg( REG_I2C_WR, value );

  i2c_wait_not_empty();
  cfg_set_reg( REG_I2C_RD, 1 );
  usleep( I2C_DELAY );
  u_long reg_val = cfg_get_reg( REG_I2C_RD );

  LOG( "reg_val = " << TOHEX(reg_val) <<
       " (I2C_EMPTY_FLAG = " << TOHEX(I2C_EMPTY_FLAG) << ")" );

  if( (reg_val & I2C_EMPTY_FLAG) == 0 )
    {
      u_int count;
      i2c_flush( &count );
      LOG( "I2C_EMPTY_FLAG *not* set" <<
           ", device=" << TOHEX(dev_addr) << " reg=" << TOHEX(reg_addr) <<
           ", flushed " << count << " bytes" );
      THROW_FLX_EXCEPTION(I2C, "i2c_read_byte(): I2C_EMPTY_FLAG *not* set" <<
                          ", device=" << TOHEX(dev_addr) << " reg=" << TOHEX(reg_addr) );
    }
  return( (u_char) (reg_val & 0xFF) );
}

// ----------------------------------------------------------------------------

void FlxCard::i2c_read_bytes( uint8_t dev_addr, uint8_t reg_addr,
                              int nbytes, uint8_t *bytes )
{
  LOG( "called" );

  i2c_wait_not_full();

  // In FELIX firmware can read 1 or 2 bytes (from the given register address)
  u_long value = 1; // I2C read
  value |= ((u_long) dev_addr << 1);
  value |= ((u_long) reg_addr << 8);
  value |= (1 << 24); // 'Write two bytes' (Means something else in a read op)
  if( nbytes > 1 )
    value |= (1 << 26); // Read 2 bytes from given register address
  cfg_set_reg( REG_I2C_WR, value );

  i2c_wait_not_empty();

  // Read 'nbytes' register bytes
  for( int i=0; i<nbytes; ++i )
    {
      cfg_set_reg( REG_I2C_RD, 1 );
      usleep( I2C_DELAY );
      uint32_t val = cfg_get_reg( REG_I2C_RD );
      bytes[i] = (uint8_t) (val & 0xFF);

      // Check I2C FIFO status against expected
      if( i != nbytes-1 && (val & I2C_EMPTY_FLAG) )
        {
          // Not last byte: EMPTY not expected yet
          LOG( "bytes:" << nbytes << ", I2C_EMPTY_FLAG, but more expected" <<
               ", device=" << TOHEX(dev_addr) << " reg=" << TOHEX(reg_addr) );
          THROW_FLX_EXCEPTION(I2C, "i2c_read_bytes(" << nbytes <<
                              "): I2C_EMPTY_FLAG, but more expected" <<
                              ", device=" << TOHEX(dev_addr) << " reg=" << TOHEX(reg_addr) );
        }
      else if( i == nbytes-1 && (val & I2C_EMPTY_FLAG) == 0 )
        {
          // Last byte: expected EMPTY
          u_int count;
          i2c_flush( &count );
          LOG( "bytes:" << nbytes << ", I2C_EMPTY_FLAG *not* set" <<
               ", device=" << TOHEX(dev_addr) << " reg=" << TOHEX(reg_addr) <<
               ", flushed " << count << " bytes" );
          THROW_FLX_EXCEPTION(I2C, "i2c_read_bytes(" << nbytes <<
                              "): I2C_EMPTY_FLAG *not* set" <<
                              ", device=" << TOHEX(dev_addr) << " reg=" << TOHEX(reg_addr) <<
                              ", flushed " << count << " bytes" );
        }
    }
}

// ----------------------------------------------------------------------------

bool FlxCard::i2c_flush( u_int *count )
{
  LOG( "called" );
  u_long reg_val = cfg_get_reg( REG_I2C_RD );

  LOG( "reg_val = " << TOHEX(reg_val) <<
       " (I2C_EMPTY_FLAG = " << TOHEX(I2C_EMPTY_FLAG) << ")" );

  // Flush up to 16 bytes
  u_int cnt = 0;
  while( (reg_val & I2C_EMPTY_FLAG) == 0 && cnt < 16 )
    {
      cfg_set_reg( REG_I2C_RD, 1 );
      usleep( I2C_DELAY );
      reg_val = cfg_get_reg( REG_I2C_RD );
      LOG( "Flushed I2C byte: " << TOHEX(reg_val & 0xFF) );
      ++cnt;
    }
  if( count )
    *count = cnt;
  return( (reg_val & I2C_EMPTY_FLAG) != 0 );
}

// ----------------------------------------------------------------------------

u_int FlxCard::gbt_setup( int alignment, int channel_mode )
{
  LOG( "called" );
  LOG( "---General initialization" );

  // Initialize the GBT
  // 1- Set (TX & RX)USR ready
  LOG( "***Set TxUsrRdy & RxUsrRdy. Registers GBT_TXUSRRDY..." );

  m_bar2->GBT_TXUSRRDY = ALL_BITS;
  m_bar2->GBT_RXUSRRDY = ALL_BITS;

  LOG( "Done!" );

  // 2- GTH Quads Soft Reset
  LOG( "***Soft reset of the Quads. Register GBT_PLL_RESET..." );
  m_bar2->GBT_PLL_RESET.QPLL_RESET = 0xFFF;
  usleep(WAIT_TIME_600);
  m_bar2->GBT_PLL_RESET.QPLL_RESET = 0x000;
  sleep(2);
  LOG( "Done!" );

  LOG( "Writing delay values..." );
  m_bar2->GBT_TX_TC_DLY_VALUE1 = 0x333333333333;
  m_bar2->GBT_TX_TC_DLY_VALUE2 = 0x333333333333;
  m_bar2->GBT_TX_TC_DLY_VALUE3 = 0x333333333333;
  m_bar2->GBT_TX_TC_DLY_VALUE4 = 0x333333333333;
  LOG( "Done!" );

  LOG( "---Configuring GBT TX..." );
  gbt_tx_configuration(channel_mode, alignment);

  LOG( "---Configuring GBT RX..." );
  u_int unaligned_channels = 0;
  unaligned_channels = gbt_rx_configuration(channel_mode);

  LOG( "Done! unaligned_channels = " << unaligned_channels );

  return unaligned_channels;
}

// ----------------------------------------------------------------------------

long int FlxCard::gbt_version_delay( u_long fw_version, char *filename )
{
  LOG( "called" );

  char file_default[] = "../flx_propagation_delay.conf";
  char *fileused;
  if(filename == NULL)
    fileused = file_default;
  else
    fileused = filename;

  u_long delay = 0;
  FILE *file_delay = fopen(fileused , "r");
  if( file_delay != NULL )
    {
      int control = 0;
      u_long f_version = 0, f_delay = 0;
      char data[1024]="\0";
      while(control == 0 && fgets(data, 1024, file_delay) != NULL)
        {
          if(check_digic_value2(data, &f_version, &f_delay) == 0)
            {
              if(f_version == fw_version)
                {
                  delay = f_delay;
                  control = 1;
                }
            }
        }
      fclose(file_delay);

      if(control == 0)
        delay = FLX_GBT_VERSION_NOT_FOUND;
    }
  else
    {
      delay = FLX_GBT_FILE_NOT_FOUND;
    }

  return delay;
}

// ----------------------------------------------------------------------------

regmap_register_t *FlxCard::cfg_register( const char *name )
{
  // Find register with name <name> (case-insensitive, '_' may be '-'),
  // return pointer to the struct with info
  std::string namestr( name );
  for( size_t i=0; i<namestr.size(); ++i )
    {
      namestr[i] = toupper( namestr[i] );
      if( namestr[i] == '-' ) namestr[i] = '_';
    }

  regmap_register_t *reg;
  for( reg = regmap_registers; reg->name != NULL; ++reg )
    {
      if( strcmp(namestr.data(), reg->name) == 0 )
        return reg;
    }
  return NULL;
}

// ----------------------------------------------------------------------------

regmap_bitfield_t *FlxCard::cfg_bitfield( const char *name )
{
  // Find bitfield with name <name> (case-insensitive, '_' may be '-'),
  // return pointer to the struct with info
  std::string namestr( name );
  for( size_t i=0; i<namestr.size(); ++i )
    {
      namestr[i] = toupper( namestr[i] );
      if( namestr[i] == '-' ) namestr[i] = '_';
    }

  regmap_bitfield_t *bf;
  for( bf = regmap_bitfields; bf->name != NULL; ++bf )
    {
      if( strcmp(namestr.data(), bf->name) == 0 )
        return bf;
    }
  return NULL;
}

// ----------------------------------------------------------------------------

std::string FlxCard::cfg_bitfield_options( const char *name,
                                           bool include_all_substr )
{
  // See if 'name' is a substring of one or more bitfields,
  // compile a list of bitfield name options and return it;
  // with 'include_all_substr' false, show only options that *start* with 'name'
  std::string namestr( name );
  for( size_t i=0; i<namestr.size(); ++i )
    {
      namestr[i] = toupper( namestr[i] );
      if( namestr[i] == '-' ) namestr[i] = '_';
    }

  size_t len = namestr.size();
  std::ostringstream oss;
  int cnt = 0;
  if( len >= 2 ) // Require at least 2 chars to compile an options list
    {
      // Check for bitfield names that match the substring from start of string
      regmap_bitfield_t *bf;
      for( bf = regmap_bitfields; bf->name != NULL; ++bf )
        {
          if( strncmp(namestr.data(), bf->name, len) == 0 )
            {
              ++cnt;
              // Compiling a list of options
              oss << "  " << bf->name << std::endl;
            }
        }
      // Check if the substring is contained anywhere else within a bitfield
      // name, avoiding duplicates (condition: pos != 0)
      if( include_all_substr )
        {
          for( bf = regmap_bitfields; bf->name != NULL; ++bf )
            {
              std::string n = std::string( bf->name );
              size_t pos = n.find( namestr );
              if( pos != std::string::npos && pos != 0 )
                {
                  ++cnt;
                  // Compiling a list of options
                  oss << "  " << bf->name << std::endl;
                }
            }
        }
    }
  if( cnt >= 1 )
    {
      // Found one or more options: return the compiled list
      return oss.str();
    }
  return std::string();
}

// ----------------------------------------------------------------------------

u_long FlxCard::cfg_get_reg( const char *key )
{
  LOG( "register = " << key );

  regmap_register_t *reg = cfg_register( key );
  if( reg )
    {
      if( !(reg->flags & REGMAP_REG_READ) )
        {
          LOG( "Register " << key << " not readable!" );
          LOG( "reg->flags   = " << TOHEX(reg->flags) );
          LOG( "reg->address = " << TOHEX(reg->address) );
          LOG( "reg->name    = " << reg->name );
          LOG( "REGMAP_REG_READ = " << TOHEX(REGMAP_REG_READ) );
          THROW_FLX_EXCEPTION(REG_ACCESS, "Register not readable!" );
        }
      u_long *v = (u_long *)(m_bar2Base + reg->address);
      return(*v);
    }

  LOG( "Register \"" << key << "\" unknown" );
  THROW_FLX_EXCEPTION(REG_ACCESS, "Register \"" << key << "\" unknown" );
}

// ----------------------------------------------------------------------------

u_long FlxCard::cfg_get_option( const char *key, bool show_options )
{
  LOG( "key = " << key );

  regmap_bitfield_t *bf = cfg_bitfield( key );
  if( bf )
    {
      LOG( "bitfield found" );
      if( !(bf->flags & REGMAP_REG_READ) )
        {
          LOG( "Bitfield " << key << " not readable!" );
          THROW_FLX_EXCEPTION(REG_ACCESS, "Bitfield " << key << " not readable!");
        }

      LOG( "m_bar2Base = " << TOHEX(m_bar2Base) );
      LOG( "bf->address  = " << TOHEX(bf->address) );

      u_long *v = (u_long *)(m_bar2Base + bf->address);
      u_long regvalue = *v;
      LOG( "regvalue(1)  = " << TOHEX(regvalue) );
      regvalue = (regvalue & bf->mask) >> bf->shift;
      LOG( "bf->shift    = " << TOHEX(bf->shift) );
      LOG( "bf->mask     = " << TOHEX(bf->mask) );
      LOG( "regvalue(2)  = " << TOHEX(regvalue) );
      LOG( "end of method" );
      return regvalue;
    }

  if( show_options )
    {
      printf( "### Field name \"%s\" not found", key );
      std::string str = cfg_bitfield_options( key );
      if( !str.empty() )
        printf( ", suggestions are:\n%s", str.c_str() );
      else
        printf( "\n" );
    }

  LOG( "Bitfield \"" << key << "\" unknown" );
  THROW_FLX_EXCEPTION(REG_ACCESS, "Bitfield \"" << key << "\" unknown");
}

// ----------------------------------------------------------------------------

void FlxCard::cfg_set_reg( const char *key, u_long value )
{
  LOG( "register = " << key << ", value = " << value );

  regmap_register_t *reg = cfg_register( key );
  if( reg )
    {
      if( !(reg->flags & REGMAP_REG_WRITE) )
        {
          LOG( "Register \"" << key << "\" not writeable!" );
          THROW_FLX_EXCEPTION(REG_ACCESS, "Register \"" << key << "\" not writeable!");
        }
      u_long *v = (u_long *)(m_bar2Base + reg->address);
      *v = value;
      return;
    }

  LOG( "Register \"" << key << "\" unknown" );
  THROW_FLX_EXCEPTION(REG_ACCESS, "Register \"" << key << "\" unknown");
}

// ----------------------------------------------------------------------------

void FlxCard::cfg_set_option( const char *key, u_long value, bool show_options )
{
  LOG( "key = " << key << ", value = " << value );

  regmap_bitfield_t *bf = cfg_bitfield( key );
  if( bf )
    {
      if( !(bf->flags & REGMAP_REG_WRITE) )
        {
          LOG( "Bitfield " << key << " not writeable!" );
          THROW_FLX_EXCEPTION(REG_ACCESS, "Bitfield " << key << " not writeable!");
        }

      // Check if provided value is in range
      u_long max = bf->mask >> bf->shift;
      if( value > max && max > 0 )
        {
          LOG( "Bitfield " << key << ": value " << value
               << " out-of-range [0,0x" << std::hex << max << "]" );
          THROW_FLX_EXCEPTION(REG_ACCESS, "Bitfield " << key << ": value " << value
                              << std::hex << " (0x" << value << ") out-of-range [0,0x"
                              << std::uppercase << max << "]" );
        }

      u_long *v = (u_long *)(m_bar2Base + bf->address);

      u_long regvalue = *v;
      regvalue &=~ bf->mask;
      regvalue |= (value << bf->shift) & bf->mask;

      *v = regvalue;
      return;
    }

  if( show_options )
    {
      printf( "### Field name \"%s\" not found", key );
      std::string str = cfg_bitfield_options( key );
      if( !str.empty() )
        printf( ", suggestions are:\n%s", str.c_str() );
      else
        printf( "\n" );
    }

  LOG( "Bitfield \"" << key << "\" unknown" );
  THROW_FLX_EXCEPTION(REG_ACCESS, "Bitfield \"" << key << "\" unknown");
}

// ----------------------------------------------------------------------------

void FlxCard::irq_enable( u_int interrupt )
{
  LOG( "called for interrupt " << interrupt );

  if( interrupt == ALL_IRQS )
    {
      LOG( "Enabling all interrupts" );
      u_int i;
      for( i = 0; i < NUM_INTERRUPTS; ++i )
        {
          // Enable the interrupt by direct access to the register in the FLX card
          m_bar1->INT_TAB_ENABLE |= (1 << i);

          // ...and tell the driver the interrupt is denabled
          int iores = ioctl(m_fd, UNMASK_IRQ, &i);
          if( iores < 0 )
            {
              LOG( "Error from ioctl(UNMASK_IRQ)" );
              THROW_FLX_EXCEPTION(IOCTL, "Error from ioctl(UNMASK_IRQ)");
            }
        }
    }
  else
    {
      // Enable the interrupt by direct access to the register in the FLX card
      m_bar1->INT_TAB_ENABLE |= (1 << interrupt);

      // ...and tell the driver the interrupt is enabled
      int iores = ioctl(m_fd, UNMASK_IRQ, &interrupt);
      if( iores < 0 )
        {
          LOG( "Error from ioctl(UNMASK_IRQ)" );
          THROW_FLX_EXCEPTION(IOCTL, "Error from ioctl(UNMASK_IRQ)");
        }
    }
}

// ----------------------------------------------------------------------------

void FlxCard::irq_disable( u_int interrupt )
{
  LOG( "called for interrupt " << interrupt );

  if( interrupt == ALL_IRQS )
    {
      LOG( "Disabling all interrupts" );
      u_int i;
      for( i = 0; i < NUM_INTERRUPTS; ++i )
        {
          // Disable the interrupt by direct access to the register in the FLX card
          m_bar1->INT_TAB_ENABLE &= ~(1 << i);

          // ...and tell the driver the interrupt is disabled
          int iores = ioctl(m_fd, MASK_IRQ, &i);
          if( iores < 0 )
            {
              LOG( "Error from ioctl(MASK_IRQ)" );
              THROW_FLX_EXCEPTION(IOCTL, "Error from ioctl(MASK_IRQ)");
            }
        }
    }
  else
    {
      LOG( "111 " );
      // Disable the interrupt by direct access to the register in the FLX card
      m_bar1->INT_TAB_ENABLE &= ~(1 << interrupt);

      // ...and tell the driver the interrupt is disabled
      LOG( "222 " );

      int iores = ioctl(m_fd, MASK_IRQ, &interrupt);
      LOG( "333 " );
      if( iores < 0 )
        {
          LOG( "Error from ioctl(MASK_IRQ)" );
          THROW_FLX_EXCEPTION(IOCTL, "Error from ioctl(MASK_IRQ)");
        }
      LOG( "444 ");
    }
  LOG( "done for interrupt " << interrupt );
}

// ----------------------------------------------------------------------------

void FlxCard::irq_wait( u_int interrupt )
{
  LOG( "called for interrupt " << interrupt );

  int iores = ioctl(m_fd, WAIT_IRQ, &interrupt);
  if( iores < 0 )
    {
      LOG( "Error from ioctl(WAIT_IRQ)" );
      THROW_FLX_EXCEPTION(IOCTL, "Error from ioctl(WAIT_IRQ)");
    }
}

// ----------------------------------------------------------------------------

void FlxCard::irq_reset_counters( u_int interrupt )
{
  LOG( "called for interrupt " << interrupt );

  if( interrupt == ALL_IRQS )
    {
      LOG( "Clearing all interrupt counters" );
      u_int i;
      for( i = 0; i < NUM_INTERRUPTS; ++i )
        {
          int iores = ioctl(m_fd, RESET_IRQ_COUNTERS, &i);
          if( iores < 0 )
            {
              LOG( "Error from ioctl(RESET_IRQ_COUNTERS)" );
              THROW_FLX_EXCEPTION(IOCTL, "Error from ioctl(RESET_IRQ_COUNTERS)");
            }
        }
    }
  else
    {
      int iores = ioctl(m_fd, RESET_IRQ_COUNTERS, &interrupt);
      if( iores < 0 )
        {
          LOG( "Error from ioctl(RESET_IRQ_COUNTERS)" );
          THROW_FLX_EXCEPTION(IOCTL, "Error from ioctl(RESET_IRQ_COUNTERS)");
        }
    }
}

// ----------------------------------------------------------------------------

void FlxCard::irq_cancel( u_int interrupt )
{
  LOG( "called for interrupt " << interrupt );

  if( interrupt == ALL_IRQS )
    {
      LOG( "Clearing all interrupt counters" );
      u_int i;
      for( i = 0; i < NUM_INTERRUPTS; ++i )
        {
          int iores = ioctl(m_fd, CANCEL_IRQ_WAIT, &i);
          if( iores < 0 )
            {
              LOG( "Error from ioctl(CANCEL_IRQ_WAIT)" );
              THROW_FLX_EXCEPTION(IOCTL, "Error from ioctl(CANCEL_IRQ_WAIT)");
            }
        }
    }
  else
    {
      int iores = ioctl(m_fd, CANCEL_IRQ_WAIT, &interrupt);
      if( iores < 0 )
        {
          LOG( "Error from ioctl(CANCEL_IRQ_WAIT)" );
          THROW_FLX_EXCEPTION(IOCTL, "Error from ioctl(CANCEL_IRQ_WAIT)");
        }
    }
}

// ----------------------------------------------------------------------------

void FlxCard::irq_clear( u_int interrupt )
{
  LOG( "called for interrupt " << interrupt );

  if( interrupt == ALL_IRQS )
    {
      LOG( "Clearing all interrupt counters" );
      u_int i;
      for( i = 0; i < NUM_INTERRUPTS; ++i )
        {
          int iores = ioctl(m_fd, CLEAR_IRQ, &i);
          if( iores < 0 )
            {
              LOG( "Error from ioctl(CLEAR_IRQ)" );
              THROW_FLX_EXCEPTION(IOCTL, "Error from ioctl(CLEAR_IRQ)");
            }
        }
    }
  else
    {
      int iores = ioctl(m_fd, CLEAR_IRQ, &interrupt);
      if( iores < 0 )
        {
          LOG( "Error from ioctl(CLEAR_IRQ)" );
          THROW_FLX_EXCEPTION(IOCTL, "Error from ioctl(CLEAR_IRQ)");
        }
    }
}

// ----------------------------------------------------------------------------

u_long FlxCard::openBackDoor( int bar )
{
  LOG( "called" );

  if( bar == 0 )
    return m_bar0Base;
  else if( bar == 1 )
    return m_bar1Base;
  else if( bar == 2 )
    return m_bar2Base;
  else
    THROW_FLX_EXCEPTION(PARAM, "openBackDoor(): select 0,1 or 2 for parameter 'bar'");
}

// ----------------------------------------------------------------------------
// Method coded by Anna Stollenwerk (and improved by M. Joos)

monitoring_data_t FlxCard::get_monitoring_data( u_int mon_mask )
{
  LOG( "called" );

  // Exception commented out: the application needs to make sure it doesn't call
  // this function for an FLX-709, except for 'FPGA_MONITOR'
  //if( !(m_cardType == 711 || m_cardType == 712) )
  //  {
  //    LOG( "only supports the FLX-711 and FLX-712" );
  //    THROW_FLX_EXCEPTION(HW, "get_monitoring_data(): only supports the FLX-711 and FLX-712");
  //  }

  // Read monitoring data from the FPGA
  u_long lvalue;//, number_channels;
  float  fvalue;
  monitoring_data_t mondata;

  if( mon_mask & FPGA_MONITOR )
    {
      LOG( "Reading FPGA data" );
      lvalue = m_bar2->FPGA_CORE_TEMP;
      fvalue = (((float)lvalue * 503.975)/4096.0 - 273.15);
      mondata.fpga.temperature = fvalue;
      LOG( "fvalue (1) = " << fvalue );

      lvalue = m_bar2->FPGA_CORE_VCCINT;
      fvalue = lvalue * 3.0 / 4096.0;
      mondata.fpga.vccint = fvalue;
      LOG( "fvalue (2) = " << fvalue );
      lvalue = m_bar2->FPGA_CORE_VCCAUX;
      fvalue = lvalue * 3.0 / 4096.0;
      mondata.fpga.vccaux = fvalue;
      LOG( "fvalue (3) = " << fvalue );

      lvalue = m_bar2->FPGA_CORE_VCCBRAM;
      fvalue = lvalue * 3.0 / 4096.0;
      mondata.fpga.vccbram = fvalue;
      LOG( "fvalue (4) = " << fvalue );

      lvalue = m_bar2->TACH_CNT;
      LOG( "lvalue = " << lvalue);
      if( lvalue )
        {
          fvalue = 600000000 / lvalue;
          mondata.fpga.fanspeed = fvalue;
          LOG( "fvalue (5) = " << fvalue );
        }
      else
        {
          mondata.fpga.fanspeed = 0;
          LOG( "fvalue (5) = 0" );
        }

      lvalue = m_bar2->FPGA_DNA;
      mondata.fpga.dna = lvalue;
    }

  // Read monitoring data from the MiniPODs
  if( mon_mask & (POD_MONITOR_ALL | POD_MONITOR_LOS |
                  POD_MONITOR_TEMP_VOLT |
                  POD_MONITOR_POWER | POD_MONITOR_POWER_RX) &&
      (m_cardType == 712 || m_cardType == 711) )
    {
      LOG( "Reading MiniPods data" );

      u_char lsb, msb, reg_addr;

      // Check for presence of MiniPOD devices, based on reading a MiniPOD register
      // that should contain an ASCII character
      int podnum;
      for( podnum=0; podnum<MINIPOD_CNT; ++podnum )
        mondata.minipod[podnum].absent = true;

      for( podnum=0; podnum<MINIPOD_CNT; ++podnum )
        {
          reg_addr = 152; // First char of 'vname'
          i2c_read( POD_NAMES[podnum], reg_addr, &lsb );
          if( lsb >= 0x20 && lsb <= 0x7E ) { // Printable char?
            mondata.minipod[podnum].absent = false;
          } else {
            // Try one more time
            i2c_read( POD_NAMES[podnum], reg_addr, &lsb );
            if( lsb >= 0x20 && lsb <= 0x7E ) // Printable char?
              mondata.minipod[podnum].absent = false;
          }
        }

      if( mon_mask & (POD_MONITOR_ALL | POD_MONITOR_LOS) )
        {
          // Read the LOS bits once (the second read below will fetch the valid status)
          for( podnum=0; podnum<MINIPOD_CNT; ++podnum )
            {
              // Skip reading registers from devices not present, i.e. not detected
              if( mondata.minipod[podnum].absent )
                continue;

              // Reading again after a 1 second sleep. See FLX-393 (Henk: make that 0.5s)
              i2c_read( POD_NAMES[podnum], 0x09, &msb );
              i2c_read( POD_NAMES[podnum], 0x0a, &lsb );
            }
          usleep( 500000 ); // 0.5 sec
        }

      for( podnum=0; podnum<MINIPOD_CNT; ++podnum )
        {
          strcpy( mondata.minipod[podnum].name, POD_NAMES[podnum] );

          // Skip reading registers from devices not present, i.e. not detected
          if( mondata.minipod[podnum].absent )
            continue;

          if( mon_mask & (POD_MONITOR_ALL | POD_MONITOR_LOS) )
            {
              i2c_read( POD_NAMES[podnum], 0x09, &msb );
              i2c_read( POD_NAMES[podnum], 0x0a, &lsb );
              mondata.minipod[podnum].los = (msb << 8) + lsb;
            }

          if( mon_mask & (POD_MONITOR_ALL | POD_MONITOR_TEMP_VOLT) )
            {
              // As the temperature has a tolerance of +/- 3 C there is no point
              // in reading the digits after the comma from offset 0x1d
              i2c_read( POD_NAMES[podnum], 0x1c, &msb );
              //i2c_read( POD_NAMES[podnum], 0x1d, &lsb );

              LOG( "tmp dev = " << POD_NAMES[podnum] << " msb = " << int(msb) );

              //mondata.minipod[podnum].temp = msb + (float)lsb / 256.0;
              // Note: According to the manual the MSB provides the temperature as a signed 2's complement.
              //       Therefore this simple assignment will only work for positive temperatures
              mondata.minipod[podnum].temp = msb;

              i2c_read( POD_NAMES[podnum], 0x20, &msb );
              i2c_read( POD_NAMES[podnum], 0x21, &lsb );
              mondata.minipod[podnum].v33 = (float)((msb << 8) + lsb) * 0.0001;

              i2c_read( POD_NAMES[podnum], 0x22, &msb );
              i2c_read( POD_NAMES[podnum], 0x23, &lsb );
              mondata.minipod[podnum].v25 = (float)((msb << 8) + lsb) * 0.0001;
            }

          if( mon_mask & (POD_MONITOR_ALL | POD_MONITOR_POWER | POD_MONITOR_POWER_RX) )
            {
              for( int index=0; index<12; ++index )
                {
                  LOG( "Reading optical_power "
                       << 11 - index << ", registers " << 0x40 + (index * 2)
                       << " and " << 0x41 + (index * 2) );
                  i2c_read( POD_NAMES[podnum], 0x40 + (index * 2), &msb );
                  i2c_read( POD_NAMES[podnum], 0x41 + (index * 2), &lsb );
                  mondata.minipod[podnum].optical_power[11 - index] = ((msb << 8) + lsb) / 10.0;
                }
            }

          if( mon_mask & POD_MONITOR_ALL )
            {
              for( reg_addr = 152; reg_addr < 168; ++reg_addr )
                {
                  i2c_read( POD_NAMES[podnum], reg_addr, &lsb );
                  mondata.minipod[podnum].vname[reg_addr - 152] = lsb;
                }
              mondata.minipod[podnum].vname[15] = 0;

              for( reg_addr = 168; reg_addr < 171; ++reg_addr )
                {
                  i2c_read( POD_NAMES[podnum], reg_addr, &lsb );
                  mondata.minipod[podnum].voui[reg_addr - 168] = lsb;
                }

              for( reg_addr = 171; reg_addr < 187; ++reg_addr )
                {
                  i2c_read( POD_NAMES[podnum], reg_addr, &lsb );
                  mondata.minipod[podnum].vpnum[reg_addr - 171] = lsb;
                }
              mondata.minipod[podnum].vpnum[15] = 0;

              for( reg_addr = 187; reg_addr < 189; ++reg_addr )
                {
                  i2c_read( POD_NAMES[podnum], reg_addr, &lsb );
                  mondata.minipod[podnum].vrev[reg_addr - 187] = lsb;
                }

              for( reg_addr = 189; reg_addr < 205; ++reg_addr )
                {
                  i2c_read( POD_NAMES[podnum], reg_addr, &lsb );
                  mondata.minipod[podnum].vsernum[reg_addr - 189] = lsb;
                }
              mondata.minipod[podnum].vsernum[15] = 0;

              for( reg_addr = 205; reg_addr < 213; ++reg_addr )
                {
                  i2c_read( POD_NAMES[podnum], reg_addr, &lsb );
                  mondata.minipod[podnum].vdate[reg_addr - 205] = lsb;
                }
              mondata.minipod[podnum].vdate[7] = 0;
            }
        }
    }

  if( mon_mask & POWER_MONITOR &&
      (m_cardType == 712 || m_cardType == 711) )
    {
      // Read monitoring data from the LTC2991 devices
      LOG( "Reading LTC2991 data" );
      u_char lsb, msb;

      // Read monitoring data from LTC-1
      mondata.ltc1.name = LTC_NAMES[0];

      i2c_write( LTC_NAMES[0], 0x06, 0x00 );
      i2c_write( LTC_NAMES[0], 0x07, 0x03 );
      i2c_write( LTC_NAMES[0], 0x08, 0x10 );
      i2c_write( LTC_NAMES[0], 0x01, 0xf8 );

      i2c_read( LTC_NAMES[0], 0x0a, &msb );
      i2c_read( LTC_NAMES[0], 0x0b, &lsb );
      fvalue = (((msb & 0x3f) << 8) + lsb) * 0.00030518 / 0.040215;
      mondata.ltc1.VCCINT_current = fvalue;

      i2c_read( LTC_NAMES[0], 0x0c, &msb );
      i2c_read( LTC_NAMES[0], 0x0d, &lsb );
      fvalue = (((msb & 0x3f) << 8) + lsb) * 0.00030518;
      mondata.ltc1.VCCINT_voltage = fvalue;

      i2c_read( LTC_NAMES[0], 0x0e, &msb );
      i2c_read( LTC_NAMES[0], 0x0f, &lsb );
      fvalue = (((msb & 0x3f) << 8) + lsb) * 0.00030518 / 0.040215;
      mondata.ltc1.MGTAVCC_current = fvalue;

      i2c_read( LTC_NAMES[0], 0x10, &msb );
      i2c_read( LTC_NAMES[0], 0x11, &lsb );
      fvalue = (((msb & 0x3f) << 8) + lsb) * 0.00030518;
      mondata.ltc1.MGTAVCC_voltage = fvalue;

      // First read - dummy
      // According to Kai, the data of the first read may not be correct:
      i2c_read( LTC_NAMES[0], 0x12, &msb );
      i2c_read( LTC_NAMES[0], 0x13, &lsb );
      // Second read - get the real data
      i2c_read( LTC_NAMES[0], 0x12, &msb );
      i2c_read( LTC_NAMES[0], 0x13, &lsb );
      fvalue = (((msb & 0x3f) << 8) + lsb) * 0.0625;
      mondata.ltc1.FPGA_internal_diode_temperature = fvalue;

      i2c_read( LTC_NAMES[0], 0x16, &msb );
      i2c_read( LTC_NAMES[0], 0x17, &lsb );
      if( m_cardType == 712 )
        {
          fvalue = (((msb & 0x3f) << 8) + lsb) * 0.00030518 / 0.040215;
          mondata.ltc1.MGTAVTT_current = fvalue;
        }
      else
        {
          fvalue = (((msb & 0x3f) << 8) + lsb) * 0.00030518;
          mondata.ltc1.MGTAVTTC_voltage = fvalue;
        }

      i2c_read( LTC_NAMES[0], 0x18, &msb );
      i2c_read( LTC_NAMES[0], 0x19, &lsb );
      fvalue = (((msb & 0x3f) << 8) + lsb) * 0.00030518;
      if( m_cardType == 712 )
        {
          mondata.ltc1.MGTAVTT_voltage = fvalue;
        }
      else
        {
          mondata.ltc1.MGTVCCAUX_voltage = fvalue;
        }

      i2c_read( LTC_NAMES[0], 0x1a, &msb );
      i2c_read( LTC_NAMES[0], 0x1b, &lsb );
      fvalue = (((msb & 0x3f) << 8) + lsb) * 0.0625;
      mondata.ltc1.LTC2991_1_internal_temperature = fvalue;

      i2c_read( LTC_NAMES[0], 0x1c, &msb );
      i2c_read( LTC_NAMES[0], 0x1d, &lsb );
      fvalue = (((msb & 0x3f) << 8) + lsb) * 0.00030518 + 2.5;
      mondata.ltc1.vcc = fvalue;

      // Read monitoring data from LTC-2
      mondata.ltc2.name = LTC_NAMES[1];

      i2c_write( LTC_NAMES[1], 0x06, 0x00 );
      i2c_write( LTC_NAMES[1], 0x07, 0x30 );
      i2c_write( LTC_NAMES[1], 0x08, 0x10 );
      i2c_write( LTC_NAMES[1], 0x01, 0xf8 );

      i2c_read( LTC_NAMES[1], 0x0a, &msb );
      i2c_read( LTC_NAMES[1], 0x0b, &lsb );
      fvalue = (((msb & 0x3f) << 8) + lsb) * 0.00030518 / 0.040215;
      mondata.ltc2.PEX0P9V_current = fvalue;

      i2c_read( LTC_NAMES[1], 0x0c, &msb );
      i2c_read( LTC_NAMES[1], 0x0d, &lsb );
      fvalue = (((msb & 0x3f) << 8) + lsb) * 0.00030518;
      mondata.ltc2.PEX0P9V_voltage = fvalue;

      i2c_read( LTC_NAMES[1], 0x0e, &msb );
      i2c_read( LTC_NAMES[1], 0x0f, &lsb );
      fvalue = (((msb & 0x3f) << 8) + lsb) * 0.00030518 / 0.040215;
      mondata.ltc2.SYS18_current = fvalue;

      i2c_read( LTC_NAMES[1], 0x10, &msb );
      i2c_read( LTC_NAMES[1], 0x11, &lsb );
      fvalue = (((msb & 0x3f) << 8) + lsb) * 0.00030518;
      mondata.ltc2.SYS18_voltage = fvalue;

      i2c_read( LTC_NAMES[1], 0x12, &msb );
      i2c_read( LTC_NAMES[1], 0x13, &lsb );
      if( m_cardType == 712 )
        {
          fvalue = (((msb & 0x3f) << 8) + lsb) * 0.00030518 / 0.040215;;
          mondata.ltc2.SYS25_current = fvalue;
        }
      else
        {
          fvalue = (((msb & 0x3f) << 8) + lsb) * 0.00030518;
          mondata.ltc2.SYS12_voltage = fvalue;
        }

      i2c_read( LTC_NAMES[1], 0x14, &msb );
      i2c_read( LTC_NAMES[1], 0x15, &lsb );
      fvalue = (((msb & 0x3f) << 8) + lsb) * 0.00030518;
      mondata.ltc2.SYS25_voltage = fvalue;

      i2c_read( LTC_NAMES[1], 0x16, &msb );
      i2c_read( LTC_NAMES[1], 0x17, &lsb );
      fvalue = ((((msb & 0x3f) << 8) + lsb) * 0.0625) - 24.0;
      mondata.ltc2.PEX8732_internal_diode_temperature = fvalue;

      i2c_read( LTC_NAMES[1], 0x1a, &msb );
      i2c_read( LTC_NAMES[1], 0x1b, &lsb );
      fvalue = (((msb & 0x3f) << 8) + lsb) * 0.0625;
      mondata.ltc2.LTC2991_2_internal_temperature = fvalue;

      i2c_read( LTC_NAMES[1], 0x1c, &msb );
      i2c_read( LTC_NAMES[1], 0x1d, &lsb );
      fvalue = (((msb & 0x3f) << 8) + lsb) * 0.00030518 + 2.5;
      mondata.ltc2.vcc = fvalue;
    }

  if( mon_mask & POWER_MONITOR &&
      (m_cardType == 182 || m_cardType == 155) )
    {
      // DEBUG: configure INA226 devices (moved to flx-init):
      // assume we want everywhere a 1 mA current-LSB,
      // then the calibration value is calculated as 5120/Rshunt
      // with Rshunt in mOhm
      /*
      try {
        const float *sh = &INA226_RSHUNT[0];
        uint32_t calib;
        uint32_t config = 0x4127; // The default
        config &= ~(0x7 << 9);    // Clear AVG bits
        config |= (0x2 << 9);     // AVG = 2 (16 samples)
        for( const std::string &n : INA226_NAMES )
          {
            uint8_t inareg[2];
            // Configuration (different from default):
            // - average over 8 measurements
            inareg[0] = (uint8_t) ((config >> 8) & 0xFF);
            inareg[1] = (uint8_t) ((config >> 0) & 0xFF);
            i2c_write( n.c_str(), 0x00, inareg, 2 );

            // Calibration value
            calib = (uint32_t) ((float) 5120.0/(*sh));
            inareg[0] = (uint8_t) ((calib >> 8) & 0xFF);
            inareg[1] = (uint8_t) ((calib >> 0) & 0xFF);
            i2c_write( n.c_str(), 0x05, inareg, 2 );
            ++sh;
          }
      }
      catch( FlxException &ex ) {
        std::cout << "### ERROR INA226 Calib: " << ex.what() << std::endl;
      }
      */

      // Read monitoring data from the INA226 devices (NB: 2-byte registers)
      try {
        ina226_parameters_t *ina = &mondata.ina226[0];
        const std::vector<std::string> *names;
        const float *sh;
        if( m_cardType == 182 )
          {
            names = &INA226_NAMES_182;
            sh = &INA226_RSHUNT_182[0];
          }
        else
          {
            names = &INA226_NAMES_155;
            sh = &INA226_RSHUNT_155[0];
          }
        uint8_t inareg[2];
        for( const std::string &n : *names )
          {
            ina->name = n;
            ina->r_shunt = *sh;
            ++sh;

            const char *name = n.c_str();
            i2c_read( name, 0x00, inareg, 2 );
            ina->config_reg = ((uint32_t) inareg[0] << 8) | (uint32_t) inareg[1];
            i2c_read( name, 0x01, inareg, 2 );
            ina->shunt_volt_reg = ((uint32_t) inareg[0] << 8) | (uint32_t) inareg[1];
            i2c_read( name, 0x02, inareg, 2 );
            ina->bus_volt_reg = ((uint32_t) inareg[0] << 8) | (uint32_t) inareg[1];
            i2c_read( name, 0x03, inareg, 2 );
            ina->power_reg = ((uint32_t) inareg[0] << 8) | (uint32_t) inareg[1];
            i2c_read( name, 0x04, inareg, 2 );
            ina->current_reg = ((uint32_t) inareg[0] << 8) | (uint32_t) inareg[1];
            i2c_read( name, 0x05, inareg, 2 );
            ina->calib_reg = ((uint32_t) inareg[0] << 8) | (uint32_t) inareg[1];
            // Manufacturer ID, expect 0x5449
            i2c_read( name, 0xFE, inareg, 2 );
            ina->manufacturer_id = ((uint32_t) inareg[0] << 8) | (uint32_t) inareg[1];

            ++ina;
          }
      }
      catch( FlxException &ex ) {
        std::cout << "### ERROR INA226: " << ex.what() << std::endl;
      }

      // Read monitoring data from the TMP435 devices
      try {
        tmp435_parameters_t *tmp = &mondata.tmp435[0];
        const std::vector<std::string> *names;
        if( m_cardType == 182 )
          names = &TMP435_NAMES_182;
        else
          names = &TMP435_NAMES_155;
        uint8_t regval;
        for( const std::string &n : *names )
          {
            tmp->name = n;

            const char *name = n.c_str();
            i2c_read( name, 0xFD, &regval );
            tmp->device_id = regval;

            i2c_read( name, 0xFE, &regval );
            tmp->manufacturer_id = regval;

            i2c_read( name, 0x00, &regval );
            tmp->t_local_hi = regval;
            i2c_read( name, 0x15, &regval );
            tmp->t_local_lo = (regval >> 4) * 625;

            i2c_read( name, 0x01, &regval );
            tmp->t_remote_hi = regval;
            i2c_read( name, 0x10, &regval );
            tmp->t_remote_lo = (regval >> 4) * 625;

            i2c_read( name, 0x02, &regval );
            tmp->status = regval;

            ++tmp;
          }
      }
      catch( FlxException &ex ) {
        std::cout << "### ERROR TMP435: " << ex.what() << std::endl;
      }

      // Read monitoring data from the LTM4700 device
      try {
        // LTM4700 (*UNDER DEVELOPMENT*)
        ltm4700_parameters_t *ltm = &mondata.ltm4700;
        ltm->mfr_special_id = 0;
        ltm->vin = 0; ltm->iin = 0; ltm->pin = 0;
        ltm->vout[0] = 0; ltm->iout[0] = 0; ltm->pout[0] = 0;
        ltm->vout[1] = 0; ltm->iout[1] = 0; ltm->pout[1] = 0;
        ltm->temp_die = 0; ltm->temp_ext[0] = 0; ltm->temp_ext[1] = 0;

        const char *name = "POWER_GEN";
        ltm->name = std::string( name );
        i2c_read( name, 0x88, (uint8_t *) &ltm->vin, 2 ); // L11
        i2c_read( name, 0x89, (uint8_t *) &ltm->iin, 2 ); // L11
        i2c_read( name, 0x97, (uint8_t *) &ltm->pin, 2 ); // L11
        i2c_read( name, 0x8E, (uint8_t *) &ltm->temp_die, 2 ); // L11
        // Channel 0
        i2c_write( name, 0x00, 0 ); // Set PAGE 0
        i2c_read( name, 0x8B, (uint8_t *) &ltm->vout[0], 2 ); // L16
        i2c_read( name, 0x8C, (uint8_t *) &ltm->iout[0], 2 ); // L11
        i2c_read( name, 0x96, (uint8_t *) &ltm->pout[0], 2 ); // L11
        i2c_read( name, 0x8D, (uint8_t *) &ltm->temp_ext[0], 2 ); // L11
        // Channel 1
        i2c_write( name, 0x00, 1 ); // Set PAGE 1
        i2c_read( name, 0x8B, (uint8_t *) &ltm->vout[1], 2 ); // L16
        i2c_read( name, 0x8C, (uint8_t *) &ltm->iout[1], 2 ); // L11
        i2c_read( name, 0x96, (uint8_t *) &ltm->pout[1], 2 ); // L11
        i2c_read( name, 0x8D, (uint8_t *) &ltm->temp_ext[1], 2 ); // L11

        i2c_read( name, 0xE7, (uint8_t *) &ltm->mfr_special_id, 2 );
      }
      catch( FlxException &ex ) {
        std::cout << "### ERROR LTM4700: " << ex.what() << std::endl;
      }

      // Read monitoring data from the ADM1x66 device(s)
      try {
        mondata.adm1066[0].valid = false;
        mondata.adm1066[1].valid = false;
        mondata.adm1266.valid    = false;

        const std::vector<std::string> *names;
        bool has_adm1066;
        uint8_t regval;
        if( m_cardType == 182 )
          {
            // Check if ADM1066 or ADM1266
            i2c_read( ADM1066_NAMES_182[0].c_str(), 0xF4, &regval );
            if( regval == 0x41 )
              {
                names = &ADM1066_NAMES_182;
                has_adm1066 = true;
              }
            else
              {
                names = &ADM1266_NAMES_182;
                has_adm1066 = false;
              }
          }
        else
          {
            names = &ADM1266_NAMES_155;
            has_adm1066 = false;
          }

        if( has_adm1066 )
          {
            // ADM1066 (*UNDER DEVELOPMENT*)
            adm1066_parameters_t *adm = &mondata.adm1066[0];
            for( const std::string &n : *names )
              {
                adm->name = n;
                adm->valid = true;

                const char *name = n.c_str();
                i2c_read( name, 0xF4, &regval );
                adm->manufacturer_id = regval;

                i2c_read( name, 0xF5, &regval );
                adm->silicon_rev = regval;

                // Inputs configuration (10 inputs: VP1-4, VH, VX1-5)
                for( uint8_t i=0; i<10; ++i )
                  for( uint8_t j=0; j<8; ++j )
                    {
                      i2c_read( name, i*0x8+j, &regval );
                      adm->input_cfg[i][j] = regval;
                    }

                // Fault/status registers (up to UVSTAT2)
                for( uint8_t i=0; i<6; ++i )
                  {
                    i2c_read( name, 0xE0+i, &regval );
                    adm->fault_stat[i] = regval;
                  }

                // Clear limit warnings
                i2c_write( name, 0x82, 0x10 );

                // Start (single, 16x average) ADC round-robin 
                i2c_write( name, 0x82, 1+4 );
                //i2c_write( name, 0x82, 1 );
                usleep( 100000 ); // 12 chan, 16x averaging: 84ms

                // ADC value registers
                for( uint8_t i=0; i<8+4; ++i )
                  {
                    adm->adc[i] = 0;
                    i2c_read( name, 0xA0+i*2, &regval );
                    adm->adc[i] = (regval << 8);
                    i2c_read( name, 0xA0+i*2+1, &regval );
                    adm->adc[i] |= regval;
                  }

                i2c_write( name, 0x82, 0 );

                ++adm;
              }
          }
        else
          {
            // ADM1266 (*UNDER DEVELOPMENT*)
            adm1266_parameters_t *adm = &mondata.adm1266;
            const char *name = (*names)[0].c_str();
            adm->name = (*names)[0];
            adm->valid = true;
            i2c_read( name, 0xAD, (u_char*) &adm->ic_device_id, 3 );
            i2c_read( name, 0xAE, (u_char*) &adm->ic_device_rev, 8 );
            //adm->ic_device_id  = 0x12345678;
            //adm->ic_device_rev = 0xAABB12345678;

            // ADC value registers
            for( uint8_t i=0; i<17; ++i )
              {
                // Select ("PAGE" command)
                i2c_write( name, 0x00, i );
                // Read value ("READ_VOUT" command)
                i2c_read( name, 0x8B, (u_char*) &adm->adc[i], 2 );
                // Read "STATUS_VOUT"
                i2c_read( name, 0x7A, (u_char*) &adm->status_vout[i] );
              }
          }
      }
      catch( FlxException &ex ) {
        std::cout << "### ERROR ADM1066: " << ex.what() << std::endl;
      }
    }

  if( mon_mask & FIREFLY_MONITOR &&
      (m_cardType == 182 || m_cardType == 155) )
    {
      LOG( "Reading FireFly config/status (WORK-IN-PROGRESS)");

      const std::vector<std::string> *ff_names;
      if( m_cardType == 182 )
        ff_names = &FF_NAMES_TX_182;
      else
        ff_names = &FF_NAMES_TX_155;

      std::string vendor_part, vendor_sn;
      for( uint32_t i=0; i<ff_names->size(); ++i )
        {
          ffly_tx_parameters_t *ffly_tx = &mondata.ffly_tx[i];
          ffly_tx->name = (*ff_names)[i];
          vendor_part.clear();
          vendor_sn.clear();
          ffly_tx->absent = !fireFlyDetect( (*ff_names)[i], vendor_part, vendor_sn );
          if( ffly_tx->absent )
            continue;
          ffly_tx->vendor_part   = vendor_part;
          ffly_tx->vendor_serial = vendor_sn;

          // Read all 128 bytes of the lower page, translate
          // and map into a ffly_tx_parameters_t struct
          const char *name = (*ff_names)[i].c_str();
          uint8_t page[128];
          uint8_t ch, addr;
          for( addr=0; addr<114; ++addr )
            {
              // Skip a number of addresses
              if( addr >= 76 && addr <= 94 )
                continue;
              i2c_read( name, addr, &ch );
              page[addr] = ch;
            }

          // Translate page bytes to struct members
          ffly_tx->status                 = (uint32_t) page[2];
          ffly_tx->status_summ            = (uint32_t) page[6];
          ffly_tx->latched_alarms_tx_los  = ((uint32_t) page[7] << 8) | (uint32_t) page[8];
          ffly_tx->latched_alarms_tx_fault= ((uint32_t) page[9] << 8) | (uint32_t) page[10];
          ffly_tx->latched_alarms_temp    = (uint32_t) page[17];
          ffly_tx->latched_alarms_vcc33   = (uint32_t) page[18];
          ffly_tx->latched_alarms_cdr_lol = ((uint32_t) page[20] << 8) | (uint32_t) page[21];
          ffly_tx->case_temp              = (int32_t) page[22]; // Signed bit twos complement (+/- 3C)
          ffly_tx->vcc                    = ((uint32_t) page[26] << 8) | (uint32_t) page[27]; // [0.1 mV]
          ffly_tx->elapsed_optime         = ((uint32_t) page[38] << 8) | (uint32_t) page[39]; // [2 hours]
          ffly_tx->chan_disable           = ((uint32_t) page[52] << 8) | (uint32_t) page[53];
          ffly_tx->squelch_disable        = ((uint32_t) page[56] << 8) | (uint32_t) page[57];
          ffly_tx->polarity_invert        = ((uint32_t) page[58] << 8) | (uint32_t) page[59];
          ffly_tx->input_equalization     =
            ((uint64_t) page[62] << (uint64_t)40) | ((uint64_t) page[63] << (uint64_t)32) |
            ((uint64_t) page[64] << 24) | ((uint64_t) page[65] << 16) |
            ((uint64_t) page[66] << 8)  | (uint64_t) page[67];
          ffly_tx->cdr_enable             = ((uint32_t) page[74] << 8) | (uint32_t) page[75];
          ffly_tx->mask_tx_los_alarms     = ((uint32_t) page[95] << 8) | (uint32_t) page[96];
          ffly_tx->mask_fault_flags       = ((uint32_t) page[97] << 8) | (uint32_t) page[98];
          ffly_tx->mask_temp_alarms       = (uint32_t) page[105];
          ffly_tx->mask_vcc33_alarms      = (uint32_t) page[106];
          ffly_tx->mask_cdr_lol_alarms    = ((uint32_t) page[108] << 8) | (uint32_t) page[109];
          ffly_tx->firmware_version       = (((uint64_t) page[111] << 16) |
                                             ((uint64_t) page[112] << 8)  | (uint64_t) page[113]);
        }

      if( m_cardType == 182 )
        ff_names = &FF_NAMES_RX_182;
      else
        ff_names = &FF_NAMES_RX_155;

      for( uint32_t i=0; i<ff_names->size(); ++i )
        {
          ffly_rx_parameters_t *ffly_rx = &mondata.ffly_rx[i];
          ffly_rx->name = (*ff_names)[i];
          vendor_part.clear();
          vendor_sn.clear();
          ffly_rx->absent = !fireFlyDetect( (*ff_names)[i], vendor_part, vendor_sn );
          if( ffly_rx->absent )
            continue;
          ffly_rx->vendor_part   = vendor_part;
          ffly_rx->vendor_serial = vendor_sn;

          // Read all 128 bytes of the lower page, translate
          // and map into a ffly_rx_parameters_t struct
          const char *name = (*ff_names)[i].c_str();
          uint8_t page[128];
          uint8_t ch, addr;
          for( addr=0; addr<114; ++addr )
            {
              // Skip a number of addresses
              if( addr >= 40 && addr <= 50 )
                continue;
              if( addr >= 76 && addr <= 94 )
                continue;
              i2c_read( name, addr, &ch );
              page[addr] = ch;
            }

          // Translate page bytes to struct members
          ffly_rx->status                 = (uint32_t) page[2];
          ffly_rx->status_summ            = (uint32_t) page[6];
          ffly_rx->latched_alarms_rx_los  = ((uint32_t) page[7] << 8) | (uint32_t) page[8];
          ffly_rx->latched_alarms_rx_power= (((uint64_t) page[14] << 16) |
                                             ((uint64_t) page[15] << 8)  | (uint64_t) page[16]);
          ffly_rx->latched_alarms_temp    = (uint32_t) page[17];
          ffly_rx->latched_alarms_vcc33   = (uint32_t) page[18];
          ffly_rx->latched_alarms_cdr_lol = ((uint32_t) page[20] << 8) | (uint32_t) page[21];
          ffly_rx->case_temp              = (int32_t) page[22]; // Signed bit twos complement (+/- 3C)
          ffly_rx->vcc                    = ((uint32_t) page[26] << 8) | (uint32_t) page[27]; // [0.1 mV]
          ffly_rx->elapsed_optime         = ((uint32_t) page[38] << 8) | (uint32_t) page[39]; // [2 hours]
          ffly_rx->chan_disable           = ((uint32_t) page[52] << 8) | (uint32_t) page[53];
          ffly_rx->output_disable         = ((uint32_t) page[54] << 8) | (uint32_t) page[55];
          ffly_rx->polarity_invert        = ((uint32_t) page[58] << 8) | (uint32_t) page[59];
          ffly_rx->output_swing           =
            ((uint64_t) page[62] << (uint64_t)40) | ((uint64_t) page[63] << (uint64_t)32) |
            ((uint64_t) page[64] << 24) | ((uint64_t) page[65] << 16) |
            ((uint64_t) page[66] << 8)  | (uint64_t) page[67];
          ffly_rx->output_preemphasis     =
            ((uint64_t) page[68] << (uint64_t)40) | ((uint64_t) page[69] << (uint64_t)32) |
            ((uint64_t) page[70] << 24) | ((uint64_t) page[71] << 16) |
            ((uint64_t) page[72] << 8)  | (uint64_t) page[73];
          ffly_rx->cdr_enable             = ((uint32_t) page[74] << 8) | (uint32_t) page[75];
          // ...
          ffly_rx->firmware_version       = (((uint64_t) page[111] << 16) |
                                             ((uint64_t) page[112] << 8)  | (uint64_t) page[113]);
        }

      if( m_cardType == 182 )
        ff_names = &FF_NAMES_TXRX_182;
      else
        ff_names = &FF_NAMES_TXRX_155;

      for( uint32_t i=0; i<ff_names->size(); ++i )
        {
          ffly_tr_parameters_t *ffly_tr = &mondata.ffly_tr[i];
          ffly_tr->name = (*ff_names)[i];
          vendor_part.clear();
          vendor_sn.clear();
          ffly_tr->absent = !fireFlyDetect( (*ff_names)[i], vendor_part, vendor_sn );
          if( ffly_tr->absent )
            continue;
          ffly_tr->vendor_part   = vendor_part;
          ffly_tr->vendor_serial = vendor_sn;

          // Read all 128 bytes of the lower page, translate
          // and map into a ffly_tr_parameters_t struct
          const char *name = (*ff_names)[i].c_str();
          uint8_t page[128];
          uint8_t ch, addr;
          for( addr=0; addr<105; ++addr )
            {
              if( addr >= 42 && addr <= 68 )
                continue;
              if( addr >= 72 && addr <= 85 )
                continue;
              if( addr >= 87 && addr <= 97 )
                continue;
              i2c_read( name, addr, &ch );
              page[addr] = ch;
            }

          // Translate page bytes to struct members
          ffly_tr->status                 = (uint32_t) page[2];
          ffly_tr->latched_alarms_tx_los  = (uint32_t) page[3];
          ffly_tr->latched_alarms_tx_fault= (uint32_t) page[4];
          ffly_tr->latched_alarms_cdr_lol = (uint32_t) page[5];
          ffly_tr->latched_alarms_temp    = (uint32_t) page[6];
          ffly_tr->latched_alarms_vcc     = (uint32_t) page[7];
          ffly_tr->latched_alarms_rx_power= ((uint32_t) page[9] << 8) | (uint32_t) page[10];
          ffly_tr->elapsed_optime         = ((uint32_t) page[19] << 8) | (uint32_t) page[20]; // [2 hours]
          ffly_tr->case_temp              = (int32_t) page[22]; // Signed bit twos complement (+/- 3C)
          ffly_tr->vcc_3v3                = ((uint32_t) page[26] << 8) | (uint32_t) page[27]; // [0.1 mV]
          ffly_tr->vcc_1v8                = ((uint32_t) page[28] << 8) | (uint32_t) page[29]; // [0.1 mV]
          ffly_tr->rx_optical_power[0]    = ((uint32_t) page[34] << 8) | (uint32_t) page[35]; // [0.1 uW]
          ffly_tr->rx_optical_power[1]    = ((uint32_t) page[36] << 8) | (uint32_t) page[37]; // [0.1 uW]
          ffly_tr->rx_optical_power[2]    = ((uint32_t) page[38] << 8) | (uint32_t) page[39]; // [0.1 uW]
          ffly_tr->rx_optical_power[3]    = ((uint32_t) page[40] << 8) | (uint32_t) page[41]; // [0.1 uW]
          ffly_tr->firmware_revision      = (((uint64_t) page[69] << 16) |
                                             ((uint64_t) page[70] << 8)  | (uint64_t) page[71]);
          ffly_tr->tx_chan_disable        = (int32_t) page[86];
          ffly_tr->cdr_enable             = (int32_t) page[98];
          // ...
        }
    }

  LOG( "done" );
  return mondata;
}

// ----------------------------------------------------------------------------

float FlxCard::fpga_temperature()
{
  float f = (((float) m_bar2->FPGA_CORE_TEMP * 503.975)/4096.0 - 273.15);
  return f;
}

// ----------------------------------------------------------------------------

int FlxCard::fpga_fanspeed()
{
  int rpm = 0;
  uint64_t val = m_bar2->TACH_CNT;
  if( val != 0 )
    {
      rpm = 600000000 / val;
      if( rpm > 20000 )
        // Not a sensible value, so return -1 to indicate that
        rpm = -1;
    }
  return rpm;
}

// ----------------------------------------------------------------------------

std::vector<int> FlxCard::minipods_temperature()
{
  std::vector<int> t;
  monitoring_data_t m;
  m = this->get_monitoring_data( POD_MONITOR_TEMP_VOLT );
  minipod_parameters_t *mp = m.minipod;
  for( int i=0; i<8; ++i, ++mp )
    {
      if( mp->absent )
        t.push_back( -1 );
      else
        t.push_back( mp->temp );
    }
  return t;
}

// ----------------------------------------------------------------------------

std::vector<float> FlxCard::minipods_optical_power()
{
  std::vector<float> p;
  monitoring_data_t m;
  m = this->get_monitoring_data( POD_MONITOR_POWER );
  minipod_parameters_t *mp = m.minipod;
  for( int i=0; i<8; ++i, ++mp )
    {
      if( mp->absent )
        for( int chan=0; chan<12; ++chan )
          p.push_back( 0.0 );
      else
        for( int chan=0; chan<12; ++chan )
          p.push_back( mp->optical_power[chan] );
    }
  return p;
}

// ----------------------------------------------------------------------------

// FireFly registers
const uint8_t FF_PAGE_SELECT        = 127;

const uint8_t FF_CERN_VENDOR_PART   = 171;
const uint8_t FF_CERN_VENDOR_SERIAL = 189;
const uint8_t FF_CERN_PART_SIZE     = 16;
const uint8_t FF_CERN_SERIAL_SIZE   = 10;

const uint8_t FF_VENDOR_PART        = 168;
const uint8_t FF_VENDOR_SERIAL      = 196;
const uint8_t FF_PART_SIZE          = 13;
const uint8_t FF_SERIAL_SIZE        = 10;

bool FlxCard::fireFlyDetect( const std::string &device_name,
                             std::string &vendor_part,
                             std::string &vendor_sn )
{
  // Check presence and type of FireFly devices
  // for each FireFly read information from upper page 0:
  // - set page select to 0: i2c_write( name, FF_PAGE_SELECT, 0 );
  // - read vendor name first char and
  //   check for being ASCII: i2c_read( name, 152, &ch );
  // - read vendor part nr  : for( addr=171; addr<187; ++addr )
  //                            i2c_read( name, addr, &ch );
  // - read vendor serial nr: for( addr=189; addr<199; ++addr )
  //                            i2c_read( name, addr, &ch );
  const char *name = device_name.c_str();
  uint8_t ch, addr;
  bool found = true;

  i2c_write( name, FF_PAGE_SELECT, 0 );

  // Check if CERN device type
  // (i.e. contains string "CERN" in its description, or check it dynamically)
  i2c_device_t *i2cdev = m_i2cDevices;
  for( ; i2cdev->name != NULL; ++i2cdev )
    if( strcmp(name, i2cdev->name) == 0 )
      break;
  bool is_cern_type = (strstr( i2cdev->description, "CERN" ) != 0);
  if( !is_cern_type )
    {
      // Determine whether we have a CERN-type FireFly or not:
      // must check FF_VENDOR_PART first
      // (as it is located a few chars before FF_CERN_VENDOR_PART)
      i2c_read( name, FF_VENDOR_PART, &ch );
      if( ch < 0x20 || ch > 0x7E ) // Non-printable char?
        {
          // It must be a CERN type then
          i2c_read( name, FF_CERN_VENDOR_PART, &ch );
          if( ch < 0x20 || ch > 0x7E ) // Non-printable char?
            found = false; // Read part string nevertheless (below)
          is_cern_type = true;
        }
    }

  if( is_cern_type )
    {
      for( addr=FF_CERN_VENDOR_PART; addr<FF_CERN_VENDOR_PART+FF_CERN_PART_SIZE; ++addr )
        {
          i2c_read( name, addr, &ch );
          vendor_part.push_back( ch );
        }
      for( addr=FF_CERN_VENDOR_SERIAL; addr<FF_CERN_VENDOR_SERIAL+FF_CERN_SERIAL_SIZE; ++addr )
        {
          i2c_read( name, addr, &ch );
          vendor_sn.push_back( ch );
        }
    }
  else
    {
      for( addr=FF_VENDOR_PART; addr<FF_VENDOR_PART+FF_PART_SIZE; ++addr )
        {
          i2c_read( name, addr, &ch );
          vendor_part.push_back( ch );
        }
      for( addr=FF_VENDOR_SERIAL; addr<FF_VENDOR_SERIAL+FF_SERIAL_SIZE; ++addr )
        {
          i2c_read( name, addr, &ch );
          vendor_sn.push_back( ch );
        }
    }
  return true;
}

// ----------------------------------------------------------------------------

std::pair<int,int> FlxCard::ec_elink_indices()
{
#if REGMAP_VERSION < 0x500
  return std::pair<int,int>( 0x3F, 0x3F );
#else
  int ec_tohost = m_bar2->AXI_STREAMS_TOHOST.EC_INDEX;
  int ec_fromhost = m_bar2->AXI_STREAMS_FROMHOST.EC_INDEX;
  return std::pair<int,int>( ec_tohost, ec_fromhost );
#endif // REGMAP_VERSION
}

// ----------------------------------------------------------------------------

std::pair<int,int> FlxCard::ic_elink_indices()
{
#if REGMAP_VERSION < 0x500
  return std::pair<int,int>( 0x3E, 0x3E );
#else
  int ic_tohost = m_bar2->AXI_STREAMS_TOHOST.IC_INDEX;
  int ic_fromhost = m_bar2->AXI_STREAMS_FROMHOST.IC_INDEX;
  return std::pair<int,int>( ic_tohost, ic_fromhost );
#endif // REGMAP_VERSION
}

// ----------------------------------------------------------------------------

u_long FlxCard::rxusrclk_freq( u_int channel )
{
  LOG( "called" );

  m_bar2->RXUSRCLK_FREQ.CHANNEL = channel & 0x3F;

  // Set up watchdog
  m_timeout = 0;
  struct itimerval timer;
  timer.it_value.tv_sec     = 1; // One second
  timer.it_value.tv_usec    = 0;
  timer.it_interval.tv_sec  = 0;
  timer.it_interval.tv_usec = 0; // Only one shot
  setitimer(ITIMER_REAL, &timer, NULL);

  u_long valid;
  do
    {
      valid = m_bar2->RXUSRCLK_FREQ.VALID;
      if( m_timeout )
        {
          THROW_FLX_EXCEPTION(HW, "rxusrclk_freq(): Timeout wait-for-valid");
        }
    } while( valid == 0 );

  // Stop watchdog
  timer.it_value.tv_usec = 0;
  timer.it_value.tv_sec = 0;
  setitimer(ITIMER_REAL, &timer, NULL);

  u_long value = m_bar2->RXUSRCLK_FREQ.VAL;
  return value;
}

// ----------------------------------------------------------------------------

void FlxCard::configure( std::string filename,
                         bool        do_links,
                         bool        do_registers )
{
  // Read a configuration from file
  this->readConfiguration( filename );

  if( m_fd < 0 )
    {
      THROW_FLX_EXCEPTION( CONFIG, "FLX-device not opened" );
    }

  if( do_links )
    {
      // Check firmware compatibility with this configuration
      uint32_t mode = m_linkConfig[0].linkMode();
      if( mode == flxcard::LINKMODE_FULL && !m_fullmodeType )
        {
          THROW_FLX_EXCEPTION( CONFIG, "Link config FULL: incompatible with "
                               << firmware_type_string().c_str() << " firmware!" );
        }
      else if( (mode == flxcard::LINKMODE_GBT ||
                mode == flxcard::LINKMODE_GBTWIDE) &&
               (m_lpgbtType || m_fullmodeType) )
        {
          THROW_FLX_EXCEPTION( CONFIG, "Link config GBT: incompatible with "
                               << firmware_type_string().c_str() << " firmware!" );
        }
      else if( (mode == flxcard::LINKMODE_LPGBT10_F5 ||
                mode == flxcard::LINKMODE_LPGBT5_F5 ||
                mode == flxcard::LINKMODE_LPGBT10_F12 ||
                mode == flxcard::LINKMODE_LPGBT5_F12) &&
               !m_lpgbtType )
        {
          THROW_FLX_EXCEPTION( CONFIG, "Link config lpGBT: incompatible with "
                               << firmware_type_string().c_str() << " firmware!" );
        }

      // Configure the FELIX device links
      int dma_index_invalid_count;
      this->configureLinks( m_linkConfig, &dma_index_invalid_count );

      if( dma_index_invalid_count > 0 )
        {
          std::cerr << "### WARNING: " << dma_index_invalid_count << " (E-)links "
                    << "with invalid DMA index in config file; set to 0" << std::endl;
        }
    }

  if( do_registers )
    {
      // Apply additional register settings provided in the configuration file, if any
      for( flxcard::regsetting_t &r : m_regSettings )
        this->cfg_set_option( r.name.c_str(), r.value );
    }
}

// ----------------------------------------------------------------------------

// Configuration file: YAML file keys of the {key,value}-pairs
#ifndef FFLASH_STATIC
static const char *KEY_FORMAT     = "Format";
static const char *KEY_LINKS      = "Links";
static const char *KEY_LINK       = "Link";
static const char *KEY_LINKMODE   = "LinkMode";
static const char *KEY_LTITTC     = "LtiTtc";
static const char *KEY_TTCCLOCK   = "SetTtcClock";
static const char *KEY_CHUNKMAX   = "LinkChunkMaxSize";
static const char *KEY_EGROUP     = "Egroup";
static const char *KEY_EGROUPS_TH = "EgroupsToHost";
static const char *KEY_WIDTH_TH   = "WidthToHost";
static const char *KEY_ENA_TH     = "EnableToHost";
static const char *KEY_MODE_TH    = "ModeToHost";
static const char *KEY_STREAMID   = "StreamId";
static const char *KEY_DMAINDICES = "DmaIndices";
static const char *KEY_EGROUPS_FH = "EgroupsFromHost";
static const char *KEY_WIDTH_FH   = "WidthFromHost";
static const char *KEY_ENA_FH     = "EnableFromHost";
static const char *KEY_MODE_FH    = "ModeFromHost";
static const char *KEY_TTCOPTION  = "TtcOption";
static const char *KEY_SETTINGS   = "RegisterSettings";
static const char *KEY_FIELDNAME  = "Name";
static const char *KEY_FIELDVAL   = "Value";
#endif // FFLASH_STATIC

void FlxCard::readConfiguration( std::string filename )
{
#ifndef FFLASH_STATIC
  // Read the link configuration file
  std::ifstream file( filename.c_str() );
  if( !file.is_open() )
    {
      THROW_FLX_EXCEPTION( CONFIG, "Failed to open YELC configuration file: "
                           << filename );
    }

  // File name extension
  std::string ext;
  size_t pos = filename.find_last_of( "." );
  if( pos != std::string::npos )
    ext = filename.substr( pos+1 );
  if( ext != std::string( "yelc" ) )
    std::cerr << "### WARNING: expected .yelc configuration file extension" << std::endl;

  // Read file contents (ASCII)
  int32_t  linknr = 0;
  uint32_t egroup = 0;
  uint32_t width, enables, chunksizes, linkmode, streamid, dmaindices;
  bool     lti_ttc, ttc_clock;
  uint64_t modes;
  uint32_t ttc_option;
  m_regSettings.clear();
  try {
    YAML::Node yaml = YAML::Load( file );
    if( !yaml[KEY_FORMAT] )
      {
        THROW_FLX_EXCEPTION( CONFIG, "YELC-file has incorrect format (\"Format\" missing)" );
      }
    int format = yaml[KEY_FORMAT].as<int>();
    if( format != 2 )
      {
        THROW_FLX_EXCEPTION( CONFIG, "YELC-file: only \"Format\" 2 supported" );
      }

    YAML::Node links = yaml[KEY_LINKS];
    for( YAML::const_iterator it = links.begin(); it != links.end(); ++it )
      {
        YAML::Node lnk = *it;
        linknr = lnk[KEY_LINK].as<int>();
        if( linknr > flxcard::FLX_LINKS ) // Shouldn't read in past this number!
          continue;
        if( linknr == -1 ) // Emulator configuration
          linknr = flxcard::FLX_LINKS;

        chunksizes = lnk[KEY_CHUNKMAX].as<int>();
        m_linkConfig[linknr].setMaxChunkWord( chunksizes );

        linkmode = lnk[KEY_LINKMODE].as<int>();
        m_linkConfig[linknr].setLinkMode( linkmode );

        if( lnk[KEY_LTITTC] )
          lti_ttc = lnk[KEY_LTITTC].as<bool>();
        else
          lti_ttc = false;
        m_linkConfig[linknr].setLtiTtc( lti_ttc );

        if( linknr == 0 )
          {
            if( lnk[KEY_TTCCLOCK] )
              ttc_clock = lnk[KEY_TTCCLOCK].as<bool>();
            else
              ttc_clock = false;
            m_linkConfig[linknr].setTtcClock( ttc_clock );
          }

        YAML::Node egrps = lnk[KEY_EGROUPS_TH];
        std::istringstream iss;
        for( YAML::const_iterator it2 = egrps.begin(); it2 != egrps.end(); ++it2 )
          {
            YAML::Node egrp = *it2;
            egroup = egrp[KEY_EGROUP].as<int>();

            // Convert to integers (some from a hexadecimal representation)
            width = 0, enables = 0; modes = 0;
            width = egrp[KEY_WIDTH_TH].as<int>();
            iss.clear();
            iss.str( egrp[KEY_ENA_TH].as<std::string>() );
            iss >> std::hex >> enables;
            iss.clear();
            iss.str( egrp[KEY_MODE_TH].as<std::string>() );
            iss >> std::hex >> modes;
            m_linkConfig[linknr].setWidthToHost( egroup, width );
            m_linkConfig[linknr].setEnablesToHost( egroup, enables );
            m_linkConfig[linknr].setModesToHost( egroup, modes );

            // Optional item (for backwards-compatibility)
            if( egrp[KEY_DMAINDICES] )
              {
                dmaindices = 0;
                iss.clear();
                iss.str( egrp[KEY_DMAINDICES].as<std::string>() );
                iss >> std::hex >> dmaindices;
                m_linkConfig[linknr].setDmaIndices( egroup, dmaindices );
              }

            streamid = 0;
            iss.clear();
            iss.str( egrp[KEY_STREAMID].as<std::string>() );
            iss >> std::hex >> streamid;
            m_linkConfig[linknr].setStreamIdBits( egroup, streamid );
          }

        egrps = lnk[KEY_EGROUPS_FH];
        for( YAML::const_iterator it2 = egrps.begin(); it2 != egrps.end(); ++it2 )
          {
            YAML::Node egrp = *it2;
            egroup = egrp[KEY_EGROUP].as<int>();

            // Convert to integers (some from a hexadecimal representation)
            width = 0; enables = 0; modes = 0;
            width = egrp[KEY_WIDTH_FH].as<int>();
            iss.clear();
            iss.str( egrp[KEY_ENA_FH].as<std::string>() );
            iss >> std::hex >> enables;
            iss.clear();
            iss.str( egrp[KEY_MODE_FH].as<std::string>() );
            iss >> std::hex >> modes;
            if( egrp[KEY_TTCOPTION] )
              {
                ttc_option = egrp[KEY_TTCOPTION].as<int>();
                m_linkConfig[linknr].setTtcOptionFromHost( egroup, ttc_option );
              }
            m_linkConfig[linknr].setWidthFromHost( egroup, width );
            m_linkConfig[linknr].setEnablesFromHost( egroup, enables );
            m_linkConfig[linknr].setModesFromHost( egroup, modes );
          }
      }

    YAML::Node settings = yaml[KEY_SETTINGS];
    flxcard::regsetting_t regset;
    for( YAML::const_iterator it = settings.begin(); it != settings.end(); ++it )
      {
        YAML::Node setting = *it;
        std::string field = setting[KEY_FIELDNAME].as<std::string>();
        uint64_t value = setting[KEY_FIELDVAL].as<uint64_t>();
        regset.name = field;
        regset.value = value;
        m_regSettings.push_back( regset );
      }
  }
  catch( std::exception &ex ) {
    // Rethrow
    THROW_FLX_EXCEPTION( CONFIG, "Error reading YAML-file: " << ex.what() );
  }

  file.close();
#endif // FFLASH_STATIC
}

// ----------------------------------------------------------------------------

unsigned int FlxCard::link_mode()
{
  unsigned int linkmode = flxcard::LINKMODE_INVALID;

#if REGMAP_VERSION >= 0x500
  if( m_fullmodeType )
    {
      linkmode = flxcard::LINKMODE_FULL;
    }
  else if( m_lpgbtType )
    {
      if( m_lpgbt5Gbps )
        {
          if( m_lpgbtFec12 )
            linkmode = flxcard::LINKMODE_LPGBT5_F12;
          else
            linkmode = flxcard::LINKMODE_LPGBT5_F5;
        }
    else
      {
        if( m_lpgbtFec12 )
          linkmode = flxcard::LINKMODE_LPGBT10_F12;
        else
          linkmode = flxcard::LINKMODE_LPGBT10_F5;
      }
    }
  else
    {
      uint64_t wm = m_bar2->WIDE_MODE;
      if( wm == 1 )
        linkmode = flxcard::LINKMODE_GBTWIDE;
      else
        linkmode = flxcard::LINKMODE_GBT;
    }
#endif // REGMAP_VERSION
  return linkmode;
}

// ----------------------------------------------------------------------------

// FELIX e-link numbering
static const int ELINK_MASK         = 0x07FF;
static const int ELINK_SHIFT        = 0;
static const int LINK_MASK          = 0x07C0;
static const int LINK_SHIFT         = 6;
// FELIX e-link numbering: GBT
static const int EGROUP_MASK        = 0x0038;
static const int EGROUP_SHIFT       = 3;
static const int EPATH_MASK         = 0x0007;
// FELIX e-link numbering: lpGBT
static const int EGROUP_MASK_LPGBT  = 0x001C;
static const int EGROUP_SHIFT_LPGBT = 2;
static const int EPATH_MASK_LPGBT   = 0x0003;

void FlxCard::readConfiguration()
{
#if REGMAP_VERSION >= 0x500
  LOG( "called" );
  if( m_fd < 0 )
    {
      LOG( "not open" );
      return;
    }

  // Link mode
  unsigned int linkmode = this->link_mode();
  for( uint64_t lnk=0; lnk<m_numberOfChans; ++lnk )
    m_linkConfig[lnk].setLinkMode( linkmode );

  // Obtain EC/IC e-link indices (offset in FLX e-link number
  // with respect to link number) as used by the firmware
  std::pair<int,int> p;
  p = this->ec_elink_indices();
  int ecToHostIndex   = p.first;
  int ecFromHostIndex = p.second;
  p = this->ic_elink_indices();
  int icToHostIndex   = p.first;
  int icFromHostIndex = p.second;

  // Store the firmware-assigned EC/IC e-link numbers in the link configurations
  for( uint64_t lnk=0; lnk<m_numberOfChans; ++lnk )
    {
      m_linkConfig[lnk].setEcToHostIndex( ecToHostIndex );
      m_linkConfig[lnk].setIcToHostIndex( icToHostIndex );
      m_linkConfig[lnk].setAuxToHostIndex( icToHostIndex+1 );

      m_linkConfig[lnk].setEcFromHostIndex( ecFromHostIndex );
      m_linkConfig[lnk].setIcFromHostIndex( icFromHostIndex );
      m_linkConfig[lnk].setAuxFromHostIndex( icFromHostIndex+1 );
    }

  uint64_t lnk, egroup;
  uint32_t enables, width, truncation;
  uint64_t modes;

  // To Host:
  for( lnk=0; lnk<m_numberOfChans; ++lnk )
    {
      truncation = 0;
      if( lnk < flxcard::FLX_LINKS/2 )
      for( egroup=0; egroup<flxcard::FLX_TOHOST_GROUPS-1; ++egroup )
        {
          volatile flxcard_decoding_egroup_ctrl_t *config =
            &m_bar2->DECODING_EGROUP_CTRL_GEN[lnk].DECODING_EGROUP[egroup].EGROUP;
          // There are only 8 enable bits: the width setting determines
          // what the E-links width is (they are the same within an E-group)
          enables = (uint32_t) config->EPATH_ENA;
          width   = (uint32_t) config->EPATH_WIDTH;
          modes   = config->PATH_ENCODING; // 4 bits per E-path!
          // Collect HDLC truncation bits
          truncation |= (config->ENABLE_TRUNCATION << (egroup+3));

          // FULL mode?
          if( egroup == 0 && linkmode == flxcard::LINKMODE_FULL )
            if( (m_bar2->DECODING_EGROUP_CTRL_GEN[lnk].DECODING_EGROUP[0].EGROUP.EPATH_ENA & 1) != 0 )
              enables |= flxcard::FULL_ENABLED;

          m_linkConfig[lnk].setWidthToHost( egroup, width );

          // Store in our local configuration structure
          m_linkConfig[lnk].setEnablesToHost( egroup, enables );
          m_linkConfig[lnk].setModesToHost( egroup, modes );
        }

      // and the EC+IC+AUX+TTCtoHost egroup
      enables = 0x0000;

      volatile flxcard_mini_egroup_tohost_t *mini_egroup_tohost =
        &m_bar2->MINI_EGROUP_TOHOST_GEN[lnk].MINI_EGROUP_TOHOST;

      // To-host EC enabled/disabled
      if( mini_egroup_tohost->EC_ENABLE )
        enables |= flxcard::EC_ENABLED;

      // To-host EC encoding: 4 bits
      modes = (uint64_t) mini_egroup_tohost->EC_ENCODING << (flxcard::EC_ENABLED_BIT*4);

      // To-host IC enabled/disabled
      if( mini_egroup_tohost->IC_ENABLE )
        enables |= flxcard::IC_ENABLED;

      // To-host AUX enabled/disabled
      if( mini_egroup_tohost->AUX_ENABLE && m_firmwareType == FIRMW_LTDB )
        enables |= flxcard::AUX_ENABLED;

      // TTC-to-Host enabled/disabled
      // (stored as part of link #0 configuration)
      if( lnk == 0 && m_bar2->TTC_TOHOST_ENABLE )
        enables |= flxcard::TTC2H_ENABLED;

      // Collect HDLC truncation bits
      if( mini_egroup_tohost->ENABLE_EC_TRUNCATION )
        truncation |= (1<<0);
      if( mini_egroup_tohost->ENABLE_IC_TRUNCATION )
        truncation |= (1<<1);
      if( mini_egroup_tohost->ENABLE_AUX_TRUNCATION )
        truncation |= (1<<2);
      // ..and configure
      m_linkConfig[lnk].setMaxChunkWord( truncation );

      // Store in our local configuration structure
      m_linkConfig[lnk].setEnablesToHost( flxcard::TOHOST_MINI_GROUP, enables );
      m_linkConfig[lnk].setModesToHost( flxcard::TOHOST_MINI_GROUP, modes );

      // TTC Clock setting (stored as part of link #0 configuration)
      if( lnk == 0 )
        {
          bool ttc_clock = (m_bar2->MMCM_MAIN.LCLK_SEL == 0);
          m_linkConfig[lnk].setTtcClock( ttc_clock );
        }

      // E-link assigned DMA controller indices
      int elinknr;
      int max_pathnr;
      if( m_lpgbtType )
        max_pathnr = 4;
      else
        max_pathnr = 8;
      if( lnk < flxcard::FLX_LINKS/2 )
      for( egroup=0; egroup<flxcard::FLX_TOHOST_GROUPS-1; ++egroup )
        {
          uint32_t dma_indices = 0;
          for( int path=0; path<max_pathnr; ++path )
            {
              if( m_lpgbtType )
                elinknr = ((lnk << LINK_SHIFT) |
                           (egroup << EGROUP_SHIFT_LPGBT) | path);
              else
                elinknr = ((lnk << LINK_SHIFT) |
                           (egroup << EGROUP_SHIFT) | path);

              // Set 'address' (and read current DMA index setting)
              m_bar2->CRTOHOST_DMA_DESCRIPTOR_2.AXIS_ID = elinknr;
              // Read DMA index setting
              dma_indices |= ((m_bar2->CRTOHOST_DMA_DESCRIPTOR_2.DESCR_READ & 0xF) << (path*4));
            }
          m_linkConfig[lnk].setDmaIndices( egroup, dma_indices );
        }
      uint32_t dma_indices = 0;
      // EC
      elinknr = (lnk << LINK_SHIFT) | ecToHostIndex;
      m_bar2->CRTOHOST_DMA_DESCRIPTOR_2.AXIS_ID = elinknr;
      // Read EC DMA index setting, store as path 7
      dma_indices |= ((m_bar2->CRTOHOST_DMA_DESCRIPTOR_2.DESCR_READ & 0xF) << (7*4));
      // IC
      elinknr = (lnk << LINK_SHIFT) | icToHostIndex;
      m_bar2->CRTOHOST_DMA_DESCRIPTOR_2.AXIS_ID = elinknr;
      // Read EC DMA index setting, store as path 6
      dma_indices |= ((m_bar2->CRTOHOST_DMA_DESCRIPTOR_2.DESCR_READ & 0xF) << (6*4));
      // AUX
      elinknr = (lnk << LINK_SHIFT) | (icToHostIndex + 1);
      m_bar2->CRTOHOST_DMA_DESCRIPTOR_2.AXIS_ID = elinknr;
      // Read AUX DMA index setting, store as path 5
      dma_indices |= ((m_bar2->CRTOHOST_DMA_DESCRIPTOR_2.DESCR_READ & 0xF) << (5*4));
      // TTCtoHost (stored as part of link #0 configuration)
      if( lnk == 0 )
        {
          elinknr = (24 << LINK_SHIFT); // E-link 0x600
          m_bar2->CRTOHOST_DMA_DESCRIPTOR_2.AXIS_ID = elinknr;
          // Read TTCtoHost DMA index setting, store as path 0
          dma_indices |= ((m_bar2->CRTOHOST_DMA_DESCRIPTOR_2.DESCR_READ & 0xF) << (0*4));
        }
      // Store as group setting
      m_linkConfig[lnk].setDmaIndices( flxcard::TOHOST_MINI_GROUP, dma_indices );

      // Stream ID bits
      uint64_t *p = (uint64_t *) &m_bar2->PATH_HAS_STREAM_ID[lnk];
      uint64_t streamid = *p;
      for( egroup=0; egroup<flxcard::FLX_TOHOST_GROUPS-1; ++egroup )
        {
          m_linkConfig[lnk].setStreamIdBits( egroup, streamid & 0xFF );
          streamid >>= 8;
        }

      // EXTRA: Also set enable-bits that match the *actual* EC/IC e-link numbers
      if( mini_egroup_tohost->EC_ENABLE )
        {
          int epath;
          if( m_lpgbtType )
            {
              egroup = ((ecToHostIndex & EGROUP_MASK_LPGBT) >> EGROUP_SHIFT_LPGBT);
              epath = ecToHostIndex & EPATH_MASK_LPGBT;
            }
          else
            {
              egroup = ((ecToHostIndex & EGROUP_MASK) >> EGROUP_SHIFT);
              epath = ecToHostIndex & EPATH_MASK;
            }
          uint32_t ena = m_linkConfig[lnk].enablesToHost( egroup );
          m_linkConfig[lnk].setEnablesToHost( egroup, ena | (1<<epath) );

          // EC mode
          uint64_t mod = m_linkConfig[lnk].modesToHost( egroup );
          mod &= ~(0xF << (epath*4));
          mod |= (uint64_t) mini_egroup_tohost->EC_ENCODING << (epath*4);
          m_linkConfig[lnk].setModesToHost( egroup, mod );

          // EC width: 2 bits
          m_linkConfig[lnk].setWidthToHost( egroup, 0 );
        }

      if( mini_egroup_tohost->IC_ENABLE )
        {
          int epath;
          if( m_lpgbtType )
            {
              egroup = ((icToHostIndex & EGROUP_MASK_LPGBT) >> EGROUP_SHIFT_LPGBT);
              epath = icToHostIndex & EPATH_MASK_LPGBT;
            }
          else
            {
              egroup = ((icToHostIndex & EGROUP_MASK) >> EGROUP_SHIFT);
              epath = icToHostIndex & EPATH_MASK;
            }
          uint32_t ena = m_linkConfig[lnk].enablesToHost( egroup );
          m_linkConfig[lnk].setEnablesToHost( egroup, ena | (1<<epath) );

          // IC mode: HDLC
          uint64_t mod = m_linkConfig[lnk].modesToHost( egroup );
          mod &= ~(0xF << (epath*4));
          mod |= (uint64_t) 2 << (epath*4);
          m_linkConfig[lnk].setModesToHost( egroup, mod );

          // IC width: 2 bits
          m_linkConfig[lnk].setWidthToHost( egroup, 0 );
        }
    }

  // From Host:
  for( lnk=0; lnk<m_numberOfChans; ++lnk )
    {
      if( lnk < flxcard::FLX_LINKS/2 )
      for( egroup=0; egroup<flxcard::FLX_FROMHOST_GROUPS-1; ++egroup )
        {
          volatile flxcard_encoding_egroup_ctrl_t *config =
            &m_bar2->ENCODING_EGROUP_CTRL_GEN[lnk].ENCODING_EGROUP[egroup].ENCODING_EGROUP_CTRL;
          uint32_t width, ttc_opt;
          enables = (uint32_t) config->EPATH_ENA;
          width   = (uint32_t) config->EPATH_WIDTH;
          modes   = config->PATH_ENCODING; // 4 bits per E-path!
          ttc_opt = config->TTC_OPTION;

          // Store in our local configuration structure
          m_linkConfig[lnk].setWidthFromHost( egroup, width );
          m_linkConfig[lnk].setTtcOptionFromHost( egroup, ttc_opt );
          m_linkConfig[lnk].setEnablesFromHost( egroup, enables );
          m_linkConfig[lnk].setModesFromHost( egroup, modes );
        }

      // and the EC+IC+AUX egroup
      enables = 0x0000;

      volatile flxcard_mini_egroup_fromhost_t *mini_egroup_fromhost =
        &m_bar2->MINI_EGROUP_FROMHOST_GEN[lnk].MINI_EGROUP_FROMHOST;

      // From-host EC enabled/disabled
      if( mini_egroup_fromhost->EC_ENABLE )
        enables |= flxcard::EC_ENABLED;

      // From-host EC encoding: 4 bits
      modes = (uint64_t) mini_egroup_fromhost->EC_ENCODING<<(flxcard::EC_ENABLED_BIT*4);

      // From-host IC enabled/disabled
      if( mini_egroup_fromhost->IC_ENABLE )
        enables |= flxcard::IC_ENABLED;
 
      // From-host AUX enabled/disabled
      if( mini_egroup_fromhost->AUX_ENABLE && m_firmwareType == FIRMW_LTDB )
        enables |= flxcard::AUX_ENABLED;

      // Store in our local configuration structure
      m_linkConfig[lnk].setEnablesFromHost( flxcard::FROMHOST_MINI_GROUP, enables );
      m_linkConfig[lnk].setModesFromHost( flxcard::FROMHOST_MINI_GROUP, modes );

      // FromHost link mode: GBT or LTI (applies to FULLMODE only)
      if( (m_bar2->LINK_FULLMODE_LTI & (1 << lnk)) != 0 )
        m_linkConfig[lnk].setLtiTtc( true );
      else
        m_linkConfig[lnk].setLtiTtc( false );

      // EXTRA: Also set enable-bits that match the *actual* EC/IC e-link numbers
      // NB: special for STRIP (lpGBT) firmware which has 5 FromHost epaths per egroup
      if( mini_egroup_fromhost->EC_ENABLE )
        {
          int epath;
          if( m_lpgbtType )
            {
              if( m_firmwareType == FIRMW_STRIP )
                {
                  egroup = ecFromHostIndex/5;
                  epath  = ecFromHostIndex - (ecFromHostIndex/5)*5;
                }
              else
                {
                  egroup = ((ecFromHostIndex & EGROUP_MASK_LPGBT) >>
                            EGROUP_SHIFT_LPGBT);
                  epath = ecFromHostIndex & EPATH_MASK_LPGBT;
                }
            }
          else
            {
              egroup = ((ecFromHostIndex & EGROUP_MASK) >> EGROUP_SHIFT);
              epath = ecFromHostIndex & EPATH_MASK;
            }
          uint32_t ena = m_linkConfig[lnk].enablesFromHost( egroup );
          m_linkConfig[lnk].setEnablesFromHost( egroup, ena | (1<<epath) );

          // EC mode
          uint64_t mod = m_linkConfig[lnk].modesFromHost( egroup );
          mod &= ~(0xF << (epath*4));
          mod |= (uint64_t) mini_egroup_fromhost->EC_ENCODING << (epath*4);
          m_linkConfig[lnk].setModesFromHost( egroup, mod );

          // EC width: 2 bits
          m_linkConfig[lnk].setWidthFromHost( egroup, 0 );
        }

      if( mini_egroup_fromhost->IC_ENABLE )
        {
          int epath;
          if( m_lpgbtType )
            {
              if( m_firmwareType == FIRMW_STRIP )
                {
                  egroup = icFromHostIndex/5;
                  epath  = icFromHostIndex - (icFromHostIndex/5)*5;
                }
              else
                {
                  egroup = ((icFromHostIndex & EGROUP_MASK_LPGBT) >> EGROUP_SHIFT_LPGBT);
                  epath = icFromHostIndex & EPATH_MASK_LPGBT;
                }
            }
          else
            {
              egroup = ((icFromHostIndex & EGROUP_MASK) >> EGROUP_SHIFT);
              epath = icFromHostIndex & EPATH_MASK;
            }
          uint32_t ena = m_linkConfig[lnk].enablesFromHost( egroup );
          m_linkConfig[lnk].setEnablesFromHost( egroup, ena | (1<<epath) );

          // IC mode: HDLC
          uint64_t mod = m_linkConfig[lnk].modesFromHost( egroup );
          mod &= ~(0xF << (epath*4));
          mod |= (uint64_t) 2 << (epath*4);
          m_linkConfig[lnk].setModesFromHost( egroup, mod );

          // IC width: 2 bits
          m_linkConfig[lnk].setWidthFromHost( egroup, 0 );
        }
    }
  m_configRead = true;
#endif // REGMAP_VERSION
}

// ----------------------------------------------------------------------------

u_int FlxCard::number_of_elinks_tohost( int dma_index )
{
  if( !m_configRead )
    this->readConfiguration();

  u_int cnt = 0;
  for( u_int chan=0; chan<m_numberOfChans; ++chan )
    {
      if( dma_index == -1 )
        {
          cnt += m_linkConfig[chan].numberOfElinksToHost();
          cnt += m_linkConfig[chan].numberOfEcIcToHost();
        }
      else
        {
          cnt += m_linkConfig[chan].numberOfElinksToHost( dma_index );
        }
    }
  return cnt;
}

// ----------------------------------------------------------------------------

u_int FlxCard::number_of_elinks_toflx()
{
  if( !m_configRead )
    this->readConfiguration();

  u_int cnt = 0;
  for( u_int chan=0; chan<m_numberOfChans; ++chan )
    {
      cnt += m_linkConfig[chan].numberOfElinksFromHost();
      cnt += m_linkConfig[chan].numberOfEcIcFromHost();
    }
  return cnt;
}

// ----------------------------------------------------------------------------

std::vector<flxcard::elink_descr_t> FlxCard::elinks_tohost( int dma_index )
{
  if( !m_configRead )
    this->readConfiguration();

  // Compile a list of descriptors of enabled ToHost e-link numbers
  std::vector<flxcard::elink_descr_t> elinks, chan_elinks;
  for( u_int chan=0; chan<m_numberOfChans; ++chan )
    {
      chan_elinks = m_linkConfig[chan].elinksEnabledToHost( chan, dma_index );
      elinks.insert( elinks.end(), chan_elinks.begin(), chan_elinks.end() );
    }
  return elinks;
}

// ----------------------------------------------------------------------------

std::vector<flxcard::elink_descr_t> FlxCard::elinks_toflx()
{
  if( !m_configRead )
    this->readConfiguration();

  // Compile a list of descriptors of enabled FromHost e-link numbers
  std::vector<flxcard::elink_descr_t> elinks, chan_elinks;
  bool exclude_ttc = true;
  for( u_int chan=0; chan<m_numberOfChans; ++chan )
    {
      chan_elinks = m_linkConfig[chan].elinksEnabledFromHost( chan, exclude_ttc );
      elinks.insert( elinks.end(), chan_elinks.begin(), chan_elinks.end() );
    }
  return elinks;
}

// ----------------------------------------------------------------------------

bool FlxCard::is_elink_enabled( u_int channel, u_int egroup, u_int epath, bool is_to_flx )
{
#if REGMAP_VERSION < 0x0500
  if (m_firmwareType == FIRMW_FULL) {
    return !is_to_flx && ((m_bar2->CR_FM_PATH_ENA >> channel) & 1);
  }
  // MROD
  else if (m_firmwareType == FIRMW_MROD) {
    u_long eproc_ena = m_bar2->MROD_EP0_CSMENABLE;
    return !is_to_flx && ((eproc_ena >> channel) & 1);
  }
  // GBT mode
  else {
    u_long eproc_ena = is_to_flx ?
      m_bar2->CR_GBT_CTRL[channel].EGROUP_FROMHOST[egroup].FROMHOST.EPROC_ENA :
      m_bar2->CR_GBT_CTRL[channel].EGROUP_TOHOST[egroup].TOHOST.EPROC_ENA;
    // Central Router documentation p. 13
    switch (epath) {
    case 0: return ((eproc_ena >> 3) & 1) || ((eproc_ena >> 7) & 1);
    case 1: return ((eproc_ena >> 1) & 1) || ((eproc_ena >> 8) & 1);
    case 2: return ((eproc_ena >> 4) & 1) || ((eproc_ena >> 9) & 1);
    case 3: return ((eproc_ena >> 0) & 1) || ((eproc_ena >> 10) & 1);
    case 4: return ((eproc_ena >> 5) & 1) || ((eproc_ena >> 11) & 1);
    case 5: return ((eproc_ena >> 2) & 1) || ((eproc_ena >> 12) & 1);
    case 6: return ((eproc_ena >> 6) & 1) || ((eproc_ena >> 13) & 1);
    case 7: return ((eproc_ena >> 14) & 1);
    default: return false;
    }
    }
  return false;

#else
  //if( !m_configRead )
  //  this->readConfiguration();
  //return m_linkConfig[channel].isEnabled( egroup, epath, 0, !is_to_flx );

  if (m_firmwareType == FIRMW_MROD) {
    u_long eproc_ena = m_bar2->MROD_EP0_CSMENABLE;
    return !is_to_flx && ((eproc_ena >> channel) & 1);
  }
  else {
    // GBT mode, FULL mode, LPGBT mode
    uint32_t enables_phase2;
    if( is_to_flx ) {
      enables_phase2 = (uint32_t) m_bar2->ENCODING_EGROUP_CTRL_GEN[channel].ENCODING_EGROUP[egroup].ENCODING_EGROUP_CTRL.EPATH_ENA;
    } else {
      enables_phase2 = (uint32_t) m_bar2->DECODING_EGROUP_CTRL_GEN[channel].DECODING_EGROUP[egroup].EGROUP.EPATH_ENA;
    }
    return (enables_phase2 >> epath) & 0x01;
  }
#endif // REGMAP_VERSION
}

// ----------------------------------------------------------------------------

unsigned int FlxCard::tohost_elink_dmaid( uint16_t elinknr )
{
#if REGMAP_VERSION < 0x0500
  return 0;
#else
  m_bar2->CRTOHOST_DMA_DESCRIPTOR_2.AXIS_ID = elinknr;
  return (m_bar2->CRTOHOST_DMA_DESCRIPTOR_2.DESCR_READ & 0xF);
#endif
}

// ----------------------------------------------------------------------------

unsigned int FlxCard::tohost_elink_dmaid( u_int channel, u_int egroup, u_int epath )
{
#if REGMAP_VERSION < 0x0500
  return 0;
#else
  uint16_t elinknr = flxcard::LinkConfig::elinkNumber( channel, egroup, epath, m_lpgbtType );
  return this->tohost_elink_dmaid( elinknr );
#endif
}

// ----------------------------------------------------------------------------

bool FlxCard::has_tohost_elink_streams( u_int channel, u_int egroup, u_int epath )
{
  if(channel > m_bar2->NUM_OF_CHANNELS){
    return false;
  }
  u_long enable = 0;
#if REGMAP_VERSION < 0x0500
  switch (egroup)
    {
    case 0:
      enable = m_bar2->PATH_HAS_STREAM_ID[channel].TOHOST.EGROUP0;
      break;
    case 1:
      enable = m_bar2->PATH_HAS_STREAM_ID[channel].TOHOST.EGROUP1;
      break;
    case 2:
      enable = m_bar2->PATH_HAS_STREAM_ID[channel].TOHOST.EGROUP2;
      break;
    case 3:
      enable = m_bar2->PATH_HAS_STREAM_ID[channel].TOHOST.EGROUP3;
      break;
    case 4:
      enable = m_bar2->PATH_HAS_STREAM_ID[channel].TOHOST.EGROUP4;
      break;
    case 5:
      enable = m_bar2->PATH_HAS_STREAM_ID[channel].TOHOST.EGROUP5;
      break;
    case 6:
      enable = m_bar2->PATH_HAS_STREAM_ID[channel].TOHOST.EGROUP6;
      break;
    default:
      break;
    }
#else
  switch (egroup)
    {
    case 0:
      enable = m_bar2->PATH_HAS_STREAM_ID[channel].HAS_STREAM_ID.EGROUP0;
      break;
    case 1:
      enable = m_bar2->PATH_HAS_STREAM_ID[channel].HAS_STREAM_ID.EGROUP1;
      break;
    case 2:
      enable = m_bar2->PATH_HAS_STREAM_ID[channel].HAS_STREAM_ID.EGROUP2;
      break;
    case 3:
      enable = m_bar2->PATH_HAS_STREAM_ID[channel].HAS_STREAM_ID.EGROUP3;
      break;
    case 4:
      enable = m_bar2->PATH_HAS_STREAM_ID[channel].HAS_STREAM_ID.EGROUP4;
      break;
    case 5:
      enable = m_bar2->PATH_HAS_STREAM_ID[channel].HAS_STREAM_ID.EGROUP5;
      break;
    case 6:
      enable = m_bar2->PATH_HAS_STREAM_ID[channel].HAS_STREAM_ID.EGROUP6;
      break;
    default:
      break;
    }
#endif // REGMAP_VERSION
    return static_cast<bool>( (enable >> epath) & 1);
}

// ----------------------------------------------------------------------------

uint8_t FlxCard::detector_id()
{
  uint8_t did{0};
#if REGMAP_VERSION >= 0x0500
  if( m_bar2->DETECTOR_ID.VALID ) {
    did = static_cast<int>(m_bar2->DETECTOR_ID.VALUE);
  }
#endif
  return did;
}

// ----------------------------------------------------------------------------

uint16_t FlxCard::connector_id( uint16_t channel )
{
#if REGMAP_VERSION < 0x0500
  return static_cast<uint16_t>(m_deviceNumber);
#else
  if( channel > m_bar2->NUM_OF_CHANNELS ) {
    THROW_FLX_EXCEPTION(PARAM, "connector_id called for invalid channel " << channel);
  }
  uint16_t cid{0};
  if( channel < 24 ) {
    if( m_deviceNumber == 0 ) {
      if( m_bar2->CONNECTOR_ID_0.VALID )
        cid = m_bar2->CONNECTOR_ID_0.VALUE;
      else
        cid = 0;
    } else {
      if( m_bar2->CONNECTOR_ID_1.VALID )
        cid = m_bar2->CONNECTOR_ID_1.VALUE;
      else
        cid = 1;
    }
  } else {
    if( m_deviceNumber == 0 ) {
      if( m_bar2->CONNECTOR_ID_2.VALID )
        cid = m_bar2->CONNECTOR_ID_2.VALUE;
      else
        cid = 2;
    } else {
      if( m_bar2->CONNECTOR_ID_3.VALID )
        cid = m_bar2->CONNECTOR_ID_3.VALUE;
      else
        cid = 3;
    }
  }
  return cid;
#endif // REGMAP_VERSION
}

// ----------------------------------------------------------------------------
// Private functions (not part of the user API)
// ----------------------------------------------------------------------------

u_long FlxCard::map_memory_bar( u_long pci_addr, size_t size )
{
  LOG( "called" );

  // Get system page size
  long pagesz = sysconf(_SC_PAGE_SIZE);
  LOG( "pagesz = " << TOHEX(pagesz) );

  // Sanity check
  if( pagesz == -1 )
    pagesz = 0x10000;

  // Turn value into its matching bitmask: mmap requires pagesize alignment
  u_long offset = pci_addr & (pagesz-1);
  pci_addr &= (0xffffffffffffffffL & (~(pagesz-1)));

  void *vaddr = mmap(0, size, (PROT_READ|PROT_WRITE), MAP_SHARED, m_fd, pci_addr);
  if( vaddr == MAP_FAILED )
    {
      LOG( "Error from mmap for pci_addr = "
            << TOHEX(pci_addr) << " and size = " << size );
      THROW_FLX_EXCEPTION(MAPERROR, "Error from mmap for pci_addr = "
                          << TOHEX(pci_addr) << " and size = " << size);
    }

  return (u_long)vaddr + offset;
}

// ----------------------------------------------------------------------------

void FlxCard::unmap_memory_bar( u_long vaddr, size_t size )
{
  LOG( "called" );

  int ret = munmap((void *)vaddr, size);
  if( ret )
    {
      LOG( "Error from munmap, errno = " << errno );
      THROW_FLX_EXCEPTION(UNMAPERROR, "Error from munmap, errno = " << errno);
    }
}

// ----------------------------------------------------------------------------

void FlxCard::i2c_wait_not_full()
{
  LOG( "called" );

  // Set up watchdog
  m_timeout = 0;
  struct itimerval timer;
  timer.it_value.tv_sec = 1;     // One second
  timer.it_value.tv_usec = 0;
  timer.it_interval.tv_sec = 0;
  timer.it_interval.tv_usec = 0; // Only one shot
  setitimer(ITIMER_REAL, &timer, NULL);

  u_long status = cfg_get_reg(REG_I2C_WR);
  while( status & I2C_FULL_FLAG )
    {
      usleep(I2C_SLEEP);
      if( m_timeout )
        {
          THROW_FLX_EXCEPTION(I2C, "Timeout I2C-wait-not-full");
        }
      status = cfg_get_reg(REG_I2C_WR);
    }

  // Stop watchdog
  timer.it_value.tv_usec = 0;
  timer.it_value.tv_sec = 0;
  setitimer(ITIMER_REAL, &timer, NULL);
}

// ----------------------------------------------------------------------------

void FlxCard::i2c_wait_not_empty()
{
  LOG( "called" );

  // Set up watchdog
  m_timeout = 0;
  struct itimerval timer;
  timer.it_value.tv_sec = 1;     // One second
  timer.it_value.tv_usec = 0;
  timer.it_interval.tv_sec = 0;
  timer.it_interval.tv_usec = 0; // Only one shot
  setitimer(ITIMER_REAL, &timer, NULL);

  u_long status = cfg_get_reg(REG_I2C_RD);
  while( status & I2C_EMPTY_FLAG )
    {
      usleep(I2C_SLEEP);
      if( m_timeout )
        {
          THROW_FLX_EXCEPTION(I2C, "Timeout I2C-wait-not-empty");
        }
      status = cfg_get_reg(REG_I2C_RD);
    }

  // Stop watchdog
  timer.it_value.tv_usec = 0;
  timer.it_value.tv_sec = 0;
  setitimer(ITIMER_REAL, &timer, NULL);
}

// ----------------------------------------------------------------------------

int FlxCard::i2c_parse_address_string( const char *str,
                                       u_char     *switch1_val,
                                       u_char     *switch2_val,
                                       u_char     *switch3_val,
                                       u_char     *dev_addr )
{
  // This method understands three formats of device string
  // Format 1 is a symbolic name such as "ADN2814". The names are defined
  //          in the arrays i2c_devices_FLX_7[09,10,11] at the beginning of this file
  // Format 2 has the structure "P1:ADD" with P1 = Port number (of switch 1),
  //          ADDR = address of the I2C device
  // Format 3 has the structure "P1:P2:ADD" with P1 = Port number of switch 1,
  //          P2 = Port number of switch 3, ADDR = address of the i2c device
  //
  // The I2C-switches port numbers have two formats too.
  // In a device string of format 2 or 3 the port is decimal
  // (e.g. "4:0x70" refers to port 4 and address 0x70)
  // The port numbers as provided by the i2c_devices_FLX_7xx structures
  // are coded as a bit mask. e.g. 0x8 selectes the 4th port of the switch.
  // This function converts all switch port numbers to the binary format
  // (a value 'switchX_val' ready to write directly into the switch).

  LOG( "called, string=" << str );

  // Initialize values to be returned
  *switch1_val = 0;
  *switch2_val = 0;
  *switch3_val = 0;
  *dev_addr = 0;

  // Check if we have a device string of Format 1
  std::string namestr( str );
  for( size_t i=0; i<namestr.size(); ++i )
    {
      // Convert to uppercase and '-' to '_'
      namestr[i] = toupper( namestr[i] );
      if( namestr[i] == '-' ) namestr[i] = '_';
    }

  // Sanity
  i2c_device_t *devices = m_i2cDevices;
  if( devices == 0 )
    return -1;

  for( ; devices->name != NULL; ++devices )
    {
      if( strcmp(namestr.data(), devices->name) == 0 )
        {
          LOG( "Device found!" );
          LOG( "devices->name        = " << devices->name );
          LOG( "devices->description = " << devices->description );
          LOG( "devices->address     = " << (u_int)devices->address );
          LOG( "devices->switch_val  = "
               << (u_int)devices->i2c_switch_val[0] << " "
               << (u_int)devices->i2c_switch_val[1] << " "
               << (u_int)devices->i2c_switch_val[2] );

          *switch1_val = devices->i2c_switch_val[0];
          *switch2_val = devices->i2c_switch_val[1];
          *switch3_val = devices->i2c_switch_val[2];
          *dev_addr    = devices->address;

          return 0;
        }
    }

  // Check if we have a device string of Format 2 or 3
  LOG( "Check if we have a device of Format 2 or 3" );

  char *pcolon1 = strchr(const_cast<char*>(str), ':');
  if( pcolon1 == NULL )
    {
      LOG( "Failed to find a ':' character" );
      return I2C_DEVICE_ERROR_NOT_EXISTING;
    }

  LOG( "rest string = " << pcolon1 );

  unsigned long int val;
  char *pend;
  char *pcolon2 = strchr((pcolon1 + 1), ':');
  if( pcolon2 == NULL )
    {
      LOG( "Format 2 detected" );

      val = strtoul( str, &pend, 0 );
      if( *pend != ':' )
        {
          LOG( "Invalid first I2C port number" );
          return I2C_DEVICE_ERROR_INVALID_PORT;
        }
      *switch1_val = (u_char) (1 << val);

      val = strtoul( pcolon1 + 1, &pend, 0 );
      if( *pend != '\0' )
        {
          LOG( "Invalid I2C address" );
          return I2C_DEVICE_ERROR_INVALID_ADDRESS;
        }
      *dev_addr = (u_char) val;
    }
  else
    {
      LOG( "Format 3 detected" );

      val = strtoul(str, &pend, 0);
      if( *pend != ':' )
        {
          LOG( "Invalid first I2C port number (2)" );
          return I2C_DEVICE_ERROR_INVALID_PORT;
        }
      *switch1_val = (u_char) (1 << val);

      val = strtoul( pcolon1 + 1, &pend, 0 );
      if( *pend != ':' )
        {
          LOG( "Invalid second I2C port number" );
          return I2C_DEVICE_ERROR_INVALID_PORT;
        }
      *switch2_val = (u_char) (1 << val);

      val = strtoul( pcolon2 + 1, &pend, 0 );
      if( *pend != '\0' )
        {
          LOG( "Invalid I2C address" );
          return I2C_DEVICE_ERROR_INVALID_ADDRESS;
        }
      *dev_addr = (u_char) val;
    }

  LOG( "binary *switch1_val = " << (u_int)*switch1_val );
  LOG( "binary *switch2_val = " << (u_int)*switch2_val );
  LOG( "*dev_addr     = " << (u_int)*dev_addr );

  return 0;
}

// ----------------------------------------------------------------------------

void FlxCard::i2c_set_switches( u_char switch1_val,
                                u_char switch2_val,
                                u_char switch3_val )
{
  // Set the I2C switch or switches, depending on card type
  if( m_cardType == 712 || m_cardType == 711 )
    {
      i2c_write_byte( I2C_ADDR_SWITCH1_FLX_711, switch1_val );
    }
  else if( m_cardType == 710 )
    {
      i2c_write_byte( I2C_ADDR_SWITCH1_FLX_710, switch1_val );
    }
  else if( m_cardType == 709 )
    {
      i2c_write_byte( I2C_ADDR_SWITCH1_FLX_709, switch1_val );
      i2c_write_byte( I2C_ADDR_SWITCH2_FLX_709, switch2_val );
    }
  else if( m_cardType == 128 )
    {
      i2c_write_byte( I2C_ADDR_SWITCH1_FLX_128, switch1_val );
      i2c_write_byte( I2C_ADDR_SWITCH2_FLX_128, switch2_val );
      i2c_write_byte( I2C_ADDR_SWITCH3_FLX_128, switch3_val );
    }
  else if( m_cardType == 182 )
    {
      i2c_write_byte( I2C_ADDR_SWITCH1_FLX_182, switch1_val );
    }
  else if( m_cardType == 155 )
    {
      i2c_write_byte( I2C_ADDR_SWITCH1_FLX_155, switch1_val );
    }
}

// ----------------------------------------------------------------------------

void FlxCard::gbt_tx_configuration( int channel_tx_mode, int alignment )
{
  LOG( "called" );

  // 2- GBT TX configuration

  // 2.1- Quads Soft TX reset
  LOG( "***Quads TX soft reset. Register GBT_SOFT_TX_RESET..." );

  m_bar2->GBT_SOFT_TX_RESET.RESET_ALL = 0xFFF;
  usleep(WAIT_TIME_600);
  m_bar2->GBT_SOFT_TX_RESET.RESET_ALL = 0x000;
  sleep(2);
  LOG( "Done!" );

  // 2.2- Set TX mode
  LOG( "***Configuring TX mode. Register GBT_DATA_TXFORMAT1/2..." );

  if( channel_tx_mode != FLX_GBT_TMODE_FEC )
    {
      LOG( "WideBus-->0x55555500555555" );
      m_bar2->GBT_DATA_TXFORMAT1 = 0x555555555555;
      m_bar2->GBT_DATA_TXFORMAT2 = 0x555555555555;
    }
  else
    {
      LOG( "FEC-->0x0" );
      m_bar2->GBT_DATA_TXFORMAT1 = 0x00000000000000;
      m_bar2->GBT_DATA_TXFORMAT2 = 0x00000000000000;
    }

  // 2.3 TX time domain crossing selection
  LOG( "***Set TX time domain crossing mode. Register GBT_TX_TC_METHOD..." );
  if( alignment == FLX_GBT_ALIGNMENT_CONTINUOUS )
    {
      LOG( "CONTINUOUS-->0x0FFF0FFF" );
      m_bar2->GBT_TX_TC_METHOD = ALL_BITS;
    }
  else
    {
      LOG( "ONE-->0x0" );
      m_bar2->GBT_DATA_TXFORMAT1 = 0x00000000000000;
      m_bar2->GBT_DATA_TXFORMAT2 = 0x00000000000000;
    }

  // 2.4- GBT TX reset
  LOG( "***GBT TX reset. Register GBT_TX_RESET..." );
  m_bar2->GBT_TX_RESET = ALL_BITS;
  usleep(WAIT_TIME_600);
  m_bar2->GBT_TX_RESET = 0;
  sleep(1);
  LOG( "Done!" );
}

// ----------------------------------------------------------------------------

int FlxCard::gbt_rx_configuration( int channel_rx_mode )
{
  LOG( "called" );
  // 3-GBT RX configuration

  // 3.1- RX latency Optimization configuration. Write REG_21, 0x0
  //LOG( "***Set RX latency optimization configuration. Register 0x5550..." );
  //aux_value = m_bar2->GBT_RX_OPT;
  //aux_value = aux_value & 0xFFFFFFFF000000;  // Bits 47-24 are reserved.

  //m_bar2->GBT_RX_OPT = aux_value;  // Latency Optimization are 0s.
  //LOG( "Done!" );

  // 3.2 GTH RX reset
  LOG( "***GTH RX reset. Register GBT_GTRX_RESET..." );
  m_bar2->GBT_GTRX_RESET = ALL_BITS;
  usleep(WAIT_TIME_600);
  m_bar2->GBT_GTRX_RESET = 0;
  LOG( "Done!" );

  // 3.3 Set GBT RX encoding mode
  LOG( "***Configuring RX mode. Register GBT_DATA_RXFORMAT1/2..." );
  if( channel_rx_mode != FLX_GBT_TMODE_FEC )
    {
      LOG( "WideBus-->0x55555500555555" );
      m_bar2->GBT_DATA_RXFORMAT1 = 0x555555555555;
      m_bar2->GBT_DATA_RXFORMAT2 = 0x555555555555;
    }
  else
    {
      LOG( "FEC-->0x0" );
      m_bar2->GBT_DATA_RXFORMAT1 = 0x0000000000000000;
      m_bar2->GBT_DATA_RXFORMAT2 = 0x0000000000000000;
    }

  // 3.4 RX alignment
  LOG( "***Channel alignment..." );

  LOG( "Using software for alignment" );
  m_bar2->GBT_MODE_CTRL.RX_ALIGN_SW   = 1;
  m_bar2->GBT_MODE_CTRL.DESMUX_USE_SW = 0;

  LOG( "Determining number of channels..." );
  u_long num_channels   = m_bar2->NUM_OF_CHANNELS;
  u_long pcie_endpoints = m_bar2->NUMBER_OF_PCIE_ENDPOINTS;
  if( pcie_endpoints == 0 )
    {
      // Determine number of channels on the basis of card type
      if( m_cardType == 712 || m_cardType == 711 )
        pcie_endpoints = 2;
      else
        pcie_endpoints = 1;
    }
  LOG( "Channels detected: " << num_channels << "*" << pcie_endpoints );
  num_channels *= pcie_endpoints;

//#define SKIP_ALIGNMENT
#ifdef SKIP_ALIGNMENT
  // Obsolete functions:
  // gbt_software_alignment, gbt_channel_alignment, gbt_topbot_xxx, gbt_shift_phase
  u_int unaligned_channels = 0;
#else
  LOG( "***Starting alignment..." );
  u_int unaligned_channels = gbt_software_alignment(num_channels);
  if( unaligned_channels != 0 )
    // Software alignment
    LOG( "Not all channels align, something must be wrong" );
#endif // SKIP_ALIGNMENT

  // 3.5 GBT RX reset
  LOG( "***GBT TX reset. Register GBT_RX_RESET..." );
  m_bar2->GBT_RX_RESET = ALL_BITS;
  usleep(WAIT_TIME_600);
  // Write REG_25 ----> 0
  m_bar2->GBT_RX_RESET = 0;
  sleep(1);
  LOG( "Done!" );

  // 3.6 Write REG_1 bit 0 --->1; Write REG_1 bit 0 --->0
  LOG( "***Checking alignment status. Register GBT_GENERAL_CTRL..." );
  usleep(WAIT_TIME_600);
  m_bar2->GBT_GENERAL_CTRL = 0x1;
  usleep(WAIT_TIME_600);
  m_bar2->GBT_GENERAL_CTRL = 0x0;
  usleep(WAIT_TIME_600);

  // Read REG_ALIGNMENT_DONE CH[0-23].  If 0, phase alignment?
  u_long alignment_status = m_bar2->GBT_ALIGNMENT_DONE;
  if( unaligned_channels == 0 )
    {
      u_int chan;
      for( chan=0; chan<num_channels; ++chan )
        {
          if( (alignment_status & (1ULL << chan)) == 0 )
            {
              ++unaligned_channels;
              LOG( "Channel " << chan << " not aligned" );
            }
        }
    }
  LOG( "Final check:" );
  LOG( "Alignment_status = " << alignment_status );
  LOG( "***Checking header locked. Register GBT_RX_IS_HEADER..." );
  alignment_status = m_bar2->GBT_RX_IS_HEADER;
  LOG( "Header_locked = " << alignment_status );

  return unaligned_channels;
}

// ----------------------------------------------------------------------------

int FlxCard::gbt_software_alignment( int number_channels )
{
  LOG( "called" );

  bool found;
  int channel;
  int unaligned_channels = 0;
  for( channel=0; channel<number_channels; ++channel )
    {
      LOG( "---Trying to align channel " << channel << " ..." );
      found = gbt_channel_alignment(channel);
      if( found )
        {
          LOG( "Aligned!" );
        }
      else
        {
          LOG( "Not aligned!" );
          ++unaligned_channels;
        }
    }
  return unaligned_channels;
}

// ----------------------------------------------------------------------------

bool FlxCard::gbt_channel_alignment( u_int channel )
{
  u_long phase_found = 0, phase_table[10];
  u_int  oddeven, topbot;
  bool   found;
  // Henk: note that 'found' and 'phase_found' appear to be equivalent:
  //       -> should simplify gbt_topbot_alignment()

  LOG( "called" );
  // 4 Software RX alignment. (Using 240M clock) METHOD PER CHANNEL (x24)/

  // SET TOPBOT & ODDEVEN bits-->0
  topbot  = 0;
  oddeven = 0;
  gbt_topbot_oddeven_set( channel, topbot, oddeven );
  // Check the phase
  phase_found = m_bar2->GBT_ALIGNMENT_DONE;
  phase_found = phase_found & (u_long)(1ULL << channel);

  // Trying alignment with topbot=0
  found = gbt_topbot_alignment( channel, topbot, &phase_found, &oddeven );

  // No phase with topbot=0 --> topbot=1
  if( !found )
    {
      LOG( "***Not good phase for TOPBOT=0, checking TOPBOT=1..." );
      topbot  = 1;
      oddeven = 0;
      gbt_topbot_oddeven_set( channel, topbot, oddeven );
      // Check the phase
      phase_found = m_bar2->GBT_ALIGNMENT_DONE;
      phase_found = phase_found & (u_long)(1ULL << channel);

      // Trying alignment with topbot=1
      found = gbt_topbot_alignment( channel, topbot, &phase_found, &oddeven );
      if( !found )
        LOG( "***Phase not found" );
      else
        LOG( "***Final values: TOPBOT=1 ODDEVEN=" << oddeven );
    }
  else
    {
      LOG( "***Good phase for TOPBOT=0, checking TOPBOT=1..." );
      // Topbot=0 has a phase, check with topbot=1
      for( int phase=0; phase<10; ++phase )
        {
          phase_table[phase] = m_bar2->GBT_CLK_SAMPLED;
          phase_table[phase] = phase_table[phase] & (u_long)(1ULL << channel);
          // Switching the phase
          gbt_shift_phase( channel );
        }

      if( phase_table[1] > 0 )
        {
          if( phase_table[0] == 0 )
            topbot = 1;
          else
            topbot = 0;
        }
      else
        {
          if( phase_table[5] == 0 )
            topbot = 0;
          else
            topbot = 1;
        }

      if( topbot == 1 )
        {
          // TOPBOT=1 recommended
          // Set channel TOPBOT=1
          gbt_topbot_oddeven_set( channel, topbot, oddeven );
          // Switching 5 phases
          for( int phase=0; phase<5; ++phase )
            phase_found = gbt_shift_phase( channel );
        }
    }

  for( int phase=0; phase<10; ++phase )
    {
      gbt_shift_phase( channel );
      phase_table[phase] = m_bar2->GBT_CLK_SAMPLED;
      phase_table[phase] = phase_table[phase] & (u_long)(1ULL << channel);
    }

  return found;
}

// ----------------------------------------------------------------------------

void FlxCard::gbt_topbot_oddeven_set( uint channel, u_int topbot, u_int oddeven )
{
  LOG( "called" );

  // Initially all the registers are 0s (NB: these are *static* variables)
  static u_long top_bot = 0, odd_even = 0;

  if( oddeven == 0 )
    {
      // Set channel ODDEVEN=0
      odd_even &= (~(u_long)(1ULL << channel));
      m_bar2->GBT_ODD_EVEN = odd_even; // CHECK REGISTER
    }
  else
    {
      // Set channel ODDEVEN=1
      odd_even |= (u_long)(1ULL << channel);
      m_bar2->GBT_ODD_EVEN = odd_even;
    }

  if( topbot == 0 )
    {
      // Set channel TOPBOT=0
      top_bot &= (~(u_long)(1ULL << channel));
      m_bar2->GBT_TOPBOT = top_bot;
    }
  else
    {
      // Set channel TOPBOT=1
      top_bot |= (u_long)(1ULL << channel);
      m_bar2->GBT_TOPBOT = top_bot;
    }

  // Alignment status reset
  m_bar2->GBT_GENERAL_CTRL = 1;
  usleep(WAIT_TIME_200);
  m_bar2->GBT_GENERAL_CTRL = 0;
  usleep(WAIT_TIME_600);
}

// ----------------------------------------------------------------------------

bool FlxCard::gbt_topbot_alignment( u_int channel, u_int topbot,
                                    u_long *phase_found, u_int *oddeven )
{
  LOG( "called" );

  // Trying alignment with the given topbot setting
  // and initially with oddeven=0 (Henk: setting here is redundant)
  *oddeven = 0;

  bool found = false;
  int phase_shift_cnt = 10;
  while( phase_shift_cnt > 0 )
    {
      if( *phase_found == 0 )
        {
          if( phase_shift_cnt > 1 )
            {
              --phase_shift_cnt;
              // Switching the phase
              *phase_found = gbt_shift_phase( channel );
            }
          else
            {
              if( *oddeven == 0 )
                {
                  // If no phase and oddeven=0 --> oddeven=1
                  gbt_topbot_oddeven_set( channel, topbot, 1 );
                  phase_shift_cnt = 10;
                  *oddeven = 1;
                  // Switching to first phase & check
                  *phase_found = gbt_shift_phase( channel );
                }
              else
                {
                  // No phase for this topbot
                  phase_shift_cnt = 0; // Exit while-loop
                  // Leave TOP and ODDEVEN set to 0
                  gbt_topbot_oddeven_set( channel, 0, 0 );
                }
            }
        }
      else
        {
          // Good phase found
          phase_shift_cnt = 0; // Exit while-loop
          found = true;
        }
    }
  return found;
}

// ----------------------------------------------------------------------------

u_long FlxCard::gbt_shift_phase( u_int channel )
{
  LOG( "called" );

  // Switching the phase
  m_bar2->GBT_RXSLIDE_MANUAL = (u_long) (1ULL << channel);
  usleep(WAIT_TIME_200);
  m_bar2->GBT_RXSLIDE_MANUAL = 0;
  usleep(WAIT_TIME_200);
  m_bar2->GBT_RXSLIDE_MANUAL = (u_long) (1ULL << channel);
  usleep(WAIT_TIME_200);
  m_bar2->GBT_RXSLIDE_MANUAL = 0;
  usleep(WAIT_TIME_200);

  // Alignment Status reset
  m_bar2->GBT_GENERAL_CTRL = 1;
  usleep(WAIT_TIME_200);
  m_bar2->GBT_GENERAL_CTRL = 0;
  usleep(WAIT_TIME_600);

  // Check
  u_long phase_found = 0;
  phase_found = m_bar2->GBT_ALIGNMENT_DONE;
  phase_found = phase_found & (u_long)(1ULL << channel);

  return phase_found;
}

// ----------------------------------------------------------------------------

int FlxCard::check_digic_value2( const char *str, u_long *version, u_long *delay )
{
  char *pend   = NULL;
  char *pcolon = strchr(const_cast<char*>(str), ':');

  LOG( "called" );

  if( pcolon == NULL )
    return -1;

  *pcolon = '\0';

  *version = strtoul(str, &pend, 0);
  if( *pend != '\0' )
    return -2;

  *delay = strtoul(pcolon + 1, &pend, 0);
  if( *delay == 0 )
    return -3;

  return 0;
}

// ----------------------------------------------------------------------------

static const std::string LOCKBIT2STR[] =
  { "DMA0", "DMA1", "I2C", "FLASH", "ELINK", "---",  "---",  "---",
    "---",  "---",  "---",  "---",  "---",   "---",  "---",  "---",
    "DMA0", "DMA1", "DMA2", "DMA3", "DMA4",  "DMA5", "DMA6", "DMA7",
    "---",  "---",  "---",  "---",  "---",   "---",  "---",  "---"
  };

std::string FlxCard::lockbits2string( u_int locks )
{
  std::ostringstream oss;
  if( locks == 0 )
    {
      oss << "<None>";
    }
  else if( locks == LOCK_ALL )
    {
      oss << "<All>";
    }
  else
    {
      // Deal with legacy lock bits: show only once
      if( (locks & LOCK_DMA0) && (locks & LOCK_DMA(0)) )
        locks &= ~LOCK_DMA0;
      if( (locks & LOCK_DMA1) && (locks & LOCK_DMA(1)) )
        locks &= ~LOCK_DMA1;

      for( int i=0; i<32; ++i )
        if( locks & (1<<i) )
          {
            oss << LOCKBIT2STR[i];
            // Any subsequent bits set? (then add a space)
            if( i < 31 && (locks & ~((1<<(i+1)) - 1)) != 0 )
              oss << " ";
          }
    }
  return oss.str();
}

// ----------------------------------------------------------------------------

void FlxCard::configureLinks( flxcard::LinkConfig *link_config,
                              int *dma_index_invalid_count )
{
#if REGMAP_VERSION >= 0x500
  if( m_fd < 0 ) return;

  // Disable any active emulators
  m_bar2->FE_EMU_ENA.EMU_TOHOST = 0;
  m_bar2->FE_EMU_ENA.EMU_TOFRONTEND = 0;

  int chans = m_bar2->NUM_OF_CHANNELS;
  if( chans > flxcard::FLX_LINKS )
    chans = flxcard::FLX_LINKS;

  // Obtain EC/IC e-link indices (offset in FLX e-link number
  // with respect to link number) as used by the firmware
  std::pair<int,int> p;
  p = this->ec_elink_indices();
  int ecToHostIndex   = p.first;
  //int ecFromHostIndex = p.second;
  p = this->ic_elink_indices();
  int icToHostIndex   = p.first;
  //int icFromHostIndex = p.second;

  // Write configuration into the selected FLX-device
  flxcard::LinkConfig *cfg;
  uint32_t linkmode;
  uint32_t enables;
  uint64_t modes;
  uint32_t width;
  uint32_t ttc_opt;
  uint32_t truncation_word;
  int      dma_invalid_cnt = 0;

  // To Host:
  // ========
  int lnk, max_grp = flxcard::FLX_TOHOST_GROUPS-1;
  for( lnk=0; lnk<chans; ++lnk )
    {
      cfg = &link_config[lnk];
      linkmode = cfg->linkMode();
      if( linkmode == flxcard::LINKMODE_FULL )
        {
          if( lnk >= flxcard::FLX_LINKS/2 )
            return;

          // FULL mode: the FULL mode enable bit is 'hidden' in group 0
          int grp = 0;
          if( (cfg->enablesToHost( grp ) & flxcard::FULL_ENABLED) != 0 )
            m_bar2->DECODING_EGROUP_CTRL_GEN[lnk].DECODING_EGROUP[grp].EGROUP.EPATH_ENA = 1;
          else
            m_bar2->DECODING_EGROUP_CTRL_GEN[lnk].DECODING_EGROUP[grp].EGROUP.EPATH_ENA = 0;
        }
      else
        {
          // The E-groups
          if( lnk < flxcard::FLX_LINKS/2 )
          for( int grp=0; grp<max_grp; ++grp )
            {
              enables         = cfg->enablesToHost( grp );
              modes           = cfg->modesToHost( grp );
              truncation_word = cfg->maxChunkWord();
              width           = cfg->widthToHost( grp );

              volatile flxcard_decoding_egroup_ctrl_t *ctrl =
                &m_bar2->DECODING_EGROUP_CTRL_GEN[lnk].DECODING_EGROUP[grp].EGROUP;
              ctrl->EPATH_ENA     = enables;
              ctrl->EPATH_WIDTH   = width;
              ctrl->PATH_ENCODING = modes;
              // For now: set HDLC truncation globally
              if( truncation_word > 0 )
                ctrl->ENABLE_TRUNCATION = 1;
              else
                ctrl->ENABLE_TRUNCATION = 0;
            }

          // and the EC+IC+AUX+TTCtoHost group
          enables = cfg->enablesToHost( flxcard::TOHOST_MINI_GROUP );
          modes   = cfg->modesToHost( flxcard::TOHOST_MINI_GROUP );

          volatile flxcard_mini_egroup_tohost_t *mini_egroup_tohost =
            &m_bar2->MINI_EGROUP_TOHOST_GEN[lnk].MINI_EGROUP_TOHOST;

          // To-host EC enabled/disabled
          if( enables & flxcard::EC_ENABLED )
            mini_egroup_tohost->EC_ENABLE = 1;
          else
            mini_egroup_tohost->EC_ENABLE = 0;

          // To-host EC mode
          mini_egroup_tohost->EC_ENCODING =
            (modes >> (flxcard::EC_ENABLED_BIT*4)) & 0xF;

          // To-host IC enabled/disabled
          if( enables & flxcard::IC_ENABLED )
            mini_egroup_tohost->IC_ENABLE = 1;
          else
            mini_egroup_tohost->IC_ENABLE = 0;

          // To-host SCA-AUX enabled/disabled
          if( enables & flxcard::AUX_ENABLED && m_firmwareType == FIRMW_LTDB )
            mini_egroup_tohost->AUX_ENABLE = 1;
          else
            mini_egroup_tohost->AUX_ENABLE = 0;

          // For now: set HDLC truncation globally
          if( truncation_word > 0 )
            {
              // Don't truncate IC
              mini_egroup_tohost->ENABLE_EC_TRUNCATION = 1;
              mini_egroup_tohost->ENABLE_IC_TRUNCATION = 0;
              mini_egroup_tohost->ENABLE_AUX_TRUNCATION = 1;
            }
          else
            {
              mini_egroup_tohost->ENABLE_EC_TRUNCATION = 0;
              mini_egroup_tohost->ENABLE_IC_TRUNCATION = 0;
              mini_egroup_tohost->ENABLE_AUX_TRUNCATION = 0;
            }
        }

      // Some global settings (done when configuring link 0)
      if( lnk == 0 )
        {
          // To-host TTC enabled/disabled
          // (stored as part of link #0 configuration)
          enables = cfg->enablesToHost( flxcard::TOHOST_MINI_GROUP );
          if( enables & flxcard::TTC2H_ENABLED )
            m_bar2->TTC_TOHOST_ENABLE = 1;
          else
            m_bar2->TTC_TOHOST_ENABLE = 0;

          // Select TTC Clock ? (stored as part of link #0 configuration)
          if( cfg->ttcClock() )
            m_bar2->MMCM_MAIN.LCLK_SEL = 0;

          // lpGBT link mode flavour setting: 10.24Gbs or 5.12Gbs
          if( linkmode == flxcard::LINKMODE_LPGBT10_F5 ||
              linkmode == flxcard::LINKMODE_LPGBT10_F12 )
            m_bar2->LPGBT_DATARATE = 0x000000000000;
          else if( linkmode == flxcard::LINKMODE_LPGBT5_F5 ||
                   linkmode == flxcard::LINKMODE_LPGBT5_F12 )
            m_bar2->LPGBT_DATARATE = 0xFFFFFFFFFFFF;

          // lpGBT link mode flavour setting: FEC5 or FEC12
          if( linkmode == flxcard::LINKMODE_LPGBT10_F5 ||
              linkmode == flxcard::LINKMODE_LPGBT5_F5 )
            m_bar2->LPGBT_FEC = 0x000000000000;
          else if( linkmode == flxcard::LINKMODE_LPGBT5_F12 ||
                   linkmode == flxcard::LINKMODE_LPGBT10_F12 )
            m_bar2->LPGBT_FEC = 0xFFFFFFFFFFFF;
        }

      // E-link DMA controller indices
      int elinknr, dma_index;
      int max_pathnr;
      if( linkmode == flxcard::LINKMODE_LPGBT10_F5 ||
          linkmode == flxcard::LINKMODE_LPGBT5_F5 )
        max_pathnr = 4;
      else
        max_pathnr = 8;

      // DMA count includes the single FromHost DMA, hence the '-1'
      // (NB: this might change in the future)
      int dma_count = m_bar2->GENERIC_CONSTANTS.DESCRIPTORS - 1;
      if( lnk < flxcard::FLX_LINKS/2 )
      for( int group=0; group<max_grp; ++group )
        {
          uint32_t dma_indices = cfg->dmaIndices( group );
          for( int path=0; path<max_pathnr; ++path )
            {
              if( m_lpgbtType )
                elinknr = ((lnk << LINK_SHIFT) |
                           (group << EGROUP_SHIFT_LPGBT) | path);
              else
                elinknr = ((lnk << LINK_SHIFT) |
                           (group << EGROUP_SHIFT) | path);

              // Set 'address' (and read current DMA index setting)
              m_bar2->CRTOHOST_DMA_DESCRIPTOR_2.AXIS_ID = elinknr;
              // Write DMA index setting, but check for a valid number
              dma_index = (dma_indices >> (path*4)) & 0xF;
              if( dma_index > dma_count-1 )
                {
                  dma_index = 0; // Use DMA index 0 instead
                  ++dma_invalid_cnt;
                }
              m_bar2->CRTOHOST_DMA_DESCRIPTOR_1.DESCR = dma_index;
            }
        }

      // EC E-link DMA index
      uint32_t dma_indices = cfg->dmaIndices( flxcard::TOHOST_MINI_GROUP );
      elinknr = (lnk << LINK_SHIFT) | ecToHostIndex;
      m_bar2->CRTOHOST_DMA_DESCRIPTOR_2.AXIS_ID = elinknr;
      // DMA setting stored in Egroup 7, path 7
      dma_index = (dma_indices >> (7*4)) & 0xF;
      if( dma_index > dma_count-1 )
        {
          dma_index = 0; // Use DMA index 0 instead
          ++dma_invalid_cnt;
        }
      m_bar2->CRTOHOST_DMA_DESCRIPTOR_1.DESCR = dma_index;

      // IC E-link DMA index
      elinknr = (lnk << LINK_SHIFT) | icToHostIndex;
      m_bar2->CRTOHOST_DMA_DESCRIPTOR_2.AXIS_ID = elinknr;
      // DMA setting stored in Egroup 7, path 6
      dma_index = (dma_indices >> (6*4)) & 0xF;
      if( dma_index > dma_count-1 )
        {
          dma_index = 0; // Use DMA index 0 instead
          ++dma_invalid_cnt;
        }
      m_bar2->CRTOHOST_DMA_DESCRIPTOR_1.DESCR = dma_index;

      // AUX E-link DMA index (use: IC plus 1)
      elinknr = (lnk << LINK_SHIFT) | (icToHostIndex + 1);
      m_bar2->CRTOHOST_DMA_DESCRIPTOR_2.AXIS_ID = elinknr;
      // DMA setting stored in Egroup 7, path 5
      dma_index = (dma_indices >> (5*4)) & 0xF;
      if( dma_index > dma_count-1 )
        {
          dma_index = 0; // Use DMA index 0 instead
          ++dma_invalid_cnt;
        }
      m_bar2->CRTOHOST_DMA_DESCRIPTOR_1.DESCR = dma_index;

      // TTCtoHost DMA index (stored as part of link #0 configuration)
      if( lnk == 0 )
        {
          elinknr = (24 << LINK_SHIFT); // E-link 0x600
          m_bar2->CRTOHOST_DMA_DESCRIPTOR_2.AXIS_ID = elinknr;
          // DMA setting stored in Egroup 7, path 0
          dma_index = dma_indices & 0xF;
          if( dma_index > dma_count-1 )
            {
              dma_index = 0; // Use DMA index 0 instead
              ++dma_invalid_cnt;
            }
          m_bar2->CRTOHOST_DMA_DESCRIPTOR_1.DESCR = dma_index;
        }

      // Stream ID bits
      uint64_t *p = (uint64_t *) &m_bar2->PATH_HAS_STREAM_ID[lnk];
      uint64_t val = *p;
      for( int grp=0; grp<max_grp; ++grp )
        {
          // Clear and set Egroup's streamid bits
          val &= ~(0xFFULL << (grp*8));
          val |= (((uint64_t) cfg->streamIdBits( grp ) & 0xFFULL) << (grp*8));
        }
      *p = val;
    }

  // From Host:
  // ==========
  max_grp = flxcard::FLX_FROMHOST_GROUPS-1;
  for( lnk=0; lnk<chans; ++lnk )
    {
      cfg = &link_config[lnk];
      // The E-groups
      if( lnk < flxcard::FLX_LINKS/2 )
      for( int grp=0; grp<max_grp; ++grp )
        {
          enables = cfg->enablesFromHost( grp );
          modes   = cfg->modesFromHost( grp );
          width   = cfg->widthFromHost( grp );
          ttc_opt = cfg->ttcOptionFromHost( grp );

          volatile flxcard_encoding_egroup_ctrl_t *ctrl =
            &m_bar2->ENCODING_EGROUP_CTRL_GEN[lnk].ENCODING_EGROUP[grp].ENCODING_EGROUP_CTRL;
          ctrl->EPATH_ENA     = enables;
          ctrl->EPATH_WIDTH   = width;
          ctrl->PATH_ENCODING = modes;
          ctrl->TTC_OPTION    = ttc_opt;
        }

      // and the EC+IC+AUX group
      enables = cfg->enablesFromHost( flxcard::FROMHOST_MINI_GROUP );
      modes   = cfg->modesFromHost( flxcard::FROMHOST_MINI_GROUP );

      volatile flxcard_mini_egroup_fromhost_t *mini_egroup_fromhost =
        &m_bar2->MINI_EGROUP_FROMHOST_GEN[lnk].MINI_EGROUP_FROMHOST;

      // From-host EC enabled/disabled
      if( enables & flxcard::EC_ENABLED )
        mini_egroup_fromhost->EC_ENABLE = 1;
      else
        mini_egroup_fromhost->EC_ENABLE = 0;

      // From-host EC mode
      mini_egroup_fromhost->EC_ENCODING = (modes >> (flxcard::EC_ENABLED_BIT*4)) & 0xF;

      // From-host IC enabled/disabled
      if( enables & flxcard::IC_ENABLED )
        mini_egroup_fromhost->IC_ENABLE = 1;
      else
        mini_egroup_fromhost->IC_ENABLE = 0;

      // From-host SCA-AUX enabled/disabled
      if( enables & flxcard::AUX_ENABLED && m_firmwareType == FIRMW_LTDB )
        mini_egroup_fromhost->AUX_ENABLE = 1;
      else
        mini_egroup_fromhost->AUX_ENABLE = 0;

      if( linkmode == flxcard::LINKMODE_FULL )
        {
          // FromHost GBT or LTI
          uint64_t lti = m_bar2->LINK_FULLMODE_LTI;
          // SPECIAL: since the LINK_FULLMODE_LTI register is in Endpoint-0 only,
          // configure Endpoint-0/-1 links identically
          // (NB: of course only has effect when applied to FLX device #0)
          if( cfg->ltiTtc() )
            lti |= ((1 << lnk) | (1 << (12+lnk)));
          else
            lti &= ~((1 << lnk) | (1 << (12+lnk)));
          m_bar2->LINK_FULLMODE_LTI = lti;
        }
    }

  this->soft_reset();

  *dma_index_invalid_count = dma_invalid_cnt;

#else
  THROW_FLX_EXCEPTION( CONFIG, "FlxCard::configureLinks() not implemented for RM4 firmware" );
#endif // REGMAP_VERSION
}

// ----------------------------------------------------------------------------
