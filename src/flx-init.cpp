/*******************************************************************/
/*                                                                 */
/* This is the C++ source code of the flx-init application         */
/*                                                                 */
/* Author: Markus Joos, CERN                                       */
/*                                                                 */
/* From Nov 2020: maintenance by H.Boterenbrood, Nikhef            */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <strings.h>
#include <string.h>
#include <sys/file.h>
#include <stdarg.h>
#include <time.h>
#include <iostream>
#include <fstream>

#include "flxcard/FlxCard.h"
#include "flxcard/FlxException.h"
#include "felixtag.h"

typedef struct
{
  unsigned int  address; /* 16-bit register address */
  unsigned char value;   /*  8-bit register data    */
} clockchip_reg_t;

// Si5345 settings for lpGBT-type firmware
#include "clock-config/Si5345-RevD-SI5345D40_079to320_632-Registers.h"

// Si5324 settings for lpGBT-type firmware
// File .h below created from .txt by:
// awk -v ARRAYNAME=Si5324Regs_320 -f si5324-regs.awk Si5324_160_316to320_632_1clkin2clksout.txt
#include "clock-config/Si5324_160_316to320_632_1clkin2clksout.h"

// Si5345 settings for FLX-MROD firmware
#include "clock-config/Si5345-RevB-711_4008_BNL711_8x200-Registers.h"

// For other firmware:
// using the (slightly) modified original file
// (NB: Si5345 files have the same #ifdef include guard, register array name,
//      #define for the number of registers and the 'typedef struct'
//      so their names have been changed in this include file)
#include "clock-config/Si5345-RevD-711_4008_BNL711_8x240-Registers.h"

// Si5345 settings for the FLX-182 hardware (see JIRA ticket FLX-2100)
// (NB: some changes made to file contents to prevent identical names):
// => for GBT, LTDB, FULL and FELIG
#include "clock-config/Si5345-RevD-SI5345D-FLX182_SI5345A_240_474_Project-Registers.h"
// => for lpGBT, STRIP, PIXEL, FELIG_LPGBT, FELIG_PIXEL
#include "clock-config/Si5345-RevD-SI5345D-FLX182_SI5345A_320_632_Project-Registers.h"
// => for INTERLAKEN SI5345A use the 'SI5345A_156_25' file
// => for INTERLAKEN SI5345B use the 'SI5345A_240_474' file
#include "clock-config/Si5345-RevD-SI5345D-FLX182_SI5345A_156.25_Project-Registers.h"

clockchip_reg_t Si5324Regs_240[] = {
  {0x00, 0x14}, {0x01, 0xE4}, {0x02, 0xA2}, {0x03, 0x15}, {0x04, 0x12}, {0x05, 0xED}, {0x06, 0x3F}, {0x07, 0x29}, {0x08, 0x00}, {0x09, 0xC0},
  {0x0A, 0x08}, {0x0B, 0x42}, {0x13, 0x09}, {0x14, 0x3E}, {0x15, 0xFF}, {0x16, 0xDF}, {0x17, 0x1F}, {0x18, 0x3F}, {0x19, 0xE0}, {0x1F, 0x00},
  {0x20, 0x00}, {0x21, 0x01}, {0x22, 0x00}, {0x23, 0x00}, {0x24, 0x01}, {0x28, 0xE0}, {0x29, 0x00}, {0x2A, 0xF5}, {0x2B, 0x00}, {0x2C, 0x00},
  {0x2D, 0x51}, {0x2E, 0x00}, {0x2F, 0x00}, {0x30, 0x51}, {0x37, 0x1B}, {0x83, 0x1F}, {0x84, 0x02}, {0x89, 0x01}, {0x8A, 0x0F}, {0x8B, 0xFF},
  {0x8E, 0x00}, {0x8F, 0x00}, {0x88, 0x40},
};

clockchip_reg_t Si5328Regs_240[] = {
  {0x00, 0x14}, {0x01, 0xE4}, {0x02, 0xA2}, {0x03, 0x15}, {0x04, 0x92}, {0x05, 0xED}, {0x06, 0x2F}, {0x07, 0x2A}, {0x08, 0x00}, {0x09, 0xC0},
  {0x0A, 0x00}, {0x0B, 0x40}, {0x13, 0x29}, {0x14, 0x3E}, {0x15, 0xFE}, {0x16, 0xDF}, {0x17, 0x1F}, {0x18, 0x3F}, {0x19, 0xE0}, {0x1F, 0x00},
  {0x20, 0x00}, {0x21, 0x01}, {0x22, 0x00}, {0x23, 0x00}, {0x24, 0x01}, {0x28, 0x60}, {0x29, 0x01}, {0x2A, 0x8B}, {0x2B, 0x00}, {0x2C, 0x00},
  {0x2D, 0x14}, {0x2E, 0x00}, {0x2F, 0x00}, {0x30, 0x53}, {0x37, 0x00}, {0x83, 0x1F}, {0x84, 0x02}, {0x89, 0x01}, {0x8A, 0x0F}, {0x8B, 0xFF},
  {0x8E, 0x00}, {0x8F, 0x00}, {0x88, 0x40},
};

clockchip_reg_t Ics8nRegs[] = {
  {0x12, 0xB8},{0x00, 0x2A},{0x01, 0x2A},{0x02, 0x25},{0x03, 0x25},{0x04, 0x15},{0x05, 0x00},{0x06, 0xDF},{0x07, 0xCC},{0x08, 0x4D},
  {0x09, 0x10},{0x0A, 0xF8},{0x0B, 0xDC},{0x0C, 0x0A},{0x0D, 0x8A},{0x0E, 0x92},{0x0F, 0x12},{0x10, 0x00},{0x11, 0x00},{0x13, 0x00},
  {0x14, 0x1F},{0x15, 0x1F},{0x16, 0x1F},{0x17, 0x1F},{0x12, 0xA0}
};

// ----------------------------------------------------------------------------

// Function prototypes
void display_help        ( );
void gbt_soft_reset      ( );
void si5324_configure    ( );
void si5328_configure    ( );
void si5345_configure    ( int input_sel );
void si5345_configure_182( );
bool si5345_detected     ( const char *name );
int  si5345_write_regs   ( const char *name, u_int n_registers, clockchip_reg_t *regs );
void si5345_output_enable( const char *name );
void si53xx_read_regs    ( );
void si570_set_freq      ( const char *name, uint64_t freq );
bool si570_calc_divs     ( uint64_t frequency, double fxtal,
                           uint64_t *new_rfreq,
                           uint32_t *new_n1, uint32_t *new_hsdiv );
void idt240_init         ( );
void minipods_reset      ( );
void firefly_reset       ( );
void ina226_configure    ( );
void ics8n_configure     ( );
void lti_init            ( );
void sfp_los_check       ( );
void str_upper           ( char *str );

// Printf function with a timestamp preceding the output
int tprintf( const char *format, ... );
std::string timestamp( );

// Globals
FlxCard flxCard;
bool    verbose = false;
u_int   card_type = 0;
u_int   firmw_type = 0;
bool    lpgbt_firmw_type = false;

// ----------------------------------------------------------------------------

int main(int argc, char **argv)
{
  int  opt;
  bool locks_override    = false;
  int  device_number     = 0;
  int  card_number       = 0;
  int  input_sel         = 0;
  int  alignment_mode    = FLX_GBT_ALIGNMENT_ONE;
  int  transmission_mode = FLX_GBT_TMODE_FEC;
  int  try_open_max      = 5;
  bool read_clock_regs   = false;
  bool run_los_check     = false;
  bool clock_local       = false;
  bool clock_ttc         = false;
  std::vector<std::string> filenames;

  while((opt = getopt(argc, argv, "hvVc:ELTGI:a:t:s:")) != -1)
    {
      switch (opt)
        {
        case 'h':
          display_help();
          return 0;

        case 'V':
          tprintf( "Version %s\n", FELIX_TAG );
          return 0;

        case 'v':
          verbose = true;
          break;

        case 'c':
          card_number = atoi( optarg );
          try {
            // Map card number to the corresponding device number
            device_number = FlxCard::card_to_device_number( card_number );
          }
          catch( FlxException &ex ) {
            std::cout << timestamp() << " ### ERROR: " << ex.what() << std:: endl;
            return 1;
          }
          if( device_number < 0 )
            {
              tprintf( "### -c: Card number %d not in range [0,%u]\n",
                       card_number, FlxCard::number_of_cards()-1 );
              return 1;
            }
          break;

        case 'E':
          locks_override = true;
          break;

        case 'L':
          clock_local = true;
          break;

        case 'T':
          clock_ttc = true;
          break;

        case 'G':
          read_clock_regs = true;
          break;

        case 'I':
          input_sel = atoi( optarg );
          break;

        case 'a':
          run_los_check = true;
          if( strcasecmp( optarg, "ONE" ) == 0 )
            {
              alignment_mode = FLX_GBT_ALIGNMENT_ONE;
            }
          else
            {
              if( strcasecmp( optarg, "CONTINUOUS" ) == 0 )
                alignment_mode = FLX_GBT_ALIGNMENT_CONTINUOUS;
              else
                {
                  tprintf( "### -a: Invalid alignment type\n" );
                  return 1;
                }
            }
          break;

        case 't':
          run_los_check = true;
          if( strcasecmp( optarg, "FEC" ) == 0)
            {
              transmission_mode = FLX_GBT_TMODE_FEC;
            }
          else
            {
              if( strcasecmp( optarg, "WideBus" ) == 0 )
                {
                  transmission_mode = FLX_GBT_TMODE_WideBus;
                }
              else
                {
                  tprintf( "### -t: Invalid transmission mode\n" );
                  return 1;
                }
            }
          break;

        case 's':
          // Number of seconds to try to open the card (i.e. once per second)
          try_open_max = atoi(optarg);
          break;

        default:
          printf( "Usage: flx-init COMMAND [OPTIONS]\n"
                  "Try flx-init -h for more information.\n" );
          return 1;
        }
    }

  // Get .yelc configuration file names, if any given
  while( optind < argc )
    {
      // Check if file exists and can be opened
      std::ifstream file( argv[optind] );
      if( !file.is_open() )
        {
          tprintf( "### Config file, could not open: %s\n", argv[optind] );
          return 1;
        }
      file.close();
      filenames.push_back( std::string(argv[optind]) );
      ++optind;
    }

  if( clock_local && clock_ttc )
    {
      tprintf( "WARNING: select one of -L/-T options\n" );
      clock_local = false;
      clock_ttc = false;
    }

  try {
    tprintf( "Opening card %d (device %d)", card_number, device_number );
    if( locks_override )
      printf( " (Locks override)" );
    printf( "...\n" );

    uint32_t locks = (locks_override ? LOCK_NONE : LOCK_ALL);

    // In case of lock issues, retry opening the device every second
    // until successful, for a configurable number of times
    int try_open_count = 0;
    while( true )
      {
        try {
          flxCard.card_open( device_number, locks );
        }
        catch( FlxException &ex ) {
          if( ex.errorId() != FlxException::LOCK_VIOLATION ||
              try_open_count >= try_open_max )
            // Not a lock issue or persistent lock issue: rethrow exception
            throw ex;
          ++try_open_count;
          tprintf( "Waiting for locks, retrying... (%d)\n", try_open_count );
          sleep( 1 );
          continue; // Retry card_open()
        }
        break; // Card succesfully opened
      }

    card_type = flxCard.card_type();
    tprintf( "Card type: FLX-%ld\n", card_type );

    firmw_type = flxCard.firmware_type();
    tprintf( "Firmware : %s\n", flxCard.firmware_type_string().c_str() );
    lpgbt_firmw_type = flxCard.lpgbt_type();

    if( read_clock_regs )
      {
        // Read and display clock chip registers, then exit
        si53xx_read_regs();
        flxCard.card_close();
        return 0;
      }

    // Actively select Local or TTC clock ?
    if( clock_local )
      flxCard.cfg_set_option( BF_MMCM_MAIN_LCLK_SEL, 1 );
    else if( clock_ttc )
      flxCard.cfg_set_option( BF_MMCM_MAIN_LCLK_SEL, 0 );

    tprintf( "Clock    : " );
    if( clock_local || clock_ttc )
      printf( "set to " );
    if( flxCard.cfg_get_option( BF_MMCM_MAIN_LCLK_SEL ) )
      printf( "Local\n" );
    else
      printf( "TTC\n" );

    tprintf( "FLX soft reset\n" );
    flxCard.soft_reset();

    if( card_type == 709 )
      {
        if( run_los_check )
          sfp_los_check();

        si5324_configure();
        gbt_soft_reset();
      }
    else if( card_type == 710 )
      {
        ics8n_configure();
      }
    else if( card_type == 128 )
      {
        si5328_configure();
        gbt_soft_reset();
      }

    if( card_type == 182 || card_type == 155 )
      {
        si5345_configure_182();
      }
    else
      {
        si5345_configure( input_sel );
      }

    gbt_soft_reset();

    if( card_type == 710 )
      {
        idt240_init();
      }

    tprintf( "Setting up links...\n" );
    int bad_channels = flxCard.gbt_setup( alignment_mode, transmission_mode );
    tprintf( "Links set up\n" );

    if( bad_channels )
      tprintf( "WARNING: %d channels not aligned\n", bad_channels );
    else if( verbose )
      tprintf( "All channels align\n" );

    if( verbose )
      tprintf( "Disabling interrupts\n" );
    flxCard.irq_disable( ALL_IRQS );

    if( !(card_type == 709 || card_type == 182 || card_type == 155) )
      {
        minipods_reset();
      }

    if( card_type == 182 || card_type == 155 )
      {
        firefly_reset();
        ina226_configure();
      }

    lti_init(); // Added 9 Dec 2024 (see FLX-2134)

    // Apply configuration files, if any were given
    if( filenames.size() > 0 )
      {
        uint32_t dev_index = 0;
        try {
          uint32_t ndevices = flxCard.cfg_get_reg( REG_NUMBER_OF_PCIE_ENDPOINTS );
          while( dev_index < filenames.size() && dev_index < ndevices )
            {
              flxCard.card_close();
              flxCard.card_open( device_number + dev_index, locks );
              flxCard.configure( filenames[dev_index] );
              std::cout << timestamp() << " FLX device " << device_number+dev_index
                        << " configured: " << filenames[dev_index] << std::endl;
              ++dev_index;
            }
        }
        catch( FlxException &ex ) {
          std::cout << timestamp() << " ### FLX device " << device_number+dev_index
                    << " config: " << ex.what() << std::endl;
        }
      }

    if( verbose )
      tprintf( "Closing card..." );
    flxCard.card_close();
  }
  catch( FlxException &ex ) {
    std::cout << timestamp() << " ### ERROR: " << ex.what() << std:: endl;
    return 1;
  }

  return 0;
}

// ----------------------------------------------------------------------------

void display_help()
{
  printf("Usage: flx-init [OPTIONS] [<yelc-file1>] [<yelc-file2>]\n");
  printf("Initializes an FLX card.\n");
  printf("Options:\n");
  printf("  -c <number>         Use card indicated by <number> (default: 0).\n");
  printf("  -h                  Display help.\n");
  printf("  -V                  Display the version number.\n");
  printf("  -v                  Display verbose output.\n");
  printf("  -E                  Execute the command even if resources are locked.\n");
  printf("  -L                  Set clock to 'Local' (default: leave-as-is).\n");
  printf("  -T                  Set clock to 'TTC' (default: leave-as-is).\n");
  printf("  -s <secs>           If necessary wait up to <secs> seconds for locks to be freed\n");
  printf("                      when attempting to open the card (default: 5 secs).\n");
  printf("GBT calibration options:\n");
  printf("  -a ONE|CONTINUOUS   Select alignment type (default: ONE).\n");
  printf("  -t FEC|WideBus      Select transmission mode (default: FEC).\n\n");
  printf("Clock configuration options:\n");
  printf("  -I <input_sel>      The value given is written to register HK_CTRL_FMC_SI5345_INSEL\n");
  printf("                      before Si5345 initialisation;\n");
  printf("                      valid <input_sel> values are (default: 0):\n");
  printf("                        0: FPGA (LA01), 1: FMC OSC, 2: FPGA (LA18)\n");
  printf("  -G                  Read and display Si53xx chip registers (does not init anything),\n");
  printf("                      i.e. Si5324 (for a 709) and/or Si5345 (2x for a 182 or 155)\n\n");
  printf("E-link configuration with .yelc files <yelc-fileX>:\n");
  printf("  Provide a filename per FLX device to configure.\n");
  printf("  Example: to initialize and then configure both devices of the *second* FLX-712 card\n");
  printf("  in a system (first file for 1st device of the FLX-712, second file for the 2nd device):\n");
  printf("       flx-init -c1 lpGBT-8bit.yelc lpGBT-16bit.yelc\n");
  printf("  To configure only the first device, provide a single file name.\n");
}

// ----------------------------------------------------------------------------

void gbt_soft_reset()
{
  tprintf( "Links soft reset\n" );
  flxCard.cfg_set_reg( REG_GBT_SOFT_RESET, 0xFFFFFFFFFFFF );
  flxCard.cfg_set_reg( REG_GBT_SOFT_RESET, 0 );
}

// ----------------------------------------------------------------------------

void si5324_configure()
{
  const char *name = "SI5324";

  tprintf( "Configuring %s...\n", name );

  clockchip_reg_t *regs;
  u_int n_registers;
  if( lpgbt_firmw_type )
    {
      regs = Si5324Regs_320; // See #include file
      n_registers = sizeof( Si5324Regs_320 )/sizeof( clockchip_reg_t );
    }
  else
    {
      regs = Si5324Regs_240;
      n_registers = sizeof( Si5324Regs_240 )/sizeof( clockchip_reg_t );
    }

  u_char reg_addr, reg_val;
  for( u_int i=0; i<n_registers; ++i )
    {
      reg_addr = (u_char) (regs[i].address & 0xFF);
      reg_val  = regs[i].value;
      if( verbose )
        printf( "Write 0x%X to register 0x%02X\n", reg_val, reg_addr );
      flxCard.i2c_write( name, reg_addr, reg_val );

      // Check (except register 0x88)
      flxCard.i2c_read( name, reg_addr, &reg_val );
      if( reg_val != regs[i].value && reg_addr != 0x88 )
        tprintf( "### %s register 0x%02X: wrote 0x%02X, read 0x%02X\n",
                 name, reg_addr, regs[i].value, reg_val );
    }

  tprintf( "%s configured (%d registers)\n", name, n_registers );

  reg_addr = 0x81;
  flxCard.i2c_read( name, reg_addr, &reg_val );
  tprintf( "%s: Register 129 = 0x%02X\n", name, reg_val );
  tprintf( "        LOS2_INT: %s\n", (reg_val & 0x4) ? "ERROR: CLK2 LOS" : "CLK2 OK" );

  reg_addr = 0x82;
  flxCard.i2c_read( name, reg_addr, &reg_val );
  tprintf( "%s: Register 130 = 0x%02X\n", name, reg_val );
  tprintf( "        LOL_INT:  %s\n", (reg_val & 0x1) ? "PLL unlocked" : "PLL locked" );
}

// ----------------------------------------------------------------------------

void si5328_configure()
{
  const char *name = "SI5328";

  tprintf( "Configuring %s...\n", name );

  clockchip_reg_t *regs = Si5328Regs_240;
  u_int n_registers = sizeof( Si5328Regs_240 )/sizeof( clockchip_reg_t );

  u_char reg_addr, reg_val;
  for( u_int i=0; i<n_registers; ++i )
    {
      reg_addr = (u_char) (regs[i].address & 0xFF);
      reg_val  = regs[i].value;
      if( verbose )
        printf( "Write 0x%02X to register 0x%02X\n", reg_val, reg_addr );
      flxCard.i2c_write( name, reg_addr, reg_val );

      // Check (except register 0x88)
      flxCard.i2c_read( name, reg_addr, &reg_val );
      if( reg_val != regs[i].value && reg_addr != 0x88 )
        tprintf( "### %s register 0x%02X: wrote 0x%02X, read 0x%02X\n",
                 name, reg_addr, regs[i].value, reg_val );
    }

  tprintf( "%s configured (%d registers)\n", name, n_registers );

  reg_addr = 0x81;
  flxCard.i2c_read( name, reg_addr, &reg_val );
  tprintf( "%s: Register 129 = 0x%02X\n", name, reg_val );
  tprintf( "        LOS2_INT: %s\n", (reg_val & 0x4) ? "ERROR: CLK2 LOS" : "CLK2 OK" );

  reg_addr = 0x82;
  flxCard.i2c_read( name, reg_addr, &reg_val );
  tprintf( "%s: Register 130 = 0x%02X\n", name, reg_val );
  tprintf( "        LOL_INT:  %s\n", (reg_val & 0x1) ? "PLL unlocked" : "PLL locked" );
}

// ----------------------------------------------------------------------------

void si5345_configure( int input_sel )
{
  if( card_type != 709 &&
      card_type != 710 &&
      card_type != 711 &&
      card_type != 712 &&
      card_type != 181 )
    {
      tprintf( "WARNING: skipping si5345_configure(), only for "
               "FLX-709, FLX-710, FLX-711, FLX-712 or FLX-181\n" );
      return;
    }

  // This is for non-FLX-182/155 cards
  const char *name = "SI5345";

  // Select the configuration to use on the basis of the firmware type
  clockchip_reg_t *regs;
  u_int n_registers;
  std::string cfg_str;
  if( lpgbt_firmw_type )
    {
      regs = (clockchip_reg_t *) si5345_revd_registers_40_079_320_632; // See #include file
      n_registers = sizeof( si5345_revd_registers_40_079_320_632 )/sizeof( clockchip_reg_t );
      cfg_str = "'40.079-320-632'";
    }
  else if( firmw_type == FIRMW_MROD )
    {
      regs = (clockchip_reg_t *) si5345_revb_registers_8x200; // See #include file
      n_registers = sizeof( si5345_revb_registers_8x200 )/sizeof( clockchip_reg_t );
      cfg_str = "'8x200'";
    }
  else
    {
      regs = (clockchip_reg_t *) si5345_revd_registers_8x240; // See #include file
      n_registers = sizeof( si5345_revd_registers_8x240 )/sizeof( clockchip_reg_t );
      cfg_str = "'8x240'";
    }

  tprintf( "Configuring %s: %s...\n", name, cfg_str.c_str() );

  // See https://gitlab.cern.ch/atlas-tdaq-felix/hardware/-/blob/master/TTCfx/tools/Si5345_40.00mhz.sh
  // for the source of inspiration
  // Note: the ".sh" file in turn is derived from a software-generated ".h" file.

  tprintf( "%s hard reset\n", name );
  flxCard.cfg_set_option( "HK_CTRL_FMC_SI5345_SEL", 1 );
  if( verbose )
    printf( "Write %d to register HK_CTRL_FMC_SI5345_INSEL\n", input_sel );
  flxCard.cfg_set_option( "HK_CTRL_FMC_SI5345_INSEL", input_sel );
  flxCard.cfg_set_option( "HK_CTRL_FMC_SI5345_OE", 1 );
  sleep(1);
  flxCard.cfg_set_option( "HK_CTRL_FMC_SI5345_RSTN", 0 );
  sleep(1);
  flxCard.cfg_set_option( "HK_CTRL_FMC_SI5345_RSTN", 1 );

  // Detect
  if( si5345_detected( name ) )
    {
      // Configure
      int reg_cnt = si5345_write_regs( name, n_registers, regs );
      tprintf( "%s configured (%d registers)\n", name, reg_cnt );

      // Enable
      tprintf( "%s enabling output\n", name );
      si5345_output_enable( name );
    }

  // Special for FELIG-type firmware (for the time being)
  if( firmw_type == FIRMW_FELIG_GBT ||
      firmw_type == FIRMW_FELIG_LPGBT ||
      firmw_type == FIRMW_FELIG_PIXEL )
    {
      // Specific for FELIG (described in FLX-1543):
      // apply a reset using this 48-bit register
      flxCard.cfg_set_reg( REG_GBT_GTRX_RESET, 0xFFFFFFFFFFFF );
      flxCard.cfg_set_reg( REG_GBT_GTRX_RESET, 0x000000000000 );
    }
}

// ----------------------------------------------------------------------------

void si5345_configure_182()
{
  if( card_type != 182 )
    {
      tprintf( "WARNING: skipping si5345_configure_182(), for FLX-182 only\n" );
      return;
    }

  // Early prototype FLX-182s have a different I2C address for the SI5345-A device
  const char *NAMES_A[] = { "SI5345A", "SI5345A_BETA" };
  const char *name = NAMES_A[0];

  // Select the configuration to use on the basis of the firmware type
  clockchip_reg_t *regs;
  u_int n_registers;
  std::string cfg_str;
  if( lpgbt_firmw_type )
    {
      regs = (clockchip_reg_t *) si5345_revd_registers_320_632; // See #include file
      n_registers = sizeof( si5345_revd_registers_320_632 )/sizeof( clockchip_reg_t );
      cfg_str = "'320-632'";
    }
  else if( firmw_type == FIRMW_INTERLAKEN )
    {
      regs = (clockchip_reg_t *) si5345_revd_registers_156_25; // See #include file
      n_registers = sizeof( si5345_revd_registers_156_25 )/sizeof( clockchip_reg_t );
      cfg_str = "'156.25'";
    }
  else
    {
      regs = (clockchip_reg_t *) si5345_revd_registers_240_474; // See #include file
      n_registers = sizeof( si5345_revd_registers_240_474 )/sizeof( clockchip_reg_t );
      cfg_str = "'240-474'";
    }

  tprintf( "%s hard reset\n", name );
  flxCard.cfg_set_option( "HK_CTRL_FMC_SI5345_SEL", 1 );
  // ###NB: skip setting 'input_sel' ?
  //if( verbose )
  //  printf( "Write %d to register HK_CTRL_FMC_SI5345_INSEL\n", input_sel );
  //flxCard.cfg_set_option( "HK_CTRL_FMC_SI5345_INSEL", input_sel );
  flxCard.cfg_set_option( "HK_CTRL_FMC_SI5345_OE", 1 );
  sleep(1);
  flxCard.cfg_set_option( "HK_CTRL_FMC_SI5345_RSTN", 0 );
  sleep(1);
  flxCard.cfg_set_option( "HK_CTRL_FMC_SI5345_RSTN", 1 );

  // Detect the SI5345A (2 potential addresses)
  if( !si5345_detected( name ) )
    {
      name = NAMES_A[1];
      if( si5345_detected( name ) )
        tprintf( "Found %s\n", name );
    }

  tprintf( "Configuring %s: %s...\n", name, cfg_str.c_str() );

  // Configure
  int reg_cnt = si5345_write_regs( name, n_registers, regs );
  tprintf( "%s configured (%d registers)\n", name, reg_cnt );

  // Enable
  tprintf( "%s enabling output\n", name );
  si5345_output_enable( name );

  // Special for FELIG-type firmware (for the time being)
  if( firmw_type == FIRMW_FELIG_GBT ||
      firmw_type == FIRMW_FELIG_LPGBT ||
      firmw_type == FIRMW_FELIG_PIXEL )
    {
      // Specific for FELIG (described in FLX-1543):
      // apply a reset using this 48-bit register
      flxCard.cfg_set_reg( REG_GBT_GTRX_RESET, 0xFFFFFFFFFFFF );
      flxCard.cfg_set_reg( REG_GBT_GTRX_RESET, 0x000000000000 );
    }

  // Detect the SI5345B (2 potential addresses)
  // (early prototype FLX-182 has a different I2C address for the SI5345B device)
  const char *NAMES_B[] = { "SI5345B", "SI5345B_BETA" };
  name = NAMES_B[0];
  if( !si5345_detected( name ) )
    {
      name = NAMES_B[1];
      if( si5345_detected( name ) )
        tprintf( "Found %s\n", name );
    }

  regs = (clockchip_reg_t *) si5345_revd_registers_240_474; // See #include file
  n_registers = sizeof( si5345_revd_registers_240_474 )/sizeof( clockchip_reg_t );
  cfg_str = "'240-474'";

  tprintf( "Configuring %s: %s...\n", name, cfg_str.c_str() );

  // Configure
  reg_cnt = si5345_write_regs( name, n_registers, regs );
  tprintf( "%s configured (%d registers)\n", name, reg_cnt );

  // Enable
  tprintf( "%s enabling output\n", name );
  si5345_output_enable( name );
}

// ----------------------------------------------------------------------------

bool si5345_detected( const char *name )
{
  u_char current_page;
  u_char reg_index, reg_val;

  // Set the register page to 0
  current_page = 0;
  reg_index = 0x01;
  flxCard.i2c_write( name, reg_index, current_page );

  // Check whether the Si5345 chip is present and accessible
  // by reading the device part number from a register
  reg_index = 0x02;
  flxCard.i2c_read( name, reg_index, &reg_val );
  if( reg_val != 0x45 )
    {
      tprintf( "### %s part nr: read 0x%02X, expected 0x45; "
               "device not present/accessible?\n", name, reg_val );
      //tprintf( "(continuing)\n" );
      return false;
    }
  return true;
}

// ----------------------------------------------------------------------------

int si5345_write_regs( const char *name, u_int n_registers, clockchip_reg_t *regs )
{
  u_char reg_page, reg_index, reg_val;
  u_char current_page = 0xFF;
  int reg_cnt = 0;
  for( u_int i=0; i<n_registers; ++i )
    {
      // Skip this particular setting, if present (means: 're-program I2C address')
      if( regs[i].address == 0x000B )
        continue;

      // Split address into page and 8-bit register index, copy data
      reg_page  = (regs[i].address >> 8) & 0xFF;
      reg_index = regs[i].address & 0xFF;
      reg_val   = regs[i].value;
      ++reg_cnt;

      // Change the page number ?
      if( current_page != reg_page )
        {
          if( verbose )
            printf( "Set page from 0x%02X to 0x%02X\n",
                    current_page, reg_page );
          u_char page_set_addr = 0x01;
          flxCard.i2c_write( name, page_set_addr, reg_page );
          current_page = reg_page;
        }

      if( verbose )
        tprintf( "Write 0x%02X to register 0x%02X on page 0x%02X\n",
                 reg_val, reg_index, reg_page );

      flxCard.i2c_write( name, reg_index, reg_val );

      if( regs[i].address == 0x540 && reg_val == 1 )
        // End of configuration preamble: pause for 300ms
        usleep( 300*1000 );

      if( verbose )
        {
          u_char read_val;
          flxCard.i2c_read( name, reg_index, &read_val );
          if( read_val != reg_val )
            {
              // These registers cannot be read back
              if( (reg_page == 5 && reg_index == 0x14) ||
                  (reg_page == 0 && reg_index == 0x1c) )
                printf( "\n" );
              else
                tprintf( "### %s page %d, index 0x%02X: wrote 0x%02X, "
                         "read 0x%02X (ignore for write-only registers)\n",
                         name, reg_page, reg_index, reg_val, read_val );
            }
        }
    }
  return reg_cnt;
}

// ----------------------------------------------------------------------------

void si5345_output_enable( const char *name )
{
  u_char current_page, reg_index, reg_val;

  // Set page to 0
  if( verbose )
    printf( "Set page to 0, sleep 1 sec\n" );
  current_page = 0;
  reg_index = 0x01;
  flxCard.i2c_write( name, reg_index, current_page );

  sleep(1);

  flxCard.cfg_set_option( "HK_CTRL_FMC_SI5345_OE", 0 );

  reg_index = 0x0D;
  flxCard.i2c_read( name, reg_index, &reg_val );
  tprintf( "%s LOS register = 0x%02X\n", name, reg_val );

  reg_index = 0x12;
  flxCard.i2c_read( name, reg_index, &reg_val );
  flxCard.i2c_write( name, reg_index, 0 );
  flxCard.i2c_read( name, reg_index, &reg_val );
  tprintf( "%s Sticky LOS register = 0x%02X\n", name, reg_val );

  reg_index = 0x0E;
  int loop;
  for( loop=1; loop<6; ++loop )
    {
      flxCard.i2c_read( name, reg_index, &reg_val );
      tprintf( "%s LOL register = 0x%02X\n", name, reg_val );
      reg_val &= 0x2;
      if( reg_val == 0 )
        {
          tprintf( "%s found lock within %d secs\n", name, loop );
          break;
        }
      sleep(1);
    }
  if( reg_val != 0 )
    tprintf( "### %s Lock (bit 1) not found in %d secs\n", name, loop-1 );

  reg_index = 0x13;
  flxCard.i2c_read( name, reg_index, &reg_val );
  flxCard.i2c_write( name, reg_index, 0 );
  flxCard.i2c_read( name, reg_index, &reg_val );
  tprintf( "%s sticky LOL register = 0x%02X\n", name, reg_val );
}

// ----------------------------------------------------------------------------

void si53xx_read_regs()
{
  const char *name;
  clockchip_reg_t *regs;
  u_int n_registers;

  printf( "Clock chip registers " );

  if( card_type == 709 )
    {
      name = "SI5324";
      printf( "==> %s:\n", name );
      u_char byt;
      flxCard.i2c_read(name, 0x81, &byt);
      printf("LSB of register lock register 0x81 = %d\n", byt & 0x1);
      flxCard.i2c_read(name, 0x82, &byt);
      printf("LSB of register lock register 0x81 = %d\n", byt & 0x1);

      // Read back the same registers that have been written to
      // (so dependent on the firmware type)
      if( lpgbt_firmw_type )
        {
          regs = Si5324Regs_320; // From #include file
          n_registers = sizeof( Si5324Regs_320 )/sizeof( clockchip_reg_t );
        }
      else
        {
          regs = Si5324Regs_240;
          n_registers = sizeof( Si5324Regs_240 )/sizeof( clockchip_reg_t );
        }

      u_char reg_addr, reg_val;
      for( u_int i=0; i<n_registers; ++i )
        {
          reg_addr = (u_char) (regs[i].address & 0xFF);
          if( verbose )
            printf( "Reading register %d\n", reg_addr );

          flxCard.i2c_read( name, reg_addr, &reg_val );
          printf( "Addr %3d = 0x%02X\n", reg_addr, reg_val );
        }
      printf( "End of %s register dump (%d registers)\n", name, n_registers );
    }

  if( card_type != 709 && card_type != 710 && card_type != 711 && card_type != 712 &&
      card_type != 181 && card_type != 182 && card_type != 155 )
    {
      // Si5345 not on this FLX-card type
      return;
    }
  else if( card_type == 709 )
    {
      printf( "\n" );
    }

  // Read back the same registers that have been written to
  // (so dependent on the firmware type)
  name = "SI5345";
  std::string cfg_str;
  if( card_type == 182 || card_type == 155 )
    {
      name = "SI5345A";

      if( lpgbt_firmw_type )
        {
          regs = (clockchip_reg_t *) si5345_revd_registers_320_632; // See #include file
          n_registers = sizeof( si5345_revd_registers_320_632 )/sizeof( clockchip_reg_t );
          cfg_str = "'320-632'";
        }
      else if( firmw_type == FIRMW_INTERLAKEN )
        {
          regs = (clockchip_reg_t *) si5345_revd_registers_156_25; // See #include file
          n_registers = sizeof( si5345_revd_registers_156_25 )/sizeof( clockchip_reg_t );
          cfg_str = "'156.25'";
        }
      else
        {
          regs = (clockchip_reg_t *) si5345_revd_registers_240_474; // See #include file
          n_registers = sizeof( si5345_revd_registers_240_474 )/sizeof( clockchip_reg_t );
          cfg_str = "'240-474'";
        }

    }
  else if( lpgbt_firmw_type )
    {
      regs = (clockchip_reg_t *) si5345_revd_registers_40_079_320_632; // See #include file
      n_registers = sizeof( si5345_revd_registers_40_079_320_632 )/sizeof( clockchip_reg_t );
      cfg_str = "'40.079-320-632'";
    }
  else if( firmw_type == FIRMW_MROD )
    {
      regs = (clockchip_reg_t *) si5345_revb_registers_8x200; // See #include file
      n_registers = sizeof( si5345_revb_registers_8x200 )/sizeof( clockchip_reg_t );
      cfg_str = "'8x200'";
    }
  else
    {
      regs = (clockchip_reg_t *) si5345_revd_registers_8x240; // See #include file
      n_registers = sizeof( si5345_revd_registers_8x240 )/sizeof( clockchip_reg_t );
      cfg_str = "'8x240'";
    }

  printf( "==> %s: (config %s)\n", name, cfg_str.c_str() );
  u_char reg_page, reg_index, reg_val;
  u_char current_page = 0xFF, page_set_addr = 0x01;
  for( u_int i=0; i<n_registers; ++i )
    {
      // Split address into page and 8-bit register index, copy data
      reg_page  = (regs[i].address >> 8) & 0xFF;
      reg_index = regs[i].address & 0xFF;

      // Change the page number ?
      if( current_page != reg_page )
        {
          flxCard.i2c_write( name, page_set_addr, reg_page );
          current_page = reg_page;
        }

      flxCard.i2c_read( name, reg_index, &reg_val );
      printf("Pg%2d, Addr 0x%2X = 0x%02X\n", current_page, reg_index, reg_val );
    }
  printf( "End of %s register dump (%d registers)\n", name, n_registers );

  // Set page back to 0
  flxCard.i2c_write( name, page_set_addr, 0 );

  if( (card_type == 182 || card_type == 155) && firmw_type == FIRMW_INTERLAKEN )
    {
      // The second SI5345
      name = "SI5345B";

      regs = (clockchip_reg_t *) si5345_revd_registers_240_474; // See #include file
      n_registers = sizeof( si5345_revd_registers_240_474 )/sizeof( clockchip_reg_t );
      cfg_str = "'240-474'";

      printf( "\n==> %s: (config %s)\n", name, cfg_str.c_str() );
      current_page = 0xFF;
      for( u_int i=0; i<n_registers; ++i )
        {
          // Split address into page and 8-bit register index, copy data
          reg_page  = (regs[i].address >> 8) & 0xFF;
          reg_index = regs[i].address & 0xFF;

          // Change the page number ?
          if( current_page != reg_page )
            {
              flxCard.i2c_write( name, page_set_addr, reg_page );
              current_page = reg_page;
            }

          flxCard.i2c_read( name, reg_index, &reg_val );
          printf("Page%2d, Addr 0x%2X = 0x%02X\n", current_page, reg_index, reg_val );
        }
      printf( "End of %s register dump (%d registers)\n", name, n_registers );

      // Set page back to 0
      flxCard.i2c_write( name, page_set_addr, 0 );
    }
}

// ----------------------------------------------------------------------------

// SI570 register addresses
#define SI570_HSDIV_N1_REG    7
#define SI570_N1_RFREQ4_REG   8
#define SI570_RFREQ3_REG      9
#define SI570_RFREQ2_REG      10
#define SI570_RFREQ1_REG      11
#define SI570_RFREQ0_REG      12
#define SI570_CTRL_REG        135
#define SI570_FREEZE_DCO_REG  137

// SI570 register bits
#define SI570_RESET           0x80
#define SI570_NEWFREQ         0x40
#define SI570_FREEZE_M        0x20
#define SI570_RECALL          0x01
#define SI570_FREEZE_DCO      0x10

// This SI570's start-up frequency: 40.08 MHz
#define SI570_BASE_FREQ       40.08

void si570_set_freq( const char *name, uint64_t freq )
{
  /* From the SI570 datasheet (2007 edition):
     Programming Procedure
     ---------------------
     The following steps must be followed to set a new output frequency:
     1. Read the frequency configuration (RFREQ, HS_DIV, and N1)
        from the device after power-up or reset.
     2. Calculate the actual nominal crystal frequency
        (fXTAL) as: fXTAL = (f0 x HS_DIV x N1)/RFREQ
        where f0 is the nominal output frequency.
     3. Choose new output frequency (f1).
     4. Choose the output dividers (HS_DIV and N1) for the
        new output frequency by ensuring the DCO
        oscillation frequency (fosc) is within the allowed
        internal oscillator frequency (See Table 12) where:
        fosc = f1 x HS_DIV x N1.
     5. Calculate the new crystal frequency multiplication
        ratio (RFREQ1) as: fosc = fXTAL x RFREQ.
     6. Freeze the DCO (bit 5 of Register 137).
     7. Write the frequency configuration (RFREQ, HS_DIV, and N1).
     8. Unfreeze the DCO and assert the NewFreq bit (bit 6
        of Register 135) within the maximum delay specified
        in Table 12, "Programming Constraints", on page 11.

     In addition (from datasheet):
     The sets of dividers
     should be sorted to minimize fosc for power dissipation
     and to minimize N1 divider's power consumption.
     Silicon Laboratories' Si57x software automatically
     provides this optimization and returns the smallest
     HS_DIV x N1 combination with the highest HS_DIV value.
  */
  // Read the current configuration
  uint8_t bytes[6];
  flxCard.i2c_read( name, SI570_HSDIV_N1_REG,  &bytes[0] ); 
  flxCard.i2c_read( name, SI570_N1_RFREQ4_REG, &bytes[1] ); 
  flxCard.i2c_read( name, SI570_RFREQ3_REG,    &bytes[2] ); 
  flxCard.i2c_read( name, SI570_RFREQ2_REG,    &bytes[3] ); 
  flxCard.i2c_read( name, SI570_RFREQ1_REG,    &bytes[4] ); 
  flxCard.i2c_read( name, SI570_RFREQ0_REG,    &bytes[5] ); 

  // Extract the HS_DIV and N1 divider values
  uint32_t hsdiv, n1;
  hsdiv = (uint32_t) bytes[0] >> 5;
  n1    = (((uint32_t) bytes[0] & 0x1F) << 2) | (((uint32_t) bytes[1] & 0xC0) >> 6);
  // Add offsets
  hsdiv += 4;
  n1    += 1;

  // Determine the RFREQ value
  uint64_t rfreq;
  rfreq  = ((uint64_t) bytes[1] & 0x3F) << (uint64_t)32;
  rfreq |= ((uint64_t) bytes[2] << 24);
  rfreq |= ((uint64_t) bytes[3] << 16);
  rfreq |= ((uint64_t) bytes[4] << 8);
  rfreq |= ((uint64_t) bytes[5] << 0);

  // The frequency multiplier as a 'double' value,
  // by dividing its binary representation by 2^28 (see datasheet)
  double rfreq_d = (double) rfreq / (double) 0x10000000;

  // The requested frequency in MHz
  double freq_d  = (double) freq / (double) 1000000.0;

  // The crystal frequency
  double fxtal = (SI570_BASE_FREQ * (double) hsdiv * (double) n1) / rfreq_d;
  // The new fosc must be within allowed limits (see datasheet): TO CHECK!
  // (this one most likely does not comply!)
  double fosc = freq_d * (double) hsdiv * (double) n1;
  // For information (and debug):
  tprintf( "%s: hsdiv=%d, n1=%d, rfreq=%e, fxtal=%e MHz, fosc(new)=%e MHz\n",
           name, hsdiv, n1, rfreq_d, fxtal, fosc );

  // Assume we can reconfigure without changing HSDIV and N1
  // in this case from 40.08 to 156.25 MHz,
  // so multiply multiplier by 156.25/40.08 = 3.8984531
  // (NB: probably this won't work...)
  //double new_rfreq_d = (freq_d / (double) SI570_BASE_FREQ) * rfreq_d;
  // Binary representation, shifting it up by 28 bits (see datasheet):
  //rfreq = (uint64_t) (new_rfreq_d * (double) 0x10000000);

  // Select optimal HSDIV and N1 for frequency 'freq'
  if( !si570_calc_divs( freq, fxtal, &rfreq, &n1, &hsdiv ) )
    {
      tprintf( "### %s: Failed to determine settings\n", name );
      return;
    }
  rfreq_d = (double) rfreq / (double) 0x10000000;
  printf( "%s: hsdiv=%d, n1=%d, rfreq=%e\n", name, hsdiv, n1, rfreq_d );

  // Freeze DCO
  flxCard.i2c_write( name, SI570_FREEZE_DCO_REG, SI570_FREEZE_DCO ); 

  // Write the new configuration 
  hsdiv -= 4; // Subtract offset
  n1    -= 1; // Subtract offset
  bytes[0]  = (uint8_t) ((hsdiv & 0x7) << 5);
  bytes[0] |= (uint8_t) ((n1 & 0x7C) >> 2);
  bytes[1]  = (uint8_t) ((n1 & 0x03) << 6);
  bytes[1] |= (uint8_t) ((rfreq & 0x3F00000000ULL) >> (uint64_t)32);
  bytes[2]  = (uint8_t) ((rfreq & 0x00FF000000ULL) >> 24);
  bytes[3]  = (uint8_t) ((rfreq & 0x0000FF0000ULL) >> 16);
  bytes[4]  = (uint8_t) ((rfreq & 0x000000FF00ULL) >>  8);
  bytes[5]  = (uint8_t) ((rfreq & 0x00000000FFULL) >>  0);

  flxCard.i2c_write( name, SI570_HSDIV_N1_REG,  bytes[0] );
  flxCard.i2c_write( name, SI570_N1_RFREQ4_REG, bytes[1] );
  flxCard.i2c_write( name, SI570_RFREQ3_REG,    bytes[2] );
  flxCard.i2c_write( name, SI570_RFREQ2_REG,    bytes[3] );
  flxCard.i2c_write( name, SI570_RFREQ1_REG,    bytes[4] );
  flxCard.i2c_write( name, SI570_RFREQ0_REG,    bytes[5] );

  // Unfreeze DCO
  flxCard.i2c_write( name, SI570_FREEZE_DCO_REG, 0 );

  // Assert NewFreq bit
  flxCard.i2c_write( name, SI570_CTRL_REG, SI570_NEWFREQ );

  // Wait for 10 ms
  usleep( 10*1000 );
}

// ----------------------------------------------------------------------------

// Function si570_calc_divs() derived from identically named function in:
// https://github.com/Xilinx/linux-xlnx/blob/master/drivers/clk/clk-si570.c

#include <climits>
// Datasheet: 4850MHz <= fosc <= 5670MHz ('fosc' also known as 'fdco')
#define FDCO_MIN 4850000000LL
#define FDCO_MAX 5670000000LL

// All possible SI570 HS_DIV clock divider values:
static const uint32_t SI570_HS_DIV_VALUES[] = { 11, 9, 7, 6, 5, 4 };

bool si570_calc_divs( uint64_t frequency, double fxtal,
                      uint64_t *new_rfreq,
                      uint32_t *new_n1, uint32_t *new_hsdiv )
{
  uint32_t hsdiv, n1;
  uint64_t fdco, best_fdco = ULLONG_MAX;
  for( int i=0; i<6; ++i )
    {
      hsdiv = SI570_HS_DIV_VALUES[i];
      // Calculate lowest possible value for n1
      n1 = FDCO_MIN / (hsdiv * frequency);
      if( n1 == 0 || (n1 & 1) == 1 )
        ++n1;
      while( n1 <= 128 )
        {
          fdco = (uint64_t) frequency * (uint64_t) hsdiv * (uint64_t) n1;
          if( fdco > FDCO_MAX )
            break;
          if( fdco >= FDCO_MIN && fdco < best_fdco )
            {
              *new_n1    = n1;
              *new_hsdiv = hsdiv;
              *new_rfreq = (fdco << 28)/(uint64_t) (fxtal * (double)1000000.0);
              best_fdco  = fdco;
            }
          // Next valid value for n1 (1 and every even number <=128)
          n1 += (n1 == 1 ? 1 : 2);
        }
    }
  if( best_fdco == ULLONG_MAX )
    return false;
  return true;
}

// ----------------------------------------------------------------------------

void idt240_init()
{
  // Initializes IDT chips to 240 MHz
  tprintf( "Initializing IDT clocks...\n" );
  /*
  int switch_ports[] = {I2C_SWITCH_CXP1, I2C_SWITCH_CXP2};
  for( int i=0; i<2; ++i )
    {
      // Configure switch
      flxCard.i2c_write_byte(0x70, switch_ports[i]);

      // Configure 240 MHz
      flxCard.i2c_write_byte(I2C_ADDR_CXP, 0x12, 0xA8);
      flxCard.i2c_write_byte(I2C_ADDR_CXP, 0x00, 0x2A);
      flxCard.i2c_write_byte(I2C_ADDR_CXP, 0x04, 0x00);
      flxCard.i2c_write_byte(I2C_ADDR_CXP, 0x08, 0x11);
      flxCard.i2c_write_byte(I2C_ADDR_CXP, 0x0C, 0x0A);
      flxCard.i2c_write_byte(I2C_ADDR_CXP, 0x14, 0x1F);
      flxCard.i2c_write_byte(I2C_ADDR_CXP, 0x12, 0xA0);
    }
  */
  // The above to be accomplished more generally by taking
  // the switch settings and address from the corresponding
  // i2c_device_t structure i2_devices_FLX_710:
  flxCard.i2c_write( "CLOCK_CXP1", 0x12, 0xA8 );
  flxCard.i2c_write( "CLOCK_CXP1", 0x00, 0x2A );
  flxCard.i2c_write( "CLOCK_CXP1", 0x04, 0x00 );
  flxCard.i2c_write( "CLOCK_CXP1", 0x08, 0x11 );
  flxCard.i2c_write( "CLOCK_CXP1", 0x0C, 0x0A );
  flxCard.i2c_write( "CLOCK_CXP1", 0x14, 0x1F );
  flxCard.i2c_write( "CLOCK_CXP1", 0x12, 0xA0 );

  flxCard.i2c_write( "CLOCK_CXP2", 0x12, 0xA8 );
  flxCard.i2c_write( "CLOCK_CXP2", 0x00, 0x2A );
  flxCard.i2c_write( "CLOCK_CXP2", 0x04, 0x00 );
  flxCard.i2c_write( "CLOCK_CXP2", 0x08, 0x11 );
  flxCard.i2c_write( "CLOCK_CXP2", 0x0C, 0x0A );
  flxCard.i2c_write( "CLOCK_CXP2", 0x14, 0x1F );
  flxCard.i2c_write( "CLOCK_CXP2", 0x12, 0xA0 );

  tprintf( "IDT clocks done\n" );
}

// ----------------------------------------------------------------------------

typedef struct {
  const char *name;
  bool        installed;
} pod_device_t;

pod_device_t MinipodDevices[] = {
  {"1st TX", false},
  {"2nd TX", false},
  {"3rd TX", false},
  {"4th TX", false},
  {"1st RX", false},
  {"2nd RX", false},
  {"3rd RX", false},
  {"4th RX", false},
  {NULL,          false}
};

void minipods_reset()
{
  tprintf( "Resetting MiniPODs...\n" );

  // Detect installed MiniPOD devices
  u_char byt;
  pod_device_t *pod;
  for( pod = MinipodDevices; pod->name != NULL; ++pod )
    {
      u_char reg_addr = 152; // First char of 'vname'
      flxCard.i2c_read( pod->name, reg_addr, &byt );
      if( byt >= 0x20 && byt <= 0x7E ) // Printable char?
        {
          pod->installed = true;
        }
      else
        {
          // Try one more time
          flxCard.i2c_read( pod->name, reg_addr, &byt );
          if( byt >= 0x20 && byt <= 0x7E ) // Printable char?
            pod->installed = true;
        }

      if( !pod->installed )
        tprintf( "NOTE: %s not installed\n", pod->name );
    }

  for( pod = MinipodDevices; pod->name != NULL; ++pod )
    {
      if( !pod->installed )
        continue;

      if( verbose )
        printf( "Resetting device %s (part 1)\n", pod->name );
      //else
      //  tprintf( "Device %s\n", pod->name );

      flxCard.i2c_write( pod->name, 0x5d, 0xff );
      flxCard.i2c_read( pod->name, 0x5d, &byt );
      if( byt != 0xff )
        tprintf( "### %s reg address 0x5D: expected 0xFF got 0x%02X\n",
                 pod->name, byt );

      flxCard.i2c_write( pod->name, 0x5c, 0x0f );
      flxCard.i2c_read( pod->name, 0x5c, &byt );
      if( byt != 0x0f )
        tprintf( "### %s reg address 0x5C: expected 0x0F got 0x%02X\n",
                 pod->name, byt );
    }

  for( pod = MinipodDevices; pod->name != NULL; ++pod )
    {
      if( !pod->installed )
        continue;

      if( verbose )
        printf( "Resetting device %s (part 2)\n", pod->name );

      flxCard.i2c_write( pod->name, 0x5d, 0 );
      flxCard.i2c_read( pod->name, 0x5d, &byt );
      if( byt != 0 )
        tprintf( "### %s reg address 0x5D: expected 0x00 got 0x%02X\n",
                 pod->name, byt );

      flxCard.i2c_write( pod->name, 0x5c, 0 );
      flxCard.i2c_read( pod->name, 0x5c, &byt );
      if( byt != 0 )
        tprintf( "### %s reg address 0x5C: expected 0x00 got 0x%02X\n",
                 pod->name, byt );
    }

  tprintf( "MiniPODs reset done\n" );
}

// ----------------------------------------------------------------------------

// FireFly reset controlled by a TCA6424A I/O expander chip
const uint8_t TCA6424_AUTO_INCR        = 0x80;
const uint8_t TCA6424_CMD_INPUT_PORT0  = 0x00;
const uint8_t TCA6424_CMD_OUTPUT_PORT0 = 0x04;
const uint8_t TCA6424_CMD_POLINV_PORT0 = 0x08;
const uint8_t TCA6424_CMD_CONFIG_PORT0 = 0x0C;

extern std::vector<std::string> FF_NAMES_182;
extern std::vector<std::string> FF_NAMES_155;

void firefly_reset()
{
  if( card_type == 182 )
    {
      tprintf( "Resetting FireFly devices...\n" );

      // Preset I/Os to 0
      flxCard.i2c_write( "IO_EXPANDER_B", TCA6424_CMD_OUTPUT_PORT0,   0 );
      flxCard.i2c_write( "IO_EXPANDER_B", TCA6424_CMD_OUTPUT_PORT0+1, 0 );
      flxCard.i2c_write( "IO_EXPANDER_B", TCA6424_CMD_OUTPUT_PORT0+2, 0 );

      // Set I/Os of FireFly resets to output: bits 0, 4, 8, 12 and 16
      flxCard.i2c_write( "IO_EXPANDER_B", TCA6424_CMD_CONFIG_PORT0,   0xEE );
      flxCard.i2c_write( "IO_EXPANDER_B", TCA6424_CMD_CONFIG_PORT0+1, 0xEE );
      flxCard.i2c_write( "IO_EXPANDER_B", TCA6424_CMD_CONFIG_PORT0+2, 0xFE );

      // A short pause...
      usleep( 200*1000 ); // 200 ms

      // Take FireFly devices out of reset
      flxCard.i2c_write( "IO_EXPANDER_B", TCA6424_CMD_OUTPUT_PORT0,   0x11 );
      flxCard.i2c_write( "IO_EXPANDER_B", TCA6424_CMD_OUTPUT_PORT0+1, 0x11 );
      flxCard.i2c_write( "IO_EXPANDER_B", TCA6424_CMD_OUTPUT_PORT0+2, 0x01 );

      // A short pause...
      usleep( 200*1000 ); // 200 ms
    }
  else
    {
      //printf( "### (FLX-155: connected to FPGA pins; to be operated through registers)\n" );
    }

  const std::vector<std::string> *ff_names;
  if( card_type == 182 )
    ff_names = &FF_NAMES_182;
  else
    ff_names = &FF_NAMES_155;

  std::string vendor_part, vendor_sn;
  for( const std::string &dev_name: (*ff_names) )
    {
      vendor_part.clear(); vendor_sn.clear();
      if( flxCard.fireFlyDetect( dev_name, vendor_part, vendor_sn ) )
        {
          tprintf( "%s: PartNr=\"%s\", SerNr=\"%s\"\n",
                   dev_name.c_str(), vendor_part.c_str(), vendor_sn.c_str() );

          // Custom setting for the FLX-182 'TXRX' FireFly:
          // disable Clock Data Recovery (CDR, added 17 Jan 2025)
          if( card_type == 182 && dev_name == std::string( "FIREFLY_TXRX" ) )
            {
              if( vendor_part == std::string("B042504005170") )
                {
                  // Write to CDR enable register (98)
                  flxCard.i2c_write( dev_name.c_str(), 98, 0x00 );
                  tprintf( "  NB: CDR enable register set to 0x00\n" );
                }
              else
                {
                  tprintf( "  WARNING: CDR not changed, part expected \"B042504005170\"\n" );
                }
            }
        }
      else
        {
          tprintf( "### %s not found (part=%s)\n", dev_name.c_str(), vendor_part.c_str() );
        }
    }

  if( card_type == 182 )
    tprintf( "FireFly done\n" );
}

// ----------------------------------------------------------------------------

extern std::vector<std::string> INA226_NAMES_182;
extern std::vector<std::string> INA226_NAMES_155;
extern float INA226_RSHUNT_182[];
extern float INA226_RSHUNT_155[];

void ina226_configure()
{
  try {
    tprintf( "Configuring INA226 devices...\n" );

    const std::vector<std::string> *names;
    const float *sh;
    if( card_type == 182 )
      {
        names = &INA226_NAMES_182;
        sh = &INA226_RSHUNT_182[0];
      }
    else
      {
        names = &INA226_NAMES_155;
        sh = &INA226_RSHUNT_155[0];
      }
    uint32_t calib;
    uint32_t config = 0x4127; // The default
    config &= ~(0x7 << 9);    // Clear AVG bits
    config |= (0x2 << 9);     // AVG = 2 (16 samples)
    tprintf( "INA226 average over 16 samples\n" );

    // Configure the INA226s (NB: 2-byte registers)
    for( const std::string &n : *names )
      {
        uint8_t inareg[2];
        // Configuration (different from default):
        // - average over 16 measurements
        inareg[0] = (uint8_t) ((config >> 8) & 0xFF);
        inareg[1] = (uint8_t) ((config >> 0) & 0xFF);
        flxCard.i2c_write( n.c_str(), 0x00, inareg, 2 );

        // Calibration value:
        // on all devices use a 1 mA current-LSB,
        // the calibration value is then calculated as 5120/Rshunt
        // with Rshunt in mOhm (see datasheet)
        calib = (uint32_t) ((float) 5120.0/(*sh));
        inareg[0] = (uint8_t) ((calib >> 8) & 0xFF);
        inareg[1] = (uint8_t) ((calib >> 0) & 0xFF);
        flxCard.i2c_write( n.c_str(), 0x05, inareg, 2 );

        tprintf( "INA226 %-13s: Rsh=%.2f -> Calib=%d\n",
                 n.c_str(), *sh, calib );
        ++sh;
      }

    tprintf( "INA226 configured\n" );
  }
  catch( FlxException &ex ) {
    std::cout << "### ERROR INA226 config: " << ex.what() << std::endl;
  }
}

// ----------------------------------------------------------------------------

void ics8n_configure()
{
  tprintf( "Configuring ICS8N (Clock RAM)...\n" );

  clockchip_reg_t *regs = Ics8nRegs;
  u_int n_registers = sizeof( Ics8nRegs )/sizeof( clockchip_reg_t );

  u_char reg_addr, reg_val;
  for( u_int i=0; i<n_registers; ++i )
    {
      reg_addr = (u_char) (regs[i].address & 0xFF);
      reg_val  = regs[i].value;
      if( verbose )
        printf( "Write 0x%02X to register 0x%02X\n", reg_val, reg_addr );
      flxCard.i2c_write( "CLOCK_RAM", reg_addr, reg_val );

      // Check
      flxCard.i2c_read( "CLOCK_RAM", reg_addr, &reg_val );
      if( reg_val != regs[i].value )
        tprintf( "### ICS8N register 0x%02X, wrote 0x%02X, read 0x%02X\n",
                 reg_addr, regs[i].value, reg_val );
    }

  tprintf( "ICS8N configured (%d registers)\n", n_registers );
}

// ----------------------------------------------------------------------------

void lti_init()
{
  // Added 9 Dec 2024 (see FLX-2134)
#if REGMAP_VERSION >= 0x500
  tprintf( "Initializing LTI...\n" );
  usleep( 100*1000 );

  // Reset LTI
  flxCard.m_bar2->LTITTC_CTRL.LTITTC_SOFT_RESET = 1;
  flxCard.m_bar2->LTITTC_CTRL.LTITTC_SOFT_RESET = 0;

  // Reset LTI (For LTI Emulator)
  flxCard.cr_fromhost_reset();
  usleep( 100*1000 );

  tprintf( "LTI alignment: %s\n",
           (flxCard.m_bar2->LTITTC_ALIGNMENT_DONE == 1 ? "YES" : "NO") );

  // Let the CDC (LTI_FE_Sync.vhd) find a stable clock phase
  flxCard.m_bar2->LTI_SYNC_TRAINING.DECODER = 0x1;
  flxCard.m_bar2->LTI_SYNC_TRAINING.FE      = 0xFFFFFF;
  usleep( 100*1000 );

  // Stop CDC training 
  flxCard.m_bar2->LTI_SYNC_TRAINING.DECODER = 0x0;
  flxCard.m_bar2->LTI_SYNC_TRAINING.FE      = 0x0;

  //tprintf( "LTI initialized\n" );
#endif // REGMAP_VERSION
}

// ----------------------------------------------------------------------------

void sfp_los_check()
{
  tprintf( "Checking SFP LOS...\n" );

  const char *device[4]  = { "SFP1-2", "SFP2-2", "SFP3-2", "SFP4-2" };
  u_char      loss_os[4] = { 0, 0, 0, 0 };
  for( int index=0; index<4; ++index )
    {
      if( verbose )
        printf( "Reading device %s", device[index] );

      u_char aux_value;
      flxCard.i2c_read( device[index], 110, &aux_value );
      u_char rx_lost  = ((aux_value & (1 << 1)) >> 1);
      loss_os[index] = rx_lost;

      if( verbose )
        {
          printf( "Loss of Signal state: %u", rx_lost );
          if( rx_lost == 1 )
            printf( "        Signal not received or under threshold\n" );
          else
            printf( "        Receiving signal\n" );
        }
    }

  tprintf( "SFP Link Status:\n" );
  const u_int sfp_sequence[4] = { 1, 0, 2, 3 };
  for( int index=0; index<4; ++index )
    {
      if( loss_os[sfp_sequence[index]] == 1 )
        tprintf( "WARNING: channel %d has no fibre connected\n", sfp_sequence[index] );
      else
        tprintf( "Info: channel %d has a fibre connected\n", sfp_sequence[index] );
    }
}

// ----------------------------------------------------------------------------

void str_upper(char *str)
{
  do
    {
      *str = toupper((u_char) *str);
    } while (*str++);
}

// ----------------------------------------------------------------------------

int tprintf( const char *format, ... )
{
  printf( "%s ", timestamp().c_str() );
  va_list argp;
  va_start( argp, format );
  return vprintf( format, argp );
  va_end( argp );
}

// ----------------------------------------------------------------------------

std::string timestamp()
{
  char timestr[32];
  time_t rawtime;
  time( &rawtime ); // Get current time
  struct tm *timeinfo;
  timeinfo = localtime( &rawtime );
  strftime( timestr, sizeof(timestr), "%Y-%m-%d %H:%M:%S", timeinfo );
  return std::string( timestr );
}

// ----------------------------------------------------------------------------
