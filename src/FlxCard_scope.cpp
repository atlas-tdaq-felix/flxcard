/*******************************************************************/
/*                                                                 */
/* This is the test program for the FlxCard object                 */
/*                                                                 */
/* Author: Markus Joos, CERN                                       */
/*                                                                 */
/**C 2017 Ecosoft - Made from at least 80% recycled source code*****/


#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <linux/types.h>

#include "flxcard/FlxCard.h"
#include "DFDebug/DFDebug.h"
#include "ROSGetInput/get_input.h"
#include "flxcard/FlxException.h"


//Globals
FlxCard flxCards[MAXCARDS];

// prototypes
int mainhelp(void);
int setdebug(void);
void checkoffset(void);
void dumpreg(void);
void check_method(void);
void check_locking(int pnum);
void check_locking_debug(void);
void ts_test(void);
void reg_peek_poke(int mode);


/*****************************/
int main(int argc, char **argv)
/*****************************/
{
  int fun = 1;

  printf("This is FlxCard_Scope\n");

  while(fun != 0)
  {
    printf("\n");
    printf("Select an option:\n");
    printf("   1 Help                        2 Set debug parameters\n");
    printf("   3 Check register offsets      4 Dump registers\n");
    printf("   5 Check methods               6 Test time stamping\n");
    printf("   7 Peek register               8 Poke register\n");
    printf("   9 Check locking              10 Check locking (process 2)\n");
    printf("  11 Check locking debug\n");
    printf("   0 Quit\n");
    printf("Your choice: ");
    fun = getdecd(fun);
    if (fun == 1) mainhelp();
    if (fun == 2) setdebug();
    if (fun == 3) checkoffset();
    if (fun == 4) dumpreg();
    if (fun == 5) check_method();
    if (fun == 6) ts_test();
    if (fun == 7) reg_peek_poke(0);
    if (fun == 8) reg_peek_poke(1);
    if (fun == 9) check_locking(0);
    if (fun == 10) check_locking(1);
    if (fun == 11) check_locking_debug();
  }
}


/**************************/
void reg_peek_poke(int mode)
/**************************/
{
  static int cardnum, barnum, regoff;
  u_long baraddr, *regptr, regdata;

  printf("Note: All registers are treated as 64bit resources\n\n");

  printf("Enter the number (0...N-1) of the card to access\n");
  cardnum = getdecd(0);
  
  printf("Enter the number (0...2) of the BAR\n");
  barnum = getdecd(0);

  printf("Enter the offset of the register\n");
  regoff = gethexd(0);
  
  if(mode)
  {
    printf("Enter the 64 bit of data to write\n");
    regdata = gethexd(0);
  }
  
  try
  {
    flxCards[0].card_open(cardnum, LOCK_NONE);
    baraddr = flxCards[0].openBackDoor(barnum);
    regptr = (u_long *)(baraddr + regoff);

    if(mode == 0)
    {
      regdata = *regptr;
      printf("The register contains 0x%016lx\n", regdata);
    }
    else
    {
      *regptr = regdata;
      printf("Data rwitten\n");      
    }

    flxCards[0].card_close();
  }

  catch(FlxException &ex)
  {
    std::cout << "ERROR. Exception thrown: " << ex.what() << std::endl;
  }
}


/****************/
void ts_test(void)
/****************/
{
  int i;
  struct timespec ts, ts1, ts2;
  long sec[16], nsec[16];
  long long ns_waited;

  for(i = 0; i < 16; i++)
  {
    clock_gettime(CLOCK_REALTIME, &ts);
    sec[i] = ts.tv_sec;
    nsec[i] = ts.tv_nsec;
  }
  for(i = 0; i < 16; i++)
  {
    printf("%ld.%ld  ", sec[i], nsec[i]);
    if (i)
      printf("%lu", nsec[i] - nsec[i - 1]);
    printf("\n");
  }    

  clock_gettime(CLOCK_REALTIME, &ts1);
  clock_gettime(CLOCK_REALTIME, &ts2);
  ns_waited = (ts2.tv_sec * 1000000000 + ts2.tv_nsec) - (ts1.tv_sec * 1000000000 + ts1.tv_nsec); 
  printf("ts1.tv_sec  = %lu \n", ts1.tv_sec);
  printf("ts1.tv_nsec = %lu \n", ts1.tv_nsec);
  printf("ts2.tv_sec  = %lu \n", ts2.tv_sec);
  printf("ts2.tv_nsec = %lu \n", ts2.tv_nsec);
  printf("ns_waited = %llu ns\n", ns_waited);

}


/****************************/
void check_locking_debug(void)
/****************************/
{
  FlxCard flxCard1;
  int fun = 1;
  static int lbits = 0, lcard = 0;

  printf("Enter the card number ");  
  lcard = getdecd(lcard);

  printf("Enter the lock mask ");  
  lbits = getdecd(lbits);

  flxCard1.card_open(lcard, lbits);
  
  printf("Press return to call lock_mask ");  fun = getdecd(fun);
  printf("The lock mask of instance 1 is 0x%08x\n", flxCard1.lock_mask(1));   
      
  printf("Press return to call card_close");  fun = getdecd(fun);
  flxCard1.card_close();
}


/**************************/
void check_locking(int pnum)
/**************************/
{
  FlxCard flxCard1, flxCard2;
  int fun = 1;

  if(pnum == 0)
  {
    printf("calling card_open for object 1\n");  flxCard1.card_open(1, 1);
    printf("calling card_open for object 2\n");  flxCard2.card_open(1, 2);
  }
  else if(pnum == 1)
  {
    printf("calling card_open for object 1\n");  flxCard1.card_open(1, 4);
    printf("calling card_open for object 2\n");  flxCard2.card_open(1, 8);
  }
  
  printf("Press return to call lock_mask ");  fun = getdecd(fun);
  printf("calling lock_mask for object 1\n");  printf("The lock mask of instance 1 is 0x%08x\n", flxCard1.lock_mask(1));   
      
  printf("Press return to call lock_mask ");  fun = getdecd(fun);
  printf("calling lock_mask for object 2\n");  printf("The lock mask of instance 2 is 0x%08x\n", flxCard2.lock_mask(1));     

  printf("Press return to call card_close");  fun = getdecd(fun);
  printf("calling card_close for object 1\n");  flxCard1.card_close();

  printf("Press return to call card_close");  fun = getdecd(fun);
  printf("calling card_close for object 2\n");  flxCard2.card_close();
}


/*********************/
void check_method(void)
/*********************/
{
  static int card_num = 0, method_parameter, method_value_int;
  u_long method_value_long;
  u_int uival;
  int fun = 1;
  FlxCard flxCard;
  bool istrue;
  
  printf("\n=========================================================================\n");
  while (fun != 0)  
  {
    printf("\n");
    printf("Select a function of the API:\n");
    printf("   1 First select the FLX card (default: card 0)    \n");
    printf("   -------------------------------------------------\n");
    printf("   2 card_open             3 dma_max_tlp_bytes      \n");
    printf("   4 dma_reset             5 dma_stop               \n");
    printf("   6 dma_fifo_flush        7 dma_get_fw_ptr         \n");
    printf("   8 dma_wait              9 lock_mask              \n");
    printf("   10 card_close                                    \n");
    printf("   12 irq_enable          13 irq_disable            \n");
    printf("   14 dma_get_ptr         15 lock_mask              \n");   
    printf("   16 dma_cmp_even_bits   17 number_of_devices      \n");   
    printf("   -------------------------------------------------\n");
    printf("   0 Return to the main menu                        \n");
    printf("Your choice ");
    fun = getdecd(fun);
      
    if(fun == 1)
    {
      printf("Enter the number of the card (0..N-1): ");
      card_num = getdecd(card_num);
    }
    
    if(fun == 2)
    {
      printf("calling card_open\n");
      printf("Enter the lock_mask: ");
      method_parameter = gethexd(method_parameter);

      printf("Enter the card number: ");
      card_num = getdecd(card_num);

      flxCard.card_open(card_num, method_parameter);
    }
       
    if(fun == 3)
    {
      flxCard.card_open(card_num, LOCK_NONE);
      method_value_int = flxCard.dma_max_tlp_bytes();
      printf("dma_max_tlp_bytes returns 0x%08x\n", method_value_int);
      flxCard.card_close();
    }    
    
    if(fun == 4)
    { 
      flxCard.card_open(card_num, LOCK_NONE);
      printf("calling dma_reset");
      flxCard.dma_reset();
      flxCard.card_close();
    }
    
    if(fun == 5)
    { 
      printf("Enter the number of the DMA channel (0..7): ");
      method_parameter = getdecd(method_parameter);
      flxCard.card_open(card_num, LOCK_NONE);
      printf("calling dma_stop");
      flxCard.dma_stop(method_parameter);
      flxCard.card_close();
    }

    if(fun == 6)
    { 
      //flxCard.card_open(card_num, LOCK_NONE);
      printf("dma_flush has been disbled on the request of Frans");
      //flxCard.dma_fifo_flush();
      //flxCard.card_close();
    }

    if(fun == 7)
    { 
      printf("Enter the number of the DMA channel (0..7): ");
      method_parameter = getdecd(method_parameter);
      flxCard.card_open(card_num, LOCK_NONE);
      method_value_long = flxCard.dma_get_fw_ptr(method_parameter);
      printf("dma_get_fw_ptr returns 0x%016lx\n", method_value_long);
      flxCard.card_close();
    }

    if(fun == 8)
    {
      printf("calling dma_wait");
      printf("Enter the number of the DMA channel (0..7): ");
      method_parameter = getdecd(method_parameter);
      flxCard.card_open(card_num, LOCK_NONE);
      flxCard.dma_wait(method_parameter);
      flxCard.card_close();
    }
    
    if(fun == 9)
    {
      printf("calling lock_mask\n");
      method_parameter = flxCard.lock_mask(card_num);
      printf("The lock mask is 0x%08x\n", method_parameter);     
    }

    if(fun == 10)
    {
      printf("calling card_close\n");
      flxCard.card_close();
    }

    if(fun == 12)
    {
      printf("Enter the number of the interrupt to enable: ");
      method_parameter = getdecd(method_parameter);       
      printf("calling irq_enable");
      flxCard.card_open(card_num, LOCK_NONE);
      flxCard.irq_enable(method_parameter);
      flxCard.card_close();
    }
    
    if(fun == 13)
    {
      printf("Enter the number of the interrupt to disable: ");
      method_parameter = getdecd(method_parameter);       
      printf("calling irq_disable");
      flxCard.card_open(card_num, LOCK_NONE);
      flxCard.irq_disable(method_parameter);
      flxCard.card_close();
    }
    
    if(fun == 14)
    {
      printf("Enter the number of the DMA channel (0 or 1): ");
      method_parameter = getdecd(method_parameter);       
      flxCard.card_open(card_num, LOCK_NONE);
      printf("dma_get_ptr returns 0x%016lx\n", flxCard.dma_get_ptr(method_parameter));
      flxCard.card_close();
    }
    
    if(fun == 15)
    {
      method_parameter = flxCard.lock_mask(card_num);
      printf("lock_mask returns 0x%08x\n", method_parameter);
    }
    
    if(fun == 16)
    {
      printf("Enter the number of the DMA channel: ");
      method_parameter = getdecd(method_parameter);       
      istrue = flxCard.dma_cmp_even_bits(method_parameter);
      printf(" Bits are equal is: %s\n", istrue?"TRUE":"FALSE");
    }

    if(fun == 17)
    {
      uival = flxCard.number_of_devices();
      printf("%d devices found\n", uival);

    }
  }
}


/****************/
void dumpreg(void)
/****************/
{
  static int cardnum;
  u_long baraddr;
  int loop;
  flxcard_bar0_regs_t *bar0;
  flxcard_bar1_regs_t *bar1;
  u_long *bar2, lrdata;
  float fvalue;
  
  printf("Enter the number (0...N-1) of the card to access\n");
  cardnum = getdecd(0);
  try
  {
    flxCards[0].card_open(cardnum, LOCK_NONE);

    baraddr = flxCards[0].openBackDoor(0);
    bar0 = (flxcard_bar0_regs_t *)baraddr;
    baraddr = flxCards[0].openBackDoor(1);
    bar1 = (flxcard_bar1_regs_t *)baraddr;
    baraddr = flxCards[0].openBackDoor(2);
    bar2 = (u_long *)baraddr;

    printf("Dumping (some) registers of BAR0\n");
    printf("BAR0_VALUE      = 0x%08x\n", bar0->BAR0_VALUE);
    printf("BAR1_VALUE      = 0x%08x\n", bar0->BAR1_VALUE);
    printf("BAR2_VALUE      = 0x%08x\n", bar0->BAR2_VALUE);
    printf("DMA_DESC_ENABLE = 0x%02x\n", bar0->DMA_DESC_ENABLE);

    printf("\nDumping all registers of BAR1 (interrups)\n");
    printf("| Nr. |    Control |       Data |            Address |\n");
    printf("|-----|------------|------------|--------------------|\n");
    for (loop = 0; loop < 8; loop++)
      printf("|   %d | 0x%08x | 0x%08x | 0x%016lx |\n", loop, bar1->INT_VEC[loop].control, bar1->INT_VEC[loop].data, bar1->INT_VEC[loop].address);

    printf("INT_TAB_ENABLE  = 0x%02x\n", bar1->INT_TAB_ENABLE);

    printf("\nDumping (some) registers of BAR2\n");
    lrdata = bar2[0];
    printf("REG_MAP_VERSION        = 0x%016lx\n", lrdata);
    lrdata = bar2[0x10 / 8];
    printf("BOARD_ID_TIMESTAMP     = 0x%016lx\n", lrdata);
    lrdata = bar2[0x30 / 8];
    printf("GIT_COMMIT_TIME        = 0x%016lx\n", lrdata);
    lrdata = bar2[0x60 / 8];
    printf("GIT_HASH               = 0x%016lx\n", lrdata);

    u_int tloop;
    for(tloop = 0; tloop < 10000; tloop++)
    {
      lrdata = bar2[0x9320 / 8];    
	
      if (tloop < 10)
      {
        fvalue = (((float)lrdata * 503.975)/4096.0 - 273.15);
	printf("FPGA_CORE_TEMP %d       = 0x%016lx = %f C\n", tloop, lrdata, fvalue);
      }
      if (tloop == 10)
	printf("....\n");
      if (lrdata != 0)
	break;
    }
    printf("After %d reads FPGA_CORE_TEMP (still) is 0x%016lx\n", tloop, lrdata);


    lrdata = bar2[0x9840 / 8];    
    fvalue = 600000000 / lrdata;
    printf("TACH_CNT (fan speed) 1 = 0x%016lx = %f RPM\n", lrdata, fvalue);
    lrdata = bar2[0x9840 / 8];    
    fvalue = 600000000 / lrdata;
    printf("TACH_CNT (fan speed) 2 = 0x%016lx = %f RPM\n", lrdata, fvalue);
    lrdata = bar2[0x9840 / 8];    
    fvalue = 600000000 / lrdata;
    printf("TACH_CNT (fan speed) 3 = 0x%016lx = %f RPM\n", lrdata, fvalue);
    lrdata = bar2[0x9840 / 8];    
    fvalue = 600000000 / lrdata;
    printf("TACH_CNT (fan speed) 4 = 0x%016lx = %f RPM\n", lrdata, fvalue);

    flxCards[0].card_close();
  }

  catch(FlxException &ex)
  {
    std::cout << "ERROR. Exception thrown: " << ex.what() << std::endl;
  }
}


/********************/
void checkoffset(void)
/********************/
{
  flxcard_bar0_regs_t *ptr;
  ptr = (flxcard_bar0_regs_t *)0;
  printf("ptr->DMA_DESC[0].start_address     is at %p\n", (void *)&ptr->DMA_DESC[0].start_address);
  printf("ptr->DMA_DESC[0].end_address       is at %p\n", (void *)&ptr->DMA_DESC[0].end_address);
  printf("ptr->DMA_DESC_STATUS[0].fw_pointer is at %p\n", (void *)&ptr->DMA_DESC_STATUS[0].fw_pointer);
  printf("ptr->BAR1_VALUE                    is at %p\n", (void *)&ptr->BAR1_VALUE);
  printf("ptr->SOFT_RESET                    is at %p\n\n", (void *)&ptr->SOFT_RESET);

  flxcard_bar1_regs_t *ptr1;
  ptr1 = (flxcard_bar1_regs_t *)0;
  printf("ptr->INT_VEC[0].control            is at %p\n", (void *)&ptr1->INT_VEC[0].control);
  printf("ptr->INT_TAB_ENABLE                is at %p\n\n", (void *)&ptr1->INT_TAB_ENABLE);
}


/****************/
int setdebug(void)
/****************/
{
  static u_int dblevel = 20, dbpackage = DFDB_FELIXCARD;

  printf("Enter the debug level: ");
  dblevel = getdecd(dblevel);
  printf("Enter the debug package: ");
  dbpackage = getdecd(dbpackage);
  DF::GlobalDebugSettings::setup(dblevel, dbpackage);
  return(0);
}


/****************/
int mainhelp(void)
/****************/
{
  printf("\n=========================================================================\n");
  printf("Call Markus Joos, 72364, 160663 if you need more help\n");
  printf("=========================================================================\n\n");
  return(0);
}
