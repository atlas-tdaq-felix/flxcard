/*******************************************************************/
/*                                                                 */
/* This is the C++ source code of the flx-config application       */
/*                                                                 */
/* Author: Markus Joos, CERN                                       */
/*                                                                 */
/* From Nov 2020: maintenance by H.Boterenbrood, Nikhef            */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <iostream>
#include <iomanip>
using namespace std;

#if DEBUG_LEVEL > 0
#include "DFDebug/DFDebug.h"
#endif // DEBUG_LEVEL
#include "flxcard/FlxCard.h"
#include "flxcard/FlxException.h"
#include "felixtag.h"

enum cmd_mode
  {
   CMD_UNKNOWN,
   CMD_LIST,
   CMD_REGISTERS,
   CMD_GET,
   CMD_SET,
   CMD_SETRAW,
   CMD_GETRAW,
   CMD_LOAD,
   CMD_STORE,
   CMD_GET_DIRECT,
   CMD_SET_DIRECT,
  };

// ----------------------------------------------------------------------------

// Function prototypes
void   cmd_list      ( bool no_flxcard );
void   cmd_register  ( char **groups, int num_groups, bool no_flxcard );
void   cmd_set       ( char **config, int n );
void   cmd_get       ( char **config, int n );
void   cmd_load      ( char *filename );
void   cmd_store     ( char *filename );
void   print_header  ( );
void   print_register( u_long address, u_int flags, u_int lo, u_int hi,
                       const char *name, u_long value, const char *description,
                       bool without_value = false );
u_long split_key_value( char *str );
void   display_help   ( );

// Globals
FlxCard flxCard;

// ----------------------------------------------------------------------------

int main( int argc, char **argv )
{
  if( argc < 2 )
    {
      display_help();
      return 1;
    }

  int    device_number  = 0;
  u_int  raw_bar        = 2;
  u_int  raw_address    = 0;
  u_int  raw_offset     = 0;
  u_int  raw_width      = 32;
  u_long raw_value      = 0;
  bool   locks_override = false;
  bool   no_flxcard     = false;

  int opt;
#if DEBUG_LEVEL > 0
  while( (opt = getopt(argc, argv, "hd:D:Vb:r:o:w:v:En")) != -1 )
#else
  while( (opt = getopt(argc, argv, "hd:Vb:r:o:w:v:En")) != -1 )
#endif // DEBUG_LEVEL
    {
      switch( opt )
        {
        case 'd':
          device_number = atoi(optarg);
          break;

        case 'b':
          raw_bar = atoi(optarg);
          break;

        case 'r':
          sscanf(optarg, "%x", &raw_address);
          break;

        case 'o':
          raw_offset = atoi(optarg);
          break;

        case 'w':
          raw_width = atoi(optarg);
          break;

        case 'v':
          sscanf(optarg, "%lx", &raw_value);
          break;

#if DEBUG_LEVEL > 0
        case 'D':
          {
            int debuglevel = atoi(optarg);
            DF::GlobalDebugSettings::setup(debuglevel, DFDB_FELIXCARD);
          }
          break;
#endif // DEBUG_LEVEL

        case 'h':
          display_help();
          return 0;

        case 'V':
          printf( "Version %s\n", FELIX_TAG );
          return 0;

        case 'E':
          locks_override = true;
          break;

        case 'n':
          no_flxcard = true;
          break;

        default:
          fprintf( stderr, "Usage: flx-config COMMAND [OPTIONS]\n"
                   "Try flx-config -h for more information.\n" );
          return 1;
        }
    }

  if( optind == argc )
    {
      fprintf(stderr, "No command given\n"
              "Usage: flx-config COMMAND [OPTIONS]\n"
              "Try flx-config -h for more information.\n");
      return 1;
    }

  // Check for reserved command words
  int command = CMD_UNKNOWN;
  if( strcasecmp(argv[optind], "registers") == 0 )
    command = CMD_REGISTERS;
  else if( strcasecmp(argv[optind], "list") == 0 )
    command = CMD_LIST;
  else if( strcasecmp(argv[optind], "set") == 0 )
    command = CMD_SET;
  else if( strcasecmp(argv[optind], "get") == 0 )
    command = CMD_GET;
  else if( strcasecmp(argv[optind], "getraw") == 0 )
    command = CMD_GETRAW;
  else if( strcasecmp(argv[optind], "setraw") == 0 )
    command = CMD_SETRAW;
  else if( strcasecmp(argv[optind], "load") == 0 )
    command = CMD_LOAD;
  else if( strcasecmp(argv[optind], "store") == 0 )
    command = CMD_STORE;
  else
    {
      // Assume 'command' may represent a bit field name
      // (and a next argument a value to write)
      if( argc == optind + 2 )
        command = CMD_SET_DIRECT;
      else
        command = CMD_GET_DIRECT;
    }

  // Allow certain operations even on incompatible firmware versions
  bool ignore_version = false, read_config = false;
  if( command == CMD_REGISTERS || command == CMD_LIST ||
      command == CMD_GET || command == CMD_GET_DIRECT ||
      command == CMD_GETRAW )
    {
      ignore_version = true;
      locks_override = true; // No need to check resource locks either
    }

  // Allow a few commands without accessing a FELIX card
  if( no_flxcard && !(command == CMD_REGISTERS || command == CMD_LIST) )
    {
      printf( "WARNING: only commands 'list' or 'registers' possible without FLX card (option -n)\n" );
      no_flxcard = false;
    }
  else if( no_flxcard )
    {
      goto skip_open;
    }

  try {
    uint32_t locks = (locks_override ? LOCK_NONE : LOCK_ALL);

    // In case of lock issues, retry opening the device every second
    // until successful, for a configurable number of times
    int try_open_max = 5, try_open_count = 0;
    while( true )
      {
        try {
          flxCard.card_open( device_number, locks, read_config, ignore_version );
        }
        catch( FlxException &ex ) {
          if( ex.errorId() != FlxException::LOCK_VIOLATION ||
              try_open_count >= try_open_max )
            // Not a lock issue or persistent lock issue: rethrow exception
            throw ex;
          ++try_open_count;
          printf( "Waiting for locks, retrying... (%d)\n", try_open_count );
          sleep( 1 );
          continue; // Retry card_open()
        }
        break; // Card succesfully opened
      }
  }
  catch( FlxException &ex ) {
    printf( "### ERROR open: %s\n", ex.what() );
    return 1;
  }

 skip_open:

  try {
    switch( command )
      {
      case CMD_SET_DIRECT:
        {
          // Convert last argument's string to u_long
          char *errpos;
          errno = 0;
          uint64_t value = strtoul(argv[optind+1], &errpos, 0);

          // Error if strtoul returns before end-of-string reached
          if(*errpos != '\0')
            {
              printf("### ERROR: invalid <set> value: %s\n", argv[optind+1]);
              return 1;
            }
          if( errno != 0 )
            {
              perror("strtoul");
              return 1;
            }

          //flxCard.cfg_set_option(argv[optind], value, true);
          std::string namestr( argv[optind] );
          for( size_t i=0; i<namestr.size(); ++i )
            {
              namestr[i] = toupper( namestr[i] );
              if( namestr[i] == '-' ) namestr[i] = '_';
            }
          flxCard.cfg_set_option(namestr.c_str(), value, true);
        }
        break;

      case CMD_GET_DIRECT:
        {
          uint64_t value = flxCard.cfg_get_option(argv[optind], true);
          //printf("%s = 0x%lx\n", argv[optind], value);
          std::string namestr( argv[optind] );
          for( size_t i=0; i<namestr.size(); ++i )
            {
              namestr[i] = toupper( namestr[i] );
              if( namestr[i] == '-' ) namestr[i] = '_';
            }
          printf("%s = 0x%lx\n", namestr.c_str(), value);
        }
        break;

      case CMD_LIST:
        cmd_list( no_flxcard );
        break;

      case CMD_REGISTERS:
        cmd_register( argv + optind+1, argc-1-optind, no_flxcard );
        break;

      case CMD_SET:
        cmd_set( argv + optind+1, argc-1 - optind );
        break;

      case CMD_GET:
        cmd_get( argv + optind+1, argc-1 -optind );
        break;

      case CMD_SETRAW:
        {
          u_long baraddr = flxCard.openBackDoor(raw_bar);
          if( (raw_offset + raw_width) < 33)
            {
              u_int *p_value = (u_int *)(baraddr + raw_address);
              if( raw_width == 32)
                {
                  printf("Original value of the register = 0x%08x\n", *p_value);
                  *p_value = raw_value;
                  printf("New value of the register = 0x%08x\n", *p_value);
                }
              else
                {
                  u_int raw_32 = *p_value;
                  printf("Original value of the register = 0x%08x\n", raw_32);

                  // Example -w 6 -o 7 ==> 0x00001f80
                  u_int bitmask = ~(((1 << raw_width) - 1) << raw_offset);

                  raw_32 = raw_32 & bitmask;
                  raw_32 = raw_32 + (raw_value << raw_offset);
                  printf("bitmask = 0x%08x\n", bitmask);
                  printf("Shifted value = 0x%08x\n", (u_int)(raw_value << raw_offset));
                  printf("New value of the register = 0x%08x\n", raw_32);
                  *p_value = raw_32;
                }
            }
          else
            {
              u_long *p_value = (u_long *)(baraddr + raw_address);
              if( raw_width == 64)
                {
                  printf("Original value of the register = 0x%016lx\n", *p_value);
                  *p_value = raw_value;
                  printf("New value of the register = 0x%016lx\n", *p_value);
                }
              else
                {
                  u_long raw_64 = *p_value;
                  printf("Original value of the register = 0x%016lx\n", raw_64);

                  // Example: -o 32 -w 8 ==> 0xffffff00ffffffff
                  u_long bitmask = ~((u_long(1 << raw_width) - 1) << raw_offset);

                  raw_64 = raw_64 & bitmask;
                  raw_64 = raw_64 + (raw_value << raw_offset);
                  printf("bitmask = 0x%016lx\n", bitmask);
                  printf("shifted value = 0x%016lx\n", (u_long)(raw_value << raw_offset));
                  printf("New value of the register = 0x%016lx\n", raw_64);
                  *p_value = raw_64;
                }
            }
        }
        break;

      case CMD_GETRAW:
        {
          u_long baraddr = flxCard.openBackDoor(raw_bar);
          if( (raw_offset + raw_width) < 33)
            {
              u_int *p_value = (u_int *)(baraddr + raw_address);
              if( raw_width == 32)
                printf("Value of the register = 0x%08x\n", *p_value);
              else
                {
                  u_int raw_32 = *p_value;
                  raw_32 = raw_32 >> raw_offset;
                  u_int bitmask = (1 << raw_width) - 1;
                  raw_32 = raw_32 & bitmask;
                  printf("Value of the bitfield = 0x%08x\n", raw_32);
                }
            }
          else
            {
              u_long *p_value = (u_long *)(baraddr + raw_address);
              if( raw_width == 64)
                printf("Value of the register = 0x%016lx\n", *p_value);
              else
                {
                  u_long raw_64 = *p_value;
                  raw_64 = raw_64 >> raw_offset;
                  u_long bitmask = (u_long(1 << raw_width)) - 1;
                  raw_64 = raw_64 & bitmask;
                  printf("Value of the bitfield = 0x%016lx\n", raw_64);
                }
            }
        }
        break;

      case CMD_LOAD:
        if( argc < 3 || argv[2][0] == '-' )
          {
            fprintf(stderr, "No filename given\n");
            return 1;
          }
        cmd_load( *(argv + optind + 1) );
        break;

      case CMD_STORE:
        if( argc < 3 || argv[2][0] == '-' )
          {
            fprintf(stderr, "No filename given\n");
            return 1;
          }
        cmd_store( *(argv + optind + 1) );
        break;
      }
  }
  catch( FlxException &e ) {
    printf( "### ERROR: %s\n", e.what() );
    return 1;
  }

  try {
    flxCard.card_close();
  }
  catch( FlxException &ex ) {
    printf( "### ERROR close: %s\n", ex.what() );
    return 1;
  }

  return 0;
}

// ----------------------------------------------------------------------------

void cmd_list( bool no_flxcard )
{
  print_header();

  regmap_bitfield_t *bf;
  for( bf = regmap_bitfields; bf->name != NULL; ++bf )
    {
      if( bf->flags & REGMAP_REG_READ )
        {
          u_long value = (no_flxcard ? 0 : flxCard.cfg_get_option(bf->name));
          print_register( bf->address, bf->flags, bf->shift, bf->hi,
                          bf->name, value, bf->description, no_flxcard );
        }
      else
        {
          print_register( bf->address, bf->flags, bf->shift, bf->hi,
                          bf->name, 0, bf->description, true );
        }
    }
}

// ----------------------------------------------------------------------------

void cmd_register( char **groups, int num_groups, bool no_flxcard )
{
  regmap_group_t *grp;
  if( num_groups == 0 )
    {
      printf( "Existing register groups:\n" );
      printf( "-------------------------\n" );
      for( grp = regmap_groups; grp->name != NULL; ++grp )
        printf( "Reg Group %-12s : %s\n", grp->name, grp->description );
      printf( "\n('flx-config registers <grp> [<grp>]' to display "
              "registers from one or more groups)\n\n" );
      return;
    }

  if( num_groups > 1 )
    printf( "You have selected %d groups\n", num_groups );

  for( grp=regmap_groups; grp->name!=NULL; ++grp )
    {
      for( int loop=0; loop<num_groups; ++loop )
        {
          // Accept '-' instead of '_' as well
          for( int i=0; groups[loop][i]!=0; ++i )
            if( groups[loop][i] == '-' )
              groups[loop][i] = '_';

          if( strcasecmp(grp->name, groups[loop]) == 0 )
            {
              print_header();
              printf( "Registers in group %s (%s)\n",
                      grp->name, grp->description );

              for( int index=0; grp->index[index]!=-1; ++index )
                {
                  regmap_register_t *reg = &regmap_registers[grp->index[index]];
                  if( reg->flags & REGMAP_REG_READ )
                    {
                      u_long value = (no_flxcard ? 0 : flxCard.cfg_get_reg(reg->name));
                      print_register( reg->address, reg->flags, 0, 63,
                                      reg->name, value, reg->description,
                                      no_flxcard );
                    }
                  else
                    {
                      print_register( reg->address, reg->flags, 0, 63,
                                      reg->name, 0, reg->description,
                                      true );
                    }
                }
              printf( "\n" );
            }
        }
    }
}

// ----------------------------------------------------------------------------

void cmd_set( char **config, int n )
{
  int i;
  for( i = 0; i < n; ++i )
    {
      u_long value = split_key_value(config[i]);
      //flxCard.cfg_set_option(config[i], value, true);
      std::string namestr( config[i] );
      for( size_t i=0; i<namestr.size(); ++i )
        {
          namestr[i] = toupper( namestr[i] );
          if( namestr[i] == '-' ) namestr[i] = '_';
        }
      flxCard.cfg_set_option(namestr.c_str(), value, true);
    }
}

// ----------------------------------------------------------------------------

void cmd_get( char **config, int n )
{
  int i;
  for( i = 0; i < n; ++i )
    {
      std::string namestr( config[i] );
      for( size_t i=0; i<namestr.size(); ++i )
        {
          namestr[i] = toupper( namestr[i] );
          if( namestr[i] == '-' ) namestr[i] = '_';
        }
      u_long value = flxCard.cfg_get_option(namestr.c_str(),true);
      printf("%s=0x%lx\n", namestr.c_str(), value);
      //u_long value = flxCard.cfg_get_option(config[i],true);
      //printf("%s=0x%lx\n", config[i], value);
    }
}

// ----------------------------------------------------------------------------

void cmd_load( char *filename )
{
  FILE *fp;
  fp = fopen(filename, "r");
  if( fp == NULL )
    {
      fprintf(stderr, "Could not open '%s'\n", filename);
      exit(1);
    }

  char *line = NULL;
  size_t len = 0;
  while( getline(&line, &len, fp) != -1 )
    {
      while( isspace(*line) ) ++line; // Remove leading spaces
      u_long value = split_key_value(line);
      // In settings files allow 'sleep = <value>' setting
      if( strcmp(line, "sleep") == 0 ||
          strcmp(line, "SLEEP") == 0 )
        usleep( value*1000 );
      else
        flxCard.cfg_set_option(line, value);
    }

  if( line )
    free( line );
  fclose( fp );
}

// ----------------------------------------------------------------------------

void cmd_store( char *filename )
{
  FILE *fp;
  fp = fopen(filename, "w");
  if( fp == NULL )
    {
      fprintf(stderr, "Could not open '%s'\n", filename);
      exit(1);
    }

  regmap_bitfield_t *bf;
  for( bf = regmap_bitfields; bf->name != NULL; ++bf )
    {
      if( bf->flags & REGMAP_REG_WRITE && bf->flags & REGMAP_REG_READ )
        {
          if( strstr(bf->name, "I2C_WR") || strstr(bf->name, "I2C_RD") )
            printf("Not storing %s\n", bf->name);
          else
            {
              u_long value = flxCard.cfg_get_option(bf->name);
              fprintf(fp, "%s=0x%lx\n", bf->name, value);
            }
        }
    }

  fclose(fp);
}

// ----------------------------------------------------------------------------

void print_header()
{
  printf("Offset  RW Bits                                             "
         "Name              Value  Description\n");
  printf("========================================================"
         "========================================================\n");
}

// ----------------------------------------------------------------------------

void print_register( u_long address, u_int flags, u_int lo, u_int hi,
                     const char *name, u_long value, const char *description,
                     bool without_value )
{
  const int NAME_SPAN   = 46;
  const int DESC_INDENT = 7+11+NAME_SPAN+2+1+16+2;

  if( flags & REGMAP_REG_READ )
    {
      cout << setfill('0');
      cout << hex  << "0x" << setw(4) << address << dec;

      cout << " [";

      if( hi > 64)
        {
          cout << "TRIGGER ";
          lo = hi;
        }
      else
        {
          if( flags & REGMAP_REG_READ )
            cout << "R";
          else
            cout << " ";

          if( flags & REGMAP_REG_WRITE )
            cout << "W ";
          else
            cout << "  ";

          if( hi == lo)
            cout << "   " << setw(2) << lo;
          else
            cout << setw(2) << hi << ":" << setw(2) << lo;
        }

      cout << "] ";
      cout << setfill(' ') << setw(NAME_SPAN) << name;
      // Take into account that some names exceed NAME_SPAN in size
      int nospaces = 0;
      if( strlen(name) > NAME_SPAN )
        nospaces = strlen(name)-NAME_SPAN;

      if( without_value )
        {
          // Just display '---'
          cout << "               --- ";
        }
      else
        {
          u_int nblanks;
          if( hi > 64)
            nblanks = 15;
          else if( hi == lo )
            nblanks = 15;
          else
            nblanks = 16 - ((hi-lo+1+3) / 4);
          if( nblanks-nospaces >= 0)
            nblanks -= nospaces;

          cout << setfill(' ') << setw(nblanks) << "";
          cout << " 0x" << setfill('0') << setw((hi-lo+1+3)/4)
               << hex << value << dec;
        }
      cout << "  ";

      int desc_len = strlen( description );
      const char *ch = description;
      int cnt = 0;
      cout << setfill(' ');
      for( int i=0; i<desc_len; ++i, ++ch ) {
        if( i == desc_len-1 && *ch == '\n' ) // Skip a final newline
          break;
        cout << *ch;
        ++cnt;
        // In case of newline or long line continue on the next line,
        // with indentation
        if( (*ch == '\n') || (cnt > 60 && *ch == ' ') ) {
          if( *ch == ' ' ) cout << endl; // Start next line only after a space
          cout << setw(DESC_INDENT) << " ";
          cnt = 0; // Reset line width counter
        }
      }
      cout << endl;
    }
  else
    {
      cout << hex  << "0x" << address << dec;
      cout << " [";

      if( flags & REGMAP_REG_READ )
        cout << "R";
      else
        cout << " ";

      if( flags & REGMAP_REG_WRITE )
        cout << "W ";
      else
        cout << "  ";

      cout << setfill('0') << setw(2) << hi << ":" << setfill('0') << setw(2) << lo;
      cout << "] ";
      cout << setfill(' ') << setw(NAME_SPAN) << name;
      cout << "                      " << description;
      cout << endl;
    }
}

// ----------------------------------------------------------------------------

u_long split_key_value(char *str)
{
  char *pos = strchr(str, '=');
  if(pos == NULL)
    {
      printf("### ERROR: missing ‘=’ in string passed to <set> option\n");
      exit(1);
    }
  // Remove trailing spaces from 'key' string
  while( *str )
    {
      if( isspace(*str) )
        *str = '\0';
      ++str;
    }

  *pos = '\0';
  ++pos; // pos now points to value string

  // Convert second part of string 'str', i.e. ‘pos’ to u_long
  char *errpos;
  errno = 0;
  ulong val = strtoul(pos, &errpos, 0);

  // Error if strtoul returns before end-of-string reached,
  // or if the value string was empty to begin with
  if( *errpos != '\0' || *pos == '\0' )
    {
      printf("### ERROR: invalid <set> value: %s\n", pos);
      exit(1);
    }
  if( errno != 0 )
    {
      perror("strtoul");
      exit(1);
    }

  return val;
}

// ----------------------------------------------------------------------------

void display_help()
{
  printf("Usage: flx-config COMMAND [OPTIONS]\n");
  printf("\nCommands:\n");
  printf("  registers <GROUP>      List card configuration on register-group basis.\n");
  printf("                         If GROUP is not given a list of defined groups is displayed,\n");
  printf("                         to select from.\n");
  printf("  list                   Read out and display all known card bitfields\n");
  printf("                         (address, name, value, description).\n");
  printf("  set KEY=VALUE          Set option KEY to VALUE. Multiple settings may be given.\n");
  printf("  get KEY                Get option KEY. Multiple 'KEY' items may be given\n");
  printf("  setraw -b -r -o -w -v  Set a raw register or bitfield to a new value\n");
  printf("    -b                   The 'BAR' the register belongs to (0, 1 or 2) (default 2).\n");
  printf("    -r                   Offset of the register address relative to the BAR (default 0).\n");
  printf("    -o                   Offset of the first bit within the register (for bitfields) (default 0).\n");
  printf("    -w                   The width of the bitfield.\n" );
  printf("                         Use -w 32 or -w 64 to write full 32- or 64-bit registers (default 32).\n");
  printf("    -v                   The data value to be written to the register (default 0).\n");
  printf("  getraw -b -r -o -w     Get the data of a raw register or bitfield.\n");
  printf("                         The definition of -b, -r, -o and -w is the same as for setraw\n");
  printf("  store FILENAME         Store current configuration in the given file.\n");
  printf("  load FILENAME          Load the configuration in the given file.\n");
  printf("                         File format is the one produced by the 'store' command.\n");
  printf("\n");
  printf("Note: as alternative to 'flx-config get KEY' use 'flx-config KEY'\n");
  printf("      (for a single 'KEY' item only);\n");
  printf("      as alternative to 'flx-config set KEY=VALUE' use 'flx-config KEY VALUE'\n");
  printf("      (for a single 'KEY' item only).\n");
  printf("\n");
  printf("Options:\n");
  printf("  -d NUMBER              Use device indicated by NUMBER (default 0).\n");
#if DEBUG_LEVEL > 0
  printf("  -D level               Configure debug output at API level.\n");
  printf("                         0=disabled, 5, 10, 20 progressively more verbose output (default 0).\n");
#endif // DEBUG_LEVEL
  printf("  -h                     Display help.\n");
  printf("  -E                     Execute the command even if resources are locked.\n");
  printf("  -n                     Don't access an FLX-card (for commands 'registers' and 'list').\n");
  printf("  -V                     Display the version number.\n");
}

// ----------------------------------------------------------------------------
