/*******************************************************************/
/*                                                                 */
/* This is the C++ source code of the FlxDmaStat application       */
/*                                                                 */
/* Author: Markus Joos, CERN                                       */
/*                                                                 */
/* From Nov 2020: maintenance and dev by H.Boterenbrood, Nikhef    */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <iostream>

#include "flxcard/FlxCard.h"
#include "flxcard/FlxException.h"
#include "felixtag.h"

void display_help();

// Globals
FlxCard flx;

// ----------------------------------------------------------------------------

int main( int argc, char **argv )
{
  int device_number = 0;
  int dma_id = -1;
  bool trickle = false;;

  int opt;
  while( (opt = getopt(argc, argv, "hd:i:IV")) != -1 )
    {
      switch( opt )
        {
        case 'd':
          device_number = atoi(optarg);
          break;

        case 'h':
          display_help();
          return 0;

        case 'i':
          dma_id = atoi( optarg );
          break;

        case 'I':
          trickle = true;
          break;

        case 'V':
          printf( "Version %s\n", FELIX_TAG );
          return 0;

        default:
          fprintf( stderr, "Usage: flx-dma-stat COMMAND [OPTIONS]\n"
                   "Try flx-dma-stat -h for more information.\n" );
          return 1;
        }
    }

  try {
    flx.card_open( device_number, LOCK_NONE, false, true );
    printf( "Firmware: %s\n", flx.firmware_string().c_str() );

    flxcard_bar2_regs_t *bar2 = (flxcard_bar2_regs_t *) flx.bar2Address();
    int dma_cnt = bar2->GENERIC_CONSTANTS.DESCRIPTORS;
    if( trickle )
      {
#if REGMAP_VERSION >= 0x500
        int offset = bar2->GENERIC_CONSTANTS.TRICKLE_DESCRIPTOR_INDEX;
        dma_cnt = bar2->GENERIC_CONSTANTS.TRICKLE_DESCRIPTORS;
#else
        int offset = 0;
        dma_cnt = 0;
#endif // REGMAP_VERSION
        if( dma_cnt > 0 )
          {
            if( dma_id == -1 )
              {
                // All trickle DMAs
                dma_id = offset;
              }
            else if( dma_id < dma_cnt )
              {
                // A single trickle DMA
                dma_id += offset;
                dma_cnt = 1;
              }
            else
              {
                fprintf( stderr,
                         "### Trickle DMA index %d not in range [0,%d]\n",
                         dma_id, dma_cnt-1 );
                return 1;
              }
          }
        else
          {
            fprintf( stderr, "### Firmware has no Trickle DMA\n" );
            return 1;
          }
      }
    else
      {
        if( dma_id == -1 )
          {
            // Status of all DMAs, starting from index 0
            dma_id = 0;
            printf( "DMA descriptor count = %d\n", dma_cnt );
          }
        else
          {
            if( dma_id > dma_cnt-1 )
              {
                fprintf( stderr, "### DMA index %d not in range [0,%d]\n",
                         dma_id, dma_cnt-1 );
                return 1;
              }
            // DMA index provided, so display the status of this one only
            dma_cnt = 1;
          }
      }

    u_long fromhost[8];
    u_long pend[8];
    u_long pstart[8];
    u_long psoftw[8];
    u_long pfirmw[8];
    u_long even_pc[8];
    u_long even_dma[8];
    u_long done[8];
    u_long wrap[8];
    u_long tlp[8];
    u_long bufsize[8];

    flxcard_bar0_regs_t *bar0 = (flxcard_bar0_regs_t *) flx.bar0Address();
    u_long dma_desc_enable = bar0->DMA_DESC_ENABLE;

    if( trickle )
      printf( "-> Trickle DMA:\n" );
    for( int i=dma_id; i<dma_id+dma_cnt; ++i )
      {
        dma_descriptor_t *desc = &bar0->DMA_DESC[i];
        dma_status_t     *stat = &bar0->DMA_DESC_STATUS[i];

        // Take DMA controller status 'snapshot'
        fromhost[i] = desc->fromhost; // FromHost=1, ToHost=0
        pend[i]     = desc->end_address;
        pstart[i]   = desc->start_address;
        // To try reduce the chance of false 'invalid' pointers (see below)
        // (if the pointers change fast/frequently this may occur)
        if( fromhost[i] )
          {
            // Firmware follows software, so read it first
            pfirmw[i] = stat->fw_pointer;
            psoftw[i] = desc->sw_pointer;
          }
        else
          {
            // Software follows firmware, so read it first
            psoftw[i] = desc->sw_pointer;
            pfirmw[i] = stat->fw_pointer;
          }
        even_pc[i]  = stat->even_addr_pc;
        even_dma[i] = stat->even_addr_dma;
        done[i]     = stat->descriptor_done;
        wrap[i]     = desc->wrap_around;
        tlp[i]      = desc->tlp;
        if( pend[i] >= pstart[i] )
          bufsize[i]  = pend[i] - pstart[i];
        else
          bufsize[i] = 0;
      }

    printf( "               " );
    for( int i=dma_id; i<dma_id+dma_cnt; ++i )
      printf( "DMA %d           ", i );
    printf( "\n" );

    printf( "               " );
    for( int i=dma_id; i<dma_id+dma_cnt; ++i )
      printf( "--------------  " );
    printf( "\n" );

    printf( "Enabled      : " );
    for( int i=dma_id; i<dma_id+dma_cnt; ++i )
      if( dma_desc_enable & (1 << i) )
        printf( "Yes             " );
      else
        printf( "No              " );
    printf( "\n" );

    printf( "FROM_TO      : " );
    for( int i=dma_id; i<dma_id+dma_cnt; ++i )
      if( fromhost[i] )
        printf( "1 (FromHost)    " );
      else
        printf( "0 (ToHost)      " );
    printf( "\n" );

    printf( "NUM_WORDS    : " );
    for( int i=dma_id; i<dma_id+dma_cnt; ++i )
      if( tlp[i] > 0 )
        printf( "%-3lu (TLP=%-3luB)  ", tlp[i], tlp[i]*4 );
      else
        printf( "0 (TLP=0)       " );
    printf( "\n" );

    printf( "WRAP_AROUND  : " );
    for( int i=dma_id; i<dma_id+dma_cnt; ++i )
      printf( "%lu               ", wrap[i] );
    printf( "\n" );

    printf( "END_ADDRESS  : " );
    for( int i=dma_id; i<dma_id+dma_cnt; ++i )
      printf( "0x%012lx  ", pend[i] );
    printf( "\n" );

    printf( "START_ADDRESS: " );
    for( int i=dma_id; i<dma_id+dma_cnt; ++i )
      printf( "0x%012lx  ", pstart[i] );
    printf( "\n" );

    printf( "FW_POINTER   : " );
    for( int i=dma_id; i<dma_id+dma_cnt; ++i )
      printf( "0x%012lx  ", pfirmw[i] );
    printf( "\n" );

    printf( "SW_POINTER   : " );
    for( int i=dma_id; i<dma_id+dma_cnt; ++i )
      printf( "0x%012lx  ", psoftw[i] );
    printf( "\n" );

    printf( "EVEN_PC      : " );
    for( int i=dma_id; i<dma_id+dma_cnt; ++i )
      printf( "0x%ld             ", even_pc[i] );
    printf( "\n" );

    printf( "EVEN_DMA     : " );
    for( int i=dma_id; i<dma_id+dma_cnt; ++i )
      printf( "0x%ld             ", even_dma[i] );
    printf( "\n" );

    printf( "DESC_DONE    : " );
    for( int i=dma_id; i<dma_id+dma_cnt; ++i )
      if( done[i] )
        printf( "0x%ld (DONE)      ", done[i] );
      else
        printf( "0x0             " );
    printf( "\n" );

    printf( "Buffer size  : " );
    for( int i=dma_id; i<dma_id+dma_cnt; ++i )
      printf( "0x%-9lX     ", bufsize[i] );
    printf( "\n" );

    printf( "        [MB] : " );
    for( int i=dma_id; i<dma_id+dma_cnt; ++i )
      if( bufsize[i]/(1024*1024) > 0 )
        printf( "%-4ld            ", bufsize[i]/(1024*1024) );
      else
        printf( "                " );
    printf( "\n" );

    u_long nbytes_unread[8];
    printf( "Buffer unread: " );
    for( int i=dma_id; i<dma_id+dma_cnt; ++i )
      {
        nbytes_unread[i] = 0;

        if( bufsize[i] == 0 )
          {
            printf( "                " );
            continue;
          }

        // Buffer full/unread percentage:
        // how close is the consumer (pointer) 'behind' the producer (pointer)
        // in number of bytes as well as a percentage of the buffer size
        u_long nbytes, percent;
        if( wrap[i] != 0 )
          {
            // Continuous DMA

            // If pointers make sense
            if( !done[i] &&
                pfirmw[i] >= pstart[i] && pfirmw[i] <= pend[i] &&
                psoftw[i] >= pstart[i] && psoftw[i] <= pend[i] )
              {
                bool invalid = false;
                if( psoftw[i] == pfirmw[i] )
                  {
                    if( even_dma[i] != even_pc[i] )
                      {
                        // Buffer full
                        nbytes = bufsize[i];
                        percent = 100;
                      }
                    else
                      {
                        // Buffer empty
                        nbytes = 0;
                        percent = 0;
                      }
                  }
                else if( psoftw[i] < pfirmw[i] )
                  {
                    if( fromhost[i] )
                      {
                        // FromHost: data *producer* is at psoftw
                        nbytes = (pend[i]-pfirmw[i]) + (psoftw[i]-pstart[i]);

                        // Re-read odd/even status
                        dma_status_t *stat = &bar0->DMA_DESC_STATUS[i];
                        if( stat->even_addr_dma == stat->even_addr_pc )
                          {
                            printf( "###EVENx+PTRs   " );
                            invalid = true;
                          }
                      }
                    else
                      {
                        // ToHost: data *producer* is at pfirmw
                        nbytes = pfirmw[i] - psoftw[i];

                        // Re-read odd/even status
                        dma_status_t *stat = &bar0->DMA_DESC_STATUS[i];
                        if( stat->even_addr_dma != stat->even_addr_pc )
                          {
                            printf( "###EVENx+PTRs   " );
                            invalid = true;
                          }
                      }
                    percent = (100 * nbytes) / bufsize[i];
                  }
                else // psoftw > pfirmw
                  {
                    if( fromhost[i] )
                      {
                        // FromHost: data *producer* is at psoftw
                        nbytes = psoftw[i] - pfirmw[i];

                        // Re-read odd/even status
                        dma_status_t *stat = &bar0->DMA_DESC_STATUS[i];
                        if( stat->even_addr_dma != stat->even_addr_pc )
                          {
                            printf( "###EVENx+PTRs   " );
                            invalid = true;
                          }
                      }
                    else
                      {
                        // ToHost: data producer is at pfirmw
                        nbytes = (pend[i]-psoftw[i]) + (pfirmw[i]-pstart[i]);

                        // Re-read odd/even status
                        dma_status_t *stat = &bar0->DMA_DESC_STATUS[i];
                        if( stat->even_addr_dma == stat->even_addr_pc )
                          {
                            printf( "###EVENx+PTRs   " );
                            invalid = true;
                          }
                      }
                    percent = (100 * nbytes) / bufsize[i];
                  }

                if( !invalid )
                  {
                    if( dma_desc_enable & (1 << i) )
                      printf( "%-3ld%%            ", percent );
                    else
                      printf( "                " );
                  }

                nbytes_unread[i] = nbytes;
              }
            else if( !done[i] )
              {
                printf( "<invalid ptrs>  " );
              }
            else
              {
                printf( "                " );
              }
          }
        else
          {
            // One-shot DMA
            // (NB: pfirmw[] and fsoftw[] are *not* used for single-shot,
            //  (says Frans) so code below is in fact not correct;
            //  should simply be: percentage 100% when !done[] and 0% when done[],
            //  however, I do see pfirmw[] change/update...)

            // If pointer makes sense
            if( !done[i] &&
                pfirmw[i] >= pstart[i] && pfirmw[i] <= pend[i] )
              {
                if( fromhost[i] )
                  {
                    // FromHost: firmware pointer up to buffer end to transfer
                    nbytes = pend[i] - pfirmw[i];
                  }
                else
                  {
                    // ToHost: buffer start up to firmware pointer transferred
                    nbytes = pfirmw[i] - pstart[i];
                  }
                percent = (100 * nbytes) / bufsize[i];

                if( dma_desc_enable & (1 << i) )
                  printf( "%-3ld%%            ", percent );
                else
                  printf( "                " );

                nbytes_unread[i] = nbytes;
              }
            else if( !done[i] )
              {
                printf( "<invalid ptrs>  " );
              }
            else
              {
                printf( "                " );
              }
          }
      }
    printf( "\n" );

    printf( "      [bytes]: " );
    for( int i=dma_id; i<dma_id+dma_cnt; ++i )
      if( !done[i] && nbytes_unread[i] > 0 )
        printf( "%-10ld      ", nbytes_unread[i] );
      else
        printf( "                " );
    printf( "\n" );

    flx.card_close();
  }
  catch( FlxException &ex ) {
    std::cout << "### ERROR: " << ex.what() << std:: endl;
    return 1;
  }

  return 0;
}

// ----------------------------------------------------------------------------

void display_help()
{
  printf( "Usage: flx-dma-stat [OPTIONS]\n" );
  printf( "Displays the status of (an) FLX device DMA controller(s)\n\n" );
  printf( "Options:\n" );
  printf( "  -d NUMBER      Use device indicated by NUMBER. Default: 0.\n" );
  printf( "  -h             Display help.\n" );
  printf( "  -V             Display the version number.\n" );
  printf( "  -i NUMBER      Index of DMA status to display. Default: all.\n" );
#if REGMAP_VERSION >= 0x500
  printf( "  -I             Display Trickle DMA(s).\n" );
#endif // REGMAP_VERSION
}

// ----------------------------------------------------------------------------
