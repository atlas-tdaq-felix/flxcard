#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include "felixtag.h"

#include "flxcard/FlxCard.h"
#include "flxcard/FlxException.h"

void display_help();
std::string irq_counters_string( int devnr );

// ----------------------------------------------------------------------------

int main( int argc, char **argv )
{
  int device_nr = -1;
  int irq_nr = ALL_IRQS;

  int opt;
  while( (opt = getopt(argc, argv, "hd:i:V")) != -1 )
    {
      switch( opt )
        {
        case 'd':
          device_nr = atoi(optarg);
          break;

        case 'h':
          display_help();
          return 0;

        case 'i':
          irq_nr = atoi( optarg );
          break;

        case 'V':
          printf( "Version %s\n", FELIX_TAG );
          return 0;

        default:
          fprintf( stderr, "Usage: flx-irq-counters COMMAND [OPTIONS]\n"
                   "Try flx-irq-counters -h for more information.\n" );
          return 1;
        }
    }

  if( device_nr == -1 )
    {
      // Display all interrupt counters, and exit
      std::cout << irq_counters_string( device_nr );
      return 0;
    }

  FlxCard flx;
  try {
    // No lock, no configuration readout, ignore RM4 or 5 version
    flx.card_open( device_nr, LOCK_NONE, false, true );

    std::cout << "Before reset:" << std::endl
              << irq_counters_string( device_nr );

    flx.irq_reset_counters( irq_nr );

    std::cout << "After reset:" << std::endl
              << irq_counters_string( device_nr );

    flx.card_close();
  }
  catch( FlxException &ex ) {
    std::cout << "### ERROR: " << ex.what() << std::endl;
    return 1;
  }

  return 0;
}

// ----------------------------------------------------------------------------

void display_help()
{
  printf( "Usage: flx-irq-counters [OPTIONS]\n" );
  printf( "Display the interrupt (IRQ) counters from all FLX devices in the system,\n" );
  printf( "or reset one or all IRQ counters associated with the given " );
  printf( "FLX device number.\n" );
  printf( "Options:\n" );
  printf( "  -d NUMBER      Use FLX device indicated by NUMBER\n" );
  printf( "                 (default: none, and just display all counters).\n");
  printf( "  -h             Display help.\n" );
  printf( "  -V             Display the version number.\n" );
  printf( "  -i NUMBER      IRQ counter [0..7] to reset (default: all),\n" );
  printf( "                 only in combination with option -d.\n" );
}

// ----------------------------------------------------------------------------

std::string irq_counters_string( int devnr )
{
  // Use a pipe stream to retrieve and return the output of "cat /proc/flx",
  // then select the lines that start with "Interrupt count"
  const int BUFSIZE = 256;
  char buffer[BUFSIZE];

  std::string output_str;
  int cnt = 0; // Occurrence counter
  FILE *stream = popen( "cat /proc/flx", "r" );
  if( stream )
    {
      while( !feof( stream ) )
        if( fgets( buffer, BUFSIZE, stream ) != NULL )
          {
            std::string str( buffer );
            if( str.find( "Interrupt count" ) != std::string::npos )
              {
                if( devnr == -1 || cnt == devnr )
                  {
                    std::ostringstream oss;
                    oss << "Device " << cnt << ": ";
                    output_str.append( oss.str() );
                    output_str.append( str );
                  }
                ++cnt;
              }
          }
      pclose( stream );
    }
  else
    {
      output_str = "### popen() error";
    }

  return output_str;
}

// ----------------------------------------------------------------------------
